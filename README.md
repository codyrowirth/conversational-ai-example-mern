## Copy Production to Local

Prereq:

- Make sure mongotools are installed. On mac: `brew install mongodb-database-tools`
- Make sure you have the [gsutil](https://cloud.google.com/storage/docs/gsutil_install) installed and logged in (`gcloud auth login`)

Instructions:

Update the dates below to the current day before running them!!

- Start meteor in a seperate terminal window with `npm run start`
- Run: `gsutil -m cp -r "gs://v4-backups/2021-11-15/" ./` in a second terminal window. If you have a permissions error, contact Max.
- Run: `cd 2021-11-15`
- Run: `mongorestore --port 3001 --nsFrom="tcsv4.*" --nsTo="meteor.*" ./dump`

PROD DB URI (If you have SSH access to the server):
`mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`

LOCAL DB URI (for Compass or restores):
`mongodb://127.0.0.1:3001/meteor?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`
