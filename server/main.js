import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";
import { Roles } from "meteor/alanning:roles";
import password from "./.adminpassword.js";
import "../imports/api/collections/Accounts";
import "../imports/api/collections/AttributeGroups";
import "../imports/api/collections/Attributes";
import "../imports/api/collections/Canvases";
import "../imports/api/collections/CanvasGroups";
import "../imports/api/collections/Options";
import "../imports/api/collections/OptionsTemplates";
import "../imports/api/collections/Projects";
import "../imports/api/collections/User";
import "../imports/api/collections/Messages";
import "../imports/api/collections/Elements";
import "../imports/api/collections/BrancherRules";
import "../imports/api/collections/Branchers";
import "../imports/api/collections/BrancherRuleGroups";
import "../imports/api/collections/BrancherLines";
import "../imports/api/collections/CanvasesToMigrate";
import "../imports/api/collections/ShapesToMigrate";
import "../imports/api/collections/BrancherAttributesToMigrate";
import "../imports/api/collections/BranchersToMigrate";
import "../imports/api/collections/Bits";
import "../imports/api/collections/BitCases";
import "../imports/api/collections/BitCaseRules";
import "../imports/api/collections/BitCaseRuleGroups";
import "../imports/api/collections/ElementTemplates";
import "../imports/api/collections/MessageTrackingAttributes";
import "../imports/api/collections/ThesaurusEntries";
import "../imports/api/collections/ThesaurusEntryGroups";
import "../imports/api/collections/SynonymTemplates";
import "../imports/api/collections/Sessions";

import "../imports/api/methods/Accounts";
import "../imports/api/methods/AttributeGroups";
import "../imports/api/methods/Attributes";
import "../imports/api/methods/Canvases";
import "../imports/api/methods/CanvasGroups";
import "../imports/api/methods/Options";
import "../imports/api/methods/OptionsTemplates";
import "../imports/api/methods/Projects";
import "../imports/api/methods/Shapes";
import "../imports/api/methods/User";
import "../imports/api/methods/Messages";
import "../imports/api/methods/Elements";
import "../imports/api/methods/Branchers";
import "../imports/api/methods/BrancherRules";
import "../imports/api/methods/BrancherRuleGroups";
import "../imports/api/methods/BrancherLines";
import "../imports/api/methods/CanvasesToMigrate";
import "../imports/api/methods/ShapesToMigrate";
import "../imports/api/methods/Bits";
import "../imports/api/methods/BitCases";
import "../imports/api/methods/BitCaseRules";
import "../imports/api/methods/BitCaseRuleGroups";
import "../imports/api/methods/ElementTemplates";
import "../imports/api/methods/MessageTrackingAttributes";
import "../imports/api/methods/ThesaurusEntries";
import "../imports/api/methods/ThesaurusEntryGroups";
import "../imports/api/methods/SynonymTemplates";
import "../imports/api/methods/Sessions";

import "./contentExport";

import fs from "fs";

Meteor.startup(() => {
	const sendgridAPIKey = `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`;
	process.env.MAIL_URL = `smtp://apikey:${sendgridAPIKey}@smtp.sendgrid.net:587`;

	Migrations.migrateTo("latest");

	//Useful commands when developing a DB migration:
	// Migrations.unlock();
	// Migrations.migrateTo("6,rerun");

	const adminUser = Accounts.findUserByUsername("admin");
	if (!adminUser) {
		const id = Accounts.createUser({ username: "admin", password });
		Meteor.users.update(
			{ _id: id },
			{
				$set: {
					userType: "overlord",
					defaultRole: "architect",
					profile: { firstName: "tuzag", lastLane: "admin" },
				},
			}
		);
		Roles.createRole("overlord", { unlessExists: true });
		Roles.addUsersToRoles(id, "overlord");
	}
});

Meteor.publish(null, function () {
	if (this.userId) {
		return Meteor.roleAssignment.find({ "user._id": this.userId });
	} else {
		this.ready();
	}
});

// Add HTML tag language attribute for accessibility
WebApp.addHtmlAttributeHook((req) => {
	return { lang: "en" };
});

WebApp.connectHandlers.use(function (req, res, next) {
	var re = /^\/exports\/(.*)$/.exec(req.url);
	if (re !== null) {
		// Only handle URLs that start with /uploads_url_prefix/*
		var filePath = process.env.PWD + "/exports/" + re[1];
		var data = fs.readFileSync(filePath);
		res.writeHead(200, {
			"Content-Type": "application/zip",
		});
		res.write(data);
		res.end();
	} else {
		// Other urls will have default behaviors
		next();
	}
});
