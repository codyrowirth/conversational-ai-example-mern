import { basicAuth } from "../imports/api/lib";
import { Meteor } from "meteor/meteor";
import AbortController from "abort-controller";
import Messages from "../imports/api/collections/Messages";
import Projects from "../imports/api/collections/Projects";
import Elements from "../imports/api/collections/Elements";
import Bits from "../imports/api/collections/Bits";
import BitCases from "../imports/api/collections/BitCases";
import BitCaseRules from "../imports/api/collections/BitCaseRules";
import BitCaseRuleGroups from "../imports/api/collections/BitCaseRuleGroups";
import Attributes from "../imports/api/collections/Attributes";
import Options from "../imports/api/collections/Options";
import PizZip from "pizzip";
import Docxtemplater from "docxtemplater";
import Shapes from "../imports/api/collections/Shapes";
import fs from "fs";
import tempy from "tempy";
import { Text } from "slate";
import Canvases from "../imports/api/collections/Canvases";
import archiver from "archiver";
import { format } from "date-fns";
import { Random } from "meteor/random";
import { Storage } from "@google-cloud/storage";

let storage;
if (process.env.ENV === "PROD") {
	storage = new Storage();
} else {
	storage = new Storage({
		keyFilename:
			process.cwd().split(".meteor")[0] + "server/tcs-v4-1600816bfb1c.json",
	});
}

const generateWordFile = async (canvasID, projectID) => {
	const canvas = Canvases.findOne({ _id: canvasID });
	canvas.messages = [];
	const shapes = Shapes.find({ type: "message", canvasID: canvas._id });
	for (const shape of shapes) {
		const message = Messages.findOne({
			shapeID: shape._id,
		});

		canvas.messages.push({ ...message, name: shape?.data?.label });
	}

	let dbData = [];

	for (const messageIterator of canvas.messages) {
		const { message, attributes } = dbQueries(messageIterator);

		dbData.push({ canvas, message, attributes });
	}

	const project = Projects.findOne({ _id: projectID });

	const tempFilePath = await wordExport(dbData, project);
	return { tempFilePath, canvasName: canvas.name };
};

const generateRuleText = (ruleText, rules, attributes, bitCase) => {
	for (let [ruleIndex, rule] of Object.entries(rules)) {
		ruleIndex = parseInt(ruleIndex);
		if (rule.chainedOperator) {
			ruleText = generateRuleText(
				ruleText,
				rule.bitCaseRules,
				attributes,
				rule
			);
			ruleText += `)`;
		} else {
			let attributeName,
				optionName = "";
			let messageAttribute = attributes.find((messageAttribute) => {
				return messageAttribute._id === rule.attributeID;
			});

			if (!messageAttribute) {
				attributeName = "Unknown attribute. Export error.";
			} else {
				attributeName = `"${messageAttribute?.name}"`;
			}

			const operator = rule.operator || "missing rule operator";

			if (messageAttribute) {
				const option = messageAttribute.options.find((option) => {
					return option._id === rule?.optionID;
				});

				if (!(option?.name || rule.optionValue)) {
					optionName = "";
				} else {
					optionName = `"${option?.name.trim() || rule.optionValue.trim()}"`;
				}
			}

			if (rules.length > 1) {
				if (ruleIndex === 0) {
					attributeName = `(${attributeName}`;
				} else if (ruleIndex + 1 === rules.length) {
					optionName = `${optionName})`;
				}
			}

			const chain =
				ruleIndex + 1 !== rules.length ? bitCase.chainedOperator : "";

			ruleText += `${attributeName} ${operator.toLowerCase()}${
				operator.includes("Empty") ? "" : " "
			}${optionName} ${chain} `;
		}
	}

	return ruleText;
};
const serialize = (node) => {
	if (node.bitID) {
		return `[[BIT]]`;
	}

	if (node.lme) {
		const matchingLME = Elements.findOne({ _id: node.lme });
		if (matchingLME) {
			return `[[LME: ${matchingLME.name}]]`;
		}
		return `[[LME]]`;
	}

	if (node.passthrough) {
		const matchingAttribute = Attributes.findOne({ _id: node.passthrough });
		if (matchingAttribute) {
			return `[[Passthrough: ${matchingAttribute.name}]]`;
		}
		return `[[PASSTHROUGH]]`;
	}

	if (node.elementID && node.messageID) {
		const matchingLME = Elements.findOne({ _id: node.elementID });
		if (matchingLME) {
			return `[[LME: ${matchingLME.name}]]`;
		}
		return `[[LME]]`;
	}

	if (node.bold || node.italic || node.underline) {
		return node.text;
	}

	if (Text.isText(node)) {
		return node.text;
	}

	const content = node.children.map((n) => {
		return serialize(n);
	});

	if (node.type === "paragraph" || node.type === "bulleted-list") {
		return `${content.join("")}\n`;
	}
	if (node.type === "list-item") {
		return ` •  ${content.join("")}\n`;
	}
	return content.join("");
};

const toText = (obj) => {
	if (!obj) {
		return "";
	}
	if (typeof obj === "string") {
		obj = JSON.parse(obj);
	}
	let content = "";
	for (const child of obj) {
		content += serialize(child);
	}

	return content.trim();
};

const numberBits = (text) => {
	let counter = 0;
	return text.replace(/\[\[BIT]]/g, () => {
		counter++;
		return `[[BIT #${counter}]]`;
	});
};

const wordExport = async (messages, project) => {
	const content = await storage
		.bucket(`tcsv4-persistent`)
		.file(`reviewTemplate.docx`)
		.download();

	const zip = new PizZip(content[0]);

	const doc = new Docxtemplater(zip, { linebreaks: true });

	const templateData = {
		projectName: project.name,
		messageCount: messages.length,
		messages: messages.map(({ canvas, message, attributes, users }) => {
			if (message.elements) {
				message.elements = message.elements
					.sort((a, b) => a.order - b.order)
					.filter((element) => {
						//remove elements with empty content
						return !(
							!element.content ||
							element.content ===
								`[{"type":"paragraph","children":[{"text":""}]}]`
						);
					});
			}

			return {
				messageName: message.name,
				canvas: canvas.name,
				elementsList:
					message.elements.length === 0
						? "No elements"
						: message.elements.map((element) => element.name).join(", "),
				elements: message.elements.map((element) => {
					const elementContent = numberBits(toText(element.content));

					return {
						elementName: element.name,
						numberOfBits: [...elementContent.matchAll(/\[\[BIT #/g)].length,
						elementContent,
						bits: element.bits
							.filter((bit) => {
								//sometimes bits get deleted from the content, but not from
								//the DB
								if (!element.content) {
									return false;
								}
								return JSON.stringify(element.content).includes(
									`"bitID":"${bit._id}"`
								);
							})
							.map((bit, index) => {
								return {
									bitNumber: index + 1,
									bitCases: bit.bitCases.map((bitCase) => {
										let ruleText = "";
										let content = toText(bitCase.content);

										if (
											!bitCase.else &&
											!bitCase?.bitCaseRules?.length &&
											!bitCase?.bitCaseRuleGroups?.length
										) {
											return {
												bitCaseContent: content,
												rules: "No rules on this bit case.",
											};
										}
										if (!bitCase.bitCaseRuleGroups) {
											bitCase.bitCaseRuleGroups = [];
										}

										let combinedRules = [
											...bitCase.bitCaseRules,
											...bitCase?.bitCaseRuleGroups,
										].sort((a, b) => a.order - b.order);

										if (bitCase.else) {
											ruleText = "Default else case.";
										} else if (combinedRules.length === 0) {
											ruleText = "No rules on this bit case.";
										} else {
											ruleText = generateRuleText(
												ruleText,
												combinedRules,
												attributes,
												bitCase
											);
										}

										return {
											bitCaseContent: content || "[[empty]]",
											rules: ruleText,
										};
									}),
								};
							}),
					};
				}),
			};
		}),
	};

	// set the template data
	doc.setData(templateData);

	try {
		// render the document (replace all occurrences of {first_name} by John, {last_name} by Doe, ...)
		doc.render();
	} catch (error) {
		const e = {
			message: error.message,
			name: error.name,
			stack: error.stack,
			properties: error.properties,
		};
		// eslint-disable-next-line no-console
		console.log(JSON.stringify({ error: e }));
		// The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
		throw error;
	}

	const buf = doc.getZip().generate({ type: "nodebuffer" });

	const tempFilePath = tempy.file({
		name: `export.docx`,
	});
	fs.writeFileSync(tempFilePath, buf);

	setTimeout(() => {
		fs.unlinkSync(tempFilePath);
	}, 60000);

	return tempFilePath;
};

const dbQueries = (message) => {
	message.elements = Elements.find({ messageID: message._id }).fetch();
	for (const element of message.elements) {
		element.bits = Bits.find({ elementID: element._id }).fetch();
		for (const bit of element.bits) {
			bit.bitCases = BitCases.find({ bitID: bit._id }).fetch();
			for (const bitCase of bit.bitCases) {
				bitCase.bitCaseRules = BitCaseRules.find({
					bitCaseID: bitCase._id,
				}).fetch();
				bitCase.bitCaseRuleGroups = BitCaseRuleGroups.find({
					bitCaseID: bitCase._id,
				}).fetch();
				for (const bitCaseRuleGroup of bitCase.bitCaseRuleGroups) {
					bitCaseRuleGroup.bitCaseRules = BitCaseRules.find({
						parent: bitCaseRuleGroup._id,
					}).fetch();
				}
			}
		}
	}

	let attributes = Attributes.find({
		_id: { $in: message.attributes },
	}).fetch();
	for (const attribute of attributes) {
		attribute.options = Options.find({ attributeID: attribute._id }).fetch();
	}

	return { message, attributes };
};

Meteor.methods({
	async exportContent(selectedCanvases) {
		basicAuth();

		const project = Projects.findOne(
			{ _id: Meteor.user().projectID },
			{ fields: { name: 1, _id: 1 } }
		);

		const archive = archiver("zip");
		let filePaths = [];
		const randomID = Random.id();
		const fileName = `${project.name} ${format(new Date(), "M-d-yy h-mm aaa")}`;

		const zipTempFilePath = tempy.file({
			name: `${randomID}.zip`,
		});

		const output = fs.createWriteStream(zipTempFilePath);
		archive.pipe(output);

		for (const canvasID of selectedCanvases) {
			const { tempFilePath, canvasName } = await generateWordFile(
				canvasID,
				Meteor.user().projectID
			);
			filePaths.push(tempFilePath);
			await archive.file(tempFilePath, { name: `${canvasName}.docx` });
		}

		await archive.finalize();

		const storagePath = await storage
			.bucket(`contentexports.tuzagtcs.com`)
			.upload(zipTempFilePath);

		//delete the file after 10 min, we should clean this up to use a better method
		setTimeout(() => {
			fs.unlinkSync(zipTempFilePath);
		}, 1000 * 60 * 10);

		return { fileName, randomID };
	},
});
