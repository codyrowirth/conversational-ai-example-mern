import ThesaurusEntries from "../../imports/api/collections/ThesaurusEntries";
import Options from "../../imports/api/collections/Options";
import Attributes from "../../imports/api/collections/Attributes";

Migrations.add({
	version: 4,
	name: "",
	up: function () {
		// eslint-disable-next-line no-console
		const entries = ThesaurusEntries.find().fetch();
		for (const entry of entries) {
			const options = Options.find({ thesaurusEntryID: entry._id });
			let allCalculated = true;
			for (const option of options) {
				const attribute = Attributes.findOne({ _id: option.attributeID });
				if (attribute.type !== "calculated") {
					allCalculated = false;
					break;
				}

				if (allCalculated && entry.synonymList.length === 0) {
					ThesaurusEntries.remove({ _id: entry._id });
					Options.update(option._id, { $unset: { thesaurusEntryID: 1 } });
				}
			}
		}
	},
	down: () => {},
});
