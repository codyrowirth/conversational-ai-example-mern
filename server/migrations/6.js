import Branchers from "../../imports/api/collections/Branchers";
import BrancherRules from "../../imports/api/collections/BrancherRules";
import BrancherLines from "../../imports/api/collections/BrancherLines";
import Shapes from "../../imports/api/collections/Shapes";
import Canvases from "../../imports/api/collections/Canvases";

Migrations.add({
	version: 6,
	name: "fix missing attributes on branchers",
	up: function () {
		setTimeout(() => {
			let toDelete = [];
			const shapes = Shapes.find({ type: "brancher" }).fetch();
			for (const shape of shapes) {
				const branchers = Branchers.find({ shapeID: shape._id }).fetch();
				if (branchers.length > 1) {
					for (const brancher of branchers) {
						if (brancher.attributes.length === 0) {
							toDelete.push(brancher._id);
						}
					}
				}
			}
			// console.log(toDelete.length);
			Branchers.remove({ _id: { $in: toDelete } });
			toDelete = [];
			for (const shape of shapes) {
				const branchers = Branchers.find({ shapeID: shape._id }).fetch();
				if (branchers.length > 1) {
					for (const [index, brancher] of branchers.entries()) {
						if (index !== 0) {
							toDelete.push(brancher._id);
						}
					}
				}
			}
			// console.log(toDelete.length);
			Branchers.remove({ _id: { $in: toDelete } });

			let updateCount = 0;
			const branchers = Branchers.find().fetch();
			for (const brancher of branchers) {
				const brancherLines = BrancherLines.find({
					brancherID: brancher.shapeID,
				});
				for (const brancherLine of brancherLines) {
					const rules = BrancherRules.find({ lineID: brancherLine.lineID });
					for (const rule of rules) {
						if (
							rule.attributeID &&
							brancher.attributes &&
							!brancher.attributes.includes(rule.attributeID)
						) {
							brancher.attributes.push(rule.attributeID);
							Branchers.update(brancher._id, {
								$set: { attributes: brancher.attributes },
							});
							updateCount++;
						}
					}
				}
			}
		}, 1);
	},
	down: () => {},
});
