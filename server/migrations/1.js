import AttributeGroups from "../../imports/api/collections/AttributeGroups";
import Attributes from "../../imports/api/collections/Attributes";

Migrations.add({
	version: 1,
	name: "Make global attributes viewable",
	up: function () {
		AttributeGroups.update(
			{ parent: "7MYqqdNsuPLxZhmqT" },
			{ $set: { projectID: null } },
			{ multi: true }
		);

		Attributes.update(
			{ _id: "LHtksKQrFZ5N2DHyP" },
			{ $set: { projectID: null } }
		);
	},
});
