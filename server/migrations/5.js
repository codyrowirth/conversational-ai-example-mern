import Options from "../../imports/api/collections/Options";

Migrations.add({
	version: 5,
	name: "",
	up: function () {
		const options = Options.find().fetch();
		for (const option of options) {
			Options.update(option._id, {
				$set: { name: option.name.replace(/\r/g, "") },
			});
		}
	},
	down: () => {},
});
