import Options from "../../imports/api/collections/Options";
import ThesaurusEntries from "../../imports/api/collections/ThesaurusEntries";
import Attributes from "../../imports/api/collections/Attributes";

Migrations.add({
	version: 3,
	name: "Create thesarus entries for all the options in the Marti project.",
	up: function () {
		// eslint-disable-next-line no-console
		const attributes = Attributes.find(
			{ projectID: "MBbnepLq7J975xP3y" },
			{ fields: { _id: 1 } }
		)
			.fetch()
			.map((att) => att._id);

		const options = Options.find({ attributeID: { $in: attributes } });
		for (const option of options) {
			if (option.name) {
				const matchingEntry = ThesaurusEntries.findOne(
					{ name: option.name.trim() },
					{ fields: { _id: 1 } }
				);
				if (matchingEntry) {
					Options.update(option._id, {
						$set: { thesaurusEntryID: matchingEntry._id },
					});
				} else {
					const newEntry = ThesaurusEntries.insert({
						name: option.name.trim(),
						parent: null,
						projectID: "MBbnepLq7J975xP3y",
						synonymList: [],
						linkedEntryList: [],
						updatedAt: new Date(),
					});
					Options.update(option._id, {
						$set: { thesaurusEntryID: newEntry },
					});
				}
			}
		}
	},
	down: () => {
		Options.update({}, { $unset: { thesaurusEntryID: 1 } }, { multi: true });
		ThesaurusEntries.remove({});
	},
});
