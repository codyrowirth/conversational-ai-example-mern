import Options from "../../imports/api/collections/Options";
import Attributes from "../../imports/api/collections/Attributes";
import Projects from "../../imports/api/collections/Projects";

Migrations.add({
	version: 7,
	name: "",
	up: function () {
		const attributes = Attributes.find({ type: "retrieved" });
		for (const attribute of attributes) {
			const options = Options.find({ attributeID: attribute._id }).fetch();
			if (options.length) {
				Attributes.update(attribute._id, {
					$set: { defaultValue: options[0].name },
				});
				for (const option of options) {
					Options.remove(option._id);
				}
			}
		}
	},
	down: () => {},
});
