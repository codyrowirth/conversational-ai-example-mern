import { Meteor } from "meteor/meteor";
import { Picker } from "meteor/meteorhacks:picker";
import { Roles } from "meteor/alanning:roles";

export const overlordAuth = () => {
	const user = Meteor.user();

	if (!user || user.userType !== "overlord") {
		throw new Meteor.Error(
			"access-denied",
			"Access denied. Only overlords are able to access this functionality."
		);
	}
};

export const adminAuth = () => {
	const user = Meteor.user();

	if (!user || !["overlord", "admin"].includes(user.userType)) {
		throw new Meteor.Error(
			"access-denied",
			"Access denied. Only admins are able to access this functionality."
		);
	}
};

export const basicAuth = () => {
	const user = Meteor.user();
	if (!user) {
		throw new Meteor.Error(
			"access-denied",
			"You must be signed in to use this functionality."
		);
	}
};

if (Meteor.isServer) {
	//abstract this out to the server only so the client doesn't have access to package.json which may have production
	//secrets in it
	Meteor.methods({
		getVersion() {
			const packageInfo = require("../../package.json");
			return packageInfo.version;
		},
	});

	Picker.route("/version", function (params, req, res) {
		const packageInfo = require("../../package.json");
		res.end(packageInfo.version);
	});
}
