import { Meteor } from "meteor/meteor";
import CanvasGroups from "../collections/CanvasGroups";
import { basicAuth } from "../lib";

Meteor.publish("canvasGroups", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return CanvasGroups.find({
		deletedAt: null,
		projectID: Meteor.user()?.projectID,
	});
});

Meteor.methods({
	"canvasGroups.insert"(obj) {
		basicAuth();
		CanvasGroups.insert({ ...obj, projectID: Meteor.user()?.projectID });
	},
	"canvasGroups.update"(id, newData) {
		basicAuth();
		CanvasGroups.update(id, { $set: newData });
	},
	"canvasGroups.remove"(id) {
		basicAuth();
		CanvasGroups.update(id, { deletedAt: new Date() });
	},
	"canvasGroups.findByParent"(id) {
		basicAuth();
		return CanvasGroups.findOne({ parent: id, deletedAt: null });
	},
	"canvasGroups.duplicateNameCheck"({ name, _id, parent }) {
		basicAuth();
		return CanvasGroups.findOne({
			name,
			deletedAt: null,
			projectID: Meteor.user()?.projectID,
			_id: { $ne: _id },
			parent,
		});
	},
	"canvasGroups.getAllIDs"() {
		basicAuth();
		return CanvasGroups.find(
			{ projectID: Meteor.user()?.projectID },
			{ fields: { _id: 1 } }
		)
			.fetch()
			.map((doc) => doc._id);
	},
});
