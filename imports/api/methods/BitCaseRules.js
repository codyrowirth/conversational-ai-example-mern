import { Meteor } from "meteor/meteor";
import BitCaseRules from "../collections/BitCaseRules";
import { basicAuth } from "../lib";

Meteor.publish("bitCaseRules", function (bitCaseID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return BitCaseRules.find({ bitCaseID });
});

Meteor.methods({
	"bitCaseRules.insert"(obj) {
		basicAuth();
		BitCaseRules.insert({ ...obj });
	},
	"bitCaseRules.update"(id, newData) {
		basicAuth();
		BitCaseRules.update(id, { $set: newData });
	},
	"bitCaseRules.remove"(id) {
		basicAuth();
		BitCaseRules.update(id, { deletedAt: new Date() });
	},
});
