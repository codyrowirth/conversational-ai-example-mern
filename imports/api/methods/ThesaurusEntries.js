import { Meteor } from "meteor/meteor";
import ThesaurusEntries from "../collections/ThesaurusEntries.js";
import { basicAuth } from "../lib";
import Options from "../collections/Options";

Meteor.publish("thesaurusEntries", function (projectID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return ThesaurusEntries.find({ projectID, deletedAt: null });
});

Meteor.publish("thesaurusEntriesByParent", function (id) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return ThesaurusEntries.find({
		parent: id || null,
		deletedAt: null,
		projectID: Meteor.user().projectID,
	});
});

Meteor.publish("thesaurusEntry", function (id) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return ThesaurusEntries.find({ _id: id, deletedAt: null });
});

Meteor.publish("thesaurusEntriesByIDs", function (ids) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return ThesaurusEntries.find({
		_id: { $in: ids },
		deletedAt: null,
		projectID: { $in: [Meteor.user().projectID, null] },
	});
});

Meteor.publish("thesaurusEntries.search", function (searchTerm) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}

	return ThesaurusEntries.find({
		name: { $regex: searchTerm, $options: "i" },
		deletedAt: null,
		projectID: Meteor.user().projectID,
	});
});

Meteor.methods({
	"thesaurusEntries.insert"(obj) {
		basicAuth();
		const matchingEntry = ThesaurusEntries.findOne({
			name: obj.name,
			deletedAt: null,
			projectID: Meteor.user()?.projectID,
		});
		if (matchingEntry) {
			return matchingEntry._id;
		}
		return ThesaurusEntries.insert({ ...obj });
	},
	"thesaurusEntries.update"(id, newData) {
		basicAuth();
		return ThesaurusEntries.update(id, { $set: newData });
	},
	"thesaurusEntries.remove"(id) {
		basicAuth();
		Options.update(
			{ thesaurusEntryID: id },
			{ $unset: { thesaurusEntryID: 1 } },
			{ multi: true }
		);
		return ThesaurusEntries.update(id, { deletedAt: new Date() });
	},
	"thesaurusEntries.findOne"(id) {
		basicAuth();
		return ThesaurusEntries.findOne({ id, deletedAt: null });
	},
	"thesaurusEntries.duplicateNameCheck"(name) {
		basicAuth();
		return ThesaurusEntries.findOne({
			name,
			deletedAt: null,
			projectID: Meteor.user()?.projectID,
		});
	},
	"thesaurusEntries.count"() {
		basicAuth();
		return ThesaurusEntries.find({
			projectID: Meteor.user()?.projectID,
		}).count();
	},
});
