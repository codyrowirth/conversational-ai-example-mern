import { Meteor } from "meteor/meteor";
import BitCases from "../collections/BitCases";
import { basicAuth } from "../lib";
import BitCaseRuleGroups from "../collections/BitCaseRuleGroups";
import BitCaseRules from "../collections/BitCaseRules";
import { handleLMEs } from "./Elements";

Meteor.publish("bitCases", function (bitID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return BitCases.find({ bitID, deletedAt: null });
});

Meteor.publish("bitCasesByID", function (_id) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return BitCases.find({ _id, deletedAt: null });
});

Meteor.methods({
	"bitCases.insert"(obj) {
		basicAuth();
		BitCases.insert({ ...obj });
	},
	"bitCases.update"(id, newData) {
		basicAuth();
		BitCases.update(id, { $set: newData });
	},
	"bitCases.remove"(id) {
		basicAuth();
		BitCases.update(id, { deletedAt: new Date() });
	},
	"bitCases.setElse"(id, newData) {
		basicAuth();
		const changedBitCase = BitCases.findOne({ _id: id, deletedAt: null });
		if (newData?.else && changedBitCase) {
			BitCases.update(
				{ bitID: changedBitCase.bitID, _id: { $ne: id } },
				{ $set: { else: false } },
				{ multi: true }
			);
		}
		BitCases.update(id, { $set: newData });
	},
	async "bitCases.duplicate"({ bitCaseID, bitID }) {
		basicAuth();
		return await duplicateBitCase({ bitCaseID, bitID });
	},
});

export const addPassthroughsToUsedAttributes = (
	content,
	usedAttributes,
	destinationMessageDifferent = false
) => {
	content = JSON.stringify(content);
	if (usedAttributes && destinationMessageDifferent) {
		const result = content.matchAll(/"passthrough":"(.+?)"/g);
		for (const match of result) {
			const matchingID = match[1];
			usedAttributes.push(matchingID);
		}
	}
};

export const duplicateBitCase = async ({
	bitCaseID,
	newBitID,
	usedAttributes,
	createdLMEs,
	destinationMessageDifferent = false,
	destinationMessageID,
}) => {
	const bitCase = BitCases.findOne({ _id: bitCaseID, deletedAt: null }); //1
	bitCase.bitCaseRules = BitCaseRules.find({
		bitCaseID: bitCase._id,
		deletedAt: null,
		parentGroupID: null,
	}).fetch(); //2
	bitCase.bitCaseRuleGroups = BitCaseRuleGroups.find({
		bitCaseID: bitCase._id,
		deletedAt: null,
	}).fetch(); //3
	for (const ruleGroup of bitCase.bitCaseRuleGroups) {
		ruleGroup.bitCaseRules = BitCaseRules.find({
			parentGroupID: ruleGroup._id,
			deletedAt: null,
		}).fetch(); //4
	}

	//1
	const toCreate1 = { ...bitCase };
	delete toCreate1._id;
	if (newBitID) {
		toCreate1.bitID = newBitID;
	}

	toCreate1.content = await handleLMEs(
		destinationMessageDifferent,
		toCreate1.content,
		createdLMEs,
		destinationMessageID
	);

	const newBitCase = BitCases.insert(toCreate1);

	for (const bitCaseRule of bitCase.bitCaseRules) {
		//2
		const toCreate2 = { ...bitCaseRule };
		delete toCreate2._id;
		toCreate2.bitCaseID = newBitCase;
		BitCaseRules.insert(toCreate2);

		if (usedAttributes && bitCaseRule.attributeID) {
			usedAttributes.push(bitCaseRule.attributeID);
		}
	}

	for (const bitCaseRuleGroup3 of bitCase.bitCaseRuleGroups) {
		//3
		const toCreate3 = { ...bitCaseRuleGroup3 };
		delete toCreate3._id;
		toCreate3.bitCaseID = newBitCase;
		const newBitCaseRuleGroup3 = BitCaseRuleGroups.insert(toCreate3);

		for (const bitCaseRule4 of bitCaseRuleGroup3.bitCaseRules) {
			//4
			const toCreate4 = { ...bitCaseRule4 };
			delete toCreate4._id;
			toCreate4.bitCaseID = newBitCase;
			toCreate4.parentGroupID = newBitCaseRuleGroup3;
			BitCaseRules.insert(toCreate4);

			if (usedAttributes && bitCaseRule4.attributeID) {
				usedAttributes.push(bitCaseRule4.attributeID);
			}
		}

		// for (const bitCaseRuleGroup5 of bitCaseRuleGroup3.bitCaseRuleGroups) {
		// 	//5
		// 	const toCreate5 = { ...bitCaseRuleGroup5.dataValues };
		// 	delete toCreate5.id;
		// 	toCreate5.parentGroupID = newBitCaseRuleGroup3.id;
		// 	await BitCaseRule.create(toCreate5);
		//
		// 	for (const bitCaseRule6 of bitCaseRuleGroup5.bitCaseRules) {
		// 		//6
		// 		const toCreate6 = { ...bitCaseRule6.dataValues };
		// 		delete toCreate6.id;
		// 		toCreate6.parentGroupID = newBitCaseRuleGroup3.id;
		// 		await BitCaseRule.create(toCreate6);
		// 		if (usedAttributes && bitCaseRule6.attributeID) {
		// 			usedAttributes.push(bitCaseRule6.attributeID);
		// 		}
		// 	}
	}

	addPassthroughsToUsedAttributes(
		bitCase.content,
		usedAttributes,
		destinationMessageDifferent
	);

	return { ...toCreate1, _id: newBitCase };
};
