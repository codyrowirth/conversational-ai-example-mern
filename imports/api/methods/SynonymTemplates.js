import { Meteor } from "meteor/meteor";
import SynonymTemplates from "../collections/SynonymTemplates";
import { basicAuth } from "../lib";

Meteor.publish("synonymTemplates", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return SynonymTemplates.find();
});

Meteor.methods({
	"synonymTemplates.insert"(obj) {
		basicAuth();
		SynonymTemplates.insert(obj);
	},
	"synonymTemplates.update"(id, newData) {
		basicAuth();
		SynonymTemplates.update(id, { $set: newData });
	},
	"synonymTemplates.remove"(id) {
		basicAuth();
		SynonymTemplates.remove(id);
	},
});
