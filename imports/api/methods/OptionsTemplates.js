import { Meteor } from "meteor/meteor";
import OptionsTemplates from "../collections/OptionsTemplates";
import { basicAuth } from "../lib";

Meteor.publish("optionsTemplates", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return OptionsTemplates.find();
});

Meteor.methods({
	"optionsTemplates.insert"(obj) {
		basicAuth();
		OptionsTemplates.insert(obj);
	},
	"optionsTemplates.update"(id, newData) {
		basicAuth();
		OptionsTemplates.update(id, { $set: newData });
	},
	"optionsTemplates.remove"(id) {
		basicAuth();
		OptionsTemplates.remove(id);
	},
});
