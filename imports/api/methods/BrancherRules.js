import { Meteor } from "meteor/meteor";
import BrancherRules from "../collections/BrancherRules";
import { basicAuth } from "../lib";

Meteor.publish("brancherRules", function (lineID) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return BrancherRules.find({ lineID });
});

Meteor.methods({
	"brancherRules.insert"(obj) {
		basicAuth();
		BrancherRules.insert({ ...obj });
	},
	"brancherRules.update"(id, newData) {
		basicAuth();
		BrancherRules.update(id, { $set: newData });
	},
	"brancherRules.remove"(id) {
		basicAuth();
		BrancherRules.remove(id);
	},
	// "brancherRules.findByParent"(id) {
	// 	basicAuth();
	// 	return BrancherRules.findOne({ parent: id });
	// },
	"brancherRules.findOne"(lineID) {
		basicAuth();
		return BrancherRules.findOne({ lineID });
	},
});
