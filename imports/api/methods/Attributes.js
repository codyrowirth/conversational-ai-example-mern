import { Meteor } from "meteor/meteor";
import Attributes from "../collections/Attributes";
import { basicAuth } from "../lib";
import { publishComposite } from "meteor/reywood:publish-composite";
import Options from "../collections/Options";
import AttributeGroups from "../collections/AttributeGroups";
import { titleCase } from "title-case";

Meteor.publish("attribute", function (id) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}

	return Attributes.find({
		_id: id,
		deletedAt: null,
		projectID: { $in: [Meteor.user().projectID, null] },
	});
});

Meteor.publish("attributes", function (ids) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}

	return Attributes.find({
		_id: { $in: ids },
		deletedAt: null,
		projectID: { $in: [Meteor.user().projectID, null] },
	});
});

Meteor.publish("attributesByParent", function (id) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}

	return Attributes.find({ parent: id || null, deletedAt: null });
});

Meteor.publish("attributes.search", function (searchTerm) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}

	return Attributes.find({
		name: { $regex: searchTerm, $options: "i" },
		deletedAt: null,
	});
});

publishComposite("attributesByIDWithOptions", function (ids) {
	return {
		find() {
			return Attributes.find({
				_id: { $in: ids },
				deletedAt: null,
				projectID: { $in: [Meteor.user().projectID, null] },
			});
		},
		children: [
			{
				find(attribute) {
					return Options.find({ attributeID: attribute._id, deletedAt: null });
				},
			},
		],
	};
});

const recursiveExporter = async (folder, parentFolder, toReturn) => {
	const subFolders = AttributeGroups.find(
		{ parent: folder._id },
		{ sort: { name: 1 } }
	).fetch();

	for (const subFolder of subFolders) {
		await recursiveExporter(subFolder, folder, toReturn);
	}

	toReturn.push({
		"Folder/File": "Folder",
		"Parent Folder": parentFolder ? parentFolder.name : "",
		"Folder Name": folder.name,
		"File Type": "",
		Attribute: "",
		Option: "",
		"Default Option": "",
		"Session Attribute": "",
		Code: "",
	});

	let lastAttribute = "";
	const attributes = Attributes.find(
		{
			parent: folder._id,
		},
		{ sort: { name: 1 } }
	).fetch();

	for (const attribute of attributes) {
		attribute.options = Options.find({ attributeID: attribute._id }).fetch();
	}

	for (const attribute of attributes) {
		if (attribute?.options?.length) {
			for (const option of attribute.options) {
				if (attribute.name !== lastAttribute) {
					toReturn.push({
						"Folder/File": "File",
						"Parent Folder": folder.name,
						Attribute: attribute.name,
						"File Type": titleCase(attribute.type),
						Option: option.name,
						"Default Option": option.default ? "X" : "",
						"Session Attribute": attribute.sessionAttribute ? "X" : "",
						Code: attribute.code ? attribute.code : "",
						"Linked Glossary Entry": option?.glossaryEntry?.name,
					});
				} else {
					toReturn.push({
						Option: option.name,
						"Default Option": option.default ? "X" : "",
						"Linked Glossary Entry": option?.glossaryEntry?.name,
					});
				}
				lastAttribute = attribute.name;
			}
		} else {
			toReturn.push({
				"Folder/File": "File",
				Attribute: attribute.name,
				"File Type": titleCase(attribute.type),
				"Parent Folder": folder.name,
				Code: attribute.code ? attribute.code : "",
				"Session Attribute": attribute.sessionAttribute ? "X" : "",
			});
		}
	}
};

Meteor.methods({
	"attributes.insert"(obj) {
		basicAuth();
		return Attributes.insert({ ...obj, projectID: Meteor.user()?.projectID });
	},
	"attributes.duplicateNameCheck"({ name, _id }) {
		return Attributes.findOne({
			name,
			deletedAt: null,
			projectID: Meteor.user()?.projectID,
			_id: { $ne: _id || null },
		});
	},
	"attributes.update"(id, newData) {
		basicAuth();
		Attributes.update(id, { $set: newData });
	},
	"attributes.remove"(id) {
		basicAuth();
		Attributes.update(id, { deletedAt: new Date() });
	},
	"attributes.findOne"(id) {
		basicAuth();
		return Attributes.findOne({ id, deletedAt: null });
	},
	"attributes.findByParent"(id) {
		basicAuth();
		return Attributes.findOne({ parent: id, deletedAt: null });
	},
	"attributes.count"() {
		basicAuth();
		return Attributes.find({
			projectID: Meteor.user()?.projectID,
		}).count();
	},
	async "attributes.export"() {
		basicAuth();
		const rootFolders = AttributeGroups.find({
			projectID: Meteor.user().projectID,
			parent: null,
		}).fetch();

		const fields = [
			"Folder/File",
			"Parent Folder",
			"Folder Name",
			"File Type",
			"Attribute",
			"Option",
			"Default Option",
			"Session Attribute",
			"Code",
			"Linked Glossary Entry",
		];

		let toReturn = [];
		for (const rootFolder of rootFolders) {
			await recursiveExporter(rootFolder, null, toReturn);
		}

		return { data: { fields, data: toReturn } };
	},
});
