import { Meteor } from "meteor/meteor";
import Shapes from "../collections/Shapes";
import { basicAuth } from "../lib";
import Canvases from "../collections/Canvases";
import Messages from "../collections/Messages";
import Elements from "../collections/Elements";
import { elementCopy, handleLMEs } from "./Elements";
import BitCases from "../collections/BitCases";
import Bits from "../collections/Bits";
import { duplicateBitCase } from "./BitCases";
import Branchers from "../collections/Branchers";
import BrancherRules from "../collections/BrancherRules";
import BrancherLines from "../collections/BrancherLines";
import BitCaseRules from "../collections/BitCaseRules";
import BitCaseRuleGroups from "../collections/BitCaseRuleGroups";
import BrancherRuleGroups from "../collections/BrancherRuleGroups";
import React from "react";
import MessageTrackingAttributes from "../collections/MessageTrackingAttributes";

Meteor.publish("shapes", function (canvasID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return Shapes.find({ canvasID });
});

Meteor.publish("projectMessageShapes", function () {
	const projectsCanvases = Canvases.find(
		{ projectID: Meteor.user().projectID },
		{ fields: { _id: 1 } }
	);

	return Shapes.find(
		{
			canvasID: { $in: projectsCanvases.map((canvas) => canvas._id) },
			type: "message",
		},
		{ fields: { _id: 1, "data.label": 1, type: 1 } }
	);
});

Meteor.publish("portals", function () {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}

	const projectCanvases = Canvases.find(
		{ projectID: Meteor.user().projectID },
		{ fields: { _id: 1, projectID: 1 } }
	).fetch();

	return Shapes.find({
		type: "portal",
		canvasID: { $in: projectCanvases.map((canvas) => canvas._id) },
	});
});

Meteor.methods({
	"shapes.insert"(shapeData) {
		basicAuth();
		return Shapes.insert(shapeData);
	},
	"shapes.update"(id, newData) {
		basicAuth();

		return Shapes.update(id, { $set: newData });
	},
	"shapes.updateData"(id, key, value) {
		basicAuth();
		return Shapes.update(id, {
			$set: { [`data.${key}`]: value, updatedAt: new Date() },
		});
	},
	"shapes.remove"(id) {
		basicAuth();
		//remove any connecting lines
		Shapes.remove({ source: id });
		Shapes.remove({ target: id });
		return Shapes.remove(id);
	},
	"shapes.edgeBulkUpdate"(canvasID, edgeType) {
		basicAuth();
		const canvasEdges = Shapes.find({
			canvasID,
			source: { $exists: true },
			target: { $exists: true },
		}).map((shape) => shape._id);
		if (edgeType === "curved") {
			edgeType = "default";
		}
		return Shapes.update(
			{ _id: { $in: canvasEdges } },
			{ $set: { type: edgeType } },
			{ multi: true }
		);
	},
	"shapes.messageSearch"(query) {
		basicAuth();

		if (!query) {
			return [];
		}

		const canvasesInProject = Canvases.find(
			{ projectID: Meteor.user().projectID },
			{ fields: { _id: 1 } }
		);

		return Shapes.find(
			{
				type: "message",
				"data.label": {
					$regex: query,
					$options: "i",
				},
				canvasID: { $in: canvasesInProject.map((canvas) => canvas._id) },
			},
			{ limit: 25, fields: { "data.label": 1, _id: 1, canvasID: 1 } }
		).fetch();
	},
	async "shapes.paste"(copiedShapeIDs, canvasID, lastClickPosition, shiftKey) {
		const sourceShapes = Shapes.find({
			_id: { $in: copiedShapeIDs },
			source: null,
			target: null,
		}).fetch();
		const sourceLines = Shapes.find({
			_id: { $in: copiedShapeIDs },
			source: { $ne: null },
			target: { $ne: null },
		}).fetch();

		const firstShape = sourceShapes.find(
			(shape) => shape._id === copiedShapeIDs[0]
		);
		const deltaX = firstShape.position.x - lastClickPosition.x;
		const deltaY = firstShape.position.y - lastClickPosition.y;

		let shapeIDMap = {};

		for (const sourceShape of sourceShapes) {
			const oldID = sourceShape._id;
			delete sourceShape._id;
			sourceShape.canvasID = canvasID;

			sourceShape.position.x = sourceShape.position.x - deltaX;
			sourceShape.position.y = sourceShape.position.y - deltaY;

			shapeIDMap[oldID] = Shapes.insert(sourceShape);

			if (sourceShape.type === "message" && !shiftKey) {
				await copyMessage(oldID, shapeIDMap[oldID]);
			} else if (sourceShape.type === "brancher") {
				const { newBrancherID, oldBrancherID } = copyBrancher(
					oldID,
					shapeIDMap[oldID],
					shiftKey
				);
				shapeIDMap[oldBrancherID] = newBrancherID;
			}
		}

		for (const sourceLine of sourceLines) {
			const oldShapeID = sourceLine._id;
			delete sourceLine._id;
			sourceLine.target = shapeIDMap[sourceLine.target];
			sourceLine.source = shapeIDMap[sourceLine.source];

			if (sourceLine.target && sourceLine.source) {
				shapeIDMap[oldShapeID] = Shapes.insert(sourceLine);

				if (!shiftKey) {
					const brancherLines = BrancherLines.find({ lineID: oldShapeID });
					for (const brancherLine of brancherLines) {
						delete brancherLine._id;
						brancherLine.lineID = shapeIDMap[oldShapeID];
						brancherLine.brancherID = shapeIDMap[brancherLine.brancherID];
						brancherLine.target = sourceLine.target;
						BrancherLines.insert(brancherLine);

						copyBrancherRules(oldShapeID, shapeIDMap[oldShapeID]);
					}
				}
			}
		}

		return Object.values(shapeIDMap);
	},
	"shapes.portalType"(portalShapeID) {
		return Shapes.find({ source: portalShapeID }).fetch();
	},
	"shapes.followPortal"(portalShapeID) {
		const portalShape = Shapes.findOne({ _id: portalShapeID });
		return followPortal(portalShape);
	},
	"shapes.validate"(canvasID) {
		const canvasesInThisProject = Canvases.find(
			{ projectID: Meteor.user().projectID },
			{ fields: { _id: 1 } }
		).fetch();

		validatePortals(canvasID, canvasesInThisProject);
		validateBranchers(canvasID);
		validateSend(canvasID, canvasesInThisProject);
	},
	"shapes.find"(_id) {
		return Shapes.findOne({ _id });
	},
});

const validatePortals = (canvasID, canvasesInThisProject) => {
	const portals = Shapes.find({ type: "portal", canvasID }).fetch();
	for (const portal of portals) {
		portal.hasOutgoingLine = Shapes.findOne({ source: portal._id });

		if (!portal.hasOutgoingLine) {
			const matchingPortal = followPortal(portal, canvasesInThisProject);
			if (!matchingPortal) {
				Shapes.update(portal._id, { $set: { "data.status": "error" } });
			} else {
				Shapes.update(portal._id, { $unset: { "data.status": 1 } });
			}
		}
	}

	for (const portal of portals) {
		if (portal.hasOutgoingLine) {
			const matchingPortal = portals.find((portalSearch) => {
				return (
					portal.data.label === portalSearch.data.label &&
					!portalSearch.hasOutgoingLine
				);
			});
			if (!matchingPortal) {
				Shapes.update(portal._id, { $set: { "data.status": "error" } });
			} else {
				Shapes.update(portal._id, { $unset: { "data.status": 1 } });
			}
		}
	}
};

const validateBranchers = (canvasID) => {
	const branchers = Shapes.find({ type: "brancher", canvasID }).fetch();
	for (const brancher of branchers) {
		const linesOutOfBrancher = Shapes.find({ source: brancher._id }).fetch();
		let invalidBrancher;
		for (const lineOutOfBrancher of linesOutOfBrancher) {
			const rules = BrancherRules.find({
				lineID: lineOutOfBrancher._id,
			}).fetch();

			const brancherLine = BrancherLines.findOne({
				target: lineOutOfBrancher.target,
			});

			if (rules.length === 0 && !brancherLine?.else) {
				invalidBrancher = true;
				break;
			} else {
				for (const rule of rules) {
					if (brancherLine.else) {
						//ignore, they're all valid
					} else if (
						!rule.attributeID ||
						!rule.operator ||
						!(rule.optionID || rule.optionValue)
					) {
						invalidBrancher = true;
						break;
					}
				}
			}
		}
		if (invalidBrancher) {
			Shapes.update(brancher._id, { $set: { "data.status": "error" } });
		} else {
			Shapes.update(brancher._id, { $unset: { "data.status": 1 } });
		}
	}
};

const validateSend = (canvasID, canvasesInThisProject) => {
	const sendShapesOnThisCanvas = Shapes.find({
		type: "send",
		canvasID,
	}).fetch();

	for (const sendShape of sendShapesOnThisCanvas) {
		sendShape.hasIncomingLine = !!Shapes.findOne({ target: sendShape._id });
	}

	const allSendShapes = Shapes.find({
		type: "send",
		canvasID: { $in: canvasesInThisProject.map((canvas) => canvas._id) },
	}).fetch();

	for (const sendShape of allSendShapes) {
		sendShape.hasOutgoingLine = !!Shapes.findOne({ source: sendShape._id });
		sendShape.hasIncomingLine = !!Shapes.findOne({ target: sendShape._id });
	}

	for (const sendShape of sendShapesOnThisCanvas) {
		if (sendShape.hasIncomingLine) {
			const matchingSendShape = allSendShapes.find(
				(matchingSend) =>
					matchingSend._id !== sendShape._id &&
					matchingSend.hasOutgoingLine &&
					!matchingSend.hasIncomingLine
			);

			if (!matchingSendShape) {
				Shapes.update(sendShape._id, { $set: { "data.status": "error" } });
			} else {
				Shapes.update(sendShape._id, { $unset: { "data.status": 1 } });
			}
		}
	}
};

const followPortal = (portalShape, canvasesInThisProject) => {
	if (!canvasesInThisProject) {
		canvasesInThisProject = Canvases.find(
			{ projectID: Meteor.user().projectID },
			{ fields: { _id: 1 } }
		).fetch();
	}
	let portalShapes = Shapes.find(
		{
			"data.label": portalShape.data.label,
			type: "portal",
			_id: { $ne: portalShape._id },
			canvasID: { $in: canvasesInThisProject.map((canvas) => canvas._id) },
		},
		{
			fields: {
				data: { label: 1 },
				_id: 1,
				canvasID: 1,
				position: { x: 1, y: 1 },
			},
		}
	).fetch();

	portalShapes = portalShapes.filter((portal) => {
		const hasOutgoingLine = Shapes.findOne({ source: portal._id });
		return !!hasOutgoingLine;
	});

	let matchingPortal;
	if (portalShapes.length > 1) {
		matchingPortal = portalShapes.find(
			(portal) => portal.canvasID === portalShape.canvasID
		);
		if (!matchingPortal) {
			//if there isn't a match on this canvas, take the first match on any canvas
			matchingPortal = portalShapes[0];
		}
	} else {
		matchingPortal = portalShapes[0];
	}

	return matchingPortal;
};

const copyBrancherRules = (sourceBrancherShapeLineID, destinationLineID) => {
	const brancherRules = BrancherRules.find({
		lineID: sourceBrancherShapeLineID,
		parentGroupID: null,
		deletedAt: null,
	}).fetch(); //1
	const brancherRuleGroups = BrancherRuleGroups.find({
		lineID: sourceBrancherShapeLineID,
		deletedAt: null,
	}).fetch(); //2
	for (const ruleGroup of brancherRuleGroups) {
		ruleGroup.brancherRules = BrancherRules.find({
			parentGroupID: ruleGroup._id,
			deletedAt: null,
		}).fetch(); //3
	}

	for (const rule of brancherRules) {
		//1
		const toCreate1 = { ...rule };
		delete toCreate1._id;
		toCreate1.lineID = destinationLineID;
		BrancherRules.insert(toCreate1);
	}

	for (const ruleGroup2 of brancherRuleGroups) {
		//2
		const toCreate2 = { ...ruleGroup2 };
		delete toCreate2._id;
		toCreate2.lineID = destinationLineID;
		const newRuleGroup2 = BrancherRuleGroups.insert(toCreate2);

		for (const rule3 of ruleGroup2.brancherRules) {
			//3
			const toCreate3 = { ...rule3 };
			delete toCreate3._id;
			toCreate3.lineID = destinationLineID;
			toCreate3.parentGroupID = newRuleGroup2;
			BrancherRules.insert(toCreate3);
		}
	}
};

const copyMessage = async (sourceShapeID, newShapeID) => {
	const sourceMessage = Messages.findOne({ shapeID: sourceShapeID });
	const sourceMessageID = sourceMessage._id;
	delete sourceMessage._id;
	sourceMessage.shapeID = newShapeID;

	const newMessageID = Messages.insert(sourceMessage);

	let sourceElements = Elements.find({
		messageID: sourceMessageID,
		deletedAt: null,
	}).fetch();

	for (const sourceElement of sourceElements) {
		sourceElement.bits = Bits.find({ elementID: sourceElement._id }).fetch();

		for (const bit of sourceElement.bits) {
			bit.bitCases = BitCases.find({ bitID: bit._id, deletedAt: null }).fetch();
		}
	}

	let newElements = [];
	let newBitCases = [];

	for (let element of sourceElements) {
		delete element._id;
		let newElementID = await Elements.insert({
			...element,
			messageID: newMessageID,
		});
		for (let bit of element.bits) {
			const oldBitID = bit._id;
			delete bit._id;
			const newBitID = await Bits.insert({
				...bit,
				elementID: newElementID,
			});
			element.content = JSON.parse(
				JSON.stringify(element.content).replace(
					new RegExp(`"bitID":"${oldBitID}"`, "g"),
					`"bitID":"${newBitID}"`
				)
			);

			for (const bitCase of bit.bitCases) {
				const newBitCase = await duplicateBitCase({
					bitCaseID: bitCase._id,
					newBitID,
					newBitCases,
				});
				newBitCases.push(newBitCase);
			}
		}
		Elements.update(newElementID, { $set: { content: element.content } });
		newElements.push({ ...element, _id: newElementID });
	}

	await mapILMEToNewElements(newElements, sourceElements, newBitCases);

	await copyTrackingAttributes(sourceMessageID, newMessageID);
};

const mapILMEToNewElements = async (newElements, elementsDB, newBitCases) => {
	for (const element of newElements) {
		if (element.content && JSON.stringify(element.content).includes(`"lme":`)) {
			const result = JSON.stringify(element.content).matchAll(/"lme":(.+?)/g);
			for (const match of result) {
				const stringToReplace = match[0];
				const matchingID = match[1];
				const oldElement = elementsDB.find(
					(element) => `${element._id}` === matchingID
				);
				const newElement = newElements.find(
					(element) => element.name === oldElement.name
				);
				element.content = element.content.replace(
					new RegExp(stringToReplace, "g"),
					`"lme":"${newElement._id}"`
				);
			}
			Elements.update(element._id, {
				$set: { content: JSON.parse(element.content) },
			});
		}
	}

	for (const newBitCase of newBitCases) {
		if (newBitCase.content && newBitCase.content.includes(`"lme":`)) {
			const result = newBitCase.content.matchAll(/"lme":"(.+?)"/g);
			for (const match of result) {
				const stringToReplace = match[0];
				const matchingID = match[1];
				const oldElement = elementsDB.find(
					(element) => `${element._id}` === matchingID
				);
				const newElement = newElements.find(
					(element) => element.name === oldElement.name
				);
				newBitCase.content = newBitCase.content.replace(
					new RegExp(stringToReplace, "g"),
					`"lme":"${newElement._id}"`
				);
			}
			BitCases.update(newBitCase._id, {
				$set: { content: JSON.parse(newBitCase.content) },
			});
		}
	}
};

const copyTrackingAttributes = async (sourceMessageID, newMessageID) => {
	const sourceTrackingAttributes = MessageTrackingAttributes.find({
		messageID: sourceMessageID,
	});
	for (const sourceTrackingAttribute of sourceTrackingAttributes) {
		delete sourceTrackingAttribute._id;
		sourceTrackingAttribute.messageID = newMessageID;
		MessageTrackingAttributes.insert(sourceTrackingAttribute);
	}
};

const copyBrancher = (sourceShapeID, newShapeID, shiftKey) => {
	const sourceBrancher = Branchers.findOne({ shapeID: sourceShapeID });
	const oldBrancherID = sourceBrancher._id;
	delete sourceBrancher._id;
	sourceBrancher.shapeID = newShapeID;
	if (shiftKey) {
		sourceBrancher.attributes = [];
	}
	const newBrancherID = Branchers.insert(sourceBrancher);
	return { newBrancherID, oldBrancherID };
};
