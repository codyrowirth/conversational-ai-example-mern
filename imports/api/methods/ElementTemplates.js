import { Meteor } from "meteor/meteor";
import ElementTemplates from "../collections/ElementTemplates";
import { basicAuth } from "../lib";

Meteor.publish("elementTemplates", function (id) {
	if (!this.userId || !Meteor.user().projectID) {
		return this.ready();
	}

	return ElementTemplates.find({ projectID: id, deletedAt: null });
});

Meteor.methods({
	"elementTemplates.insert"(obj) {
		basicAuth();
		return ElementTemplates.insert({
			...obj,
			projectID: Meteor.user().projectID,
		});
	},
	"elementTemplates.update"(id, newData) {
		basicAuth();
		ElementTemplates.update(id, { $set: newData });
	},
	"elementTemplates.remove"(id) {
		basicAuth();
		ElementTemplates.update(id, { $set: { deletedAt: new Date() } });
	},
});
