import { counter } from "@fortawesome/fontawesome-svg-core";
import { Meteor } from "meteor/meteor";
import Elements from "../collections/Elements";
import { basicAuth } from "../lib";
import Messages from "../collections/Messages";
import { addPassthroughsToUsedAttributes, duplicateBitCase } from "./BitCases";
import Bits from "../collections/Bits";
import BitCases from "../collections/BitCases";

Meteor.publish("elements", function (messageID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return Elements.find({ messageID });
});

Meteor.methods({
	"elements.insert"(obj) {
		basicAuth();
		if (Elements.findOne({ name: obj.name, messageID: obj.messageID })) {
			let counter = 1;
			let found = true;
			let nameToAdd = obj.name;
			while (found && counter < 1000) {
				counter = counter + 1;
				const regexMatch = nameToAdd.match(/-\d+$/);
				if (!regexMatch) {
					nameToAdd = obj.name + "-2";
				} else {
					nameToAdd =
						nameToAdd.substring(0, regexMatch.index) +
						"-" +
						(parseInt(regexMatch[0].replace("-", ""), 10) + 1);
				}
				found = Elements.findOne({
					name: nameToAdd,
					messageID: obj.messageID,
					deletedAt: null,
				});
			}
			obj.name = nameToAdd;
		}
		Elements.insert(obj);
	},
	"elements.update"(id, newData) {
		basicAuth();
		let messageID = newData?.messageID;
		if (!messageID) {
			const elementDB = Elements.findOne({ _id: id });
			messageID = elementDB?.messageID;
		}
		if (newData?.name) {
			if (
				Elements.findOne({
					name: newData.name,
					messageID,
					_id: { $ne: id },
					deletedAt: null,
				})
			) {
				let counter = 1;
				let found = true;
				let nameToAdd = newData.name;
				while (found && counter < 1000) {
					counter = counter + 1;
					const regexMatch = nameToAdd.match(/-\d+$/);
					if (!regexMatch) {
						nameToAdd = newData.name + "-2";
					} else {
						nameToAdd =
							nameToAdd.substring(0, regexMatch.index) +
							"-" +
							(parseInt(regexMatch[0].replace("-", ""), 10) + 1);
					}
					found = Elements.findOne({
						name: nameToAdd,
						messageID: messageID,
						_id: { $ne: id },
						deletedAt: null,
					});
				}
				newData.name = nameToAdd;
			}
		}
		Elements.update(id, { $set: newData });
	},
	"elements.remove"(id) {
		basicAuth();
		Elements.remove(id);
	},
	"elements.getByShapeID"(shapeID) {
		const message = Messages.findOne({ shapeID });
		return Elements.find({ messageID: message._id }).fetch();
	},
	async "elements.duplicate"(element) {
		return await elementCopy(element._id, element.messageID);
	},
	async "elements.paste"(elementID, destinationMessageID) {
		return await elementCopy(elementID, destinationMessageID);
	},
});

export const handleLMEs = async (
	destinationMessageDifferent,
	content,
	createdLMEs,
	destinationMessageID
) => {
	content = content ? JSON.stringify(content) : "";
	if (destinationMessageDifferent && content.includes(`"lme":`)) {
		const result = content.matchAll(/"label":".*?","lme":"(.+?)"/g);
		for (const match of result) {
			const stringToReplace = match[0];
			const matchingID = match[1];
			if (!createdLMEs[matchingID]) {
				const newElement = await elementCopy(
					matchingID,
					destinationMessageID,
					createdLMEs
				);

				createdLMEs[matchingID] = {
					name: newElement.name,
					id: newElement._id,
				};
			}
			content = content.replace(
				new RegExp(stringToReplace, "g"),
				`"label":"${createdLMEs[matchingID].name}","lme":"${createdLMEs[matchingID].id}"`
			);
		}
	}
	if (content) {
		return JSON.parse(content);
	} else {
		return null;
	}
};

const duplicateElement = async (
	sourceElement,
	destinationMessageID,
	createdLMEs = {},
	destinationMessageDifferent = false,
	usedAttributes
) => {
	delete sourceElement._id;
	sourceElement.messageID = destinationMessageID;
	sourceElement.content = await handleLMEs(
		destinationMessageDifferent,
		sourceElement.content,
		createdLMEs,
		destinationMessageID
	);

	addPassthroughsToUsedAttributes(
		sourceElement.content,
		usedAttributes,
		destinationMessageDifferent
	);

	sourceElement.order =
		Elements.find({
			messageID: destinationMessageID,
			deletedAt: null,
		}).count() + 1;

	sourceElement._id = Elements.insert(sourceElement);
	return sourceElement;
};

const handleDuplicateElementNames = (otherElementsOnMessage, sourceElement) => {
	let counter = 0; //while loops scare me without a second catch to prevent it from running infinitely
	let checkIfNameExists = false;
	do {
		checkIfNameExists = false;
		for (const element of otherElementsOnMessage) {
			if (element.name === sourceElement.name) {
				checkIfNameExists = true;

				const regexMatch = sourceElement.name.match(/-\d+$/);

				if (regexMatch) {
					sourceElement.name =
						sourceElement.name.substring(0, regexMatch.index) +
						"-" +
						(parseInt(regexMatch[0].replace("-", ""), 10) + 1);
				} else {
					sourceElement.name += "-2";
				}
			}
		}
		counter++;
	} while (checkIfNameExists && counter < 1000);
};

const duplicateBitsAndBitCases = async (
	sourceElement,
	newElement,
	destinationMessageDifferent,
	usedAttributes,
	createdLMEs,
	destinationMessageID
) => {
	for (let bit of sourceElement.bits) {
		const oldBitID = bit._id;
		delete bit._id;
		bit.elementID = newElement._id;

		const newBitID = await Bits.insert(bit);

		newElement.content = JSON.parse(
			JSON.stringify(newElement.content).replace(
				new RegExp(`"bitID":"${oldBitID}"`, "g"),
				`"bitID":"${newBitID}"`
			)
		);

		for (const bitCase of bit.bitCases) {
			await duplicateBitCase({
				bitCaseID: bitCase._id,
				newBitID,
				usedAttributes,
				destinationMessageDifferent,
				destinationMessageID,
			});
		}
	}

	Elements.update(newElement._id, { $set: { content: newElement.content } });
	return usedAttributes;
};

const duplicateMessageAttributes = async (
	destinationMessageDifferent,
	usedAttributes,
	destinationMessageID
) => {
	usedAttributes = [...new Set(usedAttributes)];

	const destinationMessageAttributesDB = Messages.findOne(
		{ _id: destinationMessageID, deletedAt: null },
		{ fields: { attributes: 1, _id: 1 } }
	);

	const destinationMessageAttributes =
		destinationMessageAttributesDB.attributes;

	const attributesToCopy = usedAttributes.filter((att) => {
		return !destinationMessageAttributes.includes(att);
	});

	Messages.update(
		{ _id: destinationMessageAttributesDB },
		{
			$set: {
				attributes: [...destinationMessageAttributes, ...attributesToCopy],
			},
		}
	);
};

export const elementCopy = async (
	elementID,
	destinationMessageID,
	createdLMEs = {}
) => {
	const sourceElement = Elements.findOne({ _id: elementID });
	const otherElementsOnMessage = Elements.find({
		messageID: destinationMessageID,
		deletedAt: null,
	}).fetch();

	sourceElement.bits = Bits.find({ elementID: sourceElement._id }).fetch();
	for (const bit of sourceElement.bits) {
		bit.bitCases = BitCases.find({ bitID: bit._id, deletedAt: null }).fetch();
	}

	handleDuplicateElementNames(otherElementsOnMessage, sourceElement);

	let destinationMessageDifferent = !(
		sourceElement.messageID === destinationMessageID
	);
	let usedAttributes = [];

	let newElement = await duplicateElement(
		sourceElement,
		destinationMessageID,
		createdLMEs,
		destinationMessageDifferent,
		usedAttributes
	);

	usedAttributes = await duplicateBitsAndBitCases(
		sourceElement,
		newElement,
		destinationMessageDifferent,
		usedAttributes,
		createdLMEs,
		destinationMessageID
	);

	if (destinationMessageDifferent) {
		await duplicateMessageAttributes(
			destinationMessageDifferent,
			usedAttributes,
			destinationMessageID
		);
	}

	return newElement;
};
