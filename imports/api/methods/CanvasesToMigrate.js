import { Meteor } from "meteor/meteor";
import CanvasesToMigrate from "../collections/CanvasesToMigrate";
import { basicAuth } from "../lib";

Meteor.publish("canvasesToMigrate", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return CanvasesToMigrate.find({
		projectID: Meteor.user()?.projectID,
	});
});

Meteor.methods({
	"canvasesToMigrate.insert"(obj) {
		basicAuth();
		CanvasesToMigrate.insert({ ...obj, projectID: Meteor.user()?.projectID });
	},
	"canvasesToMigrate.update"(id, newData) {
		basicAuth();
		CanvasesToMigrate.update(id, { $set: newData });
	},
	"canvasesToMigrate.remove"(id) {
		basicAuth();
		CanvasesToMigrate.update(id, { deletedAt: new Date() });
	},
});
