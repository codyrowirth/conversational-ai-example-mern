import { Meteor } from "meteor/meteor";
import Options from "../collections/Options";
import { basicAuth } from "../lib";

Meteor.publish("optionsByAttribute", function (id) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return Options.find({ attributeID: id || null, deletedAt: null });
});

Meteor.publish("optionsByThesaurusEntry", function (id) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return Options.find({ thesaurusEntryID: id, deletedAt: null });
});

Meteor.publish("optionsByAttributes", function (ids) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return Options.find({ attributeID: { $in: ids } || null, deletedAt: null });
});

// Meteor.publish("attributes.search", function (searchTerm) {
//   if (!this.userId || !Meteor.user().accountID) {
//     return this.ready();
//   }
//
//   return Attributes.find({
//     name: { $regex: searchTerm, $options: "i" },
//     deletedAt: null,
//   });
// });

Meteor.methods({
	"options.insert"(obj) {
		basicAuth();
		if (obj?.name.includes("\r")) {
			obj.name = obj.name.replace(/\r/g, "");
		}
		return Options.insert(obj);
	},
	"options.update"(id, newData) {
		basicAuth();
		if (obj?.name.includes("\r")) {
			obj.name = obj.name.replace(/\r/g, "");
		}
		Options.update(id, { $set: newData });
	},
	"options.remove"(id) {
		basicAuth();
		Options.update(id, { $set: { deletedAt: new Date() } });
	},
	"options.setDefault"(id, attributeID) {
		basicAuth();
		Options.update(
			{ attributeID },
			{ $unset: { default: 1 } },
			{ multi: true }
		);
		Options.update(id, { $set: { default: true } });
	},
});
