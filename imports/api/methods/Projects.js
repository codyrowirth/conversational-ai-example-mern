import { Meteor } from "meteor/meteor";
import { adminAuth, basicAuth } from "../lib";
import Projects from "../collections/Projects";

Meteor.publish("projects", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return Projects.find({
		deletedAt: null,
		accountID: Meteor.user().accountID,
	});
});

Meteor.methods({
	"projects.insert"(name) {
		basicAuth();
		if (!Meteor.user().accountID) {
			throw new Meteor.Error(
				"As a super admin, you need an account active before creating a project"
			);
		}
		Projects.insert({
			name,
			accountID: Meteor.user().accountID,
		});
	},
	"projects.update"(id, newData) {
		adminAuth();

		Projects.update(id, { $set: newData });
	},
	"projects.remove"(id) {
		adminAuth();

		Projects.update(id, { deletedAt: new Date() });
	},
});
