import { Meteor } from "meteor/meteor";
import { adminAuth, basicAuth, overlordAuth } from "../lib";
import { Accounts } from "meteor/accounts-base";

Accounts.validateNewUser(function (user) {
	const adminUser = Accounts.findUserByUsername("admin");
	if (!adminUser || Roles.userIsInRole(Meteor.user(), ["overlord", "admin"])) {
		return true;
	}

	throw new Meteor.Error("unauthorized", "Not authorized to create new users");
});

Accounts.emailTemplates.siteName = "tuzagTCS";
Accounts.emailTemplates.from = "tuzagTCS <noreply@tuzagtcs.com>";

Accounts.emailTemplates.enrollAccount.subject = () => {
	return `Yippee Ki Yay, You've Been Invited to TCS`;
};
Accounts.emailTemplates.enrollAccount.text = (user, url) => {
	return `Bruce Willis cordially invites you to use TCSv4. It's in alpha though, so use at your own risk.\n\n${url}`;
};

Accounts.urls.enrollAccount = (token) => {
	return Meteor.absoluteUrl(`set-password?token=${token}&enroll=true`);
};

Accounts.urls.resetPassword = (token) => {
	return Meteor.absoluteUrl(`set-password?token=${token}`);
};

Meteor.methods({
	"user.setActiveAccount"(accountID) {
		overlordAuth();

		Meteor.users.update(this.userId, { $set: { accountID } });
	},
	"user.setActiveProject"(projectID) {
		basicAuth();
		Meteor.users.update(this.userId, { $set: { projectID } });
	},
	"user.create"(userInfo) {
		adminAuth();
		if (userInfo.userType === "overlord") {
			overlordAuth();
		}
		const newUser = Accounts.createUser({
			...userInfo,
		});
		Meteor.users.update(
			{ _id: newUser },
			{
				$set: {
					accountID: Meteor.user().accountID,
					defaultRole: userInfo.defaultRole,
					userType: userInfo.userType,
				},
			}
		);
		Accounts.sendEnrollmentEmail(newUser);
		return newUser;
	},
	"users.findByToken"(token) {
		return Meteor.users.findOne({
			"services.password.enroll.token": token,
		});
	},
	"user.setTypingDelay"(typingDelay) {
		overlordAuth();

		Meteor.users.update(this.userId, { $set: { typingDelay } });
	},
});

Meteor.publish("Meteor.users.info", function () {
	if (!this.userId) {
		return this.ready();
	}
	return Meteor.users.find(
		{ _id: this.userId },
		{ fields: { accountID: 1, projectID: 1, userType: 1, typingDelay: 1 } }
	);
});

Meteor.publish("Meteor.users.team", function () {
	if (!this.userId) {
		return this.ready();
	}
	return Meteor.users.find(
		{ accountID: Meteor.user().accountID || true },
		{ fields: { profile: 1, emails: 1, _id: 1, defaultRole: 1, userType: 1 } }
	);
});
