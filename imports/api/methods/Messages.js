import { Meteor } from "meteor/meteor";
import Messages from "../collections/Messages";
import { basicAuth } from "../lib";
import Shapes from "../collections/Shapes";
import Attributes from "../collections/Attributes";
import Projects from "../collections/Projects";
import Canvases from "../collections/Canvases";
import Options from "../collections/Options";
import ThesaurusEntries from "../collections/ThesaurusEntries";
import Sessions from "../collections/Sessions";

Meteor.publish("messages", function () {
	if (!this.userId || !Meteor.user()?.accountID || !Meteor.user()?.projectID) {
		return this.ready();
	}

	return Messages.find({ projectID: Meteor.user()?.projectID });
});

Meteor.publish("messagesByShapeID", function (shapeID) {
	if (!this.userId || !Meteor.user()?.accountID || !Meteor.user()?.projectID) {
		return this.ready();
	}

	return Messages.find({ projectID: Meteor.user()?.projectID, shapeID });
});

Meteor.publish("messagesByCanvasID", function (canvasID) {
	if (!this.userId || !Meteor.user()?.accountID || !Meteor.user()?.projectID) {
		return this.ready();
	}

	const shapeIDs = Shapes.find(
		{ canvasID, type: "message" },
		{ fields: { _id: 1 } }
	)
		.fetch()
		.map((shape) => shape._id);

	return Messages.find(
		{ projectID: Meteor.user()?.projectID, shapeID: { $in: shapeIDs } },
		{ fields: { _id: 1, status: 1, canvasID: 1, projectID: 1, shapeID: 1 } }
	);
});

Meteor.methods({
	"messages.insert"(data) {
		basicAuth();
		return Messages.insert({ ...data, projectID: Meteor.user().projectID });
	},
	"messages.update"(id, newData) {
		basicAuth();
		return Messages.update(id, { $set: newData });
	},
	"messages.remove"(id) {
		basicAuth();
		return Messages.remove(id);
	},
	"messages.migrate"({ newShapeID, sqlMessageID, canvasID }) {
		const project = Projects.findOne({ _id: Meteor.user()?.projectID });
		let messageQuery = { sqlID: sqlMessageID };
		let attributeQuery = {};
		if (project.erxMigration) {
			messageQuery.erxMigration = true;
			attributeQuery.erxMigration = true;
		}
		const matchingMessage = Messages.findOne(messageQuery);
		Messages.update(matchingMessage._id, {
			$set: { shapeID: newShapeID, canvasID },
		});

		attributeQuery._id = matchingMessage.listenerAttributeID;
		const attribute = Attributes.findOne(attributeQuery);

		return Shapes.update(newShapeID, {
			$set: { "data.listenerAttribute": attribute.name },
		});
	},
	"messages.bulkStatusUpdate"({ canvasID, status }) {
		const messageShapes = Shapes.find({ type: "message", canvasID }).map(
			(shape) => shape._id
		);
		return Messages.update(
			{ shapeID: { $in: messageShapes } },
			{ $set: { status } },
			{ multi: true }
		);
	},
	"messages.namesFromIDs"(messageIDs) {
		let map = {};
		for (const messageID of messageIDs) {
			const message = Messages.findOne(
				{ _id: messageID },
				{ fields: { shapeID: 1, _id: 1 } }
			);
			const shape = Shapes.findOne(
				{ _id: message.shapeID },
				{ fields: { "data.label": 1 } }
			);
			map[messageID] = shape?.data?.label;
		}
		return map;
	},
});
