import { Meteor } from "meteor/meteor";
import Branchers from "../collections/Branchers";
import { basicAuth } from "../lib";
import BrancherAttributesToMigrate from "../collections/BrancherAttributesToMigrate";
import BrancherLines from "../collections/BrancherLines";
import BranchersToMigrate from "../collections/BranchersToMigrate";
import Shapes from "../collections/Shapes";
import BrancherRulesToMigrate from "../collections/BrancherRulesToMigtate";
import BrancherRules from "../collections/BrancherRules";
import BrancherRuleGroupsToMigrate from "../collections/BrancherRuleGroupsToMigrate";
import BrancherRuleGroups from "../collections/BrancherRuleGroups";
import Projects from "../collections/Projects";

Meteor.publish("branchers", function (_id) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return Branchers.find({ _id });
});

Meteor.publish("branchersByShapeID", function (shapeID) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return Branchers.find({ shapeID });
});

Meteor.methods({
	"branchers.insert"(obj) {
		basicAuth();
		Branchers.insert({ ...obj });
	},
	"branchers.update"(id, newData) {
		basicAuth();
		Branchers.update(id, { $set: newData });
	},
	"branchers.remove"(id) {
		basicAuth();
		Branchers.remove(id);
	},
	"branchers.migrate"({ newBrancherShapeID, oldShapeID, oldCanvasID }) {
		//sqlBrancherID, sqlCanvasID
		basicAuth();
		const oldBrancherAttributes = BrancherAttributesToMigrate.find({
			sqlBrancherID: oldShapeID,
			sqlCanvasID: oldCanvasID,
		});

		const newAttributeIDs = oldBrancherAttributes.map(
			(attribute) => attribute.attributeID
		);

		Branchers.insert({
			shapeID: newBrancherShapeID,
			attributes: newAttributeIDs,
			sqlBrancherShapeID: oldShapeID,
			sqlCanvasID: oldCanvasID,
		});
	},
	"branchers.migrateRules"({ newBrancherShapeID }) {
		const project = Projects.findOne({ _id: Meteor.user()?.projectID });

		const brancherShapeLines = Shapes.find({
			source: newBrancherShapeID,
		}).fetch();
		for (const brancherShapeLine of brancherShapeLines) {
			const targetShape = Shapes.findOne({ _id: brancherShapeLine.target });

			let brancherQuery = {
				sqlCanvasID: targetShape.sqlCanvasID,
				sqlTargetShapeID: targetShape.sqlShapeID,
			};
			if (project.erxMigration) {
				brancherQuery.erxMigration = true;
			}

			const sqlBrancher = BranchersToMigrate.findOne(brancherQuery);
			let ruleGroupsQuery = {
				sqlBrancherID: sqlBrancher.sqlID,
			};
			let ruleQuery = {
				sqlBrancherID: sqlBrancher.sqlID,
			};
			if (project.erxMigration) {
				ruleGroupsQuery.erxMigration = true;
				ruleQuery.erxMigration = true;
			}

			BrancherLines.insert({
				brancherID: newBrancherShapeID,
				target: targetShape._id,
				lineID: brancherShapeLine._id,
				chainedOperator: sqlBrancher.chainedOperator || "and",
				else: sqlBrancher.else || false,
			});

			const brancherRuleGroupsToMigrate =
				BrancherRuleGroupsToMigrate.find(ruleGroupsQuery).fetch();

			let newGroups = {};

			for (const brancherRuleGroupToMigrate of brancherRuleGroupsToMigrate) {
				newGroups[brancherRuleGroupToMigrate.sqlID] = BrancherRuleGroups.insert(
					{
						...brancherRuleGroupToMigrate,
						lineID: brancherShapeLine._id,
						erxMigration: true,
					}
				);
			}

			const brancherRulesToMigrate =
				BrancherRulesToMigrate.find(ruleQuery).fetch();

			for (const brancherRuleToMigrate of brancherRulesToMigrate) {
				BrancherRules.insert({
					...brancherRuleToMigrate,
					parentRuleGroup: brancherRuleToMigrate.sqlParentGroupID
						? newGroups[brancherRuleToMigrate.sqlParentGroupID]
						: null,
					lineID: brancherShapeLine._id,
					erxMigration: true,
				});
			}
		}
	},
	// "branchers.findByParent"(id) {
	// 	basicAuth();
	// 	return Branchers.findOne({ parent: id });
	// },
});
