import { Meteor } from "meteor/meteor";
import BrancherRuleGroups from "../collections/BrancherRuleGroups";
import { basicAuth } from "../lib";

Meteor.publish("brancherRuleGroups", function (lineID) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return BrancherRuleGroups.find({ lineID });
});

Meteor.methods({
	"brancherRuleGroups.insert"(obj) {
		basicAuth();
		return BrancherRuleGroups.insert({ ...obj });
	},
	"brancherRuleGroups.update"(id, newData) {
		basicAuth();
		BrancherRuleGroups.update(id, { $set: newData });
	},
	"brancherRuleGroups.remove"(id) {
		basicAuth();
		BrancherRuleGroups.remove(id);
	},
	// "brancherRuleGroups.findByParent"(id) {
	// 	basicAuth();
	// 	return BrancherRuleGroups.findOne({ parent: id });
	// },
});
