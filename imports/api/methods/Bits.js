import { Meteor } from "meteor/meteor";
import Bits from "../collections/Bits";
import { basicAuth } from "../lib";
import BitCases from "../collections/BitCases";
import { duplicateBitCase } from "./BitCases";
import Messages from "../collections/Messages";
import Elements from "../collections/Elements";

Meteor.publish("bits", function (elementID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return Bits.find({ elementID });
});

Meteor.methods({
	"bits.insert"(obj) {
		basicAuth();
		return Bits.insert({ ...obj });
	},
	"bits.update"(id, newData) {
		basicAuth();
		return Bits.update(id, { $set: newData });
	},
	"bits.remove"(id) {
		basicAuth();
		return Bits.update(id, { deletedAt: new Date() });
	},
	"bits.paste"({ elementID, copyBitID }) {
		return pasteBit(elementID, copyBitID);
	},
});

const pasteBit = async (destinationElementID, sourceBitID) => {
	const newBitID = Bits.insert({
		elementID: destinationElementID,
	});

	const existingCases = BitCases.find(
		{
			bitID: sourceBitID,
			deletedAt: null,
		},
		{ fields: { _id: 1 } }
	).fetch();

	let usedAttributes = [];

	for (const bitCase of existingCases) {
		await duplicateBitCase({
			bitCaseID: bitCase._id,
			newBitID,
			usedAttributes,
		});
	}

	usedAttributes = [...new Set(usedAttributes)];

	const sourceBit = Bits.findOne(
		{ _id: sourceBitID },
		{ fields: { elementID: 1 } }
	);
	const parentElement = Elements.findOne(
		{ _id: sourceBit.elementID },
		{ fields: { messageID: 1 } }
	);

	const sourceMessageID = parentElement.messageID;
	const destinationElement = Elements.findOne(
		{ _id: destinationElementID },
		{ fields: { messageID: 1 } }
	);
	const destinationMessageID = destinationElement.messageID;

	if (sourceMessageID !== destinationMessageID) {
		const destinationMessageAttributesDB = Messages.findOne(
			{ _id: destinationMessageID },
			{ fields: { attributes: 1 } }
		);

		const destinationMessageAttributes =
			destinationMessageAttributesDB.attributes;

		const attributesToCopy = usedAttributes.filter((att) => {
			return !destinationMessageAttributes.includes(att);
		});

		await Messages.update(destinationMessageID, {
			$set: {
				attributes: [
					...new Set([...destinationMessageAttributes, ...usedAttributes]),
				],
			},
		});
	}

	return newBitID;
};
