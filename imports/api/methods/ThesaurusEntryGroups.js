import { Meteor } from "meteor/meteor";
import ThesaurusEntryGroups from "../collections/ThesaurusEntryGroups";
import { basicAuth } from "../lib";

Meteor.publish("thesaurusEntryGroups", function () {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return ThesaurusEntryGroups.find({
		deletedAt: null,
		projectID: Meteor.user().projectID,
	});
});

Meteor.methods({
	"thesaurusEntryGroups.insert"(obj) {
		basicAuth();
		ThesaurusEntryGroups.insert({
			...obj,
			projectID: Meteor.user()?.projectID,
		});
	},
	"thesaurusEntryGroups.update"(id, newData) {
		basicAuth();
		ThesaurusEntryGroups.update(id, { $set: newData });
	},
	"thesaurusEntryGroups.remove"(id) {
		basicAuth();
		ThesaurusEntryGroups.update(id, { deletedAt: new Date() });
	},
});
