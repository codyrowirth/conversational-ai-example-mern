import { Meteor } from "meteor/meteor";
import AttributeGroups from "../collections/AttributeGroups";
import { basicAuth } from "../lib";

Meteor.publish("attributeGroups", function () {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return AttributeGroups.find({
		deletedAt: null,
		projectID: { $in: [Meteor.user().projectID, null] },
	});
});

Meteor.methods({
	"attributeGroups.insert"(obj) {
		basicAuth();
		AttributeGroups.insert({ ...obj, projectID: Meteor.user()?.projectID });
	},
	"attributeGroups.update"(id, newData) {
		basicAuth();
		AttributeGroups.update(id, { $set: newData });
	},
	"attributeGroups.remove"(id) {
		basicAuth();
		AttributeGroups.update(id, { deletedAt: new Date() });
	},
	"attributeGroups.findByParent"(id) {
		basicAuth();
		return AttributeGroups.findOne({ parent: id, deletedAt: null });
	},
	"attributeGroups.duplicateNameCheck"({ name, _id, parent }) {
		basicAuth();
		return AttributeGroups.findOne({
			name,
			deletedAt: null,
			projectID: Meteor.user()?.projectID,
			_id: { $ne: _id },
			parent,
		});
	},
	"attributeGroups.getAllIDs"() {
		basicAuth();
		return AttributeGroups.find(
			{ projectID: Meteor.user()?.projectID },
			{ fields: { _id: 1 } }
		)
			.fetch()
			.map((doc) => doc._id);
	},
});
