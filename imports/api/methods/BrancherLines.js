import { Meteor } from "meteor/meteor";
import BrancherLines from "../collections/BrancherLines";
import { basicAuth } from "../lib";

Meteor.publish("brancherLines", function (lineID) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return BrancherLines.find({ lineID });
});

Meteor.publish("brancherLinesArray", function (lineIDs) {
	if (!this.userId || !Meteor.user().accountID || !Meteor.user().projectID) {
		return this.ready();
	}
	return BrancherLines.find({ target: { $in: lineIDs } });
});

Meteor.methods({
	"brancherLines.insert"(obj) {
		basicAuth();
		BrancherLines.insert({ ...obj });
	},
	"brancherLines.update"(id, newData) {
		basicAuth();
		BrancherLines.update(id, { $set: newData });
	},
	"brancherLines.remove"(id) {
		basicAuth();
		BrancherLines.remove(id);
	},
	// "brancherLines.findByParent"(id) {
	// 	basicAuth();
	// 	return BrancherLines.findOne({ parent: id });
	// },
});
