import { Meteor } from "meteor/meteor";
import { overlordAuth } from "../lib";
import Accounts from "../collections/Accounts";

if (Meteor.isServer) {
	Meteor.publish("accounts", function () {
		overlordAuth();

		return Accounts.find({ deletedAt: null });
	});
}
Meteor.methods({
	"accounts.insert"(name) {
		overlordAuth();

		Accounts.insert({
			name,
		});
	},
	"accounts.update"(id, newData) {
		overlordAuth();

		Accounts.update(id, { $set: newData });
	},
	"accounts.remove"(id) {
		overlordAuth();

		Accounts.update(id, { deletedAt: new Date() });
	},
});
