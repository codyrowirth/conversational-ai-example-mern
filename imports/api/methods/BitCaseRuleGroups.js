import { Meteor } from "meteor/meteor";
import BitCaseRuleGroups from "../collections/BitCaseRuleGroups";
import { basicAuth } from "../lib";

Meteor.publish("bitCaseRuleGroups", function (bitCaseID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return BitCaseRuleGroups.find({ bitCaseID });
});

Meteor.methods({
	"bitCaseRuleGroups.insert"(obj) {
		basicAuth();
		return BitCaseRuleGroups.insert({ ...obj });
	},
	"bitCaseRuleGroups.update"(id, newData) {
		basicAuth();
		BitCaseRuleGroups.update(id, { $set: newData });
	},
	"bitCaseRuleGroups.remove"(id) {
		basicAuth();
		BitCaseRuleGroups.update(id, { deletedAt: new Date() });
	},
});
