import { Meteor } from "meteor/meteor";
import Sessions from "../collections/Sessions";
import { basicAuth } from "../lib";
import Messages from "../collections/Messages";
import Attributes from "../collections//Attributes";
import Options from "../collections//Options";
import ThesaurusEntries from "../collections//ThesaurusEntries";
import Shapes from "../collections//Shapes";
import Canvases from "../collections//Canvases";

Meteor.publish("sessions", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return Sessions.find({ userID: Meteor.user()._id });
});

Meteor.methods({
	"sessions.remove"() {
		basicAuth();
		return Sessions.remove({ userID: Meteor.user()._id });
	},
	"sessions.complete"() {
		basicAuth();
		const openSessions = Sessions.find({
			userID: Meteor.user()._id,
			sessionCompleted: false,
		});
		for (const openSession of openSessions) {
			Sessions.update(openSession._id, { $set: { sessionCompleted: true } });
		}
	},
	"sessions.unmatched"() {
		const sessions = Sessions.find(
			{
				"messages.unmatched": { $ne: false, $exists: true },
			},
			{
				fields: {
					messages: {
						$elemMatch: { unmatched: { $ne: false, $exists: true } },
					},
				},
			}
		).fetch();

		let unmatchedResponses = [];

		for (const session of sessions) {
			for (const sessionMessage of session.messages) {
				const message = Messages.findOne(
					{ _id: sessionMessage.unmatched },
					{ fields: { shapeID: 1, _id: 1, listenerAttributeID: 1 } }
				);

				let thesaurusEntries = [];

				if (message?.listenerAttributeID) {
					const attribute = Attributes.findOne({
						_id: message.listenerAttributeID,
					});
					if (attribute) {
						const options = Options.find({ attributeID: attribute._id });
						for (const option of options) {
							if (option.thesaurusEntryID) {
								const thesaurusEntry = ThesaurusEntries.findOne(
									{
										_id: option.thesaurusEntryID,
									},
									{ fields: { name: 1, _id: 1 } }
								);
								thesaurusEntries.push(thesaurusEntry);
							}
						}
					}
				}

				if (message) {
					message.thesaurusEntries = thesaurusEntries;

					message.shape = Shapes.findOne(
						{ _id: message.shapeID },
						{ fields: { "data.label": 1, canvasID: 1 } }
					);

					message.canvas = Canvases.findOne(
						{ _id: message.shape.canvasID },
						{ fields: { name: 1 } }
					);

					message.sessionMessage = sessionMessage;
					message.sessionID = session._id;

					unmatchedResponses.push(message);
				}
			}
		}

		return unmatchedResponses;
	},
	"sessions.setMatched"({ sessionID, messageID }) {
		return Sessions.update(
			{ _id: sessionID, "messages._id": messageID },
			{ $set: { "messages.$.unmatched": false } }
		);
	},
	"sessions.addUnmatchedToThesaurusEntry"({ thesaurusEntryID, synonym }) {
		ThesaurusEntries.update(thesaurusEntryID, {
			$push: { synonymList: synonym },
		});
	},
});
