import { Meteor } from "meteor/meteor";
import ShapesToMigrate from "../collections/ShapesToMigrate";
import { basicAuth } from "../lib";

Meteor.publish("shapesToMigrate", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return ShapesToMigrate.find({
		projectID: Meteor.user()?.projectID,
	});
});

Meteor.publish("shapesToMigrateByCanvasID", function (canvasID) {
	if (!this.userId || !Meteor.user()?.accountID || !Meteor.user()?.projectID) {
		return this.ready();
	}

	return ShapesToMigrate.find({
		canvasID,
		type: { $nin: ["extractor"] },
	});
});

Meteor.methods({
	"shapesToMigrate.insert"(obj) {
		basicAuth();
		ShapesToMigrate.insert({ ...obj, projectID: Meteor.user()?.projectID });
	},
	"shapesToMigrate.update"(id, newData) {
		basicAuth();
		ShapesToMigrate.update(id, { $set: newData });
	},
	"shapesToMigrate.remove"(id) {
		basicAuth();
		ShapesToMigrate.update(id, { deletedAt: new Date() });
	},
});
