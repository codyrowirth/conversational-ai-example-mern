import { Meteor } from "meteor/meteor";
import MessageTrackingAttributes from "../collections/MessageTrackingAttributes";
import { basicAuth } from "../lib";

Meteor.publish("messageTrackingAttributes", function (messageID) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}
	return MessageTrackingAttributes.find({
		messageID,
		deletedAt: null,
	});
});

Meteor.methods({
	"messageTrackingAttributes.insert"(obj) {
		basicAuth();
		MessageTrackingAttributes.insert({ ...obj });
	},
	"messageTrackingAttributes.update"(id, newData) {
		basicAuth();
		MessageTrackingAttributes.update(id, { $set: newData });
	},
	"messageTrackingAttributes.remove"(id) {
		basicAuth();
		MessageTrackingAttributes.update(id, { deletedAt: new Date() });
	},
});
