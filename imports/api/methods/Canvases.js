import { Meteor } from "meteor/meteor";
import Canvases from "../collections/Canvases";
import { basicAuth } from "../lib";
import CanvasGroups from "../collections/CanvasGroups";
import Shapes from "../collections/Shapes";

Meteor.publish("canvases", function () {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return Canvases.find({
		deletedAt: null,
		projectID: Meteor.user().projectID,
	});
});

Meteor.publish("canvasesByParent", function (id) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return Canvases.find({
		parent: id || null,
		deletedAt: null,
		projectID: Meteor.user().projectID,
	});
});

Meteor.publish("canvases.search", function (searchTerm) {
	if (!this.userId || !Meteor.user().accountID) {
		return this.ready();
	}

	return Canvases.find({
		name: { $regex: searchTerm, $options: "i" },
		deletedAt: null,
	});
});

Meteor.methods({
	"canvases.insert"(obj) {
		basicAuth();
		Canvases.insert({
			...obj,
			projectID: Meteor.user()?.projectID,
			updatedAt: new Date(),
		});
	},
	"canvases.update"(id, newData) {
		basicAuth();
		Canvases.update(id, { $set: { ...newData, updatedAt: new Date() } });
	},
	"canvases.remove"(id) {
		basicAuth();
		Canvases.update(id, { $set: { deletedAt: new Date() } });
	},
	"canvases.findOne"(id) {
		basicAuth();
		return Canvases.findOne({ deletedAt: null, id });
	},
	"canvases.findByParent"(id) {
		basicAuth();
		return Canvases.findOne({ parent: id, deletedAt: null });
	},
	"canvases.findParentTree"(id) {
		basicAuth();
		let groups = CanvasGroups.find()
			.fetch()
			.map((group) => ({
				id: group._id,
				name: group.name,
				parent: group.parent,
			}));
		groups.push({ id: null, name: "[[root]]", parent: null });

		let count = 0;
		let parentTree = [];
		let currentParentID = Canvases.findOne({ _id: id, deletedAt: null }).parent;
		let currentParent = groups.find((group) => group.id === currentParentID);
		parentTree.unshift(currentParent);
		while (currentParent.id !== null && count <= 20) {
			currentParent = groups.find((group) => group.id === currentParent.parent);
			parentTree.unshift(currentParent);
			count = count + 1;
		}
		return parentTree;
	},
	"canvases.duplicateNameCheck"({ name, _id }) {
		basicAuth();
		return Canvases.findOne({
			name,
			deletedAt: null,
			projectID: Meteor.user()?.projectID,
			_id: { $ne: _id },
		});
	},
	"canvases.count"() {
		basicAuth();
		return Canvases.find({
			projectID: Meteor.user()?.projectID,
		}).count();
	},
	"canvases.quickSearch"(query) {
		return Canvases.find(
			{
				name: {
					$regex: query,
					$options: "i",
				},
				projectID: Meteor.user().projectID,
			},
			{ limit: 25, fields: { name: 1, _id: 1 } }
		).fetch();
	},
});
