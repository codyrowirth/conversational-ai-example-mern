import { Mongo } from "meteor/mongo";

const MessageTrackingAttributes = new Mongo.Collection(
	"messageTrackingAttributes"
);

export default MessageTrackingAttributes;
