import { Mongo } from "meteor/mongo";

const Shapes = new Mongo.Collection("shapes");

if (Meteor.isServer) {
	Shapes.createIndex({ source: 1 }, { name: "shapes.source" });
	Shapes.createIndex(
		{ "data.label": 1, type: 1, _id: 1, canvasID: 1 },
		{ name: "shapes-portal-matcher" }
	);
	Shapes.createIndex({ canvasID: 1 }, { name: "shapes.canvasID" });
}

export default Shapes;
