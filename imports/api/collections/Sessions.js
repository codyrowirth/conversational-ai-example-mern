import { Mongo } from "meteor/mongo";

const Sessions = new Mongo.Collection("sessions");

if (Meteor.isServer) {
	Sessions.createIndex(
		{
			"messages.unmatched": 1,
		},
		{
			name: "messages.unmatched",
		}
	);
}

export default Sessions;
