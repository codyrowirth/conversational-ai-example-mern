import { Mongo } from "meteor/mongo";

const ThesaurusEntryGroups = new Mongo.Collection("thesaurusEntryGroups");

export default ThesaurusEntryGroups;
