import { Mongo } from "meteor/mongo";

const BitCaseRuleGroups = new Mongo.Collection("bitCaseRuleGroups");

export default BitCaseRuleGroups;
