import { Mongo } from "meteor/mongo";

const BranchersToMigrate = new Mongo.Collection("branchersToMigrate");

export default BranchersToMigrate;
