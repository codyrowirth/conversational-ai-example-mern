import { Mongo } from "meteor/mongo";

const BrancherRuleGroups = new Mongo.Collection("brancherRuleGroups");

export default BrancherRuleGroups;
