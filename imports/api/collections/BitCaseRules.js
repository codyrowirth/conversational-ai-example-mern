import { Mongo } from "meteor/mongo";

const BitCaseRules = new Mongo.Collection("bitCaseRules");

export default BitCaseRules;
