import { Mongo } from "meteor/mongo";

const Elements = new Mongo.Collection("elements");

export default Elements;