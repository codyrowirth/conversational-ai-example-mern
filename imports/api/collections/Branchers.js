import { Mongo } from "meteor/mongo";

const Branchers = new Mongo.Collection("branchers");

if (Meteor.isServer) {
	Branchers.createIndex(
		{ shapeID: 1, attributes: 1 },
		{ name: "shapes.source" }
	);
}

export default Branchers;
