import { Mongo } from "meteor/mongo";

const Shapes = new Mongo.Collection("shapesToMigrate");

export default Shapes;
