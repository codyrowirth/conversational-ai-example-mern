import { Mongo } from "meteor/mongo";

const BrancherAttributes = new Mongo.Collection("brancherAttributes");

export default BrancherAttributes;
