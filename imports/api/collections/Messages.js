import { Mongo } from "meteor/mongo";

const Messages = new Mongo.Collection("messages");

if (Meteor.isServer) {
	Messages.createIndex(
		{
			projectID: 1,
			shapeID: 1,
		},
		{
			name: "messages.projectIDandShapeID",
		}
	);
}

export default Messages;
