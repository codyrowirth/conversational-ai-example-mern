import { Mongo } from "meteor/mongo";

const CanvasGroups = new Mongo.Collection("canvasGroups");

export default CanvasGroups;
