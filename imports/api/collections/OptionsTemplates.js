import { Mongo } from "meteor/mongo";

const OptionsTemplates = new Mongo.Collection("optionsTemplates");

export default OptionsTemplates;
