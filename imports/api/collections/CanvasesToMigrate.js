import { Mongo } from "meteor/mongo";

const Canvases = new Mongo.Collection("canvasesToMigrate");

export default Canvases;
