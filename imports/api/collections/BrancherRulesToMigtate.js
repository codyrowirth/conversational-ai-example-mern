import { Mongo } from "meteor/mongo";

const BrancherRulesToMigrate = new Mongo.Collection("brancherRulesToMigrate");

export default BrancherRulesToMigrate;
