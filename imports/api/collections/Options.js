import { Mongo } from "meteor/mongo";

const Options = new Mongo.Collection("options");

if (Meteor.isServer) {
	Options.createIndex(
		{ attributeID: 1, deletedAt: 1 },
		{ name: "options.attributeIDandDeleted" }
	);
}

export default Options;
