import { Mongo } from "meteor/mongo";

const Canvases = new Mongo.Collection("canvases");

if (Meteor.isServer) {
	Canvases.createIndex({ projectID: 1 }, { name: "canvases.projectID" });
}

export default Canvases;
