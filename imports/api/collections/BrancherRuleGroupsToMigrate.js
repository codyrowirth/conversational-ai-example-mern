import { Mongo } from "meteor/mongo";

const BrancherRuleGroupsToMigrate = new Mongo.Collection(
	"brancherRuleGroupsToMigrate"
);

export default BrancherRuleGroupsToMigrate;
