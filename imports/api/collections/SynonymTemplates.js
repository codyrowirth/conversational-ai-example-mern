import { Mongo } from "meteor/mongo";

const SynonymTemplates = new Mongo.Collection("synonymTemplates");

export default SynonymTemplates;
