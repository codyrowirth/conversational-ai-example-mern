import { Mongo } from "meteor/mongo";

const ThesaurusEntries = new Mongo.Collection("thesaurusEntries");

export default ThesaurusEntries;
