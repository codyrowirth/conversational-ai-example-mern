import { Mongo } from "meteor/mongo";

const BrancherAttributesToMigrate = new Mongo.Collection(
	"brancherAttributesToMigrate"
);

export default BrancherAttributesToMigrate;
