import { Mongo } from "meteor/mongo";

const AttributeGroups = new Mongo.Collection("attributeGroups");

export default AttributeGroups;
