import { Mongo } from "meteor/mongo";

const Attributes = new Mongo.Collection("attributes");

export default Attributes;
