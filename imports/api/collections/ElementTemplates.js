import { Mongo } from "meteor/mongo";

const ElementTemplates = new Mongo.Collection("elementTemplates");

export default ElementTemplates;
