import { add, format } from "date-fns";
import dateFns from "date-fns-tz";
import fetch from "node-fetch";
import fs from "fs";
const { formatInTimeZone } = dateFns;

console.log(`Deploy started at: ${format(new Date(), "h:mm aaa")}`);

const main = async () => {
	let lastDeploy = "";
	try {
		const lastDeployCall = await fetch(`https://content.tuzagtcs.com/version`);
		const text = await lastDeployCall.text();
		if (text.match(/\d*\.\d*\.\d*/)) {
			lastDeploy = text;
		}

		fs.writeFileSync(
			"./imports/releaseNotes/lastDeploy.js",
			`module.exports = "${lastDeploy}"`
		);

		await fetch(
			`https://hooks.slack.com/services/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`,
			{
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({
					text: `Deploy has commenced. Refresh warning at approximately *${formatInTimeZone(
						add(new Date(), { minutes: 10 }),
						"America/New_York",
						"h:mm aaa"
					)} ET*.`,
				}),
			}
		);
	} catch (e) {
		console.error(e);
		//file probably doesn't exist
	}
};

main().then();
