import fs from "fs";
import path from "path";
import fetch from "node-fetch";
import { format } from "date-fns";

const main = async () => {
	console.log(`Getting ready to send release notes to Slack...`);

	let lastDeploy = "";
	const lastDeployPath = path.resolve("./imports/releaseNotes/lastDeploy.js");
	try {
		lastDeploy = await import(lastDeployPath);
	} catch (e) {
		console.error(e);
	}

	const packageString = fs.readFileSync("./package.json");
	const packageObj = JSON.parse(packageString);
	const packageVersion = packageObj.version;

	if (lastDeploy !== packageVersion) {
		try {
			fs.unlinkSync(lastDeployPath);
		} catch (e) {
			//ignore, file probably already deleted
		}

		try {
			const releaseNotesContent = fs.readFileSync(
				path.resolve(`./imports/releaseNotes/${packageVersion}.md`),
				"utf8"
			);

			if (releaseNotesContent) {
				await fetch(
					`https://hooks.slack.com/services/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`,
					{
						method: "POST",
						headers: { "Content-Type": "application/json" },
						body: JSON.stringify({
							text: `*New in TCS \`${packageVersion}\`:*\n${releaseNotesContent}`,
						}),
					}
				);

				console.log(
					`Release notes sent for v. ${packageVersion}. There's no going back now...`
				);
				console.log(`Deploy finished at: ${format(new Date(), "h:mm aaa")}`);
			}
		} catch (e) {
			console.error(`Error posting release notes to slack. Details below:`);
			console.error(e);
		}
	}
};

main().then();
