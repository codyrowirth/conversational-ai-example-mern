const successImgs = [
	"https://emojis.slackmojis.com/emojis/images/1597320283/10003/catjam.gif?1597320283",
	"https://emojis.slackmojis.com/emojis/images/1547582922/5197/party_blob.gif?1547582922",
	"https://emojis.slackmojis.com/emojis/images/1539890226/4845/rickroll.gif?1539890226",
	"https://emojis.slackmojis.com/emojis/images/1520808873/3643/cool-doge.gif?1520808873",
	"https://emojis.slackmojis.com/emojis/images/1577982316/7421/typingcat.gif?1577982316",
	"https://emojis.slackmojis.com/emojis/images/1497901371/2453/alert.gif?1497901371",
	"https://emojis.slackmojis.com/emojis/images/1558099591/5711/ahhhhhhhhh.gif?1558099591",
	"https://emojis.slackmojis.com/emojis/images/1580448086/7667/think-about-it.png?1580448086",
	"https://emojis.slackmojis.com/emojis/images/1488512507/1804/aaw_yeah.gif?1488512507",
	"https://emojis.slackmojis.com/emojis/images/1563480763/5999/meow_party.gif?1563480763",
	"https://emojis.slackmojis.com/emojis/images/1605828870/11400/among-us-party.gif?1605828870",
	"https://emojis.slackmojis.com/emojis/images/1613285697/12806/meow_attention.png?1613285697",
	"https://emojis.slackmojis.com/emojis/images/1596061862/9845/meow_heart.png?1596061862",
	"https://emojis.slackmojis.com/emojis/images/1563481443/6027/meow_wow.png?1563481443",
	"https://emojis.slackmojis.com/emojis/images/1450694616/220/bananadance.gif?1450694616",
	"https://emojis.slackmojis.com/emojis/images/1464135017/461/fb-wow.gif?1464135017",
	"https://emojis.slackmojis.com/emojis/images/1584725500/8268/blob-hype.gif?1584725500",
	"https://emojis.slackmojis.com/emojis/images/1465999900/516/success.png?1465999900",
	"https://emojis.slackmojis.com/emojis/images/1471119455/978/conga_parrot.gif?1471119455",
	"https://emojis.slackmojis.com/emojis/images/1578740447/7500/partyparrot.gif?1578740447",
	"https://emojis.slackmojis.com/emojis/images/1575297777/7233/baby-yoda.png?1575297777",
	"https://emojis.slackmojis.com/emojis/images/1490885301/1973/mario_luigi_dance.gif?1490885301",
	"https://emojis.slackmojis.com/emojis/images/1554740062/5584/deployparrot.gif?1554740062",
];

export default successImgs;
