export const plainTextSerialize = (nodes) => {
	return nodes.map((n) => Node.string(n)).join("");
};
