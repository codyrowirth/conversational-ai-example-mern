import { Meteor } from "meteor/meteor";

const baseURL =
	Meteor.settings.public.ENV === "PROD"
		? `https://v4.api.tuzagtcs.com`
		: `http://localhost:3002`;

const ContentAPI = async (endpoint, method, data = {}) => {
	let options = {};

	if (data) {
		data.includeFormatting = true;
	}

	if (method !== "GET") {
		options.body = JSON.stringify(data);
	}
	const rawResult = await fetch(`${baseURL}${endpoint}`, {
		...options,
		method,
		headers: {
			"Content-Type": "application/json",
		},
	});

	const result = await rawResult.json();

	if (result.error) {
		alert(result.error);
		return result;
	} else {
		return result;
	}
};

export default ContentAPI;
