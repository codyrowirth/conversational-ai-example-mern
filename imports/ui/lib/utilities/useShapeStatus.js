const useShapeStatus = (status, data) => {
	let color = "",
		icon = "",
		iconColor = "";

	if (status === "backlog") {
		color = "#c6c6c6";
		icon = "hourglass-start";
		iconColor = "#2f3d47";
	} else if (status === "to do") {
		color = "#ffffff";
		icon = "tasks";
		iconColor = "#2f3d47";
	} else if (status === "doing") {
		color = "#ef5f2f";
		icon = "tools";
		iconColor = "#ffffff";
	} else if (status === "done") {
		color = "#7faf41";
		icon = "check";
		iconColor = "#2f3d47";
	} else if (status === "blocked") {
		color = "#d1383f";
		icon = "ban";
		iconColor = "#ffffff";
	} else if (status === "testing") {
		color = "#ef5f2f";
		icon = "flask";
		iconColor = "#ffffff";
	} else if (status === "approved") {
		color = "#7faf41";
		icon = "check-double";
		iconColor = "#2f3d47";
	} else if (status === "error") {
		color = "#d1383f";
		icon = "exclamation";
		iconColor = "#ffffff";
	} else if (status === "collab") {
		color = "#f2a524";
		icon = "user";
		iconColor = "#2f3d47";
	} else {
		color = "#16b1f0";
		icon = "circle";
		iconColor = "#16b1f0";
	}

	if (data === "color") {
		return color;
	} else if (data === "icon") {
		return icon;
	} else if (data === "iconColor") {
		return iconColor;
	}
};

export default useShapeStatus;
