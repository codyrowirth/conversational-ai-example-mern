const caseInsensitiveDedupe = (options) => {
	let dedupedOptions = [];
	let lowerTracker = [];
	for (const option of options) {
		//custom dedup-er to make it case INsensitive
		if (!lowerTracker.includes(option.toLowerCase())) {
			dedupedOptions.push(option);
			lowerTracker.push(option.toLowerCase());
		}
	}
	return dedupedOptions;
};

export default caseInsensitiveDedupe;
