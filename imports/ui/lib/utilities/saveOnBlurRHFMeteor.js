export const saveOnBlur = (model, id, evt, canvasID) => {
	Meteor.call(`${model}.update`, id, {
		[evt.target.name]: evt.target.value,
		updatedAt: new Date(),
	});
	if (canvasID) {
		Meteor.call("canvases.update", canvasID, {});
	}
};

export const saveOnChange = (model, id, selected, field, canvasID) => {
	//you MUST used a controlled component for this to work, usually used for selects
	Meteor.call(`${model}.update`, id, {
		[field.name]: selected?.value || selected,
		updatedAt: new Date(),
	});
	if (canvasID) {
		Meteor.call("canvases.update", canvasID, {});
	}
};
