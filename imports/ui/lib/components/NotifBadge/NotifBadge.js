import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./NotifBadge.scss";

const NotifBadge = (props) => {
	const { type = "count", count = "", small, alt } = props;

	return (
		<div
			className={`notif-badge ${small === true ? "small" : ""} ${
				alt === true ? "alt" : ""
			}`}
		>
			<div className="notif-badge-inner flex">
				{type === "collab" ? <FontAwesomeIcon icon="user" /> : count}
			</div>
		</div>
	);
};

export default NotifBadge;
