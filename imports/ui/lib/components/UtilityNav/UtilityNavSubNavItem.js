import React from "react";
import { NavLink } from "react-router-dom";
import NotifBadge from "../NotifBadge/NotifBadge";
import "./UtilityNavSubNavItem.scss";
import { Meteor } from "meteor/meteor";

const UtilityNavSubNavItem = ({
	subNavItem,
	setUtilitySubNavExpanded,
	mobileNavExpanded,
	setMobileNavExpanded,
}) => {
	return (
		<div
			className={`sub-nav-item ${
				"notifications" in subNavItem ? "notif-active" : ""
			}`}
		>
			<NavLink
				exact
				className="link"
				to={subNavItem.route}
				onClick={() => {
					if (mobileNavExpanded === true) {
						setUtilitySubNavExpanded(false);
						setMobileNavExpanded(false);
					}
				}}
			>
				<div className="link-inner">
					<div className="text">
						<span className="text-inner">{subNavItem.text}</span>
						{"notifications" in subNavItem && (
							<NotifBadge count={subNavItem.notifications} small={true} />
						)}
					</div>
				</div>
			</NavLink>
		</div>
	);
};

export const UtilityNavSubNavProjectItem = (props) => {
	const { subNavItem, setUtilitySubNavExpanded } = props;
	const switchProject = () => {
		Meteor.call("user.setActiveProject", subNavItem._id);
		setUtilitySubNavExpanded(false);
	};

	return (
		<div
			className={`sub-nav-item ${
				"notifications" in subNavItem ? "notif-active" : ""
			}`}
		>
			<a className="action-link" onClick={switchProject}>
				<div className="link-inner">
					<div className="text">
						<span className="text-inner">{subNavItem.name}</span>
						{"notifications" in subNavItem && (
							<NotifBadge count={subNavItem.notifications} small={true} />
						)}
					</div>
				</div>
			</a>
		</div>
	);
};

export default UtilityNavSubNavItem;
