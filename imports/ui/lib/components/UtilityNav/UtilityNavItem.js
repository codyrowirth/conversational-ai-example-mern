import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { UnmountClosed as Collapse } from "react-collapse";
import UtilityNavSubNavItem, {
	UtilityNavSubNavProjectItem,
} from "./UtilityNavSubNavItem";
import NotifBadge from "../NotifBadge/NotifBadge";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./UtilityNavItem.scss";
import { useTracker } from "meteor/react-meteor-data";
import Projects from "../../../../api/collections/Projects";
import { Meteor } from "meteor/meteor";

const UtilityNavItem = ({
	navItem,
	utilityNavOpenID,
	setUtilityNavOpenID,
	mobileNavExpanded,
	setMobileNavExpanded,
}) => {
	const [utilitySubNavExpanded, setUtilitySubNavExpanded] = useState(false);
	const { projects, activeProject } = useTracker(() => {
		if (navItem.id === "project-switcher") {
			const handler = Meteor.subscribe("projects", Meteor.user()?.accountID);

			if (!handler.ready()) {
				return { projects: [] };
			}

			return {
				projects: Projects.find({}).fetch(),
				activeProject: Projects.findOne({ _id: Meteor.user().projectID }),
			};
		} else {
			return { projects: [] };
		}
	});

	useEffect(() => {
		if (!("route" in navItem)) {
			if (utilityNavOpenID === navItem.id && utilitySubNavExpanded === true) {
				setUtilitySubNavExpanded(false);
			} else if (utilityNavOpenID !== navItem.id) {
				setUtilitySubNavExpanded(false);
			} else {
				setUtilitySubNavExpanded(true);
			}
		}
	}, [utilityNavOpenID]);

	return (
		<div className="item">
			{"route" in navItem ? (
				<NavLink
					exact
					to={navItem.route}
					onClick={() => {
						if (mobileNavExpanded === true) {
							setMobileNavExpanded(false);
						}
					}}
				>
					<div className="item-inner flex">
						<div className="text-box">
							<div className="text-box-inner flex">
								<div
									className={`text ${
										"notifications" in navItem ? "notif-active" : ""
									}`}
								>
									<span>{navItem.text}</span>
									{"notifications" in navItem && (
										<NotifBadge count={navItem.notifications} />
									)}
								</div>
							</div>
						</div>
					</div>
				</NavLink>
			) : (
				<div className="item-inner">
					<button
						className={`sub-nav-trigger ${
							utilitySubNavExpanded ? "expanded" : ""
						} ${"notifications" in navItem ? "notif-active" : ""}`}
						data-id={navItem.id}
						onClick={() => {
							if (
								utilityNavOpenID === navItem.id &&
								utilitySubNavExpanded === true
							) {
								setUtilitySubNavExpanded(false);
							} else if (
								utilityNavOpenID === navItem.id &&
								utilitySubNavExpanded === false
							) {
								setUtilitySubNavExpanded(true);
							}

							setUtilityNavOpenID(navItem.id);
						}}
					>
						<div className="text-box">
							<div className="text-box-inner flex">
								<div className="text">
									<span className="no-wrap">
										<span className="text-inner">
											{navItem.id === "project-switcher" ? (
												// TO DO: Get active project name and load into the following <span>
												<span className="active-project">
													{activeProject?.name
														? activeProject.name
														: "[Select Project]"}
												</span>
											) : navItem.id === "account" ? (
												<div>
													{navItem.text},{" "}
													{/* TO DO: Get user's first name and load into the following <span> */}
													<span className="user-name">Lorem</span>
												</div>
											) : (
												navItem.text
											)}
										</span>
										{"notifications" in navItem && (
											<NotifBadge count={navItem.notifications} />
										)}
									</span>
								</div>
								<div className="arrow">
									<FontAwesomeIcon
										icon={utilitySubNavExpanded ? "chevron-up" : "chevron-down"}
									/>
								</div>
							</div>
						</div>
					</button>
					<Collapse
						theme={{
							collapse: "ReactCollapse--collapse",
							content: "ReactCollapse--content sub-nav",
						}}
						isOpened={utilitySubNavExpanded}
					>
						{navItem.children
							? Object.values(navItem.children).map((subNavItem) => (
									<UtilityNavSubNavItem
										key={subNavItem.id}
										subNavItem={subNavItem}
										navItem={navItem}
										setUtilitySubNavExpanded={setUtilitySubNavExpanded}
										mobileNavExpanded={mobileNavExpanded}
										setMobileNavExpanded={setMobileNavExpanded}
									/>
							  ))
							: projects.map((subNavItem) => (
									<UtilityNavSubNavProjectItem
										key={subNavItem._id}
										subNavItem={subNavItem}
										navItem={navItem}
										setUtilitySubNavExpanded={setUtilitySubNavExpanded}
										mobileNavExpanded={mobileNavExpanded}
										setMobileNavExpanded={setMobileNavExpanded}
									/>
							  ))}
					</Collapse>
				</div>
			)}
		</div>
	);
};

export default UtilityNavItem;
