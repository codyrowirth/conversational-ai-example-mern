import React, { useState } from "react";
import UtilityNavItem from "./UtilityNavItem";
import "./UtilityNav.scss";

const navItems = {
	search: {
		id: "search",
		text: "Search",
		route: "/search",
	},
	help: {
		id: "help",
		text: "Help",
		children: {
			tutorials: {
				id: "tutorials",
				text: "Tutorials",
				route: "/tutorials",
			},
			documentation: {
				id: "documentation",
				text: "Documentation",
				route: "/documentation",
			},
			releaseNotes: {
				id: "release-notes",
				text: "Release Notes",
				route: "/release-notes",
			},
		},
	},
	projectSwitcher: {
		id: "project-switcher",
		text: "Projects",
	},
	account: {
		id: "account",
		text: "Hello",
		// notifications: 5,
		children: {
			alerts: {
				id: "alerts",
				text: "Alerts",
				route: "/alerts",
				notifications: 2,
			},
			tasks: {
				id: "tasks",
				text: "Tasks",
				route: "/tasks",
				notifications: 3,
			},
			profile: {
				id: "profile",
				text: "Profile",
				route: "/profile",
			},
			logout: {
				id: "logout",
				text: "Logout",
				route: "/logout",
			},
		},
	},
};

const UtilityNav = ({
	mobileNavExpanded = null,
	setMobileNavExpanded = null,
}) => {
	const [utilityNavOpenID, setUtilityNavOpenID] = useState("");

	return (
		<nav className="utility-nav">
			<div className="utility-nav-inner flex">
				{Object.values(navItems).map((navItem) => (
					<UtilityNavItem
						key={navItem.id}
						navItem={navItem}
						utilityNavOpenID={utilityNavOpenID}
						setUtilityNavOpenID={setUtilityNavOpenID}
						mobileNavExpanded={mobileNavExpanded}
						setMobileNavExpanded={setMobileNavExpanded}
					/>
				))}
			</div>
		</nav>
	);
};

export default UtilityNav;
