import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";

import InputControlRHF from "../Form/InputControlRHF";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./SearchBar.scss";

const SearchBar = ({
	placeholder,
	classes = "",
	searchBarText,
	setSearchBarText,
}) => {
	const [searchFocus, setSearchFocus] = useState(false);

	const {
		control,
		formState: { errors },
	} = useForm();

	const handleChange = (event) => {
		setSearchBarText(event.target.value);
	};

	return (
		<section
			className={`search-bar ${classes} ${searchFocus === true ? "focus" : ""}`}
		>
			<div className="flex">
				<div className="icon-box">
					<FontAwesomeIcon icon="search" />
				</div>
				<div className="search-form">
					<form>
						<Controller
							name="search"
							control={control}
							defaultValue=""
							render={({ field, field: { onChange } }) => {
								return (
									<InputControlRHF
										type="search"
										name={field.name}
										placeholder={placeholder}
										{...field}
										onFocus={() => {
											setSearchFocus(true);
										}}
										onBlur={() => {
											setSearchFocus(false);
										}}
										onChange={(event) => {
											onChange(event);
											handleChange(event);
										}}
										onKeyDown={(event) => {
											if (event.code === "Enter") {
												event.preventDefault();
											}
										}}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</form>
				</div>
			</div>
		</section>
	);
};

export default SearchBar;
