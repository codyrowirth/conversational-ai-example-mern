import React, { useEffect, useState } from "react";
import { PropagateLoader } from "react-spinners";
import useWinWidth from "../../utilities/useWinWidth";
import DataTableGroup from "./DataTableGroup";
import EditGroupModal from "./EditGroupModal";
import DeleteGroupModal from "./DeleteGroupModal";
import EditRowModal from "./EditRowModal";
import DeleteRowModal from "./DeleteRowModal";
import "../../styles/tables.scss";
import { titleCase } from "title-case";
import { useTracker } from "meteor/react-meteor-data";
import CanvasGroupsDB from "../../../../api/collections/CanvasGroups";
import CanvasesDB from "../../../../api/collections/Canvases";
import pluralize from "pluralize";
import DataTableItemRow from "./DataTableItemRow";
import { useDebounce } from "use-debounce";
import AttributeGroupsDB from "../../../../api/collections/AttributeGroups";
import AttributesDB from "../../../../api/collections/Attributes";
import DataSearch from "./DataSearch";
import ThesaurusEntryGroupsDB from "../../../../api/collections/ThesaurusEntryGroups";
import ThesaurusEntriesDB from "../../../../api/collections/ThesaurusEntries";

import Papa from "papaparse";
import downloadjs from "downloadjs";
import { useSnackbar } from "notistack";

const DataTable = ({
	type,
	toolbar,
	searchBarText,
	setSearchBarText,
	...props
}) => {
	const [editGroupModal, setEditGroupModal] = useState();
	const [deleteGroupModal, setDeleteGroupModal] = useState();
	const [editRowModal, setEditRowModal] = useState();
	const [deleteRowModal, setDeleteRowModal] = useState();
	const [all, setAll] = useState(false);
	const [value] = useDebounce(searchBarText, 1000);
	const [totalItems, setTotalItems] = useState();

	const { downloading, setDownloading, enqueueSnackbar, closeSnackbar } = props;

	const screenWidth = useWinWidth();
	let modelGroups, modelGroupsDB, modelItems, modelItemsDB;
	modelGroups = `${type}Groups`;
	modelItems = pluralize(type);

	if (type === "canvas") {
		modelGroupsDB = CanvasGroupsDB;
		modelItemsDB = CanvasesDB;
	} else if (type === "attribute") {
		modelGroupsDB = AttributeGroupsDB;
		modelItemsDB = AttributesDB;
	} else if (type === "thesaurusEntry") {
		modelGroupsDB = ThesaurusEntryGroupsDB;
		modelItemsDB = ThesaurusEntriesDB;
	}

	let { groups, items, isLoading } = useTracker(() => {
		//we need to resubscribe when the projectID changes because the publication has changed
		const groupsSub = Meteor.subscribe(modelGroups, Meteor.user()?.projectID);
		const itemsSub = Meteor.subscribe(
			`${modelItems}ByParent`,
			null,
			Meteor.user()?.projectID
		);

		if (!groupsSub.ready() || !itemsSub.ready()) {
			return { isLoading: true, groups: null, items: null };
		}
		let groups, items;
		if (searchBarText) {
			items = modelItemsDB
				.find({
					name: {
						$regex: searchBarText,
						$options: "i",
					},
					projectID: { $in: [null, Meteor.user()?.projectID] },
				})
				.fetch();
			groups = [];
		} else {
			//when switching projects, sometimes the old project attributes hang around, so filter them out in the query
			items = modelItemsDB
				.find({
					parent: null,
					projectID: { $in: [null, Meteor.user()?.projectID] },
				})
				.fetch();
			groups = modelGroupsDB
				.find({
					parent: null,
					projectID: { $in: [null, Meteor.user()?.projectID] },
				})
				.fetch();
		}
		return { groups, items, isLoading: false };
	});

	useEffect(() => {
		let subscription;
		if (searchBarText) {
			isLoading = true;
			subscription = Meteor.subscribe(`${pluralize(type)}.search`, value);
		}

		return () => {
			if (subscription) {
				subscription.stop();
			}
		};
	}, [value]);

	useEffect(() => {
		Meteor.call(`${modelItems}.count`, (err, count) => {
			setTotalItems(count);
		});

		if (!localStorage.getItem(`${type}ExpandedGroups`)) {
			localStorage.setItem(`${type}ExpandedGroups`, "");
		}

		if (localStorage.getItem(`last${type}clicked`)) {
			let maxRunCounter = 0;
			//this is a little hacky, but we have to wait for all the subgroups to open and load before we can scroll it into
			//view, so we give that two seconds to happen, and if it doesn't we give up
			const scrollToElement = setInterval(() => {
				maxRunCounter++;
				const matchingElement = document.getElementById(
					localStorage.getItem(`last${type}clicked`)
				);
				if (matchingElement) {
					const tableHeadingH =
						matchingElement.closest(".table-content").previousSibling
							.offsetHeight;

					const eleScrollParent = matchingElement.closest(".table-scroll-wrap");
					const eleTop = matchingElement.offsetTop;

					eleScrollParent.scroll({
						top: eleTop - tableHeadingH,
						behavior: "smooth",
					});

					clearInterval(scrollToElement);
				}
				if (maxRunCounter > 20) {
					clearInterval(scrollToElement);
				}
			}, 100);
		}
	}, []);

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	let grCol1W = "75%";

	if (type === "message") {
		grCol1W = "22px";
	} else if (type === "attribute") {
		grCol1W = "45%";
	} else if (toolbar === "open") {
		grCol1W = "100%";
	} else if (type === "canvas") {
		grCol1W = "70%";
	}

	let grCol1Head = titleCase(type);

	if (isLoading) {
		grCol1Head = "";
	} else if (!isLoading && type === "message") {
		grCol1Head = "TO DO";
	}

	if (type === "thesaurusEntry") {
		grCol1Head = "Entry";
	}

	let grCol3W = "25%";

	if (type === "message") {
		grCol3W = "100%";
	} else if (type === "canvas" || type === "attribute") {
		grCol3W = "30%";
	}

	let grCol3Head = "Actions";

	if (isLoading) {
		grCol3Head = "";
	} else if (!isLoading && type === "message") {
		grCol3Head = titleCase(type);
	}

	let col1W = "75%";

	if (type === "message") {
		col1W = "22px";
	} else if (type === "attribute") {
		col1W = "45%";
	} else if (toolbar === "open") {
		col1W = "100%";
	} else if (type === "canvas") {
		col1W = "70%";
	}

	let col3W = "25%";

	if (type === "message") {
		col3W = "100%";
	} else if (type === "canvas" || type === "attribute") {
		col3W = "30%";
	}

	return (
		<>
			{type === "attribute" && props.parentType !== "message" && (
				<div className="table-action-links-wrap">
					<div className="table-action-links-row top">
						<div className="table-action-links right">
							<span className="label">CSV</span>
							{/* <button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									alert("TO DO");
								}}
							>
								Import
							</button> */}
							<button
								className="action-link"
								onClick={async () => {
									setDownloading(true);
									enqueueSnackbar(
										"Export started. Please wait, this may take a minute...",
										{
											variant: "info",
											key: "exportStarted",
											persist: true,
										}
									);

									const { data } = await Meteor.callPromise(
										"attributes.export"
									);
									const csv = Papa.unparse(data);
									downloadjs(csv, "export.csv", "text/csv");
									setDownloading(false);
									closeSnackbar("exportStarted");
									enqueueSnackbar(
										"Export complete. Please check your downloads.",
										{
											variant: "success",
											key: "exportFinished",
										}
									);
								}}
								disabled={downloading}
							>
								{downloading ? "Exporting..." : "Export"}
							</button>
						</div>
					</div>
				</div>
			)}
			<DataSearch
				type={type}
				searchBarText={searchBarText}
				setSearchBarText={setSearchBarText}
			/>
			<div className="table-scroll-wrap">
				<table className="groups items">
					{screenWidth >= 768 && (
						<colgroup>
							<col style={{ width: grCol1W }} />
							{type === "attribute" && <col style={{ width: "25%" }} />}
							{toolbar !== "open" && <col style={{ width: grCol3W }} />}
						</colgroup>
					)}
					<thead className="table-heading">
						<tr>
							<th scope="col">{grCol1Head}</th>
							{type === "attribute" && (
								<th scope="col">{isLoading ? "" : "Type"}</th>
							)}
							{toolbar !== "open" && <th scope="col">{grCol3Head}</th>}
						</tr>
					</thead>
					<tbody className="table-content">
						{groups.map((group) => (
							<DataTableGroup
								group={group}
								type={type}
								toolbar={toolbar}
								key={group._id}
								depth={0}
								setEditGroupModal={setEditGroupModal}
								setDeleteGroupModal={setDeleteGroupModal}
								setEditRowModal={setEditRowModal}
								setDeleteRowModal={setDeleteRowModal}
								all={all}
								setAll={setAll}
								{...props}
							/>
						))}
						{items.length > 0 && (
							<tr>
								<td
									colSpan={type === "attribute" ? 3 : 2}
									style={{ padding: 0 }}
								>
									<table>
										{screenWidth >= 768 && (
											<colgroup>
												<col style={{ width: col1W }} />
												{type === "attribute" && (
													<col style={{ width: "25%" }} />
												)}
												{toolbar !== "open" && <col style={{ width: col3W }} />}
											</colgroup>
										)}

										<tbody>
											{items.map((item) => (
												<DataTableItemRow
													{...props}
													key={item._id}
													item={item}
													setEditRowModal={setEditRowModal}
													setDeleteRowModal={setDeleteRowModal}
													depth={0}
													type={type}
													toolbar={toolbar}
												/>
											))}
										</tbody>
									</table>
								</td>
							</tr>
						)}
					</tbody>
				</table>
			</div>
			<div className="table-action-links-wrap">
				<div className="table-action-links-row bottom">
					<div className="table-action-links left">
						{totalItems && totalItems < 1000 ? (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setAll("expand");

									Meteor.call(`${modelGroups}.getAllIDs`, (err, ids) => {
										localStorage.setItem(
											`${type}ExpandedGroups`,
											ids.join(",")
										);
									});
								}}
							>
								Expand All
							</button>
						) : (
							""
						)}
						{totalItems && totalItems > 0 ? (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setAll("collapse");
									localStorage.setItem(`${type}ExpandedGroups`, "");
								}}
							>
								Collapse All
							</button>
						) : (
							""
						)}
					</div>
				</div>
			</div>
			<EditGroupModal
				item={editGroupModal}
				hide={() => {
					setEditGroupModal(null);
				}}
				model={modelItems}
				type={type}
			/>
			<DeleteGroupModal
				item={deleteGroupModal}
				hide={() => {
					setDeleteGroupModal(null);
				}}
				model={modelItems}
			/>
			<EditRowModal
				type={type}
				item={editRowModal}
				hide={() => {
					setEditRowModal(null);
				}}
				model={modelItems}
			/>
			<DeleteRowModal
				item={deleteRowModal}
				hide={() => {
					setDeleteRowModal(null);
				}}
				model={modelItems}
			/>
		</>
	);
};

export default DataTable;
