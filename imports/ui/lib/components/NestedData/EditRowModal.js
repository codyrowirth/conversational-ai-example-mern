import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useTracker } from "meteor/react-meteor-data";
import pluralize from "pluralize";

import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import InputRHF from "../Form/InputRHF";
import SelectRHF from "../Form/SelectRHF";
import CanvasGroups from "../../../../api/collections/CanvasGroups";
import { PropagateLoader } from "react-spinners";
import AttributeGroups from "../../../../api/collections/AttributeGroups";
import { Meteor } from "meteor/meteor";
import ThesaurusEntryGroups from "../../../../api/collections/ThesaurusEntryGroups";

const EditRowModal = ({ type, item, hide }) => {
	let model, groups;

	if (type === "canvas") {
		model = CanvasGroups;
	} else if (type === "attribute") {
		model = AttributeGroups;
	} else if (type === "thesaurusEntry") {
		model = ThesaurusEntryGroups;
	} else {
		return (
			<Modal
				className="modal limit-modal end-buttons"
				show={!!item}
				hide={hide}
			>
				<h3>
					Error. This edit modal has not been created for this type of data.
				</h3>
			</Modal>
		);
	}

	groups = useTracker(() => {
		if (!model) {
			return null;
		}
		return model.find({ projectID: Meteor.user().projectID }).fetch();
	});

	return (
		<Modal className="modal limit-modal end-buttons" show={!!item} hide={hide}>
			<h3>
				Edit{" "}
				<span className="cap">
					{type === "thesaurusEntry" ? "Thesaurus Entry" : type}
				</span>
			</h3>

			{groups === null ? (
				<div style={{ textAlign: "center", paddingBottom: 20 }}>
					<PropagateLoader color="#2f3d47" />
				</div>
			) : (
				<EditNestedDataItemForm
					item={item}
					groups={groups}
					type={type}
					hide={hide}
				/>
			)}
		</Modal>
	);
};

export const EditNestedDataItemForm = ({ item, groups, type, hide, group }) => {
	const schema = yup.object().shape({
		itemName: yup.string().required(`Please enter a ${type} name`),
		group: yup.object().required("Please select a group"),
	});

	const initialGroup = groups.find((group) => group._id === item.parent);

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
		setError,
	} = useForm({
		resolver: yupResolver(schema),
		defaultValues: {
			group: initialGroup
				? { value: initialGroup._id, label: initialGroup.name }
				: { value: null, label: "[[root]]" },
		},
	});

	const onSubmit = (data) => {
		if (!data.itemName) {
			setError("itemName", "Name is required");
		} else if (!data.group) {
			setError("group", "Group is required");
		} else {
			let modelName;
			if (group) {
				modelName = `${type}Groups`;
			} else {
				modelName = pluralize(type);
			}
			Meteor.call(
				`${modelName}.duplicateNameCheck`,
				{
					name: data.itemName,
					_id: item._id,
					parent: data.group.value,
				},
				(err, duplicate) => {
					if (duplicate) {
						setError("itemName", {
							type: "string",
							message: "This name already exists. Please use a different one.",
						});
					} else {
						Meteor.call(`${modelName}.update`, item._id, {
							name: data.itemName,
							parent: data.group.value,
						});
						hide();
					}
				}
			);
		}
	};

	const onCancel = (evt) => {
		evt.preventDefault();
		hide();
	};

	return (
		<form>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="itemName"
						label="Name"
						defaultValue={item && item?.name}
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="grid-row">
				<div className="grid-col-full">
					<Controller
						name="group"
						control={control}
						render={({ field }) => {
							return (
								<SelectRHF
									name={field.name}
									label="Group"
									options={[
										{ value: null, label: "[[root]]" },
										...groups.map((option) => ({
											value: option._id,
											label: option.name,
										})),
									]}
									{...field}
									ref={null}
									error={errors}
								/>
							);
						}}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</form>
	);
};

export default EditRowModal;
