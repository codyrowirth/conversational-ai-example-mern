import React, { createContext, useState } from "react";
import DataTable from "./DataTable";

export const NestedDataContext = createContext({});

const NestedData = ({ type, toolbar = false, ...props }) => {
	const [searchBarText, setSearchBarText] = useState("");
	const [expandedSections, setExpandedSections] = useState([]);

	const { downloading, setDownloading, enqueueSnackbar, closeSnackbar } = props;

	return (
		<NestedDataContext.Provider
			value={{ type, expandedSections, setExpandedSections }}
		>
			<DataTable
				type={type}
				searchBarText={searchBarText}
				setSearchBarText={setSearchBarText}
				toolbar={toolbar}
				downloading={downloading}
				setDownloading={setDownloading}
				enqueueSnackbar={enqueueSnackbar}
				closeSnackbar={closeSnackbar}
				{...props}
			/>
		</NestedDataContext.Provider>
	);
};
export default NestedData;
