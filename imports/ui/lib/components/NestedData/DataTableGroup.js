import React, { useState, useEffect } from "react";
import { UnmountClosed as Collapse } from "react-collapse";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useWinWidth from "../../utilities/useWinWidth";
import { useTracker } from "meteor/react-meteor-data";
import CanvasGroupsDB from "../../../../api/collections/CanvasGroups";
import pluralize from "pluralize";
import CanvasesDB from "../../../../api/collections/Canvases";
import DataTableItemRow from "./DataTableItemRow";
import AttributeGroupsDB from "../../../../api/collections/AttributeGroups";
import AttributesDB from "../../../../api/collections/Attributes";
import without from "lodash.without";
import ThesaurusEntryGroupsDB from "../../../../api/collections/ThesaurusEntryGroups";
import ThesaurusEntriesDB from "../../../../api/collections/ThesaurusEntries";

const DataTableGroup = ({
	group,
	type,
	toolbar,
	depth,
	setEditGroupModal,
	setDeleteGroupModal,
	setEditRowModal,
	setDeleteRowModal,
	all,
	setAll,
	add,
	...props
}) => {
	const expandedGroups = localStorage
		.getItem(`${type}ExpandedGroups`)
		.split(",");

	const [isExpanded, setIsExpanded] = useState(
		expandedGroups.includes(group._id)
	);
	const screenWidth = useWinWidth();

	useEffect(() => {
		if (all === "expand") {
			setIsExpanded(true);
		} else if (all === "collapse") {
			setIsExpanded(false);
		}
	}, [all]);

	let modelGroups, modelGroupsDB, modelItems, modelItemsDB;
	modelItems = pluralize(type);
	modelGroups = `${type}Groups`;
	if (type === "canvas") {
		modelGroupsDB = CanvasGroupsDB;
		modelItemsDB = CanvasesDB;
	} else if (type === "attribute") {
		modelGroupsDB = AttributeGroupsDB;
		modelItemsDB = AttributesDB;
	} else if (type === "thesaurusEntry") {
		modelGroupsDB = ThesaurusEntryGroupsDB;
		modelItemsDB = ThesaurusEntriesDB;
	}

	const { groups, items, isLoading } = useTracker(() => {
		const groupsSub = Meteor.subscribe(modelGroups, Meteor.user()?.projectID);
		const itemsSub = Meteor.subscribe(
			`${modelItems}ByParent`,
			group._id,
			Meteor.user()?.projectID
		);

		if (!groupsSub.ready() || !itemsSub.ready()) {
			return { isLoading: true, groups: null, items: null };
		}
		const groups = modelGroupsDB
			.find({
				parent: group._id,
				projectID: { $in: [Meteor.user()?.projectID, null] },
			})
			.fetch();
		const items = modelItemsDB
			.find({
				parent: group._id,
				projectID: { $in: [Meteor.user()?.projectID, null] },
			})
			.fetch();

		return { groups, items, isLoading: false };
	});

	if (isLoading) {
		return <tr />;
	}

	let colSpan = 1;
	if (type === "attribute") {
		colSpan = 2;
	}
	if (add) {
		colSpan = 3;
	}

	let colWidth;
	if (type === "attribute") {
		colWidth = "45%";
	} else if (toolbar === "open") {
		colWidth = "100%";
	} else if (type === "canvas") {
		colWidth = "70%";
	} else {
		colWidth = "75%";
	}

	return (
		<>
			<tr className="subgroup" id={group._id}>
				<td className="subgroup-trigger" colSpan={colSpan}>
					<button
						className="collapse-trigger"
						onClick={() => {
							const expandedGroups = localStorage
								.getItem(`${type}ExpandedGroups`)
								.split(",");

							if (isExpanded) {
								localStorage.setItem(
									`${type}ExpandedGroups`,
									without(expandedGroups, group._id).join(",")
								);
							} else {
								localStorage.setItem(
									`${type}ExpandedGroups`,
									[...expandedGroups, group._id].join(",")
								);
							}

							setIsExpanded(!isExpanded);
							setAll(null);

							localStorage.setItem(`last${type}clicked`, group._id);
						}}
					>
						{[...Array(depth)].map((element, index) => (
							<span key={index} className="child-dash" />
						))}
						{group.name}
						<span className="arrow">
							<FontAwesomeIcon
								icon={isExpanded ? "chevron-up" : "chevron-down"}
							/>
						</span>
					</button>
				</td>
				{!add && toolbar !== "open" && (
					<td data-label="Actions" className="action-links">
						<div>
							<button
								className="action-link"
								onClick={() => {
									setEditGroupModal(group);
									// const answer = window.prompt(
									// 	`What would you like to rename the "${group.name}" ${type} group to?`,
									// 	group.name
									// );
									// if (answer) {
									// 	Meteor.call(`${type}Groups.update`, group._id, {
									// 		name: answer,
									// 	});
									// }
								}}
							>
								Edit
							</button>
							<button
								className="action-link"
								onClick={() => {
									setDeleteGroupModal(group);
									// const answer = window.confirm(
									// 	`Are you sure you want to delete the "${group.name}" ${type} group?`
									// );
									// if (answer) {
									// 	Meteor.call(
									// 		`${pluralize(type)}.findByParent`,
									// 		group._id,
									// 		(err, data) => {
									// 			if (err) {
									// 				alert(
									// 					`Error, something went wrong deleting this ${type} group`
									// 				);
									// 				console.error(err);
									// 			} else if (data) {
									// 				alert(
									// 					`There are still items in this ${type} group. Please remove them before deleting.`
									// 				);
									// 			} else {
									// 				Meteor.call(
									// 					`${type}Groups.findByParent`,
									// 					group._id,
									// 					(err2, data2) => {
									// 						if (err2) {
									// 							alert(
									// 								`Error, something went wrong deleting this ${type} group`
									// 							);
									// 							console.error(err2);
									// 						} else if (data2) {
									// 							alert(
									// 								`There are still items in this ${type} group. Please remove them before deleting.`
									// 							);
									// 						} else {
									// 							Meteor.call(`${type}Groups.remove`, group._id);
									// 						}
									// 					}
									// 				);
									// 			}
									// 		}
									// 	);
									// }
								}}
							>
								Delete
							</button>
						</div>
					</td>
				)}
			</tr>
			<tr>
				<td colSpan="3" className="expanded-panel">
					<Collapse isOpened={isExpanded}>
						<table>
							{screenWidth >= 768 && (
								<colgroup>
									<col
										style={{
											width: colWidth,
										}}
									/>
									{type === "attribute" && <col style={{ width: "25%" }} />}
									{toolbar !== "open" && (
										<col
											style={{
												width:
													type === "canvas" || type === "attribute"
														? "30%"
														: "25%",
											}}
										/>
									)}
								</colgroup>
							)}

							<tbody>
								{items.map((item) => (
									<DataTableItemRow
										item={item}
										key={item._id}
										setEditRowModal={setEditRowModal}
										setDeleteGroupModal={setDeleteGroupModal}
										depth={depth + 1}
										type={type}
										toolbar={toolbar}
										setDeleteRowModal={setDeleteRowModal}
										add={add}
										{...props}
									/>
								))}
								{groups.map((group) => (
									<DataTableGroup
										group={group}
										type={type}
										toolbar={toolbar}
										key={group._id}
										depth={depth + 1}
										setDeleteGroupModal={setDeleteGroupModal}
										setDeleteRowModal={setDeleteRowModal}
										setEditRowModal={setEditRowModal}
										setEditGroupModal={setEditGroupModal}
										all={all}
										setAll={setAll}
										add={add}
										{...props}
									/>
								))}
							</tbody>
						</table>
					</Collapse>
				</td>
			</tr>
		</>
	);
};

export default DataTableGroup;
