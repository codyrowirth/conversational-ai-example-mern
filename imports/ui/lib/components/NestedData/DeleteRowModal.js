import React, { useContext } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { NestedDataContext } from "./NestedData";

const DeleteRowModal = ({ item, hide, model }) => {
	let { type } = useContext(NestedDataContext);
	if (type === "thesaurusEntry") {
		type = "entry";
	}
	return (
		<Modal className="modal limit-modal end-buttons" show={!!item} hide={hide}>
			<h3>Do you want to delete this {type}?</h3>
			{type === "entry" ? (
				<p>
					If you delete this entry, it will also unlink all options that use it.
				</p>
			) : null}
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{item && item?.name}</strong> {type}.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						Meteor.call(`${model}.remove`, item._id);
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteRowModal;
