import React, { useState } from "react";
import { titleCase } from "title-case";
import pluralize from "pluralize";
import { useHistory } from "react-router-dom";
import { useTracker } from "meteor/react-meteor-data";
import Message from "../../../../api/collections/Messages";
import Branchers from "../../../../api/collections/Branchers";

const DataTableItemRow = ({
	item,
	depth,
	type,
	toolbar,
	setEditRowModal,
	setDeleteRowModal,
	add,
	shapeID,
	parentType,
}) => {
	const history = useHistory();

	let parent;
	if (parentType === "brancher") {
		const { brancher } = useTracker(() => {
			return { brancher: Branchers.find({ shapeID }).fetch()[0] };
		});
		parent = brancher;
	} else {
		const { message } = useTracker(() => {
			return { message: Message.find({ shapeID }).fetch()[0] };
		});
		parent = message;
	}

	return (
		<tr key={item.id}>
			<td
				data-label={type}
				className={!add ? "clickable-row" : ""}
				onClick={() => {
					localStorage.setItem(`last${type}clicked`, item._id);

					if (!add) {
						history.push(`/${pluralize(type)}/${item._id}`);
					}
				}}
			>
				<div className="item-name">
					{depth
						? [...Array(depth - 1)].map((element, index) => (
								<span key={index} className="child-dash" />
						  ))
						: null}
					{item.name}
				</div>
			</td>
			{type === "attribute" && (
				<td data-label="Type">{titleCase(item.type)}</td>
			)}
			{toolbar !== "open" && (
				<td data-label="Actions">
					{add ? (
						<div className="action-links">
							{parent?.attributes && parent.attributes.includes(item._id) ? (
								<button
									className="action-link"
									onClick={() => {
										Meteor.call(`${pluralize(parentType)}.update`, parent._id, {
											attributes: parent.attributes.filter(
												(attribute) => attribute !== item._id
											),
										});
									}}
								>
									Remove
								</button>
							) : (
								<button
									className="action-link"
									onClick={() => {
										let currentAttributes = parent?.attributes || [];
										Meteor.call(`${pluralize(parentType)}.update`, parent._id, {
											attributes: [...currentAttributes, item._id],
										});
									}}
								>
									Add
								</button>
							)}
						</div>
					) : (
						<div className="action-links different-types">
							{type === "canvas" ||
								(type === "attribute" && (
									<button
										className="action-link"
										onClick={() => {
											history.push(`/${pluralize(type)}/${item._id}`);
										}}
									>
										Open
									</button>
								))}
							<button
								className="action-link"
								onClick={() => {
									//MZM 10-11-21 Eventually this if statement won't be necessary and all
									//edit links will take you to the correct page. This is a placeholder so canvases can be renamed until
									//that panel is ready within the canvas.

									// const answer = window.prompt(
									// 	`What would you like to rename this ${type} to?`,
									// 	item.name
									// );
									// if (answer) {
									// 	Meteor.call(`${pluralize(type)}.update`, item._id, {
									// 		name: answer,
									// 	});
									// }

									setEditRowModal(item);
								}}
							>
								Edit
							</button>
							<button
								className="action-link last"
								onClick={() => {
									setDeleteRowModal(item);
								}}
							>
								Delete
							</button>
						</div>
					)}
				</td>
			)}
		</tr>
	);
};

export default DataTableItemRow;
