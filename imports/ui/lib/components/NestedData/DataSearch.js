import React from "react";
import SearchBar from "../SearchBar/SearchBar";
import pluralize from "pluralize";

const DataSearch = ({ type, searchBarText, setSearchBarText }) => {
	return (
		<SearchBar
			placeholder={
				type === "thesaurusEntry"
					? "Search entries"
					: `Search ${pluralize(type)}`
			}
			searchBarText={searchBarText}
			setSearchBarText={setSearchBarText}
		/>
	);
};

export default DataSearch;
