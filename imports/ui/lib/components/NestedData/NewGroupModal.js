import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Button from "../Button/Button";
import SelectRHF from "../Form/SelectRHF";
import InputRHF from "../Form/InputRHF";
import Modal from "../Modal/Modal";
import { useTracker } from "meteor/react-meteor-data";
import CanvasGroupsDB from "../../../../api/collections/CanvasGroups";
import pluralize from "pluralize";
import { PropagateLoader } from "react-spinners";
import AttributeGroupsDB from "../../../../api/collections/AttributeGroups";
import ThesaurusEntryGroupsDB from "../../../../api/collections/ThesaurusEntryGroups";

const NewGroupModal = ({ show, hide, type }) => {
	const model = pluralize(type);
	let modelDB;
	if (type === "canvasGroup") {
		modelDB = CanvasGroupsDB;
	} else if (type === "attributeGroup") {
		modelDB = AttributeGroupsDB;
	} else if (type === "thesaurusEntryGroup") {
		modelDB = ThesaurusEntryGroupsDB;
	}

	const { groups, isLoading } = useTracker(() => {
		const handler = Meteor.subscribe(model, Meteor.user()?.projectID);
		if (!handler.ready()) {
			return { isLoading: true, groups: null };
		}
		let groups = modelDB
			.find({ projectID: Meteor.user()?.projectID })
			.fetch()
			.map((group) => ({ value: group._id, label: group.name }));
		groups.push({ value: null, label: "[[root]]" });
		return {
			groups,
			isLoading: false,
		};
	});

	const schema = yup.object().shape({
		groupName: yup.string().required("Please enter a group name."),
		parent: yup.object().required("Please select a parent group"),
	});

	const {
		register,
		control,
		reset,
		formState: { errors },
		setError,
		handleSubmit,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		const alreadyExists = modelDB.findOne({ name: data.groupName });
		if (alreadyExists) {
			setError(
				"groupName",
				{
					type: "string",
					message:
						"A group with this name already exists. Please rename the group and try again.",
				},
				{ shouldFocus: true }
			);
			throw new Error("name exists");
		}
		Meteor.call(`${model}.insert`, {
			name: data.groupName,
			parent: data.parent.value,
		});
		hide();
		reset();
	};

	const onCancel = () => {
		hide();
	};

	if (isLoading) {
		return (
			<Modal className="modal end-buttons" show={show} hide={hide}>
				<div style={{ textAlign: "center", paddingBottom: 20 }}>
					<PropagateLoader color="#2f3d47" />
				</div>
			</Modal>
		);
	}

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Add New Group</h3>
			<form>
				<div className="grid-row">
					<div className="grid-col-half">
						<InputRHF
							type="text"
							name="groupName"
							label="Name"
							register={register}
							error={errors}
						/>
					</div>
					<div className="grid-col-half">
						<Controller
							name="parent"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Parent"
										options={groups}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
			</form>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default NewGroupModal;
