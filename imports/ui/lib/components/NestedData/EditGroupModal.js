import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useTracker } from "meteor/react-meteor-data";
import pluralize from "pluralize";

import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import InputRHF from "../Form/InputRHF";
import SelectRHF from "../Form/SelectRHF";
import CanvasGroups from "../../../../api/collections/CanvasGroups";
import AttributeGroups from "../../../../api/collections/AttributeGroups";
import ThesaurusEntryGroups from "../../../../api/collections/ThesaurusEntryGroups";
import { Meteor } from "meteor/meteor";
import { PropagateLoader } from "react-spinners";
import { EditNestedDataItemForm } from "./EditRowModal";

const EditGroupModal = ({ item, hide, type }) => {
	let model, groups;

	if (type === "canvas") {
		model = CanvasGroups;
	} else if (type === "attribute") {
		model = AttributeGroups;
	} else if (type === "thesaurusEntry") {
		model = ThesaurusEntryGroups;
	} else {
		return (
			<Modal
				className="modal limit-modal end-buttons"
				show={!!item}
				hide={hide}
			>
				<h3>
					Error. This edit modal has not been created for this type of data.
				</h3>
			</Modal>
		);
	}

	groups = useTracker(() => {
		if (!model) {
			return null;
		}
		return model.find({ projectID: Meteor.user().projectID }).fetch();
	});

	return (
		<Modal className="modal limit-modal end-buttons" show={!!item} hide={hide}>
			<h3>
				Edit <span className="cap">{type}</span> Group
			</h3>

			{groups === null ? (
				<div style={{ textAlign: "center", paddingBottom: 20 }}>
					<PropagateLoader color="#2f3d47" />
				</div>
			) : (
				<EditNestedDataItemForm
					item={item}
					groups={groups}
					type={type}
					hide={hide}
					group={true}
				/>
			)}
		</Modal>
	);
};

export default EditGroupModal;
