import React, { useContext } from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import { NestedDataContext } from "./NestedData";

const DeleteGroupModal = ({ item, hide, model }) => {
	const { type } = useContext(NestedDataContext);
	return (
		<Modal className="modal limit-modal end-buttons" show={!!item} hide={hide}>
			<h3>Do you want to delete this group?</h3>
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{item && item?.name}</strong> group.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						// Meteor.call(`${model}.remove`, item._id);
						console.log("Delete");
						alert("TO DO");
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteGroupModal;
