import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Collapse } from "react-collapse";
import useWinWidth from "../../utilities/useWinWidth";
import MainNavItem from "../MainNav/MainNavItem";
import UtilityNav from "../UtilityNav/UtilityNav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Sidebar.scss";

const navItems = {
	dashboard: {
		id: "dashboard",
		text: "Dashboard",
		icon: "home",
		route: "/",
	},
	conversations: {
		id: "conversations",
		text: "Conversations",
		icon: "plug",
		children: {
			featuredThreads: {
				id: "featuredThreads",
				text: "Featured",
				route: "/conversations/featured",
			},
			browseThreads: {
				id: "browseThreads",
				text: "Browse",
				route: "/conversations/browse",
			},
			savedThreads: {
				id: "savedThreads",
				text: "Saved",
				route: "/conversations/saved",
			},
			activeThreads: {
				id: "activeThreads",
				text: "Active",
				route: "/conversations/active",
			},
		},
	},
	canvasesParent: {
		id: "canvasesParent",
		text: "Canvases",
		icon: "drafting-compass",
		// notifications: 3,
		children: {
			canvasList: {
				id: "canvases",
				text: "Canvases",
				route: "/canvases",
			},
			comments: {
				id: "comments",
				text: "Comments",
				route: "/comments",
				// notifications: 2,
			},
		},
	},
	attributes: {
		id: "attributes",
		text: "Attributes",
		icon: "list-ul",
		children: {
			attributesManager: {
				id: "attributes-manager",
				text: "Attributes Manager",
				route: "/attributes-manager",
			},
			thesaurus: {
				id: "thesaurus",
				text: "Thesaurus",
				route: "/thesaurus",
			},
			unmatched: {
				id: "unmatched",
				text: "Unmatched",
				route: "/unmatched",
				// notifications: 1,
			},
			retrievers: {
				id: "retrievers",
				text: "Retrievers",
				route: "/retrievers",
			},
		},
	},
	testReview: {
		id: "test-review",
		text: "Test & Review",
		icon: "flask",
		children: {
			export: {
				id: "export",
				text: "Export Content",
				route: "/export-content",
			},
			archErrors: {
				id: "archErrors",
				text: "Architecture Errors",
				route: "/architecture-errors",
			},

			contentErrors: {
				id: "contentErrors",
				text: "Content Errors",
				route: "/content-errors",
			},
			contentLength: {
				id: "content-length",
				text: "Content Length",
				route: "/content-length",
			},
			testCases: {
				id: "testCases",
				text: "Test Cases",
				route: "/test-cases",
			},
			sandbox: {
				id: "sandbox",
				text: "Sandbox",
				route: "/sandbox",
			},
			review: {
				id: "review",
				text: "Review",
				route: "/review",
			},
			publish: {
				id: "publish",
				text: "Publish",
				route: "/publish",
			},
		},
	},
	reports: {
		id: "reports",
		text: "Reports",
		icon: "chart-pie",
		route: "/reports",
	},
	settings: {
		id: "settings",
		text: "Settings",
		icon: "cog",
		children: {
			global: {
				id: "global",
				text: "Global",
				route: "global-settings",
			},
			project: {
				id: "project",
				text: "Project",
				route: "project-settings",
			},
		},
	},
};

const Sidebar = () => {
	const [mobileNavExpanded, setMobileNavExpanded] = useState(false);

	const screenWidth = useWinWidth();

	return (
		<header className="sidebar">
			<div className="branding flex">
				<Link className="logo" to="/">
					<img src="/images/tcs-logo-light.svg" alt="Logo" />
				</Link>
				<button
					className="hamburger"
					aria-label="Main Navigation"
					aria-controls="navigation"
					onClick={() => {
						setMobileNavExpanded(!mobileNavExpanded);
					}}
				>
					<FontAwesomeIcon icon={mobileNavExpanded ? "times" : "bars"} />
				</button>
			</div>
			{screenWidth >= 1200 ? (
				<nav className="main-nav">
					{Object.values(navItems).map((navItem) => (
						<MainNavItem key={navItem.id} navItem={navItem} />
					))}
				</nav>
			) : (
				<Collapse
					theme={{
						collapse: "ReactCollapse--collapse",
						content: "ReactCollapse--content mobile-nav",
					}}
					isOpened={mobileNavExpanded}
				>
					<nav className="main-nav">
						{Object.values(navItems).map((navItem) => (
							<MainNavItem
								key={navItem.id}
								navItem={navItem}
								mobileNavExpanded={mobileNavExpanded}
								setMobileNavExpanded={setMobileNavExpanded}
							/>
						))}
					</nav>
					<UtilityNav
						mobileNavExpanded={mobileNavExpanded}
						setMobileNavExpanded={setMobileNavExpanded}
					/>
				</Collapse>
			)}
		</header>
	);
};

export default Sidebar;
