import React, { useEffect, useState } from "react";

import "./input.scss";
import "./checkbox.scss";
import "./toggle.scss";

const ToggleRHF = ({
	name,
	labelLeft = "",
	labelRight = "",
	toggleContent = "",
	error,
	disabled = false,
	...props
}) => {
	const [checked, setChecked] = useState(props.checked || false);

	//MZM 12/2/21, This seems really hacky, but it was the only way I could get the toggle to react to new data passed in
	//via the prop
	useEffect(() => {
		setChecked(props.checked);
	}, [props.checked]);

	return (
		<div className="toggle-wrap">
			<label
				className={`checkbox toggle ${disabled === true ? "disabled" : ""} ${
					error[name] ? "error" : ""
				}`}
			>
				{labelLeft && <span className="label">{labelLeft}</span>}
				<div className={`checkmark switch-box ${labelLeft && "label-left"}`}>
					<input
						type="checkbox"
						name={name}
						className={`${checked}`}
						{...props}
						disabled={disabled}
						onChange={(evt) => {
							if (props.onChange) {
								props.onChange(evt);
							}
							setChecked(!checked);
						}}
					/>
					<div className={`switch ${checked}`}></div>
				</div>
				{labelRight && <span className="label">{labelRight}</span>}
				{error[name] && (
					<p className="form-response error">{error[name].message}</p>
				)}
			</label>
			{toggleContent && checked === true && (
				<div className="toggle-content">
					<div className="toggle-content-inner">{toggleContent}</div>
				</div>
			)}
		</div>
	);
};

export default ToggleRHF;
