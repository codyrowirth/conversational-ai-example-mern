import React from "react";
import "./FileUpload.scss";

const FileUpload = (props) => {
	const { label } = props;

	return (
		<label>
			{label && <span className="label">{label}</span>}
			<div className="file-upload">
				<div className="file-upload-inner">
					Drag ’n’ drop image here, or click to select file
				</div>
			</div>
		</label>
	);
};

export default FileUpload;
