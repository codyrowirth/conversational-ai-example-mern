import React, { forwardRef } from "react";
import ReactDatePicker from "react-datepicker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "react-datepicker/dist/react-datepicker.css";
import "./Datepicker.scss";

const Datepicker = ({
	selectedDate,
	setSelectedDate,
	name,
	label = "",
	error,
	...props
}) => {
	return (
		<label className={`datepicker ${error[name] ? "error" : ""}`}>
			{label && <span className="label">{label}</span>}
			<ReactDatePicker
				selected={selectedDate}
				onSelect={(date) => setSelectedDate(date)}
				{...props}
			/>
      <FontAwesomeIcon icon="calendar-alt" />
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default Datepicker;
