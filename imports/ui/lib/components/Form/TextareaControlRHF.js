import React from "react";
import "./textarea.scss";

function TextareaRHF({ name, label = "", error, ...props }) {
	return (
		<label className={error[name] ? "error" : ""}>
			{label && <span className="label">{label}</span>}
			<textarea {...props} />
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
}

export default TextareaRHF;
