import React from "react";
import "./textarea.scss";

function TextareaRHF({ name, label = "", register, error, ...props }) {
	return (
		<label className={error[name] ? "error" : ""}>
			{label && <span className="label">{label}</span>}
			<textarea {...register(name)} {...props} name={name} />
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
}

export default TextareaRHF;
