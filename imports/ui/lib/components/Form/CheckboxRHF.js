import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./input.scss";
import "./checkbox.scss";

const CheckboxRHF = ({ name, label = "", register, error = {}, ...props }) => {
	const [checked, setChecked] = useState(props.checked || false);

	return (
		<label className={`checkbox ${error[name] ? "error" : ""}`}>
			<div className="checkmark">
				<input
					type="checkbox"
					name={name}
					className={`${checked}`}
					{...props}
					{...register(name)}
					onChange={(evt) => {
						if (props.onChange) {
							props.onChange(evt);
						}
						setChecked(!checked);
					}}
				/>
				{checked && <FontAwesomeIcon icon="check" />}
			</div>
			{label && <span className="label">{label}</span>}
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default CheckboxRHF;
