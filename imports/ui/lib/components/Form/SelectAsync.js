import React from "react";
import { components, NonceProvider } from "react-select";
import AsyncSelect from "react-select/async";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Async from "react-select/async";

const SelectAsync = ({
	name,
	label = "",
	options,
	panel = false,
	error = {},
	...props
}) => {
	const customStyles = {
		control: (provided, state) => {
			let controlBGColor = "var(--white)";

			if (!state.isFocused && panel === true) {
				controlBGColor = "var(--slate)";
			} else if (state.isFocused && panel === false) {
				controlBGColor = "var(--blue)";
			}

			let controlBorder = "var(--border-size) solid var(--slate)";

			if (panel === false && error[name]) {
				controlBorder = "var(--border-size) solid var(--red)";
			} else if (panel === true && error[name]) {
				controlBorder = "var(--border-size) solid var(--blue)";
			} else if (panel === true) {
				controlBorder = "var(--border-size) solid var(--white)";
			}

			return {
				...provided,
				width: `${100}%`,
				display: "flex",
				padding: `${10}px ${25}px`,
				backgroundColor: controlBGColor,
				border: controlBorder,
				borderRadius: 0,
				boxShadow: "none",
				fontSize:
					panel === true
						? "var(--font-size-supporting)"
						: "var(--font-size-body)",
				color:
					!state.isFocused && panel === true ? "var(--white)" : "var(--slate)",
				":hover": {
					border: controlBorder,
				},
			};
		},
		valueContainer: () => ({
			width: `calc(${100}% - ${18}px)`,
			padding: 0,
		}),
		input: () => ({
			margin: 0,
			padding: 0,
		}),
		indicatorsContainer: () => ({
			display: "flex",
			alignItems: "center",
		}),
		indicatorSeparator: () => ({
			display: "none",
		}),
		dropdownIndicator: () => ({
			display: "flex",
		}),
		loadingIndicator: () => ({
			display: "none",
		}),
		menu: (provided) => ({
			...provided,
			margin: `-${2}px 0 0 0`,
			border: panel === true ? 0 : "var(--border-size) solid var(--slate)",
			borderRadius: 0,
			boxShadow: 0,
		}),
		menuPortal: (provided) => ({
			...provided,
			zIndex: 99999,
		}),
		menuList: (provided) => ({
			...provided,
			padding: 0,
		}),
		option: (provided, state) => {
			let optionBGColor = "var(--white)";

			if (state.isSelected) {
				optionBGColor = "var(--blue)";
			} else if (state.isFocused) {
				optionBGColor = "var(--grey)";
			}

			return {
				...provided,
				padding: `${10}px ${25}px`,
				backgroundColor: optionBGColor,
				fontSize:
					panel === true
						? "var(--font-size-supporting)"
						: "var(--font-size-body)",
				color: "var(--slate)",
			};
		},
		singleValue: (provided, state) => ({
			...provided,
			color: "inherit",
		}),
	};

	const DropdownIndicator = (props) => {
		return (
			<components.DropdownIndicator {...props}>
				<FontAwesomeIcon
					icon={props.selectProps.menuIsOpen ? "chevron-up" : "chevron-down"}
					style={{ fontSize: `${20}px` }}
				/>
			</components.DropdownIndicator>
		);
	};

	return (
		<label className={error[name] ? "error" : ""}>
			{label && <span className="label">{label}</span>}
			<AsyncSelect
				components={{ DropdownIndicator }}
				options={options}
				placeholder=""
				menuPlacement="auto"
				styles={customStyles}
				name={name}
				menuPortalTarget={document.body}
				{...props}
			/>

			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default SelectAsync;
