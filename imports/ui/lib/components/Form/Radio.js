import React from "react";

import "./input.scss";
import "./radio.scss";

const Radio = ({ name, label = "", register, value, checked, ...props }) => {
	return (
		<label className={`radio`}>
			<input
				type="radio"
				className={`${checked === true ? "checked" : ""}`}
				{...props}
			/>
			{label && <span className="label">{label}</span>}
		</label>
	);
};

export default Radio;
