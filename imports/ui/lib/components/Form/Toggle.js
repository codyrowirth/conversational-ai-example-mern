import React, { useState } from "react";

import "./input.scss";
import "./checkbox.scss";
import "./toggle.scss";

const Toggle = ({
	name,
	labelLeft = "",
	labelRight = "",
	checked,
	setChecked,
	disabled = false,
	...props
}) => {
	return (
		<>
			<label
				className={`checkbox toggle ${disabled === true ? "disabled" : ""}`}
			>
				{labelLeft && <span className="label">{labelLeft}</span>}
				<div className={`checkmark switch-box ${labelLeft && "label-left"}`}>
					<input
						type="checkbox"
						name={name}
						className={`${checked}`}
						{...props}
						disabled={disabled}
						onChange={(evt) => {
							if (props.onChange) {
								props.onChange(evt);
							}
							setChecked(!checked);
						}}
					/>
					<div className={`switch ${checked}`}></div>
				</div>
				{labelRight && <span className="label">{labelRight}</span>}
			</label>
		</>
	);
};

export default Toggle;
