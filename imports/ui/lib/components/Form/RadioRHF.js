import React from "react";

import "./input.scss";
import "./radio.scss";

const RadioRHF = ({
	name,
	label = "",
	register,
	value,
	checked,
	error,
	...props
}) => {
	return (
		<label className={`radio ${error[name] ? "error" : ""}`}>
			<input
				type="radio"
				name={name}
				value={value}
				className={`${checked === true ? "checked" : ""}`}
				{...props}
				{...register(name)}
			/>
			{label && <span className="label">{label}</span>}
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default RadioRHF;
