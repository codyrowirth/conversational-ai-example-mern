import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./input.scss";
import "./checkbox.scss";

const CheckboxControlRHF = ({
	name = "",
	label = "",
	checked,
	error = {},
	...props
}) => {
	return (
		<label className={`checkbox ${error[name] ? "error" : ""}`}>
			<div className="checkmark">
				<input
					type="checkbox"
					name={name}
					className={`${checked}`}
					value={checked}
					{...props}
				/>
				{checked && <FontAwesomeIcon icon="check" />}
			</div>
			{label && <span className="label">{label}</span>}
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default CheckboxControlRHF;
