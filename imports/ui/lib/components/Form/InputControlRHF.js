import React from "react";
import "./input.scss";

const InputControlRHF = ({ name, label = "", error = {}, ...props }) => {
	return (
		<label className={error[name] ? "error" : ""}>
			{label && <span className="label">{label}</span>}
			<input name={name} {...props} />
			{error[name] && (
				<p className="form-response error">{error[name].message}</p>
			)}
		</label>
	);
};

export default InputControlRHF;
