import React from "react";
import "./Button.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Button = ({ addClass, text, dark = false, style = {}, ...props }) => {
	return (
		<button
			style={{ ...style }}
			className={`button ${dark === true && "dark"} ${
				addClass ? addClass : ""
			}`}
			{...props}
		>
			{props.icon ? <FontAwesomeIcon icon={props.icon} /> : null}
			<span>{text}</span>
		</button>
	);
};

export default Button;
