import React, { useState } from "react";
import { UnmountClosed } from "react-collapse";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const Collapsible = ({ children, title }) => {
	const [isOpen, setIsOpen] = useState(false);

	return (
		<>
			<div className="inner-box">
				<button
					className="collapse-trigger h5"
					onClick={(event) => {
						event.preventDefault();
						setIsOpen(!isOpen);
					}}
				>
					{title}
					<span className="arrow">
						<FontAwesomeIcon icon={isOpen ? "chevron-up" : "chevron-down"} />
					</span>
				</button>
				<UnmountClosed isOpened={isOpen}>{children}</UnmountClosed>
			</div>
		</>
	);
};

export default Collapsible;
