import React from "react";
import Button from "../Button/Button";
import "./Pagination.scss";

const Pagination = () => {
	return (
		<div className="pagination">
			<div className="pagination-inner">
				<Button
					text="Prev"
					addClass="prev-button"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				/>
				<span className="page-count">
					Page <span className="page-count-current">1</span> of{" "}
					<span className="page-count-total">25</span>
				</span>
				<Button
					text="Next"
					addClass="next-button"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				/>
			</div>
		</div>
	);
};

export default Pagination;
