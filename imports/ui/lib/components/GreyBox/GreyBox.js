import React from "react";
import "./GreyBox.scss";

const GreyBox = (props) => {
	return (
		<div
			className={`grey-box ${
				props.className != undefined ? props.className : ""
			}`}
		>
			<div className="grey-box-inner">{props.children}</div>
		</div>
	);
};

export default GreyBox;
