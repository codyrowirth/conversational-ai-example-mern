import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { Collapse } from "react-collapse";
import MainNavSubNavItem from "./MainNavSubNavItem";
import NotifBadge from "../NotifBadge/NotifBadge";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./MainNavItem.scss";

const MainNavItem = ({ navItem, mobileNavExpanded, setMobileNavExpanded }) => {
	const [subNavExpanded, setSubNavExpanded] = useState(false);

	return (
		<div className="item">
			{"route" in navItem ? (
				<NavLink
					exact
					to={navItem.route}
					onClick={() => {
						if (mobileNavExpanded === true) {
							setMobileNavExpanded(false);
						}
					}}
				>
					<div className="item-inner flex">
						<div className="icon-box">
							<div className="icon-box-inner flex">
								<FontAwesomeIcon icon={navItem.icon} />
							</div>
						</div>
						<div className="text-box">
							<div className="text-box-inner flex">
								<div
									className={`text ${
										"notifications" in navItem ? "notif-active" : ""
									}`}
								>
									<span>{navItem.text}</span>
									{"notifications" in navItem && (
										<NotifBadge count={navItem.notifications} />
									)}
								</div>
							</div>
						</div>
					</div>
				</NavLink>
			) : (
				<div className="item-inner">
					<button
						className={`sub-nav-trigger flex ${
							subNavExpanded ? "expanded" : ""
						}`}
						onClick={() => {
							setSubNavExpanded(!subNavExpanded);
						}}
					>
						<div className="icon-box">
							<div className="icon-box-inner flex">
								<FontAwesomeIcon icon={navItem.icon} />
							</div>
						</div>
						<div className="text-box">
							<div className="text-box-inner flex">
								<div
									className={`text ${
										"notifications" in navItem ? "notif-active" : ""
									}`}
								>
									<span>{navItem.text}</span>
									{"notifications" in navItem && (
										<NotifBadge count={navItem.notifications} />
									)}
								</div>
								<div className="arrow">
									<FontAwesomeIcon
										icon={subNavExpanded ? "chevron-up" : "chevron-down"}
									/>
								</div>
							</div>
						</div>
					</button>
					<Collapse
						theme={{
							collapse: "ReactCollapse--collapse",
							content: "ReactCollapse--content sub-nav",
						}}
						isOpened={subNavExpanded}
					>
						{Object.values(navItem.children).map((subNavItem) => (
							<MainNavSubNavItem
								key={subNavItem.id}
								subNavItem={subNavItem}
								setSubNavExpanded={setSubNavExpanded}
								mobileNavExpanded={mobileNavExpanded}
								setMobileNavExpanded={setMobileNavExpanded}
							/>
						))}
					</Collapse>
				</div>
			)}
		</div>
	);
};

export default MainNavItem;
