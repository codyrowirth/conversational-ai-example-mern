import React from "react";
import { NavLink } from "react-router-dom";
import NotifBadge from "../NotifBadge/NotifBadge";
import "./MainNavSubNavItem.scss";

const MainNavSubNavItem = ({
	subNavItem,
	setSubNavExpanded,
	mobileNavExpanded,
	setMobileNavExpanded,
}) => {
	return (
		<div
			className={`sub-nav-item ${
				"notifications" in subNavItem ? "notif-active" : ""
			}`}
		>
			<NavLink
				exact
				className="link"
				to={subNavItem.route}
				onClick={() => {
					if (mobileNavExpanded === true) {
						setSubNavExpanded(false);
						setMobileNavExpanded(false);
					}
				}}
			>
				<div className="link-inner flex">
					<div className="stripe"></div>
					<div className="text">
						<span>{subNavItem.text}</span>
						{"notifications" in subNavItem && (
							<NotifBadge count={subNavItem.notifications} small={true} />
						)}
					</div>
				</div>
			</NavLink>
		</div>
	);
};

export default MainNavSubNavItem;
