import React from "react";
import TimeAgo from "react-timeago";
const LastSaved = ({ timestamp }) => {
	if (!timestamp) {
		return "unknown";
	}

	return (
		<TimeAgo
			date={timestamp}
			formatter={(value, unit, suffix, epochMilliseconds, nextFormatter) => {
				if (unit === "second") {
					return "just now";
				} else {
					return nextFormatter();
				}
			}}
		/>
	);
};

export default LastSaved;
