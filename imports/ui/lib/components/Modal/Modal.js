import React from "react";
import ModalOverlay from "react-overlays/Modal";
import "./Modal.scss";
import useMousetrap from "react-hook-mousetrap";

const Modal = ({ className, show, hide, children, ...props }) => {
	const renderBackdrop = (props) => (
		<div className="overlay nodrag" {...props}></div>
	);

	useMousetrap("esc", () => {
		hide();
	});

	//MZM 12/2/21: I tried to get enforceFocus to only switch to false when the Userback.on_open hook fired, but for the
	//life of me, I couldn't get that event to fire from their JS API. If enforcing focus causes problems elsewhere, we'll
	//have to look into a better fix.

	return (
		<ModalOverlay
			className={`${className} nodrag`}
			show={!!show}
			onShow={
				props?.onShow
					? props.onShow
					: () => {
							return show;
					  }
			}
			onHide={props?.onHide ? props.onHide : hide}
			renderBackdrop={renderBackdrop}
			enforceFocus={false}
		>
			<div>{children}</div>
		</ModalOverlay>
	);
};

export default Modal;
