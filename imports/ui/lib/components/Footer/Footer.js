import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./Footer.scss";

const Footer = () => {
	const [versionNumber, setVersionNumber] = useState();
	useEffect(() => {
		Meteor.call("getVersion", (error, data) => {
			setVersionNumber(data);
		});
	}, []);

	return (
		<footer>
			<a
				className="logo"
				href="https://wearetuzag.com"
				target="_blank"
				rel="noreferrer"
			>
				<img src="/images/tuzag-logo.svg" alt="Tuzag logo" />
			</a>
			<div className="footer-text flex">
				<div className="copyright">
					Copyright {new Date().getFullYear()} tuzag, inc. All rights reserved.{" "}
					<Link to="/privacy-policy">Privacy Policy</Link>
				</div>
				{versionNumber && <div className="version">v{versionNumber}</div>}
			</div>
		</footer>
	);
};

export default Footer;
