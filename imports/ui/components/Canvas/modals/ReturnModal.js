import React from "react";
import { useForm } from "react-hook-form";

import Modal from "../../../lib/components/Modal/Modal";
import Button from "../../../lib/components/Button/Button";
import InputRHF from "../../../lib/components/Form/InputRHF";

import "./modal.scss";

const ReturnModal = ({ show, hide, setOpenPanelID }) => {
	const {
		register,
		formState: { errors },
		setFocus,
	} = useForm({
		defaultValues: {
			// TO DO: Name field default value
			name: "",
		},
	});

	const onLoad = () => {
		setFocus("name");
	};

	const onCancel = () => {
		setOpenPanelID(null);
		hide();
	};

	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!show}
			hide={hide}
			onShow={onLoad}
			onHide={onCancel}
		>
			<h3>Edit Return</h3>
			<div className="modal-section">
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<InputRHF
								type="text"
								name="name"
								label="Name"
								error={errors}
								register={register}
							/>
						</div>
					</div>
				</form>
			</div>
			<div className="button-wrap">
				<Button text="Close" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default ReturnModal;
