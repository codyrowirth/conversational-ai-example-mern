import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";
import { useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";

import Shapes from "../../../../api/collections/Shapes";
import Branchers from "../../../../api/collections/Branchers";
import BrancherLines from "../../../../api/collections/BrancherLines";

import NotificationBox from "./modal-sections/lib/NotificationBox";
import ActionLinks from "./modal-sections/lib/ActionLinks";
import BrancherSettings from "./modal-sections/BrancherSettings";
import BrancherAttributes from "./modal-sections/BrancherAttributes";
import BrancherRules from "./modal-sections/BrancherRules";
import AddAttributeModal from "./inner-modals/AddAttributeModal";
import BrancherRulesModal from "./inner-modals/BrancherRulesModal";

import Modal from "../../../lib/components/Modal/Modal";
import Button from "../../../lib/components/Button/Button";

import "./modal.scss";

const BrancherModal = ({
	type,
	data,
	shapeID,
	comments,
	show,
	hide,
	setOpenPanelID,
}) => {
	const [addAttributeModalOpen, setAddAttributeModalOpen] = useState(false);
	const [rulesModalOpen, setRulesModalOpen] = useState(false);

	const { canvasID } = useParams();

	const { brancher, loading, brancherLines } = useTracker(() => {
		const subscription = Meteor.subscribe("branchersByShapeID", shapeID);
		const shapesSub = Meteor.subscribe("shapes", canvasID);

		if (!subscription.ready() || !shapesSub.ready()) {
			return { loading: true, brancher: null };
		}

		const branchersDB = Branchers.find({ shapeID }).fetch();

		if (branchersDB.length === 0) {
			Meteor.call("branchers.insert", {
				shapeID,
				attributes: [],
			});
			return { loading: true, brancher: null };
		}

		const brancherLines = Shapes.find({ source: shapeID }).fetch();

		const targetIDs = brancherLines.map((line) => line.target);

		const brancherLinesSub = Meteor.subscribe(`brancherLinesArray`, targetIDs);

		if (!brancherLinesSub.ready()) {
			return { brancherLines: null, brancher: null, loading: true };
		}

		for (const line of brancherLines) {
			line.destinationShape = Shapes.findOne({ _id: line.target });

			const matchingBrancherLine = BrancherLines.findOne({
				brancherID: shapeID,
				target: line.target,
			});

			if (!matchingBrancherLine) {
				Meteor.call("brancherLines.insert", {
					brancherID: shapeID,
					target: line.target,
					lineID: line._id,
					chainedOperator: "and",
				});

				return { brancherLines: null, loading: true, brancher: null };
			}
			line.brancherLine = matchingBrancherLine;
		}

		return {
			loading: false,
			brancher: branchersDB[0],
			brancherLines,
		};
	});

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#fff" />
			</div>
		);
	}

	const onCancel = () => {
		setOpenPanelID(null);
		hide();
	};

	return (
		<Modal
			className="modal end-buttons"
			show={!!show}
			hide={hide}
			onHide={onCancel}
		>
			<h3>Edit Brancher</h3>
			<NotificationBox comments={comments} />
			<ActionLinks
				type={type}
				comments={comments}
				setAddAttributeModalOpen={setAddAttributeModalOpen}
				setRulesModalOpen={setRulesModalOpen}
			/>
			<BrancherSettings
				shapeID={shapeID}
				shapeData={data}
				brancher={brancher}
			/>
			<BrancherAttributes
				type={type}
				setAddAttributeModalOpen={setAddAttributeModalOpen}
				shapeID={shapeID}
			/>
			<BrancherRules
				shapeID={shapeID}
				grandparent={brancher}
				setRulesModalOpen={setRulesModalOpen}
			/>
			<div className="button-wrap">
				<Button text="Close" onClick={onCancel} />
			</div>
			<AddAttributeModal
				shapeID={shapeID}
				show={addAttributeModalOpen}
				hide={() => {
					setAddAttributeModalOpen(false);
				}}
				parentType="brancher"
				setAddAttributeModalOpen={setAddAttributeModalOpen}
				setRulesModalOpen={setRulesModalOpen}
			/>
			<BrancherRulesModal
				rulesModalOpen={rulesModalOpen}
				setRulesModalOpen={setRulesModalOpen}
				setAddAttributeModalOpen={setAddAttributeModalOpen}
				shapeID={shapeID}
				lines={brancherLines}
				grandparent={brancher}
			/>
		</Modal>
	);
};

export default BrancherModal;
