import React from "react";
import { useForm, Controller } from "react-hook-form";

import Modal from "../../../../lib/components/Modal/Modal";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import Button from "../../../../lib/components/Button/Button";

const ManageThesaurusModal = ({ show, hide }) => {
	const {
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	onSubmit = (data) => {
		// TO DO
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Thesaurus</h3>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="thesaurusEntry"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Entry"
										options={["TO DO"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div
					className="action-links"
					style={{ marginTop: "var(--spacing-standard)" }}
				>
					<span className="label">Manage</span>
					<button
						className="action-link"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					>
						Add New
					</button>
					<button
						className="action-link"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					>
						Edit
					</button>
				</div>
				<div className="button-wrap">
					<Button text="Save" onClick={handleSubmit(onSubmit)} />
					<Button text="Cancel" onClick={onCancel} />
				</div>
			</form>
		</Modal>
	);
};

export default ManageThesaurusModal;
