import React from "react";
import Button from "../../../../lib/components/Button/Button";
import Modal from "../../../../lib/components/Modal/Modal";

const DeleteAttributeModal = ({ attributeInfo, hide }) => {
	// TO DO: Get attribute name
	const attribute = {
		name: "Attribute Name Placeholder",
	};

	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!attributeInfo}
			hide={hide}
		>
			<h3>Do you want to delete this attribute?</h3>
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{attribute && attributeInfo?.matchingAttribute?.name}</strong>{" "}
				attribute.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						// Meteor.call("options.remove", option._id);
						Meteor.call(
							`messageTrackingAttributes.remove`,
							attributeInfo.trackingAttribute._id
						);
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteAttributeModal;
