import React, { Fragment, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { UnmountClosed } from "react-collapse";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useWinWidth from "../../../../lib/utilities/useWinWidth";

import Modal from "../../../../lib/components/Modal/Modal";
import InputRHF from "../../../../lib/components/Form/InputRHF";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import ToggleControlRHF from "../../../../lib/components/Form/ToggleControlRHF";
import Button from "../../../../lib/components/Button/Button";

const StatsModal = ({ show, hide }) => {
	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const isLoading = false;

	const screenWidth = useWinWidth();

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Stats</h3>
			<p>TO DO</p>
			<div className="button-wrap">
				<Button text="Close" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default StatsModal;
