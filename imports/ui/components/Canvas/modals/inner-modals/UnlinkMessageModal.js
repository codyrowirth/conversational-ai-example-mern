import React from "react";
import Button from "../../../../lib/components/Button/Button";
import Modal from "../../../../lib/components/Modal/Modal";

const UnlinkMessageModal = ({ show, hide }) => {
	// TO DO: Get message name
	const message = {
		name: "Message Name Placeholder",
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Do you want to unlink this message?</h3>
			<p>
				Please confirm that you’d like to unlink the{" "}
				<strong>{message && message?.name}</strong> message.
			</p>
			<div className="button-wrap">
				<Button
					text="Unlink"
					onClick={() => {
						// Meteor.call("options.remove", option._id);
						alert("TO DO");
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default UnlinkMessageModal;
