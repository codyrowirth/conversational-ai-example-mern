import React from "react";

import UnmatchedTable from "../../../Unmatched/UnmatchedTable";
import Modal from "../../../../lib/components/Modal/Modal";
import Button from "../../../../lib/components/Button/Button";

// TO DO: Use real data

const responses = [
	{
		_id: "responseOne",
		name: "Response one",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseTwo",
		name: "Response two",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseThree",
		name: "Response three",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseFour",
		name: "Response four",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseFive",
		name: "Response five",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
];

const UnmatchedModal = ({ show, hide }) => {
	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Unmatched User Responses</h3>
			<UnmatchedTable type="message" responses={responses} />
			<div className="button-wrap">
				<Button text="Close" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default UnmatchedModal;
