import React, { useContext, useState } from "react";
import useWinWidth from "../../../../lib/utilities/useWinWidth";

import Modal from "../../../../lib/components/Modal/Modal";
import Button from "../../../../lib/components/Button/Button";

import NewElementForm from "./NewElementForm";

import { BeatLoader, PropagateLoader } from "react-spinners";
import { useTracker } from "meteor/react-meteor-data";
import Elements from "../../../../../api/collections/Elements";
import Messages from "../../../../../api/collections/Messages";
import SaveElementTemplateModal from "../../../Elements/SaveElementTemplateModal";
import { usePopper } from "react-popper";
import RCMenu, { MenuItem as RCMenuItem } from "rc-menu";
import { Transforms } from "slate";
import ElementTemplates from "../../../../../api/collections/ElementTemplates";
import DeleteElementTemplateModal from "../../../Elements/DeleteElementTemplateModal";
import AppContext from "../../../App/AppContext";
import { useSnackbar } from "notistack";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import OptionsModal from "./OptionsModal";
import AdvancedModal from "./AdvancedModal";
import ElementModalForm from "./ElementModalForm";
import Grid from "@mui/material/Grid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { UnmountClosed } from "react-collapse";
import Loading from "../../../Sandbox/Loading";
import Options from "../../../../../api/collections/Options";
import Collapsible from "../../../../lib/components/Collapsible/Collapsible";
import "./ElementsModal.scss";
import InputControlRHF from "../../../../lib/components/Form/InputControlRHF";
import { saveOnBlur } from "../../../../lib/utilities/saveOnBlurRHFMeteor";
import useMousetrap from "react-hook-mousetrap";

const ElementsModal = ({
	comments,
	show,
	hide,
	message,
	shapeID,
	shapeData,
	setElementsModal,
	optionsModal,
	setOptionsModal,
	advancedModal,
	setAdvancedModal,
}) => {
	const [newElementFormExpanded, setNewElementFormExpanded] = useState(false);
	const { setCopyElementID, copyElementID } = useContext(AppContext);
	const [elementToEdit, setElementToEdit] = useState();
	const [saveElementTemplateModalOpen, setSaveElementTemplateModalOpen] =
		useState(false);
	const [deleteElementTemplateModalOpen, setDeleteElementTemplateModalOpen] =
		useState(false);
	const { enqueueSnackbar } = useSnackbar();
	const [elementToRename, setElementToRename] = useState(false);

	const screenWidth = useWinWidth();

	const { elements, isLoading, options } = useTracker(() => {
		const handlerElements = Meteor.subscribe("elements", message._id);
		if (!handlerElements.ready()) {
			return { elements: null, options: null, isLoading: true };
		}
		let elementsDB = Elements.find({ messageID: message._id }).fetch();

		if (message.listenerAttributeID) {
			const optionsSubscription = Meteor.subscribe(
				"optionsByAttribute",
				message.listenerAttributeID
			);
			if (!optionsSubscription.ready()) {
				return { elements: elementsDB, isLoading: false, options: null };
			} else {
				return {
					isLoading: false,
					elements: elementsDB,
					options: Options.find({
						attributeID: message.listenerAttributeID,
					}).fetch(),
				};
			}
		}
		return {
			isLoading: false,
			elements: elementsDB,
			options: null,
		};
	});

	const deleteElement = (data) => {
		Meteor.call(`elements.remove`, data._id);
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	let optionsContent;
	if (isLoading || options === null) {
		optionsContent = <Loading />;
	} else if (!message.listenerAttributeID) {
		optionsContent = <p>No listener attribute set</p>;
	} else if (options?.length === 0) {
		optionsContent = <p>Listener attribute has no options</p>;
	} else {
		optionsContent = (
			<ul>
				{options.map((option) => (
					<li key={option._id}>{option.name}</li>
				))}
			</ul>
		);
	}

	return (
		<>
			<div className="heading-with-button flex">
				<div>
					<h3>Elements</h3>
				</div>
				<div>
					<Button
						text={newElementFormExpanded ? "Close" : "Add New Element"}
						icon={newElementFormExpanded ? "times" : "plus"}
						onClick={() => {
							setNewElementFormExpanded(!newElementFormExpanded);
						}}
					/>
				</div>
			</div>
			<NewElementForm
				newElementFormExpanded={newElementFormExpanded}
				setNewElementFormExpanded={setNewElementFormExpanded}
				messageID={message._id}
			/>
			<Grid container>
				<Grid item xs={8}>
					<div className="element-table">
						<table className="elements">
							{screenWidth >= 768 && (
								<colgroup>
									<col
										style={isLoading ? { width: "100%" } : { width: "50%" }}
									/>
									<col style={isLoading ? { width: "0" } : { width: "50%" }} />
								</colgroup>
							)}
							<thead>
								<tr>
									<th scope="col">{isLoading ? "" : "Element"}</th>
									<th scope="col">{isLoading ? "" : "Actions"}</th>
								</tr>
							</thead>
							<tbody>
								{isLoading ? (
									<tr className="loader">
										<td>
											<PropagateLoader color="#2f3d47" />
										</td>
									</tr>
								) : (
									elements.map((element) => {
										return (
											<tr key={element._id}>
												<td
													data-label="Element"
													className="clickable-row"
													onClick={(event) => {
														setElementToEdit(null);
														setTimeout(() => {
															setElementToEdit(element);
														}, 10);
													}}
												>
													{element.name}
												</td>
												<td data-label="Actions">
													<div className="action-links">
														<button
															className="action-link"
															onClick={(event) => {
																event.preventDefault();
																setElementToRename(element);
															}}
														>
															Edit
														</button>
														<button
															className="action-link notif-link"
															onClick={(event) => {
																event.preventDefault();
																alert("TO DO");
															}}
														>
															<span className="notif-type">Comment</span>
															{comments > 0 ? ` (${comments})` : ""}
														</button>
														<button
															className="action-link"
															onClick={(event) => {
																event.preventDefault();
																setCopyElementID(element._id);
																window.localStorage.copyElementID = element._id;
																enqueueSnackbar("Element copied.", {
																	variant: "success",
																	key: "elementCopied",
																});
															}}
														>
															Copy
														</button>
														<button
															className="action-link"
															onClick={(event) => {
																event.preventDefault();
																Meteor.call(`elements.duplicate`, element);
															}}
														>
															Duplicate
														</button>
														<button
															className="action-link"
															onClick={(event) => {
																event.preventDefault();
																deleteElement(element);
															}}
														>
															Delete
														</button>
													</div>
												</td>
											</tr>
										);
									})
								)}
							</tbody>
						</table>
					</div>
					<div className="table-action-links-wrap">
						<div className="table-action-links-row bottom">
							<div className="table-action-links left">
								{copyElementID && (
									<button
										className="action-link"
										onClick={(event) => {
											event.preventDefault();
											Meteor.call(
												`elements.paste`,
												window.localStorage.copyElementID,
												message._id
											);
										}}
									>
										Paste
									</button>
								)}
							</div>
							<div className="table-action-links right">
								<span className="label">Templates</span>
								<LoadTemplateButton message={message} />
								<button
									className="action-link"
									onClick={(event) => {
										event.preventDefault();
										setSaveElementTemplateModalOpen(true);
									}}
								>
									Save
								</button>
								<button
									className="action-link"
									onClick={(event) => {
										setDeleteElementTemplateModalOpen(true);
									}}
								>
									Delete
								</button>
							</div>
						</div>
					</div>
				</Grid>
				<Grid item xs={4}>
					<div className={"context"}>
						<Collapsible title="Writing Prompt">
							<p>{shapeData.writingPrompt || "[[prompt not entered]]"}</p>
						</Collapsible>
						{message.architectsNotes && (
							<Collapsible title="Architect’s Notes">
								<p>{message.architectsNotes}</p>
							</Collapsible>
						)}
						<Collapsible title="Options">{optionsContent}</Collapsible>
					</div>
				</Grid>
			</Grid>
			{elementToEdit && (
				<ElementModalForm
					shapeData={shapeData}
					message={message}
					shapeID={shapeID}
					show={show}
					hide={hide}
					comments={comments}
					element={elementToEdit}
					elements={elements}
				/>
			)}
			{saveElementTemplateModalOpen && (
				<SaveElementTemplateModal
					elements={elements}
					show={saveElementTemplateModalOpen}
					hide={() => {
						setSaveElementTemplateModalOpen(false);
					}}
				/>
			)}
			{deleteElementTemplateModalOpen && (
				<DeleteElementTemplateModal
					show={deleteElementTemplateModalOpen}
					hide={() => {
						setDeleteElementTemplateModalOpen(false);
					}}
				/>
			)}
			{elementToRename && (
				<ElementRenameModal
					elementToRename={elementToRename}
					hide={() => {
						setElementToRename(null);
					}}
				/>
			)}
		</>
	);
};

const LoadTemplateButton = ({ message }) => {
	const [templateMenuOpen, setTemplateMenuOpen] = useState(false);
	const [referenceElement, setReferenceElement] = useState(null);
	const [popperElement, setPopperElement] = useState(null);
	const popper = usePopper(referenceElement, popperElement, {});

	const { loading, templates } = useTracker(() => {
		const sub = Meteor.subscribe("elementTemplates", Meteor.user().projectID);
		if (!sub.ready()) {
			return { loading: true, templates: [] };
		}

		return {
			templates: ElementTemplates.find({
				projectID: Meteor.user().projectID,
			}).fetch(),
			loading: false,
		};
	});

	return (
		<>
			<button
				className="action-link"
				ref={setReferenceElement}
				onClick={(event) => {
					setTemplateMenuOpen(!templateMenuOpen);
				}}
			>
				Load
			</button>
			{templateMenuOpen && (
				<div
					ref={setPopperElement}
					style={{ ...popper.styles.popper, zIndex: 2, background: "white" }}
					{...popper.attributes.popper}
				>
					<RCMenu>
						{loading && (
							<RCMenuItem disabled={true}>
								<BeatLoader color="#2f3d47" />
							</RCMenuItem>
						)}
						{templates.map((template) => {
							return (
								<RCMenuItem
									key={template._id}
									onClick={() => {
										setTemplateMenuOpen(false);
										for (const element of template.elements) {
											Meteor.call(`elements.insert`, {
												...element,
												messageID: message._id,
											});
										}
									}}
									style={{ padding: 12, cursor: "pointer" }}
								>
									{template.name}
								</RCMenuItem>
							);
						})}
					</RCMenu>
				</div>
			)}
		</>
	);
};

const ElementRenameModal = ({ elementToRename, hide }) => {
	const [newName, setNewName] = useState(elementToRename?.name || "");

	const save = () => {
		Meteor.call("elements.update", elementToRename._id, {
			name: newName,
		});
		hide();
	};

	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!elementToRename}
			hide={hide}
		>
			<form onSubmit={save}>
				<InputControlRHF
					type="text"
					name="name"
					label="Name"
					value={newName}
					onChange={(evt) => {
						setNewName(evt.target.value);
					}}
				/>
				<div className="button-wrap">
					<Button text="Save" type={"submit"} />
					<Button
						text="Cancel"
						onClick={() => {
							hide();
						}}
					/>
				</div>
			</form>
		</Modal>
	);
};

export default ElementsModal;
