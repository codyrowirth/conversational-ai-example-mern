import React, { useState } from "react";
import Modal from "../../../../lib/components/Modal/Modal";
import debounce from "lodash.debounce";
import { usePrevious } from "../../../../lib/utilities/usePrevious";
import { useTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import { PropagateLoader } from "react-spinners";
import BrancherRules from "../../../../../api/collections/BrancherRules";
import Attributes from "../../../../../api/collections/Attributes";
import Options from "../../../../../api/collections/Options";
import Select from "react-select";
import MaterialButton from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import TextField from "@mui/material/TextField";
import Slider from "@mui/material/Slider";
import "flexboxgrid/dist/flexboxgrid.min.css";
import without from "lodash.without";
import { saveOnBlur } from "../../../../lib/utilities/saveOnBlurRHFMeteor";
import BrancherLines from "../../../../../api/collections/BrancherLines";
import Button from "../../../../lib/components/Button/Button";
import ToggleControlRHF from "../../../../lib/components/Form/ToggleControlRHF";
import BitCaseRules from "../../../../../api/collections/BitCaseRules";
import BitCases from "../../../../../api/collections/BitCases";
import BrancherRuleGroups from "../../../../../api/collections/BrancherRuleGroups";
import BitCaseRuleGroups from "../../../../../api/collections/BitCaseRuleGroups";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";

import "./BrancherRulesModal.scss";
import { Random } from "meteor/random";

const BrancherRulesModal = ({
	rulesModalOpen,
	setRulesModalOpen,
	setAddAttributeModalOpen,
	shapeID,
	lines,
	grandparent,
}) => {
	const hide = () => {
		setRulesModalOpen(false);
	};

	return (
		<Modal show={!!rulesModalOpen} hide={hide} className="modal end-buttons">
			<GreyBox className="in-modal related-links">
				<div className="action-links">
					<span className="label">Related</span>
					<button
						className="action-link"
						onClick={(event) => {
							event.preventDefault();
							setAddAttributeModalOpen(true);
							setRulesModalOpen(false);
						}}
					>
						Manage Attributes
					</button>
				</div>
			</GreyBox>
			<h3>Manage Rules</h3>
			<div className="branch-rules">
				{lines?.map((line) => {
					return (
						<div key={line?.brancherLine?.lineID} className="branch-rule">
							<h4>{line.destinationShape?.data?.label || "[[unnamed]]"}</h4>
							<ToggleControlRHF
								name={"ifElse"}
								labelLeft="If"
								labelRight={"Else"}
								error={{}}
								checked={line?.brancherLine?.else || false}
								onChange={() => {
									Meteor.call(`brancherLines.update`, line.brancherLine._id, {
										else: !line.brancherLine.else,
									});
								}}
							/>
							{!line?.brancherLine?.else && (
								<BrancherRuleContainer
									lineID={line.brancherLine.lineID}
									type={"brancher"}
									shapeID={shapeID}
									line={line}
									grandparent={grandparent}
								/>
							)}
						</div>
					);
				})}
			</div>
			<div className="button-wrap">
				<Button text="Close" onClick={hide} />
			</div>
		</Modal>
	);
};

const ReactSelectTheme = (theme) => ({
	...theme,
	colors: {
		...theme.colors,
		primary: "var(--red)",
		primary25: "#F0BCBF",
	},
});

export const ReactSelectStyles = {
	menu: (styles) => Object.assign(styles, { zIndex: 99996 }),
	option: (styles) => Object.assign(styles, { cursor: "pointer" }),
	singleValue: (styles) => Object.assign(styles, { overflow: "visible" }),
};

const noOptionFields = ["Is Empty", "Is Not Empty"];

const textInputFields = [
	"Less",
	"Less Or Equal",
	"Greater",
	"Greater Or Equal",
	"Begins With",
	"Not Begins With",
	"Contains",
	"Not Contains",
	"Ends With",
	"Not Ends With",
];

const sliderFields = ["Between", "Not Between"];

export const BrancherRuleContainer = ({
	lineID,
	type,
	shapeID,
	grandparent,
}) => {
	const rulesModel = BrancherRules;
	const rulesGroupModel = BrancherRuleGroups;
	const parentModel = BrancherLines;

	const { attributes, isLoading } = useTracker(() => {
		const attributesSubscription = Meteor.subscribe(
			"attributesByIDWithOptions",
			grandparent.attributes || []
		);
		const parentSub = Meteor.subscribe(`${parentModel._name}`, lineID);

		if (!attributesSubscription.ready() || !parentSub.ready()) {
			return { isLoading: true, attributes: null };
		}

		const attributes = Attributes.find({
			_id: { $in: grandparent.attributes || [] },
		}).fetch();

		for (const attribute of attributes) {
			attribute.options = Options.find({
				attributeID: attribute._id,
			}).fetch();
		}

		const parent = parentModel.findOne({ lineID });

		if (!parent) {
			Meteor.call(`${parentModel._name}.insert`, {
				order: 0,
				lineID,
				chainedOperator: "and",
			});

			return { isLoading: true };
		}

		return { isLoading: false, attributes };
	});

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<div>
			<RuleGroup
				attributes={attributes}
				index={0}
				rulesModel={rulesModel}
				ruleGroupsModel={rulesGroupModel}
				parentID={lineID}
				parentModel={parentModel}
				parentKey="lineID"
			/>
		</div>
	);
};

export const BitCaseRuleContainer = ({
	bitCase,
	type,
	shapeID,
	grandparent,
	message,
}) => {
	const rulesModel = BitCaseRules;
	const parentModel = BitCases;
	const ruleGroupsModel = BitCaseRuleGroups;

	const { attributes, isLoading } = useTracker(() => {
		const loading = {
			isLoading: true,
			attributes: null,
		};

		const attributesSubscription = Meteor.subscribe(
			"attributesByIDWithOptions",
			message.attributes || []
		);

		if (!attributesSubscription.ready()) {
			return loading;
		}

		const attributes = Attributes.find({
			_id: { $in: message.attributes || [] },
		}).fetch();

		for (const attribute of attributes) {
			attribute.options = Options.find({
				attributeID: attribute._id,
			}).fetch();
		}

		return { isLoading: false, attributes };
	});

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<div
			style={{
				marginLeft: 20,
				marginRight: 20,
				marginTop: 10,
				marginBottom: 10,
			}}
		>
			<RuleGroup
				attributes={attributes}
				index={0}
				rulesModel={rulesModel}
				parentID={bitCase._id}
				parentModel={parentModel}
				ruleGroupsModel={ruleGroupsModel}
				parentKey="bitCaseID"
			/>
		</div>
	);
};

const RuleGroup = ({
	depth = 0,
	index,
	attributes,
	rulesModel,
	parentModel,
	ruleGroupsModel,
	parentID,
	nestedGroup,
	parentKey,
}) => {
	const [checkedBoxes, setCheckedBoxes] = useState([]);

	const addRule = async (rules) => {
		if (nestedGroup) {
			Meteor.call(`${rulesModel._name}.insert`, {
				parentGroupID: nestedGroup._id,
				[parentKey]: parentID,
				order: rules.length,
			});
		} else {
			Meteor.call(`${rulesModel._name}.insert`, {
				[parentKey]: parentID,
				order: rules.length,
			});
		}
	};

	const { rules, parent, isLoading } = useTracker(() => {
		const rulesSub = Meteor.subscribe(`${rulesModel._name}`, parentID);
		const ruleGroupsSub = Meteor.subscribe(
			`${ruleGroupsModel._name}`,
			parentID
		);

		let parentSub;
		parentSub = Meteor.subscribe(`${parentModel._name}`, parentID);

		if (!rulesSub.ready() || !ruleGroupsSub.ready() || !parentSub.ready()) {
			return { isLoading: true };
		}

		const rules = rulesModel
			.find({ [parentKey]: parentID, parentGroupID: nestedGroup?._id })
			.fetch();
		const ruleGroups = ruleGroupsModel
			.find({ [parentKey]: parentID, parentGroupID: nestedGroup?._id })
			.fetch();
		let parent;
		if (rulesModel._name === "brancherRules") {
			parent = parentModel.findOne({ lineID: parentID });
		} else {
			parent = parentModel.findOne({ _id: parentID });
		}

		const combinedRules = [
			...rules,
			...ruleGroups.map((group) => ({ ...group, group: true })),
		].sort((a, b) => a.order - b.order);

		if (combinedRules.length === 0) {
			addRule([]);
			return { isLoading: true };
		}

		return { isLoading: false, rules: combinedRules, parent };
	});

	if (isLoading) {
		return <div />;
	}

	const deleteRows = async () => {
		const rulesLeftInGroup = rules.length - checkedBoxes.length;
		if (nestedGroup && rulesLeftInGroup === 0) {
			await Meteor.callPromise(
				`${ruleGroupsModel._name}.remove`,
				nestedGroup._id
			);
		}
		for (const checkedBox of checkedBoxes) {
			Meteor.call(`${rulesModel._name}.remove`, checkedBox);
		}
		setCheckedBoxes([]);
	};

	const toggleChainedOperator = async () => {
		const newValue = chainedOperator === "and" ? "or" : "and";

		if (nestedGroup) {
			Meteor.call(`${ruleGroupsModel._name}.update`, nestedGroup._id, {
				chainedOperator: newValue,
			});
		} else {
			Meteor.call(`${parentModel._name}.update`, parent._id, {
				chainedOperator: newValue,
			});
		}
	};

	const group = async () => {
		const newGroupID = Random.id();

		const toMove = rules
			.filter((rule) => {
				return !rule.group && checkedBoxes.includes(rule._id);
			})
			.map((rule, index) => {
				return {
					_id: rule._id,
					parentGroupID: newGroupID,
					order: index,
				};
			});

		const remainingRules = rules
			.filter((rule) => !checkedBoxes.includes(rule._id))
			.map((rule, index) => {
				return { ...rule, group: rule.group, order: index };
			});

		const rulesToUpdate = [
			...toMove,
			...remainingRules.filter((row) => !row.group),
		];
		const groupsToUpdate = remainingRules.filter((row) => row.group);

		let toUpdate = [];

		for (const rule of rulesToUpdate) {
			toUpdate.push(
				Meteor.callPromise(`${rulesModel._name}.update`, rule._id, rule)
			);
		}
		for (const rule of groupsToUpdate) {
			toUpdate.push(
				Meteor.callPromise(`${ruleGroupsModel._name}.update`, rule._id, rule)
			);
		}

		await Promise.all(toUpdate);
		setCheckedBoxes([]);
		const newGroup = await Meteor.callPromise(
			`${ruleGroupsModel._name}.insert`,
			{
				_id: newGroupID,
				[parentKey]: parentID,
				order: rules.length - checkedBoxes.length,
				chainedOperator: "and",
			}
		);
	};

	let style = { display: "flex" };
	if (nestedGroup) {
		style.border = "1px solid black";

		if (rules?.length === 1) {
			style.borderRadius = "10px 10px 10px 10px";
		} else if (index === 0) {
			style.borderRadius = "10px 10px 0 0";
		} else if (index === rules?.length - 1) {
			style.borderRadius = "0 0 10px 10px";
		}

		style.paddingTop = 5;
		style.paddingRight = 5;
		style.paddingLeft = 5;
		style.paddingBottom = 10;
	}

	const chainedOperator =
		nestedGroup?.chainedOperator || parent?.chainedOperator;

	return (
		<div className={"row"} style={style}>
			<div className={`col-xs-${rules?.length === 1 ? 12 : 11}`}>
				{rules.map((rule, ruleIndex) => {
					if (rule.group) {
						return (
							<RuleGroup
								attributes={attributes}
								rulesModel={rulesModel}
								parentID={parentID}
								parentModel={parentModel}
								ruleGroupsModel={ruleGroupsModel}
								depth={depth + 1}
								nestedGroup={rule}
								key={rule._id}
								parentKey={parentKey}
							/>
						);
					}
					return (
						<RuleRow
							key={rule._id}
							rules={rules}
							rule={rule}
							ruleIndex={ruleIndex}
							attributes={attributes}
							checkedBoxes={checkedBoxes}
							setCheckedBoxes={setCheckedBoxes}
							nestedGroup={nestedGroup}
							rulesModel={rulesModel}
						/>
					);
				})}
			</div>
			{rules?.length === 1 ? null : (
				<div
					className={`col-xs-1`}
					style={{
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
						flex: 1,
						cursor: "pointer",
					}}
					onClick={toggleChainedOperator}
				>
					<div
						style={{
							display: "flex",
							padding: 5,
							backgroundColor: "lightGray",
							borderRadius: 4,
							boxShadow: "rgba(0,0,0,.3) 2px 2px 2px",
						}}
					>
						<span
							style={{
								display: "flex",
								padding: 5,
								color: chainedOperator === "and" ? "white" : "black",
								background: chainedOperator === "and" ? "var(--red)" : "",
								borderRadius: 4,
								border: chainedOperator === "and" ? "1px solid white" : "",
							}}
						>
							and
						</span>
						<span
							style={{
								display: "flex",
								padding: 5,
								color: chainedOperator === "or" ? "white" : "black",
								background: chainedOperator === "or" ? "var(--red)" : "",
								borderRadius: 4,
								border: chainedOperator === "or" ? "1px solid white" : "",
							}}
						>
							or
						</span>
					</div>
				</div>
			)}
			<div
				className={`col-xs-${rules?.length === 1 ? 12 : 11}`}
				style={{ textAlign: "center", marginTop: 10 }}
			>
				{checkedBoxes.length && depth < 2 ? (
					<MaterialButton
						style={{ marginLeft: 10, marginRight: 10, maxWidth: 225 }}
						variant="contained"
						color="secondary"
						fullWidth={true}
						onClick={group}
					>
						Group
					</MaterialButton>
				) : null}
				{checkedBoxes.length ? (
					<MaterialButton
						style={{ marginLeft: 10, marginRight: 10, maxWidth: 225 }}
						variant="contained"
						color="secondary"
						fullWidth={true}
						onClick={deleteRows}
					>
						Delete
					</MaterialButton>
				) : null}
				<MaterialButton
					style={{ marginLeft: 10, marginRight: 10, maxWidth: 225 }}
					fullWidth={true}
					variant="contained"
					onClick={() => {
						addRule(rules);
					}}
				>
					Add Rule{nestedGroup ? " to Group" : null}
				</MaterialButton>
			</div>
		</div>
	);
};

const RuleRow = ({
	rules,
	ruleIndex,
	rule,
	nestedGroup = false,
	attributes,
	checkedBoxes,
	setCheckedBoxes,
	rulesModel,
}) => {
	let rowStyle = {};

	if (rules?.length === 1) {
		//single rule case
		rowStyle = {
			paddingTop: 10,
			paddingBottom: 10,
			background: "white",
			borderLeft: "1px solid #343d47",
			borderRight: "1px solid #343d47",
			borderTop: "1px solid #343d47",
			borderBottom: "1px solid #343d47",
			borderRadius: "4px 4px 4px 4px",
		};
	} else if (ruleIndex === 0) {
		//first rule in group
		rowStyle = {
			marginTop: 5,
			paddingTop: 5,
			paddingBottom: 10,
			background: "white",
			borderTop: "1px solid #343d47",
			borderLeft: "1px solid #343d47",
			borderRight: "1px solid #343d47",
			borderRadius: "4px 4px 0 0",
			borderBottom: "1px solid gray",
		};
	} else if (ruleIndex + 1 === rules?.length) {
		//last rule in group
		rowStyle = {
			paddingTop: 10,
			paddingBottom: 10,
			background: "white",
			borderBottom: "1px solid #343d47",
			borderLeft: "1px solid #343d47",
			borderRight: "1px solid #343d47",
			borderRadius: "0 0 4px 4px",
		};
	} else {
		//any middle rules
		rowStyle = {
			paddingTop: 10,
			paddingBottom: 10,
			background: "white",
			borderLeft: "1px solid #343d47",
			borderRight: "1px solid #343d47",
			borderBottom: "1px solid gray",
		};
	}

	return (
		<div className={"row"} style={rowStyle}>
			<div
				className={`col-xs-1`}
				style={{ marginRight: -10, display: "flex", alignItems: "center" }}
			>
				{/*<OutOfGroup*/}
				{/*	nestedGroup={nestedGroup}*/}
				{/*	bitCaseRule={bitCaseRule}*/}
				{/*	rules={rules}*/}
				{/*/>*/}
				<GroupCheckbox
					rule={rule}
					checkedBoxes={checkedBoxes}
					setCheckedBoxes={setCheckedBoxes}
				/>
			</div>
			<div className={`col-xs-4`} style={{ paddingRight: 10 }}>
				<AttributeSelector
					rule={rule}
					attributes={attributes}
					rulesModel={rulesModel}
				/>
			</div>
			{rule?.attributeID && (
				<div className={`col-xs-3`} style={{ paddingRight: 10 }}>
					<EqualitySelector rule={rule} rulesModel={rulesModel} />
				</div>
			)}
			{rule?.operator && (
				<div className={`col-xs-4`}>
					<OptionField
						rule={rule}
						attributes={attributes}
						rulesModel={rulesModel}
					/>
				</div>
			)}
		</div>
	);
};

const GroupCheckbox = ({ checkedBoxes, setCheckedBoxes, rule }) => {
	return (
		<Checkbox
			checked={checkedBoxes.includes(rule._id)}
			onChange={(evt) => {
				if (evt.target.checked) {
					setCheckedBoxes([...checkedBoxes, rule._id]);
				} else {
					setCheckedBoxes(without(checkedBoxes, rule._id));
				}
			}}
		/>
	);
};

// const OutOfGroup = ({ nestedGroup, bitCaseRule, rules }) => {
// 	if (!nestedGroup) {
// 		return null;
// 	}
// 	return (
// 		<ArrowBackIcon
// 			style={{ cursor: "pointer" }}
// 			onClick={async () => {
// 				const otherRules = rules.filter(
// 					(rule) => rule.id !== bitCaseRule.id
// 				);
// 				// await client.service("bitCaseRule").patch(bitCaseRule.id, {
// 				// 	parentGroupID: null,
// 				// 	bitCaseID: nestedGroup.bitCaseID,
// 				// });
// 			}}
// 		/>
// 	);
// };

const AttributeSelector = ({ attributes, rule, rulesModel }) => {
	let attributeValueObj = attributes.find((attribute) => {
		return attribute._id === rule.attributeID;
	});

	return (
		<Select
			theme={ReactSelectTheme}
			styles={ReactSelectStyles}
			onChange={async (value) => {
				let toSave = { attributeID: value.value };
				if (!rule.operator) {
					toSave.operator = "Equal To";
				}
				Meteor.call(`${rulesModel._name}.update`, rule._id, toSave);
			}}
			options={attributes.map((attribute) => {
				return {
					value: attribute._id,
					label: attribute.name,
				};
			})}
			value={
				attributeValueObj && {
					value: attributeValueObj._id,
					label: attributeValueObj.name,
				}
			}
			menuPlacement="auto"
		/>
	);
};

const EqualitySelector = ({ rule, rulesModel }) => {
	const operators = [
		"Equal To",
		"Not Equal To",
		"Less",
		"Less Or Equal",
		"Greater",
		"Greater Or Equal",
		"Between",
		"Not Between",
		"Begins With",
		"Not Begins With",
		"Contains",
		"Not Contains",
		"Ends With",
		"Not Ends With",
		"Is Empty",
		"Is Not Empty",
	];

	return (
		<Select
			theme={ReactSelectTheme}
			options={operators.map((operator) => ({
				label: operator,
				value: operator,
			}))}
			styles={ReactSelectStyles}
			value={
				rule.operator && {
					value: rule.operator,
					label: rule.operator,
				}
			}
			onChange={async (value) => {
				if (noOptionFields.includes(value.value)) {
					Meteor.call(`${rulesModel._name}.update`, rule._id, {
						operator: value.value,
						optionValue: null,
						optionID: null,
					});
				} else if (textInputFields.includes(value.value)) {
					Meteor.call(`${rulesModel._name}.update`, rule._id, {
						operator: value.value,
						optionID: null,
					});
				} else if (sliderFields.includes(value.value)) {
					Meteor.call(`${rulesModel._name}.update`, rule._id, {
						operator: value.value,
						optionID: null,
					});
				} else {
					Meteor.call(`${rulesModel._name}.update`, rule._id, {
						operator: value.value,
						optionValue: null,
					});
				}
			}}
			menuPlacement="auto"
		/>
	);
};

const optionFieldSaveDebounce = debounce(
	async (value, rule, rulesModel, setSaveValue, saveValueRef) => {
		// await client
		// 	.service("bitCaseRule")
		// 	.patch(bitCaseRule.id, { optionValue: value });

		Meteor.call(`${rulesModel._name}.update`, rule._id, { optionValue: value });

		if (saveValueRef.current === value) {
			setSaveValue(null);
		}
	},
	500
);

const OptionField = ({ rule, attributes, rulesModel }) => {
	const [optionValue, setOptionValue] = useState();
	const saveValueRef = usePrevious(optionValue);

	const attributeValueObj = attributes.find((attribute) => {
		return attribute._id === rule.attributeID;
	});

	const { type } = attributeValueObj || "";

	if (noOptionFields.includes(rule.operator)) {
		return null;
	} else if (
		textInputFields.includes(rule.operator) ||
		type === "retrieved" ||
		type === "raw"
	) {
		return (
			<TextField
				variant="outlined"
				inputProps={{ style: { padding: 10 } }}
				value={optionValue || rule.optionValue || ""}
				onChange={(evt) => {
					setOptionValue(evt.target.value);
				}}
				name={"optionValue"}
				onBlur={(evt) => {
					saveOnBlur(rulesModel._name, rule._id, evt);
				}}
			/>
		);
	} else if (sliderFields.includes(rule.operator)) {
		const numberOptions = attributeValueObj.options
			.map((option) => parseFloat(option.name))
			.filter((option) => !isNaN(option));

		const rangeMax = numberOptions.length ? Math.max(...numberOptions) : 10;

		let value;
		if (optionValue) {
			value = optionValue;
		} else if (
			rule?.optionValue &&
			typeof rule.optionValue === "string" &&
			RegExp("\\d+\\-\\d+").test(rule.optionValue)
		) {
			value = rule.optionValue.split("-").map((num) => parseInt(num));
		} else {
			value = [0, rangeMax];
		}

		return (
			<div
				style={{
					paddingLeft: 15,
					paddingRight: 15,
					paddingTop: 25,
					marginBottom: -20,
				}}
			>
				<Slider
					value={value}
					onChange={(evt, newValue) => {
						setOptionValue(newValue);
						optionFieldSaveDebounce(
							newValue.join("-"),
							rule,
							rulesModel,
							setOptionValue,
							saveValueRef
						);
					}}
					min={0}
					max={rangeMax}
					valueLabelDisplay="on"
				/>
			</div>
		);
	} else if (type === "direct" || type === "calculated") {
		const optionObj = attributeValueObj?.options.find((option) => {
			return option._id === rule?.optionID;
		});

		return (
			<Select
				styles={ReactSelectStyles}
				theme={ReactSelectTheme}
				options={attributeValueObj.options.map((option) => ({
					label: option.name,
					value: option._id,
				}))}
				value={optionObj && { value: optionObj.id, label: optionObj.name }}
				onChange={async (value) => {
					setOptionValue(value);
					Meteor.call(`${rulesModel._name}.update`, rule._id, {
						optionID: value.value,
					});
					setOptionValue(null);
				}}
				menuPlacement="auto"
			/>
		);
	} else {
		return <p>Error - This attribute type has not been implemented yet</p>;
	}
};

export default BrancherRulesModal;
