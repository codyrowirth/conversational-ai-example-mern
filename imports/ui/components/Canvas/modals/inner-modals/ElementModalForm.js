import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import InputRHF from "../../../../lib/components/Form/InputRHF";
import Button from "../../../../lib/components/Button/Button";
import React, { createContext, useContext, useState } from "react";
import { useForm } from "react-hook-form";
import { UnmountClosed } from "react-collapse";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import ElementContent from "../../../Elements/ElementContent";
import { saveOnBlur } from "../../../../lib/utilities/saveOnBlurRHFMeteor";
import { useTracker } from "meteor/react-meteor-data";
import BitCases from "../../../Elements/BitCases";
import AppContext from "../../../App/AppContext";
import { useSnackbar } from "notistack";
import Options from "../../../../../api/collections/Options";
import Loading from "../../../Sandbox/Loading";
import InputControlRHF from "../../../../lib/components/Form/InputControlRHF";
import Grid from "@mui/material/Grid";
import "./ElementModalForm.scss";
export const ElementContext = createContext({});

const ElementModalForm = ({
	show,
	hide,
	comments,
	element,
	message,
	shapeID,
	shapeData,
	elements,
}) => {
	const [openBitID, setOpenBitID] = useState();
	const { setCopyBitID } = useContext(AppContext);
	const { enqueueSnackbar } = useSnackbar();

	const onCancel = () => {
		reset();
		hide();
	};

	return (
		<ElementContext.Provider value={{ openBitID, setOpenBitID }}>
			<div>
				<div className="element-container">
					<h4>Element: {element.name}</h4>
					<div className="element-content">
						<div className="text-content">
							<ElementContent
								key="elementContent"
								element={element}
								elements={elements}
								message={message}
							/>
						</div>
						{/*<div className="image-content">*/}
						{/*	<div className="element-image-preview">*/}
						{/*		<img src="/images/placeholder.png" alt="Image preview" />*/}
						{/*	</div>*/}
						{/*	<button*/}
						{/*		className="action-link"*/}
						{/*		onClick={(event) => {*/}
						{/*			event.preventDefault();*/}
						{/*			alert("TO DO");*/}
						{/*		}}*/}
						{/*	>*/}
						{/*		Delete*/}
						{/*	</button>*/}
						{/*	<form>*/}
						{/*		<div className="grid-row">*/}
						{/*			<div className="grid-col-full">*/}
						{/*				<InputRHF*/}
						{/*					type="text"*/}
						{/*					name="className"*/}
						{/*					label="Class Name"*/}
						{/*					defaultValue={element?.className}*/}
						{/*					register={register}*/}
						{/*					error={errors}*/}
						{/*				/>*/}
						{/*			</div>*/}
						{/*		</div>*/}
						{/*	</form>*/}
						{/*</div>*/}
					</div>
				</div>
				{openBitID && (
					<div className={"element-bitcases element-container"}>
						<Grid container>
							<Grid xs={8} item>
								<h4>Bit Cases</h4>
							</Grid>
							<Grid xs={4} item className={"copy-bit-button"}>
								<button
									className="action-link"
									onClick={() => {
										setCopyBitID(openBitID);

										const findNode = (id, currentNode) => {
											let i, currentChild, result;

											if (id === currentNode.bitID) {
												return currentNode;
											} else {
												// Use a for loop instead of forEach to avoid nested functions
												// Otherwise "return" will not work properly
												if (currentNode.children) {
													for (i = 0; i < currentNode.children.length; i += 1) {
														currentChild = currentNode.children[i];

														// Search in the current child
														result = findNode(id, currentChild);

														// Return the result if the node has been found
														if (result !== false) {
															return result;
														}
													}
												}

												// The node has not been found and we have no more options
												return false;
											}
										};

										const content = element.content;

										let matchingNode = { text: "pastedBit" };
										for (const paragraph of content) {
											matchingNode = findNode(openBitID, paragraph);
											break;
										}
										window.localStorage.copyBitID = openBitID;
										window.localStorage.copyBitText = matchingNode.label;

										enqueueSnackbar(
											"Bit Copied copied. To paste, click the clipboard icon in your destination element content.",
											{
												variant: "success",
												key: "elementCopied",
											}
										);
									}}
								>
									Copy Bit
								</button>
							</Grid>
						</Grid>

						<BitCases
							openBitID={openBitID}
							message={message}
							elements={elements}
							element={element}
						/>
					</div>
				)}
				{/*<div className="element-interaction">*/}
				{/*	<h4>Interaction</h4>*/}
				{/*	<button*/}
				{/*		className="action-link notif-link"*/}
				{/*		onClick={(event) => {*/}
				{/*			event.preventDefault();*/}
				{/*			alert("TO DO");*/}
				{/*		}}*/}
				{/*	>*/}
				{/*		<span className="notif-type">Comments</span>*/}
				{/*		{comments > 0 ? ` (${comments})` : ""}*/}
				{/*	</button>*/}
				{/*</div>*/}
			</div>
		</ElementContext.Provider>
	);
};
export default ElementModalForm;
