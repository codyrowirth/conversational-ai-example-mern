import React, { useState } from "react";
import Modal from "../../../../lib/components/Modal/Modal";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import NewAttributeForm from "../../../AttributesManager/NewAttributeForm";
import NestedData from "../../../../lib/components/NestedData/NestedData";
import Button from "../../../../lib/components/Button/Button";

const AddAttributeModal = ({
	shapeID,
	show,
	hide,
	parentType,
	setAddAttributeModalOpen,
	setRulesModalOpen,
}) => {
	const [newAttributeFormExpanded, setNewAttributeFormExpanded] =
		useState(false);

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			{parentType === "brancher" && (
				<GreyBox className="in-modal related-links">
					<div className="action-links">
						<span className="label">Related</span>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setRulesModalOpen(true);
								setAddAttributeModalOpen(false);
							}}
						>
							Manage Rules
						</button>
					</div>
				</GreyBox>
			)}
			<div className="heading-with-button flex">
				<div>
					<h3>Manage Attributes</h3>
				</div>
				<div>
					<Button
						text={newAttributeFormExpanded ? "Cancel" : "Add New Attribute"}
						icon={newAttributeFormExpanded ? "times" : "plus"}
						onClick={() => {
							setNewAttributeFormExpanded(!newAttributeFormExpanded);
						}}
					/>
				</div>
			</div>
			<NewAttributeForm
				newAttributeFormExpanded={newAttributeFormExpanded}
				setNewAttributeFormExpanded={setNewAttributeFormExpanded}
			/>
			<NestedData
				type="attribute"
				add={true}
				shapeID={shapeID}
				parentType={parentType}
			/>
			<div className="button-wrap">
				<Button text="Close" onClick={hide} />
			</div>
		</Modal>
	);
};

export default AddAttributeModal;
