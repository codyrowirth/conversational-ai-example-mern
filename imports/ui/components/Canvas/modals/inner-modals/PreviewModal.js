import React, { useState } from "react";
import { PropagateLoader } from "react-spinners";
import { useForm, Controller } from "react-hook-form";
import useWinWidth from "../../../../lib/utilities/useWinWidth";

import Modal from "../../../../lib/components/Modal/Modal";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import Button from "../../../../lib/components/Button/Button";
import { useTracker } from "meteor/react-meteor-data";
import Message from "../../../../../api/collections/Messages";
import Attributes from "../../../../../api/collections/Attributes";
import CheckboxControlRHF from "../../../../lib/components/Form/CheckboxControlRHF";
import { Meteor } from "meteor/meteor";
import Options from "../../../../../api/collections/Options";
import elements from "../../../../../api/collections/Elements";
import Elements from "../../../../../api/collections/Elements";
import InputControlRHF from "../../../../lib/components/Form/InputControlRHF";

const PreviewModal = ({ show, hide, shapeID }) => {
	const [previewResults, setPreviewResults] = useState(false);
	const screenWidth = useWinWidth();
	const [input, setInput] = useState({});
	const [selectedElements, setSelectedElements] = useState({});
	const [content, setContent] = useState([]);

	const { loading, attributes, message, elements } = useTracker(() => {
		const subscription = Meteor.subscribe("messagesByShapeID", shapeID);

		if (!subscription.ready()) {
			return { loading: true };
		}

		const messagesDB = Message.find({ shapeID }).fetch();

		if (messagesDB.length === 0) {
			return { loading: true };
		}

		const message = messagesDB[0];

		const attributesSubscription = Meteor.subscribe(
			"attributes",
			message.attributes || []
		);

		const elementsSub = Meteor.subscribe("elements", message._id);

		if (!attributesSubscription.ready() || !elementsSub.ready()) {
			return { loading: true };
		}

		const optionsSubscription = Meteor.subscribe(
			"optionsByAttributes",
			message.attributes || []
		);

		if (!optionsSubscription.ready()) {
			return { loading: true };
		}

		let attributes = Attributes.find({
			_id: { $in: message.attributes || [] },
		}).fetch();

		for (const attribute of attributes) {
			attribute.options = Options.find({ attributeID: attribute._id }).fetch();
		}

		return {
			loading: false,
			attributes,
			message: messagesDB[0],
			elements: Elements.find({ messageID: message._id }).fetch(),
		};
	});

	if (loading) {
		return (
			<Modal className="modal end-buttons" show={!!show} hide={hide}>
				<div style={{ textAlign: "center", paddingBottom: 20 }}>
					<PropagateLoader color="#2f3d47" />
				</div>
			</Modal>
		);
	}

	const onCancel = () => {
		setPreviewResults(false);
		hide();
	};

	const previewOrClear = async () => {
		if (previewResults) {
			setContent([]);
			setPreviewResults(false);
		} else {
			const url =
				Meteor.settings.public.ENV === "PROD"
					? `https://v4.api.tuzagtcs.com`
					: `http://localhost:3002`;

			const selectedElementsArr = [];
			for (const [element, selected] of Object.entries(selectedElements)) {
				if (selected) {
					selectedElementsArr.push(element);
				}
			}

			const dataToSend = {};
			for (const [element, selectValue] of Object.entries(input)) {
				dataToSend[element] = selectValue?.label || selectValue;
			}

			const rawResult = await fetch(`${url}/${message._id}`, {
				method: "POST",
				body: JSON.stringify({
					...dataToSend,
					limitToElementIDs: selectedElementsArr,
				}),
				headers: {
					"Content-Type": "application/json",
				},
			});
			const data = await rawResult.json();

			if (data.error) {
				alert(data.error);
			} else {
				let contentToSet = [];

				for (const [key, value] of Object.entries(data.result)) {
					if (
						!["checkboxes", "messageType", "trackingAttributes"].includes(key)
					) {
						contentToSet.push({ name: key, content: value });
					}
				}

				setContent(contentToSet);
				setPreviewResults(true);
			}
		}
	};

	const selectedElementsArr = [];
	for (const [element, selected] of Object.entries(selectedElements)) {
		if (selected) {
			selectedElementsArr.push(element);
		}
	}
	const allItemsChecked = selectedElementsArr.length === elements.length;

	return (
		<>
			<h3>Preview</h3>
			<section>
				<div className="grid-row wide-set">
					<div className="grid-col-half">
						<h4>Elements</h4>
						<table className="messages">
							{screenWidth >= 768 && (
								<colgroup>
									<col
										style={loading ? { width: "100%" } : { width: "72px" }}
									/>
									<col style={loading ? { width: "0" } : { width: "100%" }} />
								</colgroup>
							)}
							<thead>
								<tr>
									<th scope="col">
										{loading ? (
											""
										) : (
											<CheckboxControlRHF
												name="checkAll"
												onChange={(evt) => {
													let toSet = {};
													if (evt.target.checked) {
														elements.map((element) => {
															toSet[element._id] = true;
														});
													}
													setSelectedElements(toSet);
												}}
												checked={allItemsChecked}
											/>
										)}
									</th>
									<th scope="col">{loading ? "" : "Element"}</th>
								</tr>
							</thead>
							<tbody>
								{loading ? (
									<tr className="loader">
										<td>
											<PropagateLoader color="#2f3d47" />
										</td>
									</tr>
								) : (
									// TO DO: Use real data
									elements.map((element) => {
										return (
											<tr key={element._id}>
												<td data-label="Check">
													<CheckboxControlRHF
														element={element}
														checked={selectedElements[element._id]}
														onChange={(evt) => {
															setSelectedElements({
																...selectedElements,
																[element._id]: evt.target.checked,
															});
														}}
													/>
												</td>
												<td data-label="Element">
													<div className="action-links">{element.name}</div>
												</td>
											</tr>
										);
									})
								)}
							</tbody>
						</table>
					</div>
					<div className="grid-col-half">
						<h4>Attributes</h4>
						<form>
							{attributes.map((attribute) => {
								if (
									attribute.type === "retrieved" ||
									attribute.type === "raw"
								) {
									return (
										<div key={attribute._id} className="grid-row">
											<div className="grid-col-full">
												<InputControlRHF
													label={attribute.name}
													onChange={(evt) => {
														setInput({
															...input,
															[attribute.name]: evt.target.value,
														});
													}}
													value={input[attribute.name] || ""}
												/>
											</div>
										</div>
									);
								} else {
									return (
										<div key={attribute._id} className="grid-row">
											<div className="grid-col-full">
												<SelectRHF
													name={attribute._id}
													label={attribute.name}
													options={attribute.options.map((option) => ({
														value: option._id,
														label: option.name,
													}))}
													ref={null}
													value={input[attribute.name]}
													onChange={(value) => {
														setInput({ ...input, [attribute.name]: value });
													}}
												/>
											</div>
										</div>
									);
								}
							})}
						</form>
					</div>
				</div>
			</section>
			{previewResults === true && (
				<GreyBox className="in-modal no-bottom-spacing">
					{content.map((data) => (
						<div key={data.name} className="inner-box end-paragraph">
							<h5>{data.name}</h5>
							<p>{data.content}</p>
						</div>
					))}
				</GreyBox>
			)}
			<div className="button-wrap">
				<Button
					text={previewResults === true ? "Clear Content" : "Preview"}
					onClick={previewOrClear}
				/>
			</div>
		</>
	);
};

export default PreviewModal;
