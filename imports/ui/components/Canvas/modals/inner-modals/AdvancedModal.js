import React, { Fragment, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { UnmountClosed } from "react-collapse";
import { PropagateLoader } from "react-spinners";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useWinWidth from "../../../../lib/utilities/useWinWidth";

import Modal from "../../../../lib/components/Modal/Modal";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import ToggleControlRHF from "../../../../lib/components/Form/ToggleControlRHF";

import Button from "../../../../lib/components/Button/Button";

import DeleteAttributeModal from "./DeleteAttributeModal";
import UnlinkMessageModal from "./UnlinkMessageModal";
import AddMessageModal from "./AddMessageModal";
import CheckboxControlRHF from "../../../../lib/components/Form/CheckboxControlRHF";
import { saveOnChange } from "../../../../lib/utilities/saveOnBlurRHFMeteor";
import { useTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import MessageTrackingAttributes from "../../../../../api/collections/MessageTrackingAttributes";
import Attributes from "../../../../../api/collections/Attributes";
import Options from "../../../../../api/collections/Options";
import ElementsModal from "./ElementsModal";
import OptionsModal from "./OptionsModal";

const AdvancedModal = ({
	show,
	hide,
	comments,
	message,
	shapeID,
	shapeData,
	setAdvancedModal,
	elementsModal,
	setElementsModal,
	optionsModal,
	setOptionsModal,
}) => {
	const [deleteAttributeModal, setDeleteAttributeModal] = useState();
	const [unlinkMessageModal, setUnlinkMessageModal] = useState();
	const [addMessageModal, setAddMessageModal] = useState();
	// TO DO: Use rule state from DB as default value
	const [toggleOn, setToggleOn] = useState(false);
	const [rulesExpanded, setRulesExpanded] = useState(false);

	const { trackingAttributes, attributes, isLoading, messagesDB } = useTracker(
		() => {
			if (!message?._id) {
				return { isLoading: true };
			}

			const messageTrackingAttributesSub = Meteor.subscribe(
				"messageTrackingAttributes",
				message._id
			);

			const attributesSubscription = Meteor.subscribe(
				"attributesByIDWithOptions",
				message.attributes || []
			);
			if (
				!messageTrackingAttributesSub.ready() ||
				!attributesSubscription.ready()
			) {
				return { isLoading: true };
			}

			const attributes = Attributes.find({
				_id: { $in: message.attributes || [] },
			}).fetch();

			for (const attribute of attributes) {
				attribute.options = [
					...Options.find({
						attributeID: attribute._id,
					}).fetch(),
					{ _id: "clear", name: "[Clear]" },
					{
						_id: "increment",
						name: "[Increment]",
					},
				];
			}

			return {
				isLoading: false,
				trackingAttributes: MessageTrackingAttributes.find({
					messageID: message._id,
				}).fetch(),
				messagesDB: [],
				attributes,
			};
		}
	);

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const screenWidth = useWinWidth();

	const onSubmit = (data) => {
		// TO DO
	};

	const onCancel = () => {
		hide();
	};

	return (
		<>
			<h3>Advanced</h3>
			<p>
				Message ID: <code>{message?._id}</code>
			</p>
			<p>
				Shape ID: <code>{message?.shapeID}</code>
			</p>
			<section>
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<CheckboxControlRHF
								name="useCheckboxes"
								label="Use checkboxes on message?"
								error={{}}
								checked={message?.useCheckboxes || false}
								onChange={(evt) => {
									saveOnChange(
										"messages",
										message._id,
										evt.target.checked,
										evt.target
									);
								}}
							/>
							<CheckboxControlRHF
								name="forceSurvey"
								label="Force survey mode?"
								error={{}}
								checked={message?.forceSurvey || false}
								onChange={(evt) => {
									saveOnChange(
										"messages",
										message._id,
										evt.target.checked,
										evt.target
									);
								}}
							/>
						</div>
					</div>
				</form>
			</section>
			<section>
				<h4>Attributes</h4>
				<h5>Tracking Attributes</h5>
				<form>
					<div className="attributes-table">
						<table className="attributes table-toggle break-xl">
							{screenWidth >= 1200 && (
								<colgroup>
									<col
										style={isLoading ? { width: "100%" } : { width: "35%" }}
									/>
									<col style={isLoading ? { width: "0" } : { width: "35%" }} />
									<col
										style={isLoading ? { width: "0" } : { width: "150px" }}
									/>
									<col style={isLoading ? { width: "0" } : { width: "30%" }} />
								</colgroup>
							)}
							<thead>
								<tr>
									<th scope="col">{isLoading ? "" : "Attribute"}</th>
									<th scope="col">{isLoading ? "" : "Option"}</th>
									<th scope="col">{isLoading ? "" : "Rules"}</th>
									<th scope="col">{isLoading ? "" : "Actions"}</th>
								</tr>
							</thead>
							<tbody>
								{isLoading ? (
									<tr className="loader">
										<td>
											<PropagateLoader color="#2f3d47" />
										</td>
									</tr>
								) : (
									trackingAttributes.map((trackingAttribute, index) => {
										const rowClass = index % 2 === 0 ? "even" : "odd";

										let selectedAttribute, matchingAttribute, selectedOption;
										if (trackingAttribute.attributeID) {
											matchingAttribute = attributes.find(
												(attribute) =>
													attribute._id === trackingAttribute.attributeID
											);

											if (!matchingAttribute) {
												Meteor.call(`messages.update`, message._id, {
													attributes: [
														...message.attributes,
														trackingAttribute.attributeID,
													],
												});
												return (
													<td key={`fix${Math.random()}`}>
														Fixing a missing attribute for you, hold on...
													</td>
												);
											}

											selectedAttribute = {
												value: trackingAttribute._id,
												label: matchingAttribute.name,
											};

											const matchingOption = matchingAttribute.options.find(
												(option) => option._id === trackingAttribute.optionID
											);

											if (matchingOption) {
												selectedOption = {
													label: matchingOption.name,
													value: matchingOption._id,
												};
											} else {
												selectedOption = null;
											}
										}

										return (
											<Fragment key={trackingAttribute._id}>
												<tr className={`${rowClass}`}>
													<td data-label="Attribute">
														<SelectRHF
															name="attributeID"
															options={attributes.map((option) => ({
																value: option._id,
																label: option.name,
															}))}
															onChange={(value, fieldData) => {
																if (selectedOption) {
																	Meteor.call(
																		"messageTrackingAttributes.update",
																		trackingAttribute._id,
																		{ optionID: null }
																	);
																}
																saveOnChange(
																	"messageTrackingAttributes",
																	trackingAttribute._id,
																	value,
																	fieldData
																);
															}}
															value={selectedAttribute}
															error={errors}
														/>
													</td>
													<td data-label="Option">
														<SelectRHF
															name="optionID"
															options={
																matchingAttribute
																	? matchingAttribute.options.map((option) => ({
																			value: option._id,
																			label: option.name,
																	  }))
																	: []
															}
															error={errors}
															disabled={!trackingAttribute.attributeID}
															value={selectedOption}
															onChange={(value, fieldData) => {
																saveOnChange(
																	"messageTrackingAttributes",
																	trackingAttribute._id,
																	value,
																	fieldData
																);
															}}
														/>
													</td>
													<td data-label="Rules">
														<Controller
															name={`rulesToggle${trackingAttribute.id}`}
															control={control}
															render={({ field }) => {
																return (
																	<ToggleControlRHF
																		name={field.name}
																		{...field}
																		onChange={(event) => {
																			setToggleOn(event.target.checked);
																		}}
																		ref={null}
																		error={errors}
																		disabled={true}
																	/>
																);
															}}
														/>
													</td>
													<td data-label="Actions">
														<div className="action-links">
															<button
																className="action-link"
																onClick={(event) => {
																	event.preventDefault();
																	const toCopy = { ...trackingAttribute };
																	delete toCopy._id;
																	Meteor.call(
																		`messageTrackingAttributes.insert`,
																		toCopy
																	);
																}}
															>
																Duplicate
															</button>
															<button
																className="action-link"
																onClick={(event) => {
																	event.preventDefault();
																	setDeleteAttributeModal({
																		trackingAttribute,
																		matchingAttribute,
																	});
																}}
															>
																Delete
															</button>
														</div>
													</td>
												</tr>
												{toggleOn && (
													<tr className={`table-toggle-content ${rowClass}`}>
														<td colSpan="5">
															<button
																className="collapse-trigger"
																onClick={(event) => {
																	event.preventDefault();
																	setRulesExpanded(!rulesExpanded);
																}}
															>
																Rules
																<span className="arrow">
																	<FontAwesomeIcon
																		icon={
																			rulesExpanded
																				? "chevron-up"
																				: "chevron-down"
																		}
																	/>
																</span>
															</button>

															<UnmountClosed isOpened={rulesExpanded}>
																<div className="rules-panel">
																	<div className="rules-panel-inner">
																		Rule stuff goes here.
																	</div>
																</div>
															</UnmountClosed>
														</td>
													</tr>
												)}
											</Fragment>
										);
									})
								)}
							</tbody>
						</table>
					</div>
				</form>
				<div
					className="table-action-links-wrap"
					style={{ marginBottom: "var(--spacing-standard)" }}
				>
					<div className="table-action-links-row bottom">
						<div className="table-action-links left">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									Meteor.call(`messageTrackingAttributes.insert`, {
										messageID: message._id,
									});
								}}
							>
								Link Attribute
							</button>
						</div>
					</div>
				</div>
			</section>
			{/*<section className="last">*/}
			{/*	<h4>Messages</h4>*/}
			{/*	<h5>Linked Messages</h5>*/}
			{/*	<form>*/}
			{/*		<div className="messages-table">*/}
			{/*			<table className="messages">*/}
			{/*				{screenWidth >= 768 && (*/}
			{/*					<colgroup>*/}
			{/*						<col*/}
			{/*							style={isLoading ? { width: "100%" } : { width: "80%" }}*/}
			{/*						/>*/}
			{/*						<col style={isLoading ? { width: "0" } : { width: "20%" }} />*/}
			{/*					</colgroup>*/}
			{/*				)}*/}
			{/*				<thead>*/}
			{/*					<tr>*/}
			{/*						<th scope="col">{isLoading ? "" : "Message"}</th>*/}
			{/*						<th scope="col">{isLoading ? "" : "Actions"}</th>*/}
			{/*					</tr>*/}
			{/*				</thead>*/}
			{/*				<tbody>*/}
			{/*					{isLoading ? (*/}
			{/*						<tr className="loader">*/}
			{/*							<td>*/}
			{/*								<PropagateLoader color="#2f3d47" />*/}
			{/*							</td>*/}
			{/*						</tr>*/}
			{/*					) : (*/}
			{/*						messagesDB.map((message) => {*/}
			{/*							return (*/}
			{/*								<tr key={message.id}>*/}
			{/*									<td data-label="Message">{message.name}</td>*/}
			{/*									<td data-label="Actions">*/}
			{/*										<div className="action-links">*/}
			{/*											<button*/}
			{/*												className="action-link"*/}
			{/*												onClick={(event) => {*/}
			{/*													event.preventDefault();*/}
			{/*													setUnlinkMessageModal(true);*/}
			{/*												}}*/}
			{/*											>*/}
			{/*												Unlink*/}
			{/*											</button>*/}
			{/*										</div>*/}
			{/*									</td>*/}
			{/*								</tr>*/}
			{/*							);*/}
			{/*						})*/}
			{/*					)}*/}
			{/*				</tbody>*/}
			{/*			</table>*/}
			{/*		</div>*/}
			{/*	</form>*/}
			{/*	<div*/}
			{/*		className="table-action-links-wrap"*/}
			{/*		style={{ marginBottom: "var(--spacing-standard)" }}*/}
			{/*	>*/}
			{/*		<div className="table-action-links-row bottom">*/}
			{/*			<div className="table-action-links left">*/}
			{/*				<button*/}
			{/*					className="action-link"*/}
			{/*					onClick={(event) => {*/}
			{/*						event.preventDefault();*/}
			{/*						setAddMessageModal(true);*/}
			{/*					}}*/}
			{/*				>*/}
			{/*					Link Message*/}
			{/*				</button>*/}
			{/*			</div>*/}
			{/*		</div>*/}
			{/*	</div>*/}
			{/*</section>*/}
			<DeleteAttributeModal
				attributeInfo={deleteAttributeModal}
				hide={() => {
					setDeleteAttributeModal(null);
				}}
			/>
			<UnlinkMessageModal
				show={unlinkMessageModal}
				hide={() => {
					setUnlinkMessageModal(null);
				}}
			/>
			<AddMessageModal
				show={addMessageModal}
				hide={() => {
					setAddMessageModal(null);
				}}
			/>
		</>
	);
};

export default AdvancedModal;
