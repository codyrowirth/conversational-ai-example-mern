import React from "react";
import { useForm, Controller } from "react-hook-form";

import Modal from "../../../../lib/components/Modal/Modal";
import NestedData from "../../../../lib/components/NestedData/NestedData";
import Button from "../../../../lib/components/Button/Button";

const AddMessageModal = ({ show, hide }) => {
	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const onSubmit = (data) => {
		// TO DO
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Add Message</h3>
			<NestedData type="message" />
			<div className="button-wrap">
				<Button text="Add" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default AddMessageModal;
