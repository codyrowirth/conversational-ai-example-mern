import React from "react";

import commentsDB from "../../../Comments/commentsDB";

import Modal from "../../../../lib/components/Modal/Modal";
import CommentsTable from "../../../Comments/CommentsTable";
import Button from "../../../../lib/components/Button/Button";

const CommentsModal = ({ show, hide }) => {
	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Comments</h3>
			<CommentsTable view="inbox" type="message" comments={commentsDB} />
			<div className="button-wrap">
				<Button text="Close" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default CommentsModal;
