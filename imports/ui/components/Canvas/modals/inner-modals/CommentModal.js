import React from "react";

import Modal from "../../../../lib/components/Modal/Modal";
import CommentThread from "../../../Comments/CommentThread";
import Button from "../../../../lib/components/Button/Button";

import "../../../Comments/Comment.scss";

const CommentModal = ({ show, hide }) => {
	const parentCount = show !== null && show.location.length;

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<div className="comment-thread">
				<h3>Comments</h3>
				<div className="content-type">
					<div className="content-type-inner">
						{show !== null && show.contentType}
					</div>
				</div>
				<h4>{show !== null && show.location[parentCount - 1].name}</h4>
				<CommentThread thread={show !== null && show.thread} />
				<div className="button-wrap">
					<Button
						text="Post"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					/>
					<Button
						text="Close"
						onClick={(event) => {
							event.preventDefault();
							hide();
						}}
					/>
				</div>
			</div>
		</Modal>
	);
};

export default CommentModal;
