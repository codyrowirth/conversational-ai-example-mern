import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { UnmountClosed } from "react-collapse";
import { PropagateLoader } from "react-spinners";

import { useTracker } from "meteor/react-meteor-data";
import Elements from "../../../../../api/collections/Elements";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import InputRHF from "../../../../lib/components/Form/InputRHF";
import ToggleRHF from "../../../../lib/components/Form/ToggleRHF";
import FileUpload from "../../../../lib/components/Form/FileUpload";
import Button from "../../../../lib/components/Button/Button";

const NewElementForm = ({
	newElementFormExpanded,
	setNewElementFormExpanded,
	messageID,
}) => {
	const schema = yup.object().shape({
		name: yup.string().required("Please enter an element name."),
	});

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
		reset,
		setFocus,
		getValues,
		setValue,
	} = useForm({
		resolver: yupResolver(schema),
		defaultValues: {
			name: "",
			elementType: false,
		},
	});

	const { elements, isElementsLoading } = useTracker(() => {
		const handlerElements = Meteor.subscribe("elements", messageID);
		if (!handlerElements.ready()) {
			return { elements: null, isElementsLoading: true };
		}
		let elements = Elements.find({ messageID }).fetch();
		return { elements, isElementsLoading: false };
	});

	const isLoading = false;

	const onSubmit = (data, closeForm = true) => {
		const elementToAdd = {
			name: data.name,
			messageID: messageID,
		};
		if (closeForm) {
			setNewElementFormExpanded(false);
		}
		Meteor.call(`elements.insert`, elementToAdd, (err) => {
			if (err) {
				alert("Error inserting element");
			}
		});
		reset();
	};

	const onContinue = (data) => {
		onSubmit(data, false);
		setFocus("name");
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<UnmountClosed isOpened={newElementFormExpanded}>
			<GreyBox className="end-buttons">
				<h4>Add New Element</h4>
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<InputRHF
								type="text"
								name="name"
								label="Name"
								register={register}
								error={errors}
							/>
						</div>
					</div>
					<div className="grid-row">
						<div className="grid-col-full">
							<ToggleRHF
								name="elementType"
								labelLeft="Text"
								labelRight="Image"
								toggleContent={
									<div className="grid-row">
										<div className="grid-col-full">
											<FileUpload label="Image File" />
										</div>
									</div>
								}
								register={register}
								error={errors}
							/>
						</div>
					</div>
					<div className="button-wrap">
						<Button text="Save" onClick={handleSubmit(onSubmit)} />
						<Button
							text="Save and add another"
							onClick={handleSubmit(onContinue)}
						/>
						<Button
							text="Cancel"
							icon="times"
							onClick={(evt) => {
								evt.preventDefault();
								setValue("name", "");
								setNewElementFormExpanded(false);
							}}
						/>
					</div>
				</form>
			</GreyBox>
		</UnmountClosed>
	);
};

export default NewElementForm;
