import React, { Fragment, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { UnmountClosed } from "react-collapse";
import { PropagateLoader } from "react-spinners";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useWinWidth from "../../../../lib/utilities/useWinWidth";

import Modal from "../../../../lib/components/Modal/Modal";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import InputRHF from "../../../../lib/components/Form/InputRHF";
import ToggleRHF from "../../../../lib/components/Form/ToggleRHF";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import ToggleControlRHF from "../../../../lib/components/Form/ToggleControlRHF";
import Button from "../../../../lib/components/Button/Button";

import ManageThesaurusModal from "./ManageThesaurusModal";
import ElementsModal from "./ElementsModal";
import AdvancedModal from "./AdvancedModal";

const OptionsModal = ({
	show,
	hide,
	comments,
	message,
	shapeID,
	shapeData,
	setOptionsModal,
	elementsModal,
	setElementsModal,
	advancedModal,
	setAdvancedModal,
}) => {
	const [manageThesaurusModal, setManageThesaurusModal] = useState();

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const isLoading = false;

	const screenWidth = useWinWidth();

	const onSubmit = (data) => {
		// TO DO
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal
			className="modal end-buttons options-modal"
			show={!!show}
			hide={hide}
		>
			<GreyBox className="in-modal related-links">
				<div className="action-links">
					<span className="label">Related</span>
					<button
						className="action-link"
						onClick={(event) => {
							event.preventDefault();
							setElementsModal(true);
							setOptionsModal(false);
						}}
					>
						Elements
					</button>
					<button
						className="action-link"
						onClick={(event) => {
							event.preventDefault();
							setAdvancedModal(true);
							setOptionsModal(false);
						}}
					>
						Advanced
					</button>
				</div>
			</GreyBox>
			<h3>Options</h3>
			<form>
				<div className="options-table">
					<table className="options table-toggle break-xl">
						{screenWidth >= 1200 && (
							<colgroup>
								<col
									style={isLoading ? { width: "100%" } : { width: "100%" }}
								/>
								<col style={isLoading ? { width: "0" } : { width: "175px" }} />
								<col style={isLoading ? { width: "0" } : { width: "150px" }} />
								<col style={isLoading ? { width: "0" } : { width: "150px" }} />
								<col style={isLoading ? { width: "0" } : { width: "150px" }} />
							</colgroup>
						)}
						<thead>
							<tr>
								<th scope="col">{isLoading ? "" : "Label"}</th>
								<th scope="col">{isLoading ? "" : "Thesaurus"}</th>
								<th scope="col">{isLoading ? "" : "Element"}</th>
								<th scope="col">{isLoading ? "" : "Rules"}</th>
								<th scope="col">{isLoading ? "" : "Show"}</th>
							</tr>
						</thead>
						<tbody>
							{isLoading ? (
								<tr className="loader">
									<td>
										<PropagateLoader color="#2f3d47" />
									</td>
								</tr>
							) : (
								[
									{
										id: "option1",
										name: "Option One Lorem Ipsum",
									},
									{
										id: "option2",
										name: "Option Two Lorem Ipsum",
									},
									{
										id: "option3",
										name: "Option Three Lorem Ipsum",
									},
								].map((option, index) => {
									// TO DO: Use state from DB as default value
									const [toggleElementOn, setToggleElementOn] = useState(false);
									const [toggleRulesOn, setToggleRulesOn] = useState(false);
									const [rulesExpanded, setRulesExpanded] = useState(false);
									const rowClass = index % 2 === 0 ? "even" : "odd";

									return (
										<Fragment key={option.id}>
											<tr className={`${rowClass}`}>
												<td data-label="Label">
													<InputRHF
														type="text"
														name={`name-${option.id}`}
														defaultValue={option.name}
														register={register}
														error={errors}
													/>
												</td>
												<td data-label="Thesaurus">
													<div className="action-links">
														<button
															className="action-link"
															onClick={(event) => {
																event.preventDefault();
																setManageThesaurusModal(true);
															}}
														>
															Manage
														</button>
													</div>
												</td>
												<td data-label="Element">
													<Controller
														name={`elementToggle${option.id}`}
														control={control}
														render={({ field }) => {
															return (
																<ToggleControlRHF
																	name={field.name}
																	{...field}
																	onChange={(event) => {
																		setToggleElementOn(event.target.checked);
																	}}
																	ref={null}
																	error={errors}
																/>
															);
														}}
													/>
												</td>
												<td data-label="Rules">
													<Controller
														name={`rulesToggle${option.id}`}
														control={control}
														render={({ field }) => {
															return (
																<ToggleControlRHF
																	name={field.name}
																	{...field}
																	onChange={(event) => {
																		setToggleRulesOn(event.target.checked);
																	}}
																	ref={null}
																	error={errors}
																/>
															);
														}}
													/>
												</td>
												<td data-label="Show">
													<ToggleRHF
														name={`showToggle${option.id}`}
														disabled={true}
														register={register}
														error={errors}
														// TO DO: on change functionality
													/>
												</td>
											</tr>
											{toggleElementOn && (
												<tr className={`table-toggle-content ${rowClass}`}>
													<td colSpan="5">
														<Controller
															name={`element${option.id}`}
															control={control}
															render={({ field }) => {
																return (
																	<SelectRHF
																		name={field.name}
																		label="Element"
																		options={["TO DO"].map((option) => ({
																			value: option.toLowerCase(),
																			label: option,
																		}))}
																		{...field}
																		ref={null}
																		error={errors}
																	/>
																);
															}}
														/>
													</td>
												</tr>
											)}
											{toggleRulesOn && (
												<tr className={`table-toggle-content ${rowClass}`}>
													<td colSpan="5">
														<button
															className="collapse-trigger"
															onClick={(event) => {
																event.preventDefault();
																setRulesExpanded(!rulesExpanded);
															}}
														>
															Rules
															<span className="arrow">
																<FontAwesomeIcon
																	icon={
																		rulesExpanded
																			? "chevron-up"
																			: "chevron-down"
																	}
																/>
															</span>
														</button>
														<UnmountClosed isOpened={rulesExpanded}>
															<div className="rules-panel">
																<div className="rules-panel-inner">
																	Rule stuff goes here.
																</div>
															</div>
														</UnmountClosed>
													</td>
												</tr>
											)}
										</Fragment>
									);
								})
							)}
						</tbody>
					</table>
				</div>
			</form>
			<div className="table-action-links-wrap">
				<div className="table-action-links-row bottom">
					<div className="table-action-links left">
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
							}}
						>
							Show All
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
							}}
						>
							Hide All
						</button>
					</div>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
			<ManageThesaurusModal
				show={manageThesaurusModal}
				hide={() => {
					setManageThesaurusModal(null);
				}}
			/>
			<ElementsModal
				comments={comments}
				show={elementsModal}
				hide={() => {
					setElementsModal(null);
				}}
				message={message}
				shapeID={shapeID}
				shapeData={shapeData}
			/>
			<AdvancedModal
				show={advancedModal}
				hide={() => {
					setAdvancedModal(null);
				}}
			/>
		</Modal>
	);
};

export default OptionsModal;
