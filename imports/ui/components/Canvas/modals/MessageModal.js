import React, { useState, useEffect, useCallback } from "react";
import { useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";

import Messages from "../../../../api/collections/Messages";

import NotificationBox from "./modal-sections/lib/NotificationBox";
import ActionLinks from "./modal-sections/lib/ActionLinks";
import MessageSettings from "./modal-sections/MessageSettings";
import MessageAttributes from "./modal-sections/MessageAttributes";
import AddAttributeModal from "./inner-modals/AddAttributeModal";
import Dialog from "@mui/material/Dialog";
import Modal from "../../../lib/components/Modal/Modal";
import Button from "../../../lib/components/Button/Button";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import CloseIcon from "@mui/icons-material/Close";
import "./MessageModal.scss";
import IconButton from "@mui/material/IconButton";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import useMousetrap from "react-hook-mousetrap";
import PreviewModal from "./inner-modals/PreviewModal";
import AdvancedModal from "./inner-modals/AdvancedModal";
import ElementsModal from "./inner-modals/ElementsModal";
import Grid from "@mui/material/Grid";
import NestedData from "../../../lib/components/NestedData/NestedData";

const MessageModal = ({
	type,
	data,
	messageType,
	setMessageType,
	comments,
	unmatched,
	shapeID,
	setOpenPanelID,
	show,
	hide,
}) => {
	const [addAttributeModalOpen, setAddAttributeModalOpen] = useState(false);
	const [openTab, setOpenTab] = React.useState(0);

	const handleChange = (event, newValue) => {
		setOpenTab(newValue);
	};

	const { message, loading } = useTracker(() => {
		const subscription = Meteor.subscribe("messagesByShapeID", shapeID);

		if (!subscription.ready()) {
			return { loading: true, message: null };
		}

		const messagesDB = Messages.find({ shapeID }).fetch();

		if (messagesDB.length === 0) {
			Meteor.call("messages.insert", { shapeID, attributes: [] });
			return { loading: true, message: null };
		}

		return {
			loading: false,
			message: messagesDB[0],
		};
	});

	const handleKeyPress = useCallback((event) => {
		if (event.altKey) {
			if (event.code === "Digit1") {
				setOpenTab(0);
			} else if (event.code === "Digit2") {
				setOpenTab(1);
			} else if (event.code === "Digit3") {
				setOpenTab(2);
			} else if (event.code === "Digit4") {
				setOpenTab(3);
			} else if (event.code === "Digit5") {
				setOpenTab(4);
			}
		}
	}, []);

	useEffect(() => {
		document.addEventListener("keydown", handleKeyPress);

		return () => {
			document.removeEventListener("keydown", handleKeyPress);
		};
	}, [handleKeyPress]);

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#fff" />
			</div>
		);
	}

	const onCancel = () => {
		setOpenPanelID(null);
		hide();
	};

	let content;

	if (openTab === 0) {
		content = (
			<MessageSettings
				message={message}
				shapeID={shapeID}
				shapeData={data}
				setMessageType={setMessageType}
				modal={true}
			/>
		);
	} else if (openTab === 1) {
		content = (
			<>
				<Grid container spacing={3}>
					<Grid xs={6} item>
						<div>
							<NestedData
								type="attribute"
								add={true}
								shapeID={shapeID}
								parentType={"message"}
							/>
						</div>
					</Grid>
					<Grid xs={6} item>
						<MessageAttributes
							type={type}
							shapeID={shapeID}
							messageType={messageType}
							setAddAttributeModalOpen={setAddAttributeModalOpen}
							modal={true}
						/>
					</Grid>
				</Grid>
			</>
		);
	} else if (openTab === 2) {
		content = (
			<ElementsModal
				comments={comments}
				message={message}
				shapeID={shapeID}
				shapeData={data}
			/>
		);
	} else if (openTab === 3) {
		content = (
			<AdvancedModal
				comments={comments}
				message={message}
				shapeID={shapeID}
				shapeData={data}
			/>
		);
	} else if (openTab === 4) {
		content = <PreviewModal shapeID={shapeID} />;
	}
	return (
		<Dialog fullScreen open={!!show} onClose={onCancel}>
			<AppBar position="static">
				<Toolbar>
					<IconButton
						size="large"
						edge="start"
						color="inherit"
						sx={{ mr: 2 }}
						onClick={onCancel}
					>
						<CloseIcon />
					</IconButton>
					{data?.label || ""}
				</Toolbar>
			</AppBar>
			<div className="dialogContent">
				<Box
					sx={{ borderBottom: 1, borderColor: "divider" }}
					style={{ marginBottom: 20 }}
				>
					<Tabs value={openTab} onChange={handleChange} centered>
						<Tab label="Properties" />
						<Tab label="Attributes" />
						<Tab label="Elements" />
						<Tab label="Advanced" />
						<Tab label="Testing" />
					</Tabs>
				</Box>
				{/*<ActionLinks*/}
				{/*	shapeID={shapeID}*/}
				{/*	shapeData={data}*/}
				{/*	message={message}*/}
				{/*	type={type}*/}
				{/*	comments={comments}*/}
				{/*	unmatched={unmatched}*/}
				{/*	modal={true}*/}
				{/*/>*/}
				{content}
				{/*<NotificationBox comments={comments} unmatched={unmatched} />*/}
			</div>
		</Dialog>
	);
};

export default MessageModal;
