import React from "react";

import NotificationBox from "./modal-sections/lib/NotificationBox";
import ActionLinks from "./modal-sections/lib/ActionLinks";
import ThreadSettings from "./modal-sections/ThreadSettings";

import "./modal.scss";

import Modal from "../../../lib/components/Modal/Modal";
import Button from "../../../lib/components/Button/Button";

const ThreadPanel = ({
	type,
	data,
	id,
	comments,
	show,
	hide,
	setOpenPanelID,
}) => {
	const onCancel = () => {
		setOpenPanelID(null);
		hide();
	};

	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!show}
			hide={hide}
			onHide={onCancel}
		>
			<h3>Edit Thread</h3>
			<p>TO DO</p>
			{/* <NotificationBox comments={comments} />
			<ActionLinks type={type} comments={comments} />
			<ThreadSettings id={id} shapeData={data} /> */}
			<div className="button-wrap">
				<Button text="Close" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default ThreadPanel;
