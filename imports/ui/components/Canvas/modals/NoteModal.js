import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useForm } from "react-hook-form";

import Modal from "../../../lib/components/Modal/Modal";
import TextareaRHF from "../../../lib/components/Form/TextareaRHF";
import Button from "../../../lib/components/Button/Button";

const NoteEditModal = ({ shapeID, noteContent, show, hide }) => {
	const {
		register,
		formState: { errors },
		setFocus,
		handleSubmit,
	} = useForm({
		defaultValues: {
			noteContentField: noteContent,
		},
	});

	useEffect(() => {
		if (show) {
			setFocus("noteContentField");
		}
	});

	const onSubmit = (data) => {
		Meteor.call(`shapes.updateData`, shapeID, "label", data.noteContentField);
		hide();
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Edit Note</h3>
			<form>
				<TextareaRHF
					name="noteContentField"
					aria-label="Note content"
					error={errors}
					register={register}
				/>
			</form>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default NoteEditModal;
