import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useForm } from "react-hook-form";

import Modal from "../../../lib/components/Modal/Modal";
import TextareaRHF from "../../../lib/components/Form/TextareaRHF";
import Button from "../../../lib/components/Button/Button";

const TextEditModal = ({ shapeID, textContent, show, hide }) => {
	const {
		register,
		formState: { errors },
		handleSubmit,
		setFocus,
	} = useForm({
		defaultValues: {
			textContentField: textContent,
		},
	});

	useEffect(() => {
		if (show) {
			setFocus("textContentField");
		}
	});

	const onSubmit = (data) => {
		Meteor.call(`shapes.updateData`, shapeID, "label", data.textContentField);
		hide();
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Edit Text</h3>
			<form>
				<TextareaRHF
					name="textContentField"
					aria-label="Text content"
					error={errors}
					register={register}
				/>
			</form>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default TextEditModal;
