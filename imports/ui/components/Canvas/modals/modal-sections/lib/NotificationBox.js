import React, { useState } from "react";
import NotifBadge from "../../../../../lib/components/NotifBadge/NotifBadge";

import CommentsModal from "../../inner-modals/CommentsModal";
import UnmatchedModal from "../../inner-modals/UnmatchedModal";

import "./NotificationBox.scss";

const Notifications = (props) => {
	const [commentsModal, setCommentsModal] = useState();
	const [unmatchedModal, setUnmatchedModal] = useState();

	const { comments, unmatched } = props;

	return (
		(comments && comments > 0) ||
		(unmatched && unmatched > 0 ? (
			<div className="modal-section notif-box">
				<div className="notifs">
					{comments && comments > 0 && (
						<div className="notif">
							<NotifBadge count={comments} small={true} />
							<strong>New</strong> Comments (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setCommentsModal(true);
								}}
							>
								view
							</button>
							)
						</div>
					)}
					{unmatched && unmatched > 0 && (
						<div className="notif">
							<NotifBadge count={unmatched} small={true} alt={true} />
							<strong>New</strong> Unmatched User Answers (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setUnmatchedModal(true);
								}}
							>
								view
							</button>
							)
						</div>
					)}
				</div>
				<CommentsModal
					show={commentsModal}
					hide={() => {
						setCommentsModal(null);
					}}
				/>
				<UnmatchedModal
					show={unmatchedModal}
					hide={() => {
						setUnmatchedModal(null);
					}}
				/>
			</div>
		) : null)
	);
};

export default Notifications;
