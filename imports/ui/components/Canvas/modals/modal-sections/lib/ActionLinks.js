import React, { useState } from "react";

import ElementsModal from "../../inner-modals/ElementsModal";
import OptionsModal from "../../inner-modals/OptionsModal";
import AdvancedModal from "../../inner-modals/AdvancedModal";
import CommentsModal from "../../inner-modals/CommentsModal";
import UnmatchedModal from "../../inner-modals/UnmatchedModal";
import StatsModal from "../../inner-modals/StatsModal";
import PreviewModal from "../../inner-modals/PreviewModal";

import "./ActionLinks.scss";

const ActionLinks = (props) => {
	const {
		type,
		comments,
		unmatched,
		message,
		shapeData,
		shapeID,
		modal = false,
		setAddAttributeModalOpen = false,
		setRulesModalOpen = false,
	} = props;

	const [elementsModal, setElementsModal] = useState();
	const [optionsModal, setOptionsModal] = useState();
	const [advancedModal, setAdvancedModal] = useState();
	const [commentsModal, setCommentsModal] = useState();
	const [unmatchedModal, setUnmatchedModal] = useState();
	const [statsModal, setStatsModal] = useState();
	const [previewModal, setPreviewModal] = useState();

	let linksClass = "";

	// if (modal === true) {
	// 	linksClass = "modal-section";
	// } else if (modal !== true) {
	// 	linksClass = "modal-section modal-action-links";
	// }

	return (
		<>
			<div className="modal-section modal-action-links">
				{type === "message" && (
					<div className="action-links">
						<span className="label">Content</span>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setElementsModal(true);
							}}
						>
							Elements
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setOptionsModal(true);
							}}
						>
							Options
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setAdvancedModal(true);
							}}
						>
							Advanced
						</button>
					</div>
				)}

				{type === "brancher" && (
					<div className="action-links">
						<span className="label">Content</span>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setAddAttributeModalOpen(true);
							}}
						>
							Manage Attributes
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setRulesModalOpen(true);
							}}
						>
							Manage Rules
						</button>
					</div>
				)}

				<div className="action-links">
					<span className="label">Interaction</span>
					<button
						className="action-link notif-link"
						onClick={(event) => {
							event.preventDefault();
							setCommentsModal(true);
						}}
					>
						<span className="notif-type">Comments</span>
						{comments > 0 ? ` (${comments})` : ""}
					</button>
					{type === "message" && (
						<button
							className="action-link notif-link"
							onClick={(event) => {
								event.preventDefault();
								setUnmatchedModal(true);
							}}
						>
							<span className="notif-type">Unmatched</span>{" "}
							{unmatched > 0 ? ` (${unmatched})` : ""}
						</button>
					)}
				</div>

				{type === "message" && (
					<div className="action-links">
						<span className="label">Analysis</span>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setStatsModal(true);
							}}
						>
							Stats
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setPreviewModal(true);
							}}
						>
							Preview
						</button>
					</div>
				)}
			</div>
			{type === "message" && (
				<>
					<OptionsModal
						show={optionsModal}
						hide={() => {
							setOptionsModal(null);
						}}
						comments={comments}
						message={message}
						shapeID={shapeID}
						shapeData={shapeData}
						setOptionsModal={setOptionsModal}
						elementsModal={elementsModal}
						setElementsModal={setElementsModal}
						advancedModal={advancedModal}
						setAdvancedModal={setAdvancedModal}
					/>
				</>
			)}

			<CommentsModal
				show={commentsModal}
				hide={() => {
					setCommentsModal(null);
				}}
			/>

			{type === "message" && (
				<>
					<UnmatchedModal
						show={unmatchedModal}
						hide={() => {
							setUnmatchedModal(null);
						}}
					/>
					<StatsModal
						show={statsModal}
						hide={() => {
							setStatsModal(null);
						}}
					/>
				</>
			)}
		</>
	);
};

export default ActionLinks;
