import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./AttributeList.scss";
import AddAttributeModal from "../../inner-modals/AddAttributeModal";
import { useTracker } from "meteor/react-meteor-data";
import Message from "../../../../../../api/collections/Messages";
import Attributes from "../../../../../../api/collections/Attributes";
import pluralize from "pluralize";
import Branchers from "../../../../../../api/collections/Branchers";

const AttributeList = ({ shapeID, type }) => {
	const typePlural = pluralize(type);
	let model;
	if (type === "brancher") {
		model = Branchers;
	} else {
		model = Message;
	}

	const { parent, loading, attributes } = useTracker(() => {
		const subscription = Meteor.subscribe(`${typePlural}ByShapeID`, shapeID);

		if (!subscription.ready()) {
			return { loading: true, parent: null, attributes: null };
		}

		const parentsDB = model.find({ shapeID }).fetch();

		if (parentsDB.length === 0) {
			return { loading: true, parent: null, attributes: null };
		}

		const parent = parentsDB[0];

		const attributesSubscription = Meteor.subscribe(
			"attributes",
			parent.attributes || []
		);

		if (!attributesSubscription.ready()) {
			return { loading: true, parent: null, attributes: null };
		}

		return {
			loading: false,
			parent,
			attributes: Attributes.find({
				_id: { $in: parent.attributes || [] },
			}).fetch(),
		};
	});

	const remove = (idToRemove) => {
		Meteor.call(`${typePlural}.update`, parent._id, {
			attributes: parent.attributes.filter(
				(attFilter) => attFilter !== idToRemove
			),
		});
	};

	if (loading) {
		return <div />;
	}

	return (
		<div className="attribute-list">
			<div className="attributes">
				{attributes.length === 0 ? (
					<p>There aren’t any attributes on this {type}.</p>
				) : null}
				{attributes.map((attribute) => (
					<div className="attribute" key={attribute._id}>
						<div className="attribute-icon">
							<FontAwesomeIcon icon="list-ul" onClick={remove} />
						</div>
						<div className="attribute-info">{attribute.name}</div>
						{attribute._id === parent?.listenerAttributeID && (
							<div className="attribute-status">
								<FontAwesomeIcon icon="headphones" />
							</div>
						)}
						<div className="attribute-remove">
							<FontAwesomeIcon
								icon={"minus-circle"}
								onClick={() => {
									remove(attribute._id);
								}}
							/>
						</div>
					</div>
				))}
			</div>
		</div>
	);
};

export default AttributeList;
