import React from "react";
import { useTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import { useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Shapes from "../../../../../api/collections/Shapes";
import BrancherLines from "../../../../../api/collections/BrancherLines";
import BrancherRulesCollection from "../../../../../api/collections/BrancherRules";
import BrancherRuleGroups from "../../../../../api/collections/BrancherRuleGroups";
import Attributes from "../../../../../api/collections/Attributes";
import Options from "../../../../../api/collections/Options";

import "./BrancherRules.scss";

const BrancherRules = ({ shapeID, grandparent, setRulesModalOpen }) => {
	const { canvasID } = useParams();

	const { attributes, brancherLines, isLoading } = useTracker(() => {
		const attributesSubscription = Meteor.subscribe(
			"attributes",
			grandparent.attributes || []
		);

		if (!attributesSubscription.ready()) {
			return { isLoading: true, attributes: null };
		}

		const attributes = Attributes.find({
			_id: { $in: grandparent.attributes || [] },
		}).fetch();

		const handler = Meteor.subscribe("shapes", canvasID);

		if (!handler.ready()) {
			return { isLoading: true, brancherLines: null };
		}

		const brancherLines = Shapes.find({ source: shapeID }).fetch();

		const targetIDs = brancherLines.map((line) => line.target);

		const brancherLinesSub = Meteor.subscribe(`brancherLinesArray`, targetIDs);
		if (!brancherLinesSub.ready()) {
			return { isLoading: true, brancherLines: null };
		}

		for (const line of brancherLines) {
			line.destinationShape = Shapes.findOne({ _id: line.target });

			const matchingBrancherLine = BrancherLines.findOne({
				brancherID: shapeID,
				target: line.target,
			});

			if (!matchingBrancherLine) {
				Meteor.call("brancherLines.insert", {
					brancherID: shapeID,
					target: line.target,
					lineID: line._id,
					chainedOperator: "and",
				});

				return { isLoading: true, brancherLines: null };
			}
			line.brancherLine = matchingBrancherLine;
		}

		return { isLoading: false, attributes, brancherLines };
	});

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	const hasLeavingLines = brancherLines.length !== 0;

	return (
		<div className="modal-section brancher-rules">
			<p className="h4">Branch Rules</p>
			{hasLeavingLines ? (
				<div className="rule-list">
					<div className="rules">
						{brancherLines.map((line) => {
							return (
								<BrancherRuleRow
									key={line._id}
									line={line}
									grandparent={grandparent}
									attributes={attributes}
								/>
							);
						})}
					</div>
					<div className="action-links">
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setRulesModalOpen(true);
							}}
						>
							Manage Rules
						</button>
					</div>
				</div>
			) : (
				<p className="intro">
					To manage branch rules, add some branches to the canvas.
				</p>
			)}
		</div>
	);
};

const BrancherRuleRow = ({ line, grandparent, attributes }) => {
	const { rules, groupedRules, parent, options } = useTracker(() => {
		const rulesModel = BrancherRulesCollection;
		const ruleGroupsModel = BrancherRuleGroups;
		const parentKey = "lineID";
		const parentID = line?.brancherLine?.lineID;

		const rulesSub = Meteor.subscribe(`${rulesModel._name}`, parentID);
		const ruleGroupsSub = Meteor.subscribe(
			`${ruleGroupsModel._name}`,
			parentID
		);

		const parentSub = Meteor.subscribe("brancherLines", parentID);

		if (!rulesSub.ready() || !ruleGroupsSub.ready() || !parentSub.ready()) {
			return { isLoading: true };
		}

		const rules = rulesModel.find({ [parentKey]: parentID }).fetch();

		const ruleGroups = ruleGroupsModel
			.find({ [parentKey]: parentID })
			.fetch()
			.sort((a, b) => a.order - b.order);

		const parent = BrancherLines.findOne({ lineID: parentID });

		const combinedRules = [
			...rules,
			...ruleGroups.map((group) => ({ ...group, group: true })),
		]
			.filter((rule) => !rule.parentGroupID)
			.sort((a, b) => a.order - b.order);

		const groupedRules = rules.filter((rule) => rule.parentGroupID);

		const optionsSubscription = Meteor.subscribe(
			"optionsByAttributes",
			grandparent.attributes || []
		);

		if (!optionsSubscription.ready()) {
			return { isLoading: true, options: null };
		}

		const options = Options.find({
			attributeID: { $in: grandparent.attributes },
		}).fetch();

		return {
			isLoading: false,
			rules: combinedRules,
			groupedRules,
			parent,
			options,
		};
	});

	const formatRule = (rule, last) => {
		const attribute = attributes?.find(
			(attribute) => attribute._id === rule.attributeID
		)?.name;

		let operator = "";

		if (rule.operator === "Equal To") {
			operator = "is equal to";
		} else if (rule.operator === "Not Equal To") {
			operator = "is not equal to";
		} else if (rule.operator === "Less") {
			operator = "is less than";
		} else if (rule.operator === "Less Or Equal") {
			operator = "is less than or equal to";
		} else if (rule.operator === "Greater") {
			operator = "is greater than";
		} else if (rule.operator === "Greater Or Equal") {
			operator = "is greater than or equal to";
		} else if (rule.operator === "Between") {
			operator = "is between";
		} else if (rule.operator === "Not Between") {
			operator = "is not between";
		} else if (rule.operator === "Begins With") {
			operator = "begins with";
		} else if (rule.operator === "Not Begins With") {
			operator = "does not begin with";
		} else if (rule.operator === "Contains") {
			operator = "contains";
		} else if (rule.operator === "Not Contains") {
			operator = "does not contain";
		} else if (rule.operator === "Ends With") {
			operator = "ends with";
		} else if (rule.operator === "Not Ends With") {
			operator = "does not end with";
		} else if (rule.operator === "Is Empty") {
			operator = "is empty";
		} else if (rule.operator === "Is Not Empty") {
			operator = "is not empty";
		} else {
			operator = rule.operator?.toLowerCase();
		}

		let value = "";

		if (rule.optionID) {
			value = options?.find((option) => option._id === rule.optionID)?.name;
			if (!value) {
				value = "ERROR OPTION NO LONGER EXISTS";
			}
		} else if (rule.optionValue) {
			value = rule.optionValue;
		}

		if (rule.parentGroupID) {
			return (
				<li key={rule._id} className="rule-text grouped-rule">
					<strong>{attribute}</strong> {operator} <strong>{value}</strong>{" "}
					{!last && (
						<span className="chained-operator">
							{line?.brancherLine?.chainedOperator}
						</span>
					)}
				</li>
			);
		} else {
			return (
				<li key={rule._id} className="rule-text">
					If <strong>{attribute}</strong> {operator} <strong>{value}</strong>{" "}
					{!last && (
						<span className="chained-operator">
							{line?.brancherLine?.chainedOperator}
						</span>
					)}
				</li>
			);
		}
	};

	const mapRules = () => {
		if (!rules?.length) {
			return <li key="no-rules">No rules set</li>;
		} else {
			return rules?.map((rule, index) => {
				if (rule.group) {
					const childRules = groupedRules?.filter(
						(groupedRule) => groupedRule.parentGroupID === rule._id
					);

					return childRules.length ? (
						<li key={rule._id} className="group">
							If
							<ul key={rule._id}>
								{childRules.map((groupedRule, index) => {
									if (
										groupedRule.attributeID &&
										groupedRule.operator &&
										(groupedRule.operator === "Is Empty" ||
											groupedRule.operator === "Is Not Empty" ||
											groupedRule.optionID ||
											groupedRule.optionValue)
									) {
										const last = index === childRules.length - 1;
										return formatRule(groupedRule, last);
									} else {
										if (
											!groupedRule.attributeID &&
											!groupedRule.operator &&
											!groupedRule.optionID &&
											!groupedRule.optionValue
										) {
											return <li key={groupedRule._id}>[[empty rule]]</li>;
										} else {
											return <li key={groupedRule._id}>[[incomplete rule]]</li>;
										}
									}
								})}
								{index !== rules.length - 1 && (
									<li key={rule._id} className="rule-text">
										<span className="chained-operator">
											{rule.chainedOperator}
										</span>
									</li>
								)}
							</ul>
						</li>
					) : null;
				} else {
					if (
						rule.attributeID &&
						rule.operator &&
						(rule.operator === "Is Empty" ||
							rule.operator === "Is Not Empty" ||
							rule.optionID ||
							rule.optionValue)
					) {
						const last = index === rules.length - 1;
						return formatRule(rule, last);
					} else {
						if (
							!rule.attributeID &&
							!rule.operator &&
							!rule.optionID &&
							!rule.optionValue
						) {
							return (
								<li key={rule._id} className="rule-text">
									[[empty rule]]
								</li>
							);
						} else {
							return (
								<li key={rule._id} className="rule-text">
									[[incomplete rule]]
								</li>
							);
						}
					}
				}
			});
		}
	};

	return (
		<div className="rule">
			<div className="rule-icon">
				<FontAwesomeIcon icon="code-branch" />
			</div>
			<div className="rule-info">
				<p>
					Go to{" "}
					<strong>{line.destinationShape?.data?.label || "[[unnamed]]"}</strong>
				</p>
				<ul>
					{line?.brancherLine?.else === true && (
						<li key="else">When no other criteria is met</li>
					)}
					{!line?.brancherLine?.else && mapRules()}
				</ul>
			</div>
		</div>
	);
};

export default BrancherRules;
