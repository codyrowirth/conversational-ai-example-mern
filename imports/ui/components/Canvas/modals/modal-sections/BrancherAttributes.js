import React from "react";

import AttributeList from "./lib/AttributeList";

const BrancherAttributes = (props) => {
	const { type, shapeID, setAddAttributeModalOpen } = props;

	return (
		<div className="modal-section attributes-list brancher-attributes">
			<p className="h4">Attributes</p>
			<AttributeList type={type} shapeID={shapeID} />
			<div className="action-links add-attribute">
				<button
					className="action-link"
					onClick={(event) => {
						event.preventDefault();
						setAddAttributeModalOpen(true);
					}}
				>
					Manage Attributes
				</button>
			</div>
		</div>
	);
};

export default BrancherAttributes;
