import React from "react";
import { useForm } from "react-hook-form";

import AttributeList from "./lib/AttributeList";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import { useTracker } from "meteor/react-meteor-data";
import Message from "../../../../../api/collections/Messages";
import Attributes from "../../../../../api/collections/Attributes";
import { saveOnChange } from "../../../../lib/utilities/saveOnBlurRHFMeteor";

const MessageAttributes = ({
	shapeID,
	type,
	messageType,
	setAddAttributeModalOpen,
	modal,
}) => {
	const { message, loading, attributes } = useTracker(() => {
		const subscription = Meteor.subscribe("messagesByShapeID", shapeID);

		if (!subscription.ready()) {
			return { loading: true };
		}

		const messagesDB = Message.find({ shapeID }).fetch();

		if (messagesDB.length === 0) {
			return { loading: true, message: null, attributes: null };
		}

		const message = messagesDB[0];

		const attributesSubscription = Meteor.subscribe(
			"attributes",
			message.attributes || []
		);

		if (!attributesSubscription.ready()) {
			return { loading: true, message: null, attributes: null };
		}

		return {
			loading: false,
			message,
			attributes: Attributes.find({
				_id: { $in: message.attributes || [] },
			}).fetch(),
		};
	});

	const {
		register,
		formState: { errors },
	} = useForm();

	if (loading) {
		return <div />;
	}

	const matchingListenerAttribute = attributes.find(
		(att) => att._id === message.listenerAttributeID
	);

	// let attributesClass = "";

	// if (modal === true) {
	// 	attributesClass = "modal-section";
	// } else if (modal !== true) {
	// 	attributesClass = "panel-section";
	// }

	return (
		<div className="modal-section attributes-list message-attributes">
			<AttributeList type={type} shapeID={shapeID} />

			{messageType !== "statement" && (
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<SelectRHF
								name="listenerAttributeID"
								label="Listener Attribute"
								options={attributes.map((option) => ({
									value: option._id,
									label: option.name,
								}))}
								panel={modal !== true ? true : false}
								ref={null}
								value={
									message?.listenerAttributeID && matchingListenerAttribute
										? {
												value: matchingListenerAttribute._id,
												label: matchingListenerAttribute.name,
										  }
										: { value: "", label: "" }
								}
								onChange={(value, fieldData) => {
									saveOnChange("messages", message._id, value, fieldData);
									Meteor.call(
										`shapes.updateData`,
										message.shapeID,
										"listenerAttribute",
										value.label
									);
								}}
							/>
						</div>
					</div>
				</form>
			)}
		</div>
	);
};

export default MessageAttributes;
