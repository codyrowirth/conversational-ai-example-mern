import React from "react";
import { useForm, Controller } from "react-hook-form";

import InputRHF from "../../../../lib/components/Form/InputRHF";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";

const ThreadSettings = (props) => {
	const { shapeData } = props;

	const {
		register,
		control,
		formState: { errors },
	} = useForm({
		defaultValues: {
			label: shapeData?.label === "[[unnamed]]" ? "" : shapeData.label,
			// TO DO: thread default value
		},
	});

	const handleBlur = (evt) => {
		// TO DO
	};

	return (
		<div className="modal-section brancher-settings">
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="label"
							label="Name"
							onBlur={handleBlur}
							error={errors}
							register={register}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="thread"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Thread"
										options={["TO DO"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										panel={true}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<div className="action-links">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									alert("TO DO");
								}}
							>
								Edit
							</button>
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									alert("TO DO");
								}}
							>
								Reset
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	);
};

export default ThreadSettings;
