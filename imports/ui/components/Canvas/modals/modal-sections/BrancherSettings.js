import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { titleCase } from "title-case";

import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import InputRHF from "../../../../lib/components/Form/InputRHF";

const BrancherSettings = ({ shapeData, shapeID, brancher }) => {
	const { canvasID } = useParams();

	const {
		register,
		control,
		formState: { errors },
		setFocus,
	} = useForm({
		defaultValues: {
			label: shapeData?.label === "[[unnamed]]" ? "" : shapeData.label,
		},
	});

	useEffect(() => {
		setFocus("label");
	}, [setFocus]);

	const handleBlur = (evt) => {
		Meteor.call(
			`shapes.updateData`,
			shapeID,
			evt.target.name,
			evt.target.value
		);
		Meteor.call("canvases.update", canvasID, {});
	};

	return (
		<div className="modal-section brancher-settings">
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="label"
							label="Name"
							onBlur={handleBlur}
							error={errors}
							register={register}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="status"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Status"
										options={[
											"Backlog",
											"To Do",
											"Doing",
											"Done",
											"Blocked",
											"Testing",
											"Approved",
										].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										error={errors}
										ref={null}
										value={{
											value: brancher?.status || "",
											label: titleCase(brancher?.status || ""),
										}}
										onChange={(value) => {
											Meteor.call("branchers.update", brancher._id, {
												status: value.value,
											});
										}}
									/>
								);
							}}
						/>
					</div>
				</div>
			</form>
		</div>
	);
};

export default BrancherSettings;
