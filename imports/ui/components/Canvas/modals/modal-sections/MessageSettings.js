import React, { useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import {
	saveOnBlur,
	saveOnChange,
} from "../../../../lib/utilities/saveOnBlurRHFMeteor";
import { titleCase } from "title-case";
import { useParams } from "react-router-dom";
import InputRHF from "../../../../lib/components/Form/InputRHF";
import TextareaRHF from "../../../../lib/components/Form/TextareaRHF";
import CommentsModal from "../inner-modals/CommentsModal";

const MessageSettings = ({
	shapeID,
	shapeData,
	message,
	setMessageType,
	modal,
	...props
}) => {
	const { canvasID } = useParams();

	const {
		register,
		control,
		formState: { errors },
		setFocus,
	} = useForm({
		defaultValues: {
			label: shapeData?.label === "[[unnamed]]" ? "" : shapeData.label,
			writingPrompt: shapeData.writingPrompt,
			architectsNotes: message.architectsNotes,
		},
	});

	useEffect(() => {
		setFocus("label");
	}, [setFocus]);

	const handleBlur = (evt) => {
		if (["label", "writingPrompt"].includes(evt?.target?.name)) {
			Meteor.call(
				`shapes.updateData`,
				shapeID,
				evt.target.name,
				evt.target.value
			);
			if (canvasID) {
				Meteor.call("canvases.update", canvasID, {});
			}
		} else {
			saveOnBlur("messages", message._id, evt, canvasID);
		}
	};

	return (
		<div className="modal-section message-settings">
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="label"
							label="Name"
							onBlur={handleBlur}
							error={errors}
							register={register}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="type"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Type"
										options={["Conversation", "Statement"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										panel={modal !== true ? true : false}
										{...field}
										ref={null}
										error={errors}
										value={{
											value: message?.type || "",
											label: titleCase(message?.type || ""),
										}}
										onChange={(value, fieldData) => {
											setMessageType(value.value);
											saveOnChange("messages", message._id, value, fieldData);
										}}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-half">
						<TextareaRHF
							name="writingPrompt"
							label="Writing Prompt"
							className="prompt"
							onBlur={handleBlur}
							error={errors}
							register={register}
						/>
					</div>
					<div className="grid-col-half">
						<TextareaRHF
							name="architectsNotes"
							label="Architect's Notes"
							className="notes"
							onBlur={handleBlur}
							error={errors}
							register={register}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="status"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Status"
										options={[
											"Backlog",
											"To Do",
											"Doing",
											"Done",
											"Blocked",
											"Testing",
											"Approved",
										].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										error={errors}
										ref={null}
										value={{
											value: message?.status || "",
											label: titleCase(message?.status || ""),
										}}
										onChange={(value) => {
											Meteor.call("messages.update", message._id, {
												status: value.value,
											});
										}}
									/>
								);
							}}
						/>
					</div>
				</div>
				{/*<div className="grid-row">*/}
				{/*	<div className="grid-col-full">*/}
				{/*		<Controller*/}
				{/*			name="owner"*/}
				{/*			control={control}*/}
				{/*			render={({ field }) => {*/}
				{/*				return (*/}
				{/*					<SelectRHF*/}
				{/*						name={field.name}*/}
				{/*						label="Owner"*/}
				{/*						options={["TO DO"].map((option) => ({*/}
				{/*							value: option.toLowerCase(),*/}
				{/*							label: option,*/}
				{/*						}))}*/}
				{/*						panel={true}*/}
				{/*						{...field}*/}
				{/*						onBlur={() => {*/}
				{/*							handleBlur(field);*/}
				{/*						}}*/}
				{/*						ref={null}*/}
				{/*						error={errors}*/}
				{/*					/>*/}
				{/*				);*/}
				{/*			}}*/}
				{/*		/>*/}
				{/*	</div>*/}
				{/*</div>*/}
				{/*<div className="grid-row">*/}
				{/*	<div className="grid-col-full">*/}
				{/*		<Controller*/}
				{/*			name="teamMembers"*/}
				{/*			control={control}*/}
				{/*			render={({ field }) => {*/}
				{/*				return (*/}
				{/*					<SelectRHF*/}
				{/*						name={field.name}*/}
				{/*						label="Team Members"*/}
				{/*						options={["TO DO"].map((option) => ({*/}
				{/*							value: option.toLowerCase(),*/}
				{/*							label: option,*/}
				{/*						}))}*/}
				{/*						panel={true}*/}
				{/*						{...field}*/}
				{/*						onBlur={() => {*/}
				{/*							handleBlur(field);*/}
				{/*						}}*/}
				{/*						ref={null}*/}
				{/*						error={errors}*/}
				{/*					/>*/}
				{/*				);*/}
				{/*			}}*/}
				{/*		/>*/}
				{/*	</div>*/}
				{/*</div>*/}
			</form>
		</div>
	);
};

export default MessageSettings;
