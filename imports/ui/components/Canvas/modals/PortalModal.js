import React from "react";
// import { useForm, Controller } from "react-hook-form";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";

import Shapes from "../../../../api/collections/Shapes";

import Modal from "../../../lib/components/Modal/Modal";
import Button from "../../../lib/components/Button/Button";
import SelectCreatableRHF from "../../../lib/components/Form/SelectCreatableRHF";

import "./modal.scss";

const PortalModal = ({ shapeID, data, show, hide, setOpenPanelID }) => {
	const { portalNames, isLoading } = useTracker(() => {
		const handler = Meteor.subscribe("portals", Meteor.user().projectID);

		if (!handler.ready()) {
			return { isLoading: true, options: null };
		}

		const portalsDB = Shapes.find(
			{
				type: "portal",
				"data.label": { $exists: true, $nin: [null, "[[unnamed]]"] },
			},
			{ fields: { "data.label": 1 } }
		).fetch();

		const portalNames = [
			...new Set(portalsDB.map((portal) => portal.data.label)),
		];

		return {
			isLoading: false,
			portalNames,
		};
	});

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<PortalModalForm
			shapeID={shapeID}
			portalNames={portalNames}
			data={data}
			show={show}
			hide={hide}
			setOpenPanelID={setOpenPanelID}
		/>
	);
};

const PortalModalForm = ({
	portalNames,
	shapeID,
	data,
	show,
	hide,
	setOpenPanelID,
}) => {
	const onChange = (data) => {
		Meteor.call(`shapes.updateData`, shapeID, "label", data.value);
	};

	const onCancel = () => {
		setOpenPanelID(null);
		hide();
	};

	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!show}
			hide={hide}
			onHide={onCancel}
		>
			<h3>Edit Portal</h3>
			<div className="modal-section">
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<SelectCreatableRHF
								name={"name"}
								label="Name"
								value={{ label: data?.label || "", value: data?.label || "" }}
								options={[
									{
										label: "Type to create new portal, or select from below:",
										value: "",
										disabled: true,
									},
									...portalNames.map((option) => ({
										value: option,
										label: option,
									})),
								]}
								ref={null}
								autoFocus={true}
								error={{}}
								onChange={onChange}
								isOptionDisabled={(option) => option.disabled}
							/>
						</div>
					</div>
				</form>
			</div>
			<div className="button-wrap">
				<Button text="Close" onClick={onCancel} />
			</div>
		</Modal>
	);
};
export default PortalModal;
