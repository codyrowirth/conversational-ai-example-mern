import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./NodeFormatMenuMulti.scss";
import "./LineShapeMenu.scss";

const LineShapeMenu = ({ className, edgeToChange }) => {
	return (
		<div className={`format-menu line ${className}`}>
			<div className="format-menu-inner">
				<button
					className="ortho"
					aria-label="Orthogonal lines"
					onClick={(event) => {
						event.preventDefault();
						Meteor.call("shapes.update", edgeToChange[0]?.id, {
							type: "smoothstep",
						});
					}}
				>
					<FontAwesomeIcon icon="vector-square" />
				</button>
				<button
					className="curved"
					aria-label="Curved lines"
					onClick={(event) => {
						event.preventDefault();
						Meteor.call("shapes.update", edgeToChange[0]?.id, {
							type: "default",
						});
					}}
				>
					<FontAwesomeIcon icon="bezier-curve" />
				</button>
			</div>
		</div>
	);
};

export default LineShapeMenu;
