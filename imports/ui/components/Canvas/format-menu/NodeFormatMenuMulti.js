import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import HAlignLeft from "./icons/HAlignLeft";
import HAlignCenter from "./icons/HAlignCenter";
import HAlignRight from "./icons/HAlignRight";
import VAlignTop from "./icons/VAlignTop";
import VAlignCenter from "./icons/VAlignCenter";
import VAlignBottom from "./icons/VAlignBottom";

import "./NodeFormatMenuMulti.scss";

const NodeFormatMenuMulti = () => {
	return (
		<div className="format-menu multi">
			<div className="format-menu-inner">
				<button
					className="align"
					aria-label="Horizontal align left"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				>
					<HAlignLeft />
				</button>
				<button
					className="align h-center"
					aria-label="Horizontal align center"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				>
					<HAlignCenter />
				</button>
				<button
					className="align"
					aria-label="Horizontal align right"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				>
					<HAlignRight />
				</button>
				<button
					className="align"
					aria-label="Vertical align top"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				>
					<VAlignTop />
				</button>
				<button
					className="align v-center"
					aria-label="Vertical align center"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				>
					<VAlignCenter />
				</button>
				<button
					className="align"
					aria-label="Vertical align bottom"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				>
					<VAlignBottom />
				</button>
			</div>
		</div>
	);
};

export default NodeFormatMenuMulti;
