import React from "react";

const VAlignCenter = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.97 17.18">
			<g id="vertical_align_center" data-name="vertical align center">
				<path d="M21,8.59a1,1,0,0,1-1,1h-1.9v3.81a1,1,0,0,1-1,1H12.38a1,1,0,0,1-1-1V9.55H9.51v6.68a1,1,0,0,1-.95.95H3.79a1,1,0,0,1-1-.95V9.55H.92a1,1,0,0,1,0-1.91H2.83V1a1,1,0,0,1,1-.95H8.56a1,1,0,0,1,1,1V7.64h1.9V3.82a1,1,0,0,1,1-1h4.77a1,1,0,0,1,1,1V7.64H20A1,1,0,0,1,21,8.59Z" />
			</g>
		</svg>
	);
};

export default VAlignCenter;
