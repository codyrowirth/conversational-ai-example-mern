import React from "react";

const VAlignTop = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 21">
			<g id="vertical_align_top" data-name="vertical align top">
				<path d="M21,1a1,1,0,0,1-.95,1H1A1,1,0,1,1,1,0H20.05A1,1,0,0,1,21,1ZM8.59,3.82H3.82a1,1,0,0,0-1,.95V20.05a1,1,0,0,0,1,.95H8.59a1,1,0,0,0,1-.95V4.77A1,1,0,0,0,8.59,3.82Zm8.59,0H12.41a1,1,0,0,0-1,.95v9.55a1,1,0,0,0,1,.95h4.77a1,1,0,0,0,1-.95V4.77A1,1,0,0,0,17.18,3.82Z" />
			</g>
		</svg>
	);
};

export default VAlignTop;
