import React from "react";

const HAlignRight = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 20.97">
			<g id="horizontal_align_right" data-name="horizontal align right">
				<path d="M17.18,3.82V8.59a1,1,0,0,1-.95,1H6.68a1,1,0,0,1-.95-1V3.82a1,1,0,0,1,.95-1h9.55A1,1,0,0,1,17.18,3.82ZM20.05,0a1,1,0,0,0-1,1v19.1a1,1,0,0,0,1.91,0V1A1,1,0,0,0,20.05,0ZM16.23,11.45H1a1,1,0,0,0-1,1v4.77a1,1,0,0,0,1,1H16.23a1,1,0,0,0,.95-1V12.41A1,1,0,0,0,16.23,11.45Z" />
			</g>
		</svg>
	);
};

export default HAlignRight;
