import React from "react";

const VAlignBottom = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.97 21">
			<g id="vertical_align_bottom" data-name="vertical align bottom">
				<path d="M21,20.05A1,1,0,0,1,20,21H.92a1,1,0,0,1,0-1.91H20A1,1,0,0,1,21,20.05ZM3.79,17.18H8.56a1,1,0,0,0,1-.95V1a1,1,0,0,0-1-1H3.79a1,1,0,0,0-1,1V16.23A1,1,0,0,0,3.79,17.18Zm8.59,0h4.77a1,1,0,0,0,1-.95V6.68a1,1,0,0,0-1-.95H12.38a1,1,0,0,0-1,.95v9.55A1,1,0,0,0,12.38,17.18Z" />
			</g>
		</svg>
	);
};

export default VAlignBottom;
