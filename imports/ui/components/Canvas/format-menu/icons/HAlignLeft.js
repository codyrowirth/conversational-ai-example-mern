import React from "react";

const HAlignLeft = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 21.03">
			<g id="horizontal_align_left" data-name="horizontal align left">
				<path d="M1,21a1,1,0,0,1-1-1V1A1,1,0,1,1,1.91.92V20.08A1,1,0,0,1,1,21Zm19.09-9.54H4.77a1,1,0,0,0-.95.95v4.77a1,1,0,0,0,.95,1H20.05a1,1,0,0,0,.95-1h0V12.44a1,1,0,0,0-.95-.95ZM4.77,9.58h9.55a1,1,0,0,0,.95-1h0V3.85a1,1,0,0,0-.95-1H4.77a1,1,0,0,0-.95,1V8.62A1,1,0,0,0,4.77,9.58Z" />
			</g>
		</svg>
	);
};

export default HAlignLeft;
