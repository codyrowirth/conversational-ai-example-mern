import React from "react";

const HAlignCenter = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.18 21.06">
			<g id="horizontal_align_center" data-name="horizontal align center">
				<path d="M17.18,12.44v4.77a1,1,0,0,1-.95,1H9.55v1.91a1,1,0,0,1-.93,1,.94.94,0,0,1-1-.92v-2H1a1,1,0,0,1-.95-1V12.44a1,1,0,0,1,1-1H7.64V9.58H3.82a1,1,0,0,1-1-1V3.85a1,1,0,0,1,1-1H7.64V1A1,1,0,0,1,9.55.92v2h3.81a1,1,0,0,1,1,1h0V8.62a1,1,0,0,1-1,1H9.55v1.91h6.68A1,1,0,0,1,17.18,12.44Z" />
			</g>
		</svg>
	);
};

export default HAlignCenter;
