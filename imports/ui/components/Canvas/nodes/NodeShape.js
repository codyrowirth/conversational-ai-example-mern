import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import useShapeStatus from "../../../lib/utilities/useShapeStatus";

import MessageShape from "./shapes/MessageShape";
import ThreadShape from "./shapes/ThreadShape";
import BrancherShape from "./shapes/BrancherShape";
import PortalShape from "./shapes/PortalShape";
import SendShape from "./shapes/SendShape";
import ReturnShape from "./shapes/ReturnShape";

const NodeShape = (props) => {
	const { type, status = "normal" } = props;

	let shapeIcon = "",
		iconTop;

	if (type === "message") {
		shapeIcon = <MessageShape status={status} />;
		iconTop = "0";
	} else if (type === "thread") {
		shapeIcon = <ThreadShape />;
		iconTop = "0";
	} else if (type === "brancher") {
		shapeIcon = <BrancherShape status={status} />;
		iconTop = "0";
	} else if (type === "portal") {
		shapeIcon = <PortalShape />;
		iconTop = "0";
	} else if (type === "send") {
		shapeIcon = <SendShape />;
		iconTop = "-5px";
	} else if (type === "return") {
		shapeIcon = <ReturnShape />;
		iconTop = "8px";
	}

	return (
		<div className="node-shape">
			<div className="node-shape-inner">
				<div className="shape-icon">{shapeIcon}</div>
				{status !== "normal" && (
					<div
						className="status-icon"
						style={{
							marginTop: iconTop,
						}}
					>
						<FontAwesomeIcon
							icon={useShapeStatus(status, "icon")}
							style={{
								color: useShapeStatus(status, "iconColor"),
							}}
						/>
					</div>
				)}
			</div>
		</div>
	);
};

export default NodeShape;
