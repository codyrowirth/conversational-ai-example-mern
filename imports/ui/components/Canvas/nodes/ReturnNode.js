import React, { useContext, useState, useEffect } from "react";
import { Handle } from "react-flow-renderer";

import { CanvasContext } from "../Canvas";

import NodeBody from "./NodeBody";
import NodeShape from "./NodeShape";
import NodeLabel from "./NodeLabel";

import ReturnModal from "../modals/ReturnModal";

import "./node.scss";

const ReturnNode = ({ data, selected, id, xPos, yPos }) => {
	const [returnModal, setReturnModal] = useState(null);

	const { openPanelID, setOpenPanelID, flowInstance, paneClickStatus } =
		useContext(CanvasContext);

	const panelOpen = openPanelID === id;

	const type = "return";

	useEffect(() => {
		if (panelOpen) {
			setReturnModal(openPanelID);
		}
	}, [openPanelID]);

	return (
		<>
			<div className="node-wrap">
				<div className="node">
					<NodeBody type={type}>
						<NodeShape type={type} status={data.status} />
						<NodeLabel
							id={id}
							type={type}
							data={data}
							openPanelID={openPanelID}
						/>
					</NodeBody>
				</div>
				<div
				// style={{ visibility: selected && !panelOpen ? "visible" : "hidden" }}
				>
					<Handle type="default" position="top" id={`t`} />
					<Handle type="default" position="left" id={`l`} />
					<Handle type="default" position="bottom" id={`b`} />
					<Handle type="default" position="right" id={`r`} />
				</div>
			</div>
			<ReturnModal
				setOpenPanelID={setOpenPanelID}
				show={returnModal}
				hide={() => {
					setReturnModal(null);
				}}
			/>
		</>
	);
};

export default ReturnNode;
