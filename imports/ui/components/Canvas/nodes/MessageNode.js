import React, { useState, useContext, useEffect } from "react";
import { Handle } from "react-flow-renderer";
import { usePopper } from "react-popper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { CanvasContext } from "../Canvas";

import NodeBody from "./NodeBody";
import NodeShape from "./NodeShape";
import NodeLabel from "./NodeLabel";
import MessageModal from "../modals/MessageModal";

import "./node.scss";
import { useTracker } from "meteor/react-meteor-data";
import Messages from "../../../../api/collections/Messages";

const MessageNode = ({ id, data, selected, xPos, yPos }) => {
	const [messageType, setMessageType] = useState();

	const [leftWingReferenceElement, setLeftWingReferenceElement] =
		useState(null);
	const [leftWingPopperElement, setLeftWingPopperElement] = useState(null);
	const [leftWingOpen, setLeftWingOpen] = useState(false);
	const [rightWingReferenceElement, setRightWingReferenceElement] =
		useState(null);
	const [rightWingPopperElement, setRightWingPopperElement] = useState(null);
	const [rightWingOpen, setRightWingOpen] = useState(false);

	const [messageModal, setMessageModal] = useState(null);

	const { styles: leftWingStyles, attributes: leftWingAttributes } = usePopper(
		leftWingReferenceElement,
		leftWingPopperElement,
		{
			placement: "auto-end",
			modifiers: [
				{
					name: "flip",
					options: {
						allowedAutoPlacements: ["left-end"],
					},
				},
				{
					name: "offset",
					options: {
						offset: [0, -75],
					},
				},
				{
					name: "preventOverflow",
					options: {
						mainAxis: false,
						altAxis: false,
					},
				},
			],
		}
	);

	const { styles: rightWingStyles, attributes: rightWingAttributes } =
		usePopper(rightWingReferenceElement, rightWingPopperElement, {
			placement: "auto-end",
			modifiers: [
				{
					name: "flip",
					options: {
						allowedAutoPlacements: ["right-end"],
					},
				},
				{
					name: "offset",
					options: {
						offset: [0, -75],
					},
				},
				{
					name: "preventOverflow",
					options: {
						mainAxis: false,
						altAxis: false,
					},
				},
			],
		});

	const { openPanelID, setOpenPanelID, flowInstance, paneClickStatus } =
		useContext(CanvasContext);

	const panelOpen = openPanelID === id;

	const type = "message";
	const comments = 0;
	const unmatched = 0;

	useEffect(() => {
		if (panelOpen) {
			setMessageModal(openPanelID);
		}
	}, [openPanelID]);

	const message = useTracker(() => {
		const messages = Messages.find({
			shapeID: id,
		}).fetch();

		if (messages.length) {
			return messages[0];
		}
	});

	return (
		<>
			<div className={`node-wrap ${type}`}>
				<div className="node node-wings">
					<div
						className={`node-wing left-wing ${
							data?.writingPrompt ? "content" : "no-content"
						} ${leftWingOpen}`}
						ref={setLeftWingReferenceElement}
						onClick={() => {
							setLeftWingOpen(!leftWingOpen);
						}}
					>
						<div className="node-wing-inner">
							<div className="wing-icon">
								<FontAwesomeIcon icon="pen-alt" />
							</div>
						</div>
					</div>
					{leftWingOpen === true && (
						<div
							className="node-wing-content left-wing-content"
							ref={setLeftWingPopperElement}
							style={leftWingStyles.popper}
							{...leftWingAttributes.popper}
						>
							<div className="node-wing-content-inner">
								<div className="wing-text">
									{data?.writingPrompt || "(Set prompt)"}
								</div>
							</div>
						</div>
					)}
					<div className="node-body-wrap">
						<NodeBody type={type}>
							<NodeShape
								type={type}
								status={!message?.status ? "backlog" : message.status}
							/>
							<NodeLabel
								id={id}
								type={type}
								data={data}
								// comments={comments}
								// unmatched={unmatched}
								openPanelID={openPanelID}
							/>
						</NodeBody>
					</div>
					<div
						className={`node-wing right-wing ${
							data?.listenerAttribute ? "content" : "no-content"
						} ${messageType} ${
							messageType === "statement" ? false : rightWingOpen
						}`}
						ref={setRightWingReferenceElement}
						onClick={() => {
							setRightWingOpen(!rightWingOpen);
						}}
					>
						<div className="node-wing-inner">
							<div
								className={`wing-icon ${
									messageType === "statement" && "disabled"
								}`}
							>
								<FontAwesomeIcon icon="headphones" />
							</div>
						</div>
					</div>
					{messageType !== "statement" && rightWingOpen === true && (
						<div
							className="node-wing-content right-wing-content"
							ref={setRightWingPopperElement}
							style={rightWingStyles.popper}
							{...rightWingAttributes.popper}
						>
							<div className="node-wing-content-inner">
								<div className="wing-text">
									{messageType === "statement"
										? ""
										: data?.listenerAttribute || "(Set listener)"}
								</div>
							</div>
						</div>
					)}
				</div>
				<div
				// style={{ visibility: selected && !panelOpen ? "visible" : "hidden" }}
				>
					<Handle type="default" position="top" id={`t`} />
					<Handle type="default" position="left" id={`l`} />
					<Handle type="default" position="bottom" id={`b`} />
					<Handle type="default" position="right" id={`r`} />
				</div>
			</div>
			<MessageModal
				type={type}
				shapeID={id}
				data={data}
				messageType={messageType}
				setMessageType={setMessageType}
				comments={comments}
				unmatched={unmatched}
				setOpenPanelID={setOpenPanelID}
				show={messageModal}
				hide={() => {
					setMessageModal(null);
				}}
			/>
		</>
	);
};

export default MessageNode;
