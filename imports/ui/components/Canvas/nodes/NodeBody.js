import React from "react";

const NodeBody = (props) => {
	const { type, children } = props;

	return (
		<div className={`node-body ${type}`}>
			<div className="node-body-inner">{children}</div>
		</div>
	);
};

export default NodeBody;
