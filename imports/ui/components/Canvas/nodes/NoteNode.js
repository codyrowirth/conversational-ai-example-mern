import React, { useContext, useState } from "react";
import { CanvasContext } from "../Canvas";
import NoteModal from "../modals/NoteModal";
import "./node.scss";

const NoteNode = ({ data, id, selected }) => {
	const [noteModal, setNoteModal] = useState(null);

	return (
		<div className={`node-wrap note-node ${selected ? "selected" : ""}`}>
			<div
				className="node"
				onDoubleClick={(event) => {
					event.preventDefault();
					setNoteModal(true);
				}}
			>
				{data?.label || "Double click to add text."}
			</div>
			<NoteModal
				shapeID={id}
				noteContent={data?.label || ""}
				show={noteModal}
				hide={() => {
					setNoteModal(null);
				}}
			/>
		</div>
	);
};

export default NoteNode;
