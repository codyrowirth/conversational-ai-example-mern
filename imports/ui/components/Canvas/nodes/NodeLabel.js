import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import NotifBadge from "../../../lib/components/NotifBadge/NotifBadge";
import { titleCase } from "title-case";

const NodeLabel = (props) => {
	const { id, type, data, comments, unmatched, openPanelID } = props;

	let labelContents = "";
	if (type === "start" || type === "end") {
		labelContents = titleCase(type);
	} else if (data && data.label && data.label !== "[[unnamed]]") {
		labelContents = data.label;
	}

	return (
		<div className="node-label">
			<div className="node-name">{labelContents}</div>

			{((comments && comments > 0) || (unmatched && unmatched > 0)) && (
				<div className="notifs">
					{comments && comments > 0 && (
						<NotifBadge count={comments} small={true} />
					)}
					{unmatched && unmatched > 0 && (
						<NotifBadge count={unmatched} small={true} alt={true} />
					)}
				</div>
			)}

			{id && <FontAwesomeIcon icon={openPanelID === id ? "times" : "plus"} />}
		</div>
	);
};

export default NodeLabel;
