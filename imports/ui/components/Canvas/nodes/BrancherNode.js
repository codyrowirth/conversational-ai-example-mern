import React, { useContext, useState, useEffect } from "react";
import { Handle } from "react-flow-renderer";

import { CanvasContext } from "../Canvas";

import { useTracker } from "meteor/react-meteor-data";
import Branchers from "../../../../api/collections/Branchers";

import NodeBody from "./NodeBody";
import NodeShape from "./NodeShape";
import NodeLabel from "./NodeLabel";
import BrancherModal from "../modals/BrancherModal";

import "./node.scss";

const BrancherNode = ({ id, data, selected, xPos, yPos }) => {
	const [brancherModal, setBrancherModal] = useState(null);

	const { openPanelID, setOpenPanelID, flowInstance, paneClickStatus } =
		useContext(CanvasContext);

	const panelOpen = openPanelID === id;

	const type = "brancher";
	const comments = 0;

	useEffect(() => {
		if (panelOpen) {
			setBrancherModal(openPanelID);
		}
	}, [openPanelID]);

	const brancher = useTracker(() => {
		return Branchers.findOne({
			shapeID: id,
		});
	});

	return (
		<>
			<div className="node-wrap">
				<div className="node">
					<NodeBody type={type}>
						<NodeShape
							type={type}
							status={
								data.status || (!brancher?.status ? "backlog" : brancher.status)
							}
						/>
						<NodeLabel
							id={id}
							type={type}
							data={data}
							comments={comments}
							openPanelID={openPanelID}
						/>
					</NodeBody>
				</div>
				<div
					className="handles"
					// style={{ visibility: selected && !panelOpen ? "visible" : "hidden" }}
				>
					<Handle type="default" position="top" id={`t`} />
					<Handle type="default" position="left" id={`l`} />
					<Handle type="default" position="bottom" id={`b`} />
					<Handle type="default" position="right" id={`r`} />
				</div>
			</div>
			<BrancherModal
				type={type}
				shapeID={id}
				data={data}
				comments={comments}
				setOpenPanelID={setOpenPanelID}
				show={brancherModal}
				hide={() => {
					setBrancherModal(null);
				}}
			/>
		</>
	);
};

export default BrancherNode;
