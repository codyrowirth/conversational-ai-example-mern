import React from "react";
import { Handle } from "react-flow-renderer";

import NodeBody from "./NodeBody";
import NodeLabel from "./NodeLabel";

const EndNode = ({ selected }) => {
	const type = "end";

	return (
		<div className="node-wrap">
			<div className="node no-panel">
				<NodeBody type={type}>
					<NodeLabel type={type} />
				</NodeBody>
			</div>
			<div
			// style={{ visibility: selected ? "visible" : "hidden" }}
			>
				<Handle type="default" position="top" id={`t`} />
				<Handle type="default" position="left" id={`l`} />
				<Handle type="default" position="bottom" id={`b`} />
				<Handle type="default" position="right" id={`r`} />
			</div>
		</div>
	);
};

export default EndNode;
