import React, { useContext, useState, useEffect } from "react";
import { Handle } from "react-flow-renderer";

import { CanvasContext } from "../Canvas";

import NodeBody from "./NodeBody";
import NodeShape from "./NodeShape";
import NodeLabel from "./NodeLabel";

import SendModal from "../modals/SendModal";

import "./node.scss";

const SendNode = ({ data, selected, id, xPos, yPos }) => {
	const [sendModal, setSendModal] = useState(null);

	const { openPanelID, setOpenPanelID, flowInstance, paneClickStatus } =
		useContext(CanvasContext);

	const panelOpen = openPanelID === id;

	const type = "send";

	useEffect(() => {
		if (panelOpen) {
			setSendModal(openPanelID);
		}
	}, [openPanelID]);

	return (
		<>
			<div className="node-wrap">
				<div className="node">
					<NodeBody type={type}>
						<NodeShape type={type} status={data.status} />
						<NodeLabel
							id={id}
							type={type}
							data={data}
							openPanelID={openPanelID}
						/>
					</NodeBody>
				</div>
				<div
				// style={{ visibility: selected && !panelOpen ? "visible" : "hidden" }}
				>
					<Handle type="default" position="top" id={`t`} />
					<Handle type="default" position="left" id={`l`} />
					<Handle type="default" position="bottom" id={`b`} />
					<Handle type="default" position="right" id={`r`} />
				</div>
			</div>
			<SendModal
				shapeID={id}
				setOpenPanelID={setOpenPanelID}
				show={sendModal}
				hide={() => {
					setSendModal(null);
				}}
			/>
		</>
	);
};

export default SendNode;
