import React from "react";
import useShapeStatus from "../../../../lib/utilities/useShapeStatus";

const ReturnShape = (props) => {
	const { status } = props;

	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.99 50">
			<g>
				<path
					className="shape-bg"
					d="M27.89,1.79,49.65,45.3a3.24,3.24,0,0,1-1.45,4.35,3.21,3.21,0,0,1-1.46.35H3.24A3.25,3.25,0,0,1,0,46.75,3.29,3.29,0,0,1,.34,45.3L22.09,1.79a3.25,3.25,0,0,1,5.8,0Z"
				/>
				<path
					style={{ fill: useShapeStatus(status, "color") }}
					d="M27,11.25,42.25,41.71a2.27,2.27,0,0,1-1,3,2.34,2.34,0,0,1-1,.24H9.77A2.27,2.27,0,0,1,7.5,42.72a2.21,2.21,0,0,1,.24-1L23,11.25a2.27,2.27,0,0,1,3.05-1A2.2,2.2,0,0,1,27,11.25Z"
				/>
			</g>
		</svg>
	);
};

export default ReturnShape;
