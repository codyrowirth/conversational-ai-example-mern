import React from "react";
import useShapeStatus from "../../../../lib/utilities/useShapeStatus";

const PortalShape = (props) => {
	const { status } = props;

	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50">
			<g>
				<path
					className="shape-bg"
					d="M25,0h0A25,25,0,0,1,50,25h0A25,25,0,0,1,25,50h0A25,25,0,0,1,0,25H0A25,25,0,0,1,25,0Z"
				/>
				<circle
					style={{ fill: useShapeStatus(status, "color") }}
					cx="25"
					cy="25"
					r="19.5"
				/>
			</g>
		</svg>
	);
};

export default PortalShape;
