import React from "react";
import useShapeStatus from "../../../../lib/utilities/useShapeStatus";

const ThreadShape = (props) => {
	const { status } = props;

	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.99 50">
			<g>
				<g>
					<path
						className="shape-bg"
						d="M27.9,48.21,49.65,4.7A3.26,3.26,0,0,0,48.2.34,3.32,3.32,0,0,0,46.75,0H3.25A3.25,3.25,0,0,0,0,3.25,3.2,3.2,0,0,0,.35,4.7L22.1,48.21a3.25,3.25,0,0,0,5.8,0Z"
					/>
					<path
						className="shape-bg"
						d="M27.89,1.8,49.65,45.3a3.26,3.26,0,0,1-1.45,4.36,3.33,3.33,0,0,1-1.46.34H3.24A3.24,3.24,0,0,1,0,46.75,3.32,3.32,0,0,1,.34,45.3L22.09,1.8a3.24,3.24,0,0,1,5.8,0Z"
					/>
				</g>
				<g>
					<path
						style={{ fill: useShapeStatus(status, "color") }}
						d="M27,39.25,42.25,8.79a2.27,2.27,0,0,0-1-3,2.21,2.21,0,0,0-1-.24H9.77a2.27,2.27,0,0,0-2,3.29L23,39.25a2.28,2.28,0,0,0,3.05,1A2.2,2.2,0,0,0,27,39.25Z"
					/>
					<path
						style={{ fill: useShapeStatus(status, "color") }}
						d="M27,10.76,42.25,41.21a2.27,2.27,0,0,1-1,3,2.21,2.21,0,0,1-1,.24H9.77a2.27,2.27,0,0,1-2-3.29L23,10.76a2.28,2.28,0,0,1,3.05-1A2.22,2.22,0,0,1,27,10.76Z"
					/>
				</g>
			</g>
		</svg>
	);
};

export default ThreadShape;
