import React from "react";
import useShapeStatus from "../../../../lib/utilities/useShapeStatus";

const BrancherShape = (props) => {
	const { status } = props;

	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50">
			<g>
				<path
					className="shape-bg"
					d="M23.75,49.46.53,26.1a1.83,1.83,0,0,1,0-2.58h0L23.67.52a1.84,1.84,0,0,1,2.58,0h0L49.46,23.91a1.82,1.82,0,0,1,0,2.57l-23.14,23A1.83,1.83,0,0,1,23.75,49.46Z"
				/>
				<path
					style={{ fill: useShapeStatus(status, "color") }}
					d="M24.12,42.12,7.87,25.77a1.29,1.29,0,0,1,0-1.8l16.2-16.1a1.28,1.28,0,0,1,1.8,0L42.12,24.24a1.27,1.27,0,0,1,0,1.8l-16.2,16.1A1.28,1.28,0,0,1,24.12,42.12Z"
				/>
			</g>
		</svg>
	);
};

export default BrancherShape;
