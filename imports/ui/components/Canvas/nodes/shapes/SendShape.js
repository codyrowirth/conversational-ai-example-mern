import React from "react";
import useShapeStatus from "../../../../lib/utilities/useShapeStatus";

const SendShape = (props) => {
	const { status } = props;

	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.99 50">
			<g>
				<path
					className="shape-bg"
					d="M27.89,48.21,49.64,4.7A3.25,3.25,0,0,0,48.2.34,3.33,3.33,0,0,0,46.74,0H3.24A3.25,3.25,0,0,0,0,3.25,3.32,3.32,0,0,0,.34,4.7L22.09,48.21a3.25,3.25,0,0,0,5.8,0Z"
				/>
				<path
					style={{ fill: useShapeStatus(status, "color") }}
					d="M27,38.75,42.25,8.29a2.28,2.28,0,0,0-1-3,2.21,2.21,0,0,0-1-.24H9.77A2.28,2.28,0,0,0,7.49,7.27a2.39,2.39,0,0,0,.24,1L23,38.75a2.28,2.28,0,0,0,3.05,1A2.24,2.24,0,0,0,27,38.75Z"
				/>
			</g>
		</svg>
	);
};

export default SendShape;
