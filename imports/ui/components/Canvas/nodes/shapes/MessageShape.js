import React from "react";
import useShapeStatus from "../../../../lib/utilities/useShapeStatus";

const MessageShape = (props) => {
	const { status } = props;

	return (
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50">
			<g>
				<rect className="shape-bg" width="50" height="50" rx="7" />
				<rect
					style={{ fill: useShapeStatus(status, "color") }}
					x="5.5"
					y="5.5"
					width="39"
					height="39"
					rx="3"
				/>
			</g>
		</svg>
	);
};

export default MessageShape;
