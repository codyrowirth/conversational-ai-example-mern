import React, { useContext, useState, useEffect } from "react";
import { Handle, useZoomPanHelper } from "react-flow-renderer";

import { CanvasContext } from "../Canvas";

import NodeBody from "./NodeBody";
import NodeShape from "./NodeShape";
import NodeLabel from "./NodeLabel";

import PortalModal from "../modals/PortalModal";

import "./node.scss";
import { useSnackbar } from "notistack";
import { useHistory, useParams } from "react-router-dom";
import manageThesaurusModal from "../modals/inner-modals/ManageThesaurusModal";

const PortalNode = ({ data, selected, id, xPos, yPos }) => {
	const [portalModal, setPortalModal] = useState(null);
	const { enqueueSnackbar } = useSnackbar();
	const { canvasID } = useParams();
	const { setCenter } = useZoomPanHelper();
	const history = useHistory();

	const { openPanelID, setOpenPanelID, flowInstance, paneClickStatus } =
		useContext(CanvasContext);

	const panelOpen = openPanelID === id;

	const type = "portal";

	useEffect(() => {
		if (panelOpen) {
			setPortalModal(openPanelID);
		}
	}, [openPanelID]);

	const rightClick = async (evt) => {
		evt.preventDefault();
		const portalWithOutgoingLines = await Meteor.callPromise(
			`shapes.portalType`,
			id
		);
		if (portalWithOutgoingLines.length) {
			enqueueSnackbar(
				"TCS can not currently find the source of destination portals",
				{
					variant: "warning",
					key: "sourceOfDestinationPortal",
				}
			);
		} else {
			const matchingPortal = await Meteor.callPromise(
				`shapes.followPortal`,
				id
			);
			if (!matchingPortal) {
				enqueueSnackbar("No matching portal found.", {
					variant: "error",
					key: "noMatchingPortal",
				});
			} else if (matchingPortal.canvasID === canvasID) {
				setCenter(
					matchingPortal.position.x + 262.5,
					matchingPortal.position.y + 25,
					1
				);
			} else {
				history.push(
					`/canvases/${matchingPortal.canvasID}/${matchingPortal._id}?openModal=false`
				);
			}
		}
	};

	return (
		<>
			<div className="node-wrap" onContextMenu={rightClick}>
				<div className="node">
					<NodeBody type={type}>
						<NodeShape type={type} status={data?.status} />
						<NodeLabel
							id={id}
							type={type}
							data={data}
							openPanelID={openPanelID}
						/>
					</NodeBody>
				</div>
				<div
				// style={{ visibility: selected && !panelOpen ? "visible" : "hidden" }}
				>
					<Handle type="default" position="top" id={`t`} />
					<Handle type="default" position="left" id={`l`} />
					<Handle type="default" position="bottom" id={`b`} />
					<Handle type="default" position="right" id={`r`} />
				</div>
			</div>
			<PortalModal
				shapeID={id}
				data={data}
				setOpenPanelID={setOpenPanelID}
				show={portalModal}
				hide={() => {
					setPortalModal(null);
				}}
			/>
		</>
	);
};

export default PortalNode;
