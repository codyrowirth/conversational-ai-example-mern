import React, { useContext, useState } from "react";
import { CanvasContext } from "../Canvas";
import TextModal from "../modals/TextModal";
import "./node.scss";
import { Handle } from "react-flow-renderer";

const EdgeNode = ({ id, data, selected }) => {
	const [textModal, setTextModal] = useState(null);

	return (
		<div className={`node-wrap edge-node ${selected ? "selected" : ""}`}>
			<Handle
				type="default"
				position="top"
				id={`t`}
				isConnectable={false}
				style={{ opacity: 0 }}
			/>
			<Handle
				type="default"
				position="bottom"
				id={`b`}
				isConnectable={false}
				style={{ opacity: 0 }}
			/>
			<Handle
				type="default"
				position="left"
				id={`l`}
				isConnectable={false}
				style={{ opacity: 0 }}
			/>
			<Handle
				type="default"
				position="right"
				id={`r`}
				isConnectable={false}
				style={{ opacity: 0 }}
			/>

			<div
				className="node"
				onDoubleClick={(event) => {
					event.preventDefault();
					setTextModal(true);
				}}
			>
				{data?.label || ""}
			</div>
			<TextModal
				shapeID={id}
				textContent={data?.label || ""}
				show={textModal}
				hide={() => {
					setTextModal(null);
				}}
			/>
		</div>
	);
};

export default EdgeNode;
