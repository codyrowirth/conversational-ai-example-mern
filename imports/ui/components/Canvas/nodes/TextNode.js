import React, { useContext, useState } from "react";
import { CanvasContext } from "../Canvas";
import TextModal from "../modals/TextModal";
import "./node.scss";

const TextNode = ({ id, data, selected }) => {
	const [textModal, setTextModal] = useState(null);

	return (
		<div className={`node-wrap text-node ${selected ? "selected" : ""}`}>
			<div
				className="node"
				onDoubleClick={(event) => {
					event.preventDefault();
					setTextModal(true);
				}}
			>
				{data?.label || "Double click to add text."}
			</div>
			<TextModal
				shapeID={id}
				textContent={data?.label || ""}
				show={textModal}
				hide={() => {
					setTextModal(null);
				}}
			/>
		</div>
	);
};

export default TextNode;
