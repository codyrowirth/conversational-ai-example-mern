import React from "react";
import Draggable from "react-Draggable";
import {
	getBezierPath,
	getSmoothStepPath,
	getEdgeCenter,
	getMarkerEnd,
} from "react-flow-renderer";

import EdgeNode from "../nodes/EdgeNode";

const foreignObjectSize = 40;

const onEdgeClick = (evt, id) => {
	evt.stopPropagation();
	alert(`remove ${id}`);
};

export default function OrthogonalEdge({
	id,
	sourceX,
	sourceY,
	targetX,
	targetY,
	sourcePosition,
	targetPosition,
	style = {},
	data,
	arrowHeadType,
	markerEndId,
}) {
	const edgePath = getSmoothStepPath({
		sourceX,
		sourceY,
		sourcePosition,
		targetX: targetX,
		targetY: targetY,
		targetPosition,
		borderRadius: 0,
	});
	const markerEnd = getMarkerEnd(arrowHeadType, markerEndId);
	const [edgeCenterX, edgeCenterY] = getEdgeCenter({
		sourceX,
		sourceY,
		targetX,
		targetY,
	});

	return (
		<>
			<path
				id={id}
				style={style}
				className="react-flow__edge-path"
				d={edgePath}
				markerEnd={markerEnd}
			/>
			{/* <text>
        <textPath href={`#${id}`} style={{ fontSize: '12px' }} startOffset="50%" textAnchor="middle">
          {label}
        </textPath>
      </text>
      <Draggable>
        <foreignObject
          width={foreignObjectSize}
          height={foreignObjectSize}
          x={edgeCenterX - foreignObjectSize / 2}
          y={edgeCenterY - foreignObjectSize / 2}
          className="edgebutton-foreignobject"
          requiredExtensions="http://www.w3.org/1999/xhtml"
        >
        <body>
          <button
            className="edgebutton"
            onClick={(event) => onEdgeClick(event, id)}
          >
            ×
          </button>
        </body>
      </foreignObject>
      </Draggable>   */}
		</>
	);
}
