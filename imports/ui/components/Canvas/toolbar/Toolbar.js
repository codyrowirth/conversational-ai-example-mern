import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useZoomPanHelper } from "react-flow-renderer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { CanvasContext } from "../Canvas";

import LastSaved from "../../../lib/components/LastSaved/LastSaved";
import OpenPanel from "./panels/OpenPanel";
import InfoPanel from "./panels/InfoPanel";
import CollabPanel from "./panels/CollabPanel";
import MigrationPanel from "./panels/MigrationPanel";

import "./Toolbar.scss";
import { useSnackbar } from "notistack";

const Toolbar = ({
	flowInstance,
	grid,
	setGrid,
	collab,
	comments,
	lineShape,
	setLineShape,
}) => {
	const [zoomLevel, setZoomLevel] = useState(1);
	const [panelContent, setPanelContent] = useState();
	const [panelOpen, setPanelOpen] = useState(false);

	const { canvas } = useContext(CanvasContext);

	const { zoomTo } = useZoomPanHelper();
	const history = useHistory();
	const { enqueueSnackbar, closeSnackbar } = useSnackbar();

	const onPanelButton = (panelContentID) => {
		if (panelOpen) {
			if (panelContent === panelContentID) {
				setPanelContent(null);
				setPanelOpen(!panelOpen);
			} else {
				setPanelContent(panelContentID);
			}
		} else {
			setPanelContent(panelContentID);
			setPanelOpen(!panelOpen);
		}
	};

	const loadPanel = (panelContentID) => {
		if (panelContentID === "open") {
			return <OpenPanel />;
		} else if (panelContentID === "info") {
			return (
				<InfoPanel
					canvas={canvas}
					lineShape={lineShape}
					setLineShape={setLineShape}
				/>
			);
		} else if (panelContentID === "collab") {
			return <CollabPanel collab={collab} comments={comments} />;
		} else if (panelContentID === "migration") {
			return <MigrationPanel />;
		}
	};

	return (
		<div className={`canvas-toolbar-wrap ${panelOpen ? "panel-open" : ""}`}>
			<div
				className={`canvas-toolbar-panel ${
					panelContent ? `panel-content-${panelContent}` : ""
				}`}
			>
				{panelContent && loadPanel(panelContent)}
			</div>
			<nav className="canvas-toolbar">
				<div className="toolbar-inner">
					<div className="branding">
						<Link to="/">
							<img
								className="icon"
								src={`${Meteor.absoluteUrl()}images/tcs-logo-light.svg`}
								alt="Logo"
							/>
						</Link>
					</div>
					<div className="tools">
						<div className="tools-inner">
							<button
								className="tool"
								aria-label="Back"
								onClick={(event) => {
									event.preventDefault();

									if (panelOpen) {
										setPanelContent(null);
										setPanelOpen(!panelOpen);
									} else {
										history.goBack();
									}
								}}
							>
								<FontAwesomeIcon icon="chevron-left" />
							</button>
							<button
								className={`tool ${
									panelOpen && panelContent === "open" ? "active" : ""
								}`}
								aria-label="Open canvas"
								onClick={(event) => {
									event.preventDefault();
									onPanelButton("open");
								}}
							>
								<FontAwesomeIcon icon="folder-open" />
							</button>
							<button
								className={`tool ${
									panelOpen && panelContent === "info" ? "active" : ""
								}`}
								aria-label="Canvas information"
								onClick={(event) => {
									event.preventDefault();
									onPanelButton("info");
								}}
							>
								<FontAwesomeIcon icon="info" />
							</button>
							<button
								className={`tool ${
									panelOpen && panelContent === "collab" ? "active" : ""
								} ${collab.length > 0 || comments > 0 ? "alert" : ""}`}
								aria-label="Collaboration"
								onClick={(event) => {
									event.preventDefault();
									onPanelButton("collab");
								}}
							>
								<FontAwesomeIcon icon="user" />
							</button>
							<button
								className="tool"
								aria-label="Undo"
								onClick={(event) => {
									event.preventDefault();

									if (panelOpen) {
										setPanelContent(null);
										setPanelOpen(!panelOpen);
									}

									alert("TO DO");
								}}
							>
								<FontAwesomeIcon icon="undo-alt" />
							</button>
							<button
								className="tool"
								aria-label="Redo"
								onClick={(event) => {
									event.preventDefault();

									if (panelOpen) {
										setPanelContent(null);
										setPanelOpen(!panelOpen);
									}

									alert("TO DO");
								}}
							>
								<FontAwesomeIcon icon="redo-alt" />
							</button>
							<button
								className="tool"
								aria-label="Zoom out"
								onClick={(event) => {
									event.preventDefault();

									const newZoomLevel =
										zoomLevel > 0.4 ? zoomLevel - 0.2 : zoomLevel;
									zoomTo(newZoomLevel);
									setZoomLevel(newZoomLevel);
								}}
							>
								<FontAwesomeIcon icon="search-minus" />
							</button>
							<button
								className="tool"
								aria-label="Zoom in"
								onClick={(event) => {
									event.preventDefault();

									const newZoomLevel =
										zoomLevel < 3 ? zoomLevel + 0.2 : zoomLevel;
									zoomTo(newZoomLevel);
									setZoomLevel(newZoomLevel);
								}}
							>
								<FontAwesomeIcon icon="search-plus" />
							</button>
							{/* TO DO: Only show the below button to Tuzag folks */}
							<button
								className={`tool ${
									panelOpen && panelContent === "migration" ? "active" : ""
								}`}
								aria-label="Migration"
								onClick={(event) => {
									event.preventDefault();
									onPanelButton("migration");
								}}
							>
								<FontAwesomeIcon icon="random" />
							</button>
							<button
								className="tool"
								aria-label="Validate"
								onClick={async (event) => {
									event.preventDefault();
									enqueueSnackbar("Validation in progress. Please wait.", {
										variant: "info",
										key: "validateStarted",
										persist: true,
									});

									await Meteor.callPromise(`shapes.validate`, canvas._id);
									closeSnackbar("validateStarted");

									enqueueSnackbar("Validation complete. Please check shapes.", {
										variant: "success",
										key: "validateDone",
									});
								}}
							>
								<FontAwesomeIcon icon="exclamation" />
							</button>
						</div>
					</div>
					<div className="info">
						<div className="status">
							Last saved{" "}
							<span>
								<LastSaved timestamp={canvas.updatedAt} />
							</span>
						</div>
						<div className="tools">
							<div className="tools-inner">
								<button
									className="tool"
									aria-label="Toggle grid"
									onClick={(event) => {
										event.preventDefault();

										if (panelOpen) {
											setPanelContent(null);
											setPanelOpen(!panelOpen);
										}

										setGrid(!grid);
									}}
								>
									<FontAwesomeIcon icon="border-all" />
								</button>
								<button
									className="tool"
									aria-label="Help"
									onClick={(event) => {
										event.preventDefault();

										if (panelOpen) {
											setPanelContent(null);
											setPanelOpen(!panelOpen);
										}

										alert("TO DO");
									}}
								>
									<FontAwesomeIcon icon="question" />
								</button>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	);
};

export default Toolbar;
