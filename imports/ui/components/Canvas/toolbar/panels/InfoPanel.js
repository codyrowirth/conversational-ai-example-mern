import React, { useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useTracker } from "meteor/react-meteor-data";
import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import InputRHF from "../../../../lib/components/Form/InputRHF";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import CanvasGroups from "../../../../../api/collections/CanvasGroups";
import { PropagateLoader } from "react-spinners";
import Button from "../../../../lib/components/Button/Button";
import { saveOnBlur } from "../../../../lib/utilities/saveOnBlurRHFMeteor";
import { useParams } from "react-router-dom";
import LineShapeConfModal from "./panel-modals/LineShapeConfModal";
// import ConfirmationModal from "../ConfirmationModal";

const InfoPanel = ({ canvas, lineShape, setLineShape }) => {
	const [parentTree, setParentTree] = useState([]);
	const [lineShapeConfModal, setLineShapeConfModal] = useState(null);
	const [breadcrumbs, setBreadcrumbs] = useState();

	const { canvasID } = useParams();

	const schema = yup.object().shape({
		name: yup.string().required("Please enter a canvas name."),
	});

	useEffect(async () => {
		setBreadcrumbs(
			await Meteor.callPromise(`canvases.findParentTree`, canvas._id)
		);
	}, [canvas?._id]);

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
	} = useForm({
		resolver: yupResolver(schema),
		defaultValues: {
			name: canvas.name,
		},
	});

	const handleBlur = (evt) => {
		saveOnBlur("canvases", canvasID, evt, canvasID);
	};

	if (!breadcrumbs) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<div className="panel-inner">
			<h2>{canvas.name}</h2>
			<GreyBox>
				<div className="breadcrumbs">
					{breadcrumbs.map((group) => {
						return (
							<span key={group.id} className="breadcrumb">
								{group.name + "  >  "}
							</span>
						);
					})}
					<span className="breadcrumb">{canvas.name}</span>
				</div>
			</GreyBox>
			<div className="canvas-info">
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<InputRHF
								type="text"
								name="name"
								label="Name"
								onBlur={handleBlur}
								register={register}
								error={errors}
							/>
						</div>
					</div>
					<div className="grid-row">
						<div className="grid-col-full">
							<SelectRHF
								name="lines"
								label="Line Shape"
								options={["SmoothStep", "Curved", "Straight"].map((option) => ({
									value: option.toLowerCase(),
									label: option,
								}))}
								panel={true}
								ref={null}
								error={{}}
								onChange={(val) => {
									setLineShapeConfModal(val);
									if (val.value === "curved") {
										setLineShape("default");
									} else {
										setLineShape(val.value);
									}
								}}
							/>
						</div>
					</div>
				</form>
			</div>
			<div className="update-messages">
				<h3>Bulk Update</h3>
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<SelectRHF
								name="status"
								label="Message Status"
								options={["Backlog", "To Do", "Doing", "Blocked", "Done"].map(
									(option) => ({
										value: option.toLowerCase(),
										label: option,
									})
								)}
								panel={true}
								ref={null}
								error={{}}
								onChange={(val) => {
									Meteor.call("messages.bulkStatusUpdate", {
										canvasID,
										status: val.value,
									});
								}}
							/>
						</div>
					</div>
					{/*<div className="grid-row">*/}
					{/*	<div className="grid-col-full">*/}
					{/*		<Controller*/}
					{/*			name="owner"*/}
					{/*			control={control}*/}
					{/*			render={({ field }) => {*/}
					{/*				return (*/}
					{/*					<SelectRHF*/}
					{/*						name={field.name}*/}
					{/*						label="Owner"*/}
					{/*						options={["TO DO"].map((option) => ({*/}
					{/*							value: option.toLowerCase(),*/}
					{/*							label: option,*/}
					{/*						}))}*/}
					{/*						panel={true}*/}
					{/*						{...field}*/}
					{/*						ref={null}*/}
					{/*						error={errors}*/}
					{/*					/>*/}
					{/*				);*/}
					{/*			}}*/}
					{/*		/>*/}
					{/*	</div>*/}
					{/*</div>*/}
				</form>
			</div>
			{/* <div className="button-wrap">
				<Button text="Save" onClick = {handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick = {onCancel} />
			</div> */}
			<LineShapeConfModal
				show={lineShapeConfModal}
				hide={() => {
					setLineShapeConfModal(null);
				}}
				canvasID={canvasID}
			/>
		</div>
	);
};

export default InfoPanel;
