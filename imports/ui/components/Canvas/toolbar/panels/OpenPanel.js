import React, { useState } from "react";
import NewCanvasForm from "../../../Canvases/NewCanvasForm";
import Button from "../../../../lib/components/Button/Button";
import NestedData from "../../../../lib/components/NestedData/NestedData";

const OpenPanel = () => {
	const [newCanvasFormExpanded, setNewCanvasFormExpanded] = useState(false);

	return (
		<div className="panel-inner">
			<div className="heading-with-button">
				<div>
					<h2>Open Canvas</h2>
				</div>
				<div>
					<Button
						text={newCanvasFormExpanded ? "Cancel" : `Add New Canvas`}
						icon={newCanvasFormExpanded ? "times" : "plus"}
						onClick={() => {
							setNewCanvasFormExpanded(!newCanvasFormExpanded);
						}}
					/>
				</div>
			</div>
			<NewCanvasForm
				newCanvasFormExpanded={newCanvasFormExpanded}
				setNewCanvasFormExpanded={setNewCanvasFormExpanded}
				panel={true}
			/>
			<NestedData type="canvas" toolbar="open" />
		</div>
	);
};

export default OpenPanel;
