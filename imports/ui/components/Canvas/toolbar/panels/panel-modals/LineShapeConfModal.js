import React from "react";

import Button from "../../../../../lib/components/Button/Button";
import Modal from "../../../../../lib/components/Modal/Modal";

const LineShapeConfModal = ({ show, hide, canvasID }) => {
	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>All new lines will use the {show?.label.toLowerCase()} shape.</h3>
			<p>
				Do you want to change all existing lines to the{" "}
				{show?.label.toLowerCase()} shape, as well?
			</p>
			<div className="button-wrap">
				<Button
					text="Yes"
					onClick={() => {
						Meteor.call(
							"shapes.edgeBulkUpdate",
							canvasID,
							show?.label.toLowerCase()
						);
						hide();
					}}
				/>
				<Button
					text="No"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default LineShapeConfModal;
