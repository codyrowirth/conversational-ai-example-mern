import React, { useContext, useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import SelectRHF from "../../../../lib/components/Form/SelectRHF";
import Button from "../../../../lib/components/Button/Button";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";
import CanvasesToMigrate from "../../../../../api/collections/CanvasesToMigrate";
import ShapesToMigrate from "../../../../../api/collections/ShapesToMigrate";
import { CanvasContext } from "../../Canvas";
import shape from "@material-ui/core/styles/shape";

const MigrationPanel = () => {
	const [selectedCanvas, setSelectedCanvas] = useState();
	const { shapeToMigrate, setShapeToMigrate } = useContext(CanvasContext);
	const [brancherNextShapes, setBrancherNextShapes] = useState();

	const { canvases, shapes, loading } = useTracker(() => {
		let shapes;
		const canvasSub = Meteor.subscribe(
			"canvasesToMigrate",
			Meteor.user().projectID
		);

		if (!canvasSub.ready()) {
			return { loading: true };
		}

		if (selectedCanvas) {
			const shapesSub = Meteor.subscribe(
				"shapesToMigrateByCanvasID",
				selectedCanvas.value
			);

			if (!shapesSub.ready()) {
				return { loading: true };
			}

			shapes = ShapesToMigrate.find(
				{
					canvasID: selectedCanvas.value,
				},
				{ sort: { migratedToCanvasID: -1, name: 1 } }
			)
				.fetch()
				.sort((a, b) => {
					if (a?.migratedToCanvasID || b?.migratedToCanvasID) {
						return -99;
					} else {
						if (a.name < b.name) {
							return -1;
						}
						if (a.name > b.name) {
							return 1;
						}
						return 0;
					}
				});
		}

		const canvases = CanvasesToMigrate.find({
			projectID: Meteor.user().projectID,
		}).fetch();

		return { canvases, loading: false, shapes };
	});

	useEffect(() => {
		return () => {
			setShapeToMigrate(null);
		};
	}, []);

	useEffect(() => {
		if (shapeToMigrate?.type === "brancher" && shapeToMigrate?.nextShapeID) {
			const nextShapesArr = shapeToMigrate.nextShapeID
				.split(",")
				.map((id) => parseInt(id));

			const nextShapes = ShapesToMigrate.find({
				canvasID: selectedCanvas.value,
				shapeID: { $in: nextShapesArr },
			}).fetch();
			setBrancherNextShapes(nextShapes);
		} else {
			setBrancherNextShapes(null);
		}
	}, [shapeToMigrate]);

	if (loading) {
		return (
			<div className="panel-inner">
				<div style={{ textAlign: "center", paddingBottom: 20 }}>
					<PropagateLoader color="white" />
				</div>
			</div>
		);
	}

	return (
		<div className="panel-inner">
			<h2>Migration</h2>
			<div className="migration-info">
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<SelectRHF
								name={"canvas"}
								label="Canvas Name (v3)"
								options={canvases.map((option) => ({
									value: option._id,
									label: option.name,
								}))}
								panel={true}
								ref={null}
								value={selectedCanvas}
								onChange={(val) => {
									setSelectedCanvas(val);
								}}
							/>
						</div>
					</div>
					{shapes && (
						<div className="grid-row">
							<div className="grid-col-full">
								<SelectRHF
									name={"shapeName"}
									label="Shape Name (v3)"
									options={shapes.map((option) => ({
										value: option._id,
										label: `${option?.migratedToCanvasID ? "*" : ""}${
											option.name
										} (${option.type})`,
									}))}
									panel={true}
									ref={null}
									value={
										shapeToMigrate
											? {
													value: shapeToMigrate._id,
													label: `${shapeToMigrate.name} (${shapeToMigrate.type})`,
											  }
											: null
									}
									onChange={(value) => {
										const newShape = shapes
											? shapes.find((shape) => shape._id === value.value)
											: null;

										setShapeToMigrate(newShape);
									}}
								/>
								{/* TO DO: Only show the below if it's appropriate */}
								{/*<p className="form-response brancher-alert">*/}
								{/*	This shape is attached to the most recently placed brancher*/}
								{/*	and must be placed next.*/}
								{/*</p>*/}
							</div>
						</div>
					)}
					{brancherNextShapes && (
						<div className="grid-row">
							<div className="grid-col-full">
								<p style={{ marginBottom: 0 }}>Connected outgoing shapes:</p>
								<ul>
									{brancherNextShapes.map((shape) => {
										return (
											<li key={shape._id}>
												{shape.name} ({shape.type})
											</li>
										);
									})}
								</ul>
							</div>
						</div>
					)}
					<div className="button-wrap">
						<Button
							text="Clear"
							onClick={(event) => {
								event.preventDefault();
								setShapeToMigrate(null);
								setSelectedCanvas(null);
							}}
						/>
					</div>
				</form>
			</div>
		</div>
	);
};

export default MigrationPanel;
