import React, { useState } from "react";

import GreyBox from "../../../../lib/components/GreyBox/GreyBox";
import NotifBadge from "../../../../lib/components/NotifBadge/NotifBadge";
import CommentsModal from "../../modals/inner-modals/CommentsModal";

const CollabPanel = ({ collab, comments }) => {
	const [commentsModal, setCommentsModal] = useState();

	return (
		<div className="panel-inner">
			<h2>Collaboration</h2>
			{(collab.length > 0 || comments > 0) && (
				<GreyBox>
					<div className="notifs">
						{collab.length > 0 && (
							<div className="notif flex">
								<NotifBadge type="collab" small={true} alt={true} />
								<div>
									{collab.length > 0 &&
										collab.map((user, index) => {
											let separator = "";

											if (collab.length > 1) {
												if (index < collab.length - 2) {
													separator = ", ";
												} else if (index === collab.length - 2) {
													separator = " and ";
												}
											}

											return (
												<span key={user}>
													<strong>{user}</strong>
													{separator}
												</span>
											);
										})}
									{collab.length > 1 ? " are" : " is"} editing.
								</div>
							</div>
						)}
						{comments > 0 && (
							<div className="notif">
								<NotifBadge count={comments} small={true} />
								<strong>New</strong> Comments (
								<button
									className="action-link"
									onClick={(event) => {
										event.preventDefault();
										setCommentsModal(true);
									}}
								>
									view
								</button>
								)
							</div>
						)}
					</div>
				</GreyBox>
			)}
			<div className="panel-section interaction">
				<h3>Interaction</h3>
				<button
					className="action-link notif-link"
					onClick={(event) => {
						event.preventDefault();
						setCommentsModal(true);
					}}
				>
					<span className="notif-type">Comments</span> ({comments})
				</button>
			</div>
			<div className="revisions">
				<h3>Revisions</h3>
				<div className="revision">
					<button
						className="action-link date"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					>
						August 8, 2021 at 11:00 AM
					</button>
					<p className="user">Ashe Abbott DiBlasi</p>
				</div>

				<div className="revision">
					<button
						className="action-link date"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					>
						August 8, 2021 at 8:58 AM
					</button>
					<p className="user">Max Matthews</p>
				</div>

				<div className="revision">
					<button
						className="action-link date"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					>
						August 7, 2021 at 4:03 PM
					</button>
					<p className="user">Dave Bulger</p>
				</div>

				<div className="revision">
					<button
						className="action-link date"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					>
						August 6, 2021 at 1:13 PM
					</button>
					<p className="user">Ashe Abbott DiBlasi</p>
				</div>

				<div className="revision">
					<button
						className="action-link date"
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					>
						August 6, 2021 at 10:15 AM
					</button>
					<p className="user">Rebekah Grome</p>
				</div>
			</div>
			<CommentsModal
				show={commentsModal}
				hide={() => {
					setCommentsModal(null);
				}}
			/>
		</div>
	);
};

export default CollabPanel;
