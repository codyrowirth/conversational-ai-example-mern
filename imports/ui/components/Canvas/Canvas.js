import React, {
	useState,
	useRef,
	createContext,
	useEffect,
	useContext,
} from "react";
import ReactFlow, {
	ReactFlowProvider,
	MiniMap,
	Background,
	useStoreState,
	useStore,
	BezierEdge,
	StraightEdge,
	SmoothStepEdge,
	useZoomPanHelper,
	useStoreActions,
} from "react-flow-renderer";
import "./Canvas.scss";
import { useTracker } from "meteor/react-meteor-data";
import ShapesDB from "../../../api/collections/Shapes";
import { PropagateLoader } from "react-spinners";
import MessageNode from "./nodes/MessageNode";
import InsertShapePanel from "./insert-shape/InsertShapePanel";
import BrancherNode from "./nodes/BrancherNode";
import EndNode from "./nodes/EndNode";
import NoteNode from "./nodes/NoteNode";
import PortalNode from "./nodes/PortalNode";
import ReturnNode from "./nodes/ReturnNode";
import SendNode from "./nodes/SendNode";
import StartNode from "./nodes/StartNode";
import TextNode from "./nodes/TextNode";
import ThreadNode from "./nodes/ThreadNode";
import Toolbar from "./toolbar/Toolbar";
import Canvases from "../../../api/collections/Canvases";
import NodeFormatMenuMulti from "./format-menu/NodeFormatMenuMulti";
import LineShapeMenu from "./format-menu/LineShapeMenu";
import ShapesToMigrate from "../../../api/collections/ShapesToMigrate";
import { useHistory, useLocation, useParams } from "react-router-dom";
import useMousetrap from "react-hook-mousetrap";
import OrthogonalEdge from "./edges/OrthogonalEdge";
import GreyBox from "../../lib/components/GreyBox/GreyBox";
import qs from "qs";

export const CanvasContext = createContext({});

// TO DO: Canvas editors and canvas-level comment count from DB
const collab = ["Ashe Abbott DiBlasi", "Someone Else", "Another Person"];
const comments = 3;
// const collab = [];
// const comments = 0;

let brancherID;

let lastClickPosition;

const ReactFlowChild = (props) => {
	const {
		lineShapeMenu,
		setLineShapeMenu,
		multiSelect,
		setMultiSelect,
		lineShape,
		setLineShape,
		elements,
	} = props;

	const [insertShapePanelPosition, setInsertShapePanelPosition] = useState();
	const [brancherNextShapes, setBrancherNextShapes] = useState();
	const { setCenter } = useZoomPanHelper();
	const { shapeID } = useParams();
	const history = useHistory();
	const setSelectedElements = useStoreActions(
		(actions) => actions.setSelectedElements
	);

	let connectStartID, sourceHandleID;

	const {
		openPanelID,
		setOpenPanelID,
		flowInstance,
		setPaneClickStatus,
		shapeToMigrate,
		setShapeToMigrate,
	} = useContext(CanvasContext);

	const { canvasID } = useParams();

	const updatePosition = (nodeID, position) => {
		Meteor.call("shapes.update", nodeID, { position });
		Meteor.call("canvases.update", canvasID, {});
	};

	const store = useStore();

	useEffect(() => {
		if (shapeID && elements) {
			const matchingShape = elements.find((shapeDB) => shapeDB._id === shapeID);
			if (matchingShape) {
				setCenter(
					matchingShape.position.x + 262.5,
					matchingShape.position.y + 25,
					1
				);
				history.replace(`/canvases/${canvasID}`);
			}
		}
	}, [shapeID, elements]);

	useMousetrap(["left", "up", "right", "down"], (event) => {
		const selectedElems = store.getState().selectedElements;

		const nodes = store
			.getState()
			.nodes.filter(
				(n) => selectedElems?.findIndex((se) => se.id === n.id) !== -1
			);

		const updatedNodes = [];

		nodes?.forEach((elem) => {
			let nodePosition = elem.position;

			if (event.code === "ArrowLeft") {
				nodePosition = { x: elem.position.x - 12.5, y: elem.position.y };
			} else if (event.code === "ArrowUp") {
				nodePosition = { x: elem.position.x, y: elem.position.y - 12.5 };
			} else if (event.code === "ArrowRight") {
				nodePosition = { x: elem.position.x + 12.5, y: elem.position.y };
			} else if (event.code === "ArrowDown") {
				nodePosition = { x: elem.position.x, y: elem.position.y + 12.5 };
			}

			updatedNodes.push({
				id: elem.id,
				type: elem.type,
				position: nodePosition,
			});
		});

		for (const updatedNode of updatedNodes) {
			updatePosition(updatedNode.id, updatedNode.position);
		}
	});

	useMousetrap(["mod+c"], (evt) => {
		const selectedElems = store.getState().selectedElements;

		window.localStorage.copiedShapeIDs = JSON.stringify(
			selectedElems.map((element) => element.id)
		);
	});

	useMousetrap(["mod+v", "mod+shift+v"], async (evt) => {
		const copiedShapeIDs = JSON.parse(window.localStorage.copiedShapeIDs);
		const newShapeIDs = await Meteor.callPromise(
			`shapes.paste`,
			copiedShapeIDs,
			canvasID,
			lastClickPosition,
			evt.shiftKey
		);

		setSelectedElements(
			newShapeIDs.map((id) => ({
				id,
			}))
		);
	});

	useMousetrap(["mod+d"], async (evt) => {
		const selectedElems = store.getState().selectedElements;

		const copiedShapeIDs = selectedElems.map((element) => element.id);
		const newShapeIDs = await Meteor.callPromise(
			`shapes.paste`,
			copiedShapeIDs,
			canvasID,
			lastClickPosition,
			evt.shiftKey
		);

		setSelectedElements(
			newShapeIDs.map((id) => ({
				id,
			}))
		);
	});

	const onNodeDragStop = React.useCallback(
		(e, node) => {
			// get currently selected elements
			const selectedElems = store.getState().selectedElements;
			// get those as nodes to be able to access `__rf`
			const nodes = store
				.getState()
				.nodes.filter(
					(n) => selectedElems?.findIndex((se) => se.id === n.id) !== -1
				);

			const updatedNodes = [];

			nodes?.forEach((elem) => {
				// do your updating stuff
				updatedNodes.push({
					id: elem.id,
					type: elem.type,
					// @ts-ignore
					position:
						elem.__rf && elem.__rf.position
							? elem.__rf.position
							: elem.position,
				});
			});

			for (const updatedNode of updatedNodes) {
				updatePosition(updatedNode.id, updatedNode.position);
			}
		},
		[store, updatePosition]
	);

	const onSelectionDragStop = onNodeDragStop;

	const onElementsRemove = (elementsToRemove) => {
		for (const elementToRemove of elementsToRemove) {
			Meteor.call("shapes.remove", elementToRemove.id);
		}
	};

	const onPaneContextMenu = (evt) => {
		evt.preventDefault();
		setInsertShapePanelPosition({ x: evt.clientX - 170, y: evt.clientY });
		if (openPanelID) {
			setOpenPanelID(null);
		}
	};

	const onConnectStart = (evt, { nodeId, handleType }) => {
		sourceHandleID = evt?.target?.closest(".react-flow__handle")?.dataset
			?.handleid;
		connectStartID = nodeId;
	};

	const onConnectEnd = (evt) => {
		const targetShapeID =
			evt?.target?.closest(".react-flow__node")?.dataset?.id;

		const destinationHandleID = evt?.target?.closest(".react-flow__handle")
			?.dataset?.handleid;

		let closestLinkIndex, elements;

		if (!destinationHandleID) {
			elements = Array.from(
				document.querySelectorAll(
					`.react-flow__handle[data-nodeid="${targetShapeID}"]`
				)
			);

			let linkCoords = elements.map((link) => {
				let rect = link.getBoundingClientRect();
				return [rect.x, rect.y];
			});

			let distances = [];

			linkCoords.forEach((linkCoord) => {
				let distance = Math.hypot(
					linkCoord[0] - parseInt(evt.clientX),
					linkCoord[1] - parseInt(evt.clientY)
				);
				distances.push(parseInt(distance));
			});

			closestLinkIndex = distances.indexOf(Math.min(...distances));
		}

		if (connectStartID && targetShapeID && connectStartID !== targetShapeID) {
			const existingLineConnectingShapes = props.elements.find(
				(shape) =>
					shape.source === connectStartID && targetShapeID === shape.target
			);
			if (existingLineConnectingShapes) {
				Meteor.call("shapes.remove", existingLineConnectingShapes.id);
			}
			Meteor.call("shapes.insert", {
				source: connectStartID,
				target: targetShapeID,
				sourceHandle: sourceHandleID,
				targetHandle:
					destinationHandleID || elements[closestLinkIndex]?.dataset?.handleid,
				canvasID,
				type: lineShape,
			});
		}
		connectStartID = null;
		sourceHandleID = null;
	};

	const onNodeDoubleClick = (evt, node) => {
		if (
			node.id === openPanelID &&
			!evt.target.closest(".panel") &&
			!evt.target.closest(".modal") &&
			!evt.target.closest(".overlay") &&
			!evt.target.closest(".node-wing") &&
			!evt.target.closest(".node-wing-content")
		) {
			setOpenPanelID(null);
		} else if (
			!evt.target.closest(".node-wing") &&
			!evt.target.closest(".node-wing-content")
		) {
			setOpenPanelID(node.id);
			setPaneClickStatus(false);
		}
	};

	const onMoveStart = () => {
		if (insertShapePanelPosition) {
			setInsertShapePanelPosition(null);
		}
	};

	const onElementClick = (evt) => {
		if (insertShapePanelPosition) {
			setInsertShapePanelPosition(null);
		}

		if (
			openPanelID &&
			!evt.target.closest(".panel") &&
			!evt.target.closest(".modal") &&
			!evt.target.closest(".overlay") &&
			!evt.target.closest(".node-wing") &&
			!evt.target.closest(".node-wing-content")
		) {
			setOpenPanelID(null);
		}
	};

	const onClick = (evt) => {
		if (shapeToMigrate) {
			const shapePosition = flowInstance.project({
				x: evt.clientX - 170,
				y: evt.clientY,
			});
			const emptyLabelObjects = ["text", "note"];

			let additionalData = {};

			Meteor.call(
				"shapes.insert",
				{
					data: {
						label: emptyLabelObjects.includes(shapeToMigrate.type)
							? ""
							: shapeToMigrate.name,
						...additionalData,
					},
					position: shapePosition,
					type: shapeToMigrate.type,
					canvasID,
					sqlID: shapeToMigrate.sqlID,
					sqlShapeID: shapeToMigrate.shapeID,
					sqlMessageID: shapeToMigrate.sqlMessageID,
					sqlCanvasID: shapeToMigrate.sqlCanvasID,
					messageID: shapeToMigrate.messageID,
				},
				(err, newID) => {
					if (err) {
						// eslint-disable-next-line
						console.error(err);
					} else {
						if (shapeToMigrate.type === "message") {
							Meteor.call("messages.migrate", {
								newShapeID: newID,
								sqlMessageID: shapeToMigrate.sqlMessageID,
								canvasID,
							});
						}

						if (brancherID) {
							//add the connection line between the brancher and the newly inserted shape

							Meteor.call("shapes.insert", {
								source: brancherID,
								target: newID,
								canvasID,
								sourceHandle: "b",
								targetHandle: "t",
								sqlTargetShapeID: shapeToMigrate.shapeID,
								sqlTargetCanvasID: shapeToMigrate.canvasID,
								type: lineShape,
							});
						}

						if (brancherNextShapes && brancherNextShapes.length === 0) {
							//we've migrated all the shapes into the canvas, so deal with the rules migration
							Meteor.call("branchers.migrateRules", {
								newBrancherShapeID: brancherID,
							});
						}

						if (
							shapeToMigrate.type === "brancher" &&
							(!brancherNextShapes || brancherNextShapes.length === 0)
						) {
							brancherID = newID;
							const nextShapesArr = shapeToMigrate.nextShapeID.split(",");
							if (nextShapesArr.length) {
								const nextShapeID = nextShapesArr.shift();
								const nextShape = ShapesToMigrate.findOne({
									canvasID: shapeToMigrate.canvasID,
									shapeID: parseInt(nextShapeID),
								});
								setBrancherNextShapes(nextShapesArr);
								setShapeToMigrate(nextShape);
							} else {
								setShapeToMigrate(null);
							}

							Meteor.call("branchers.migrate", {
								newBrancherShapeID: brancherID,
								oldShapeID: shapeToMigrate.shapeID,
								oldCanvasID: shapeToMigrate.sqlCanvasID,
							});
						} else if (brancherNextShapes && brancherNextShapes.length) {
							let nextShapes = [...brancherNextShapes];
							const nextShapeID = nextShapes.shift();
							const nextShape = ShapesToMigrate.findOne({
								canvasID: shapeToMigrate.canvasID,
								shapeID: parseInt(nextShapeID),
							});

							setBrancherNextShapes(nextShapes);
							setShapeToMigrate(nextShape);
						} else {
							brancherID = null;
							setShapeToMigrate(null);
							setBrancherNextShapes(null);
						}
					}
				}
			);

			Meteor.call("shapesToMigrate.update", shapeToMigrate._id, {
				migratedToCanvasID: canvasID,
			});
		} else {
			lastClickPosition = flowInstance.project({
				x: evt.clientX - 170,
				y: evt.clientY,
			});
		}
	};

	const onPaneClick = (event) => {
		setPaneClickStatus(openPanelID);
		setOpenPanelID(null);
	};

	const onSelectionChange = (event) => {
		if (event && event.some((e) => Object.keys(edgeTypes).includes(e.type))) {
			setLineShapeMenu(event);
		} else {
			setLineShapeMenu(null);
		}

		if (
			event &&
			event.filter((e) => !Object.keys(edgeTypes).includes(e.type)).length > 1
		) {
			setMultiSelect(event);
		} else {
			setMultiSelect(null);
		}
	};

	let miniMapClass = "";

	if (multiSelect !== null && lineShapeMenu === null) {
		miniMapClass = "format-menu-open";
	} else if (lineShapeMenu !== null && multiSelect === null) {
		miniMapClass = "line-menu-open";
	} else if (multiSelect !== null && lineShapeMenu !== null) {
		miniMapClass = "both-menus-open";
	}

	const nodeTypes = {
		message: MessageNode,
		brancher: BrancherNode,
		end: EndNode,
		note: NoteNode,
		portal: PortalNode,
		return: ReturnNode,
		send: SendNode,
		start: StartNode,
		text: TextNode,
		thread: ThreadNode,
	};

	const edgeTypes = {
		default: BezierEdge,
		orthogonal: OrthogonalEdge,
		straight: StraightEdge,
		smoothstep: SmoothStepEdge,
	};

	return (
		<ReactFlow
			elements={props.elements}
			arrowHeadColor="$slate"
			nodeTypes={nodeTypes}
			style={{ height: "100vh", width: "100vw" }}
			onNodeDragStop={onNodeDragStop}
			snapGrid={[37.5, 37.5]}
			snapToGrid
			connectionLineType="straight"
			nodesConnectable
			defaultZoom={1}
			minZoom={0}
			maxZoom={10}
			onPaneContextMenu={onPaneContextMenu}
			onLoad={props.onLoad}
			onElementsRemove={onElementsRemove}
			onConnectStart={onConnectStart}
			onConnectEnd={onConnectEnd}
			onNodeDoubleClick={onNodeDoubleClick}
			onMoveStart={onMoveStart}
			onElementClick={onElementClick}
			onlyRenderVisibleElements
			zoomOnScroll={false}
			panOnScroll={true}
			panOnScrollSpeed={1}
			preventScrolling={false}
			onClick={onClick}
			onPaneClick={onPaneClick}
			onSelectionChange={onSelectionChange}
			onSelectionDragStop={onSelectionDragStop}
			zoomOnDoubleClick={false}
			selectionKeyCode="Meta"
			multiSelectionKeyCode="Shift"
		>
			{insertShapePanelPosition && (
				<InsertShapePanel
					position={insertShapePanelPosition}
					close={setInsertShapePanelPosition}
					flowInstance={props.flowInstance}
					setOpenPanelID={setOpenPanelID}
					setPaneClickStatus={props.paneClickStatus}
					shapes={props.elements}
				/>
			)}

			<MiniMap
				className={miniMapClass}
				nodeColor="#2f3d47"
				nodeBorderRadius={30}
				nodeStrokeWidth={15}
			/>

			{props.grid && (
				<Background variant="lines" gap={25} size={1} color="#e3e3e3" />
			)}
		</ReactFlow>
	);
};

const Canvas = ({ ...props }) => {
	const { canvasID, shapeID } = useParams();
	const [flowInstance, setFlowInstance] = useState();
	const [openPanelID, setOpenPanelID] = useState();
	const [grid, setGrid] = useState(true);
	const [paneClickStatus, setPaneClickStatus] = useState(false);
	const flowWrapper = useRef(null);
	const [shapeToMigrate, setShapeToMigrate] = useState();
	const [multiSelect, setMultiSelect] = useState(null);
	const [lineShapeMenu, setLineShapeMenu] = useState(null);
	const [lineShape, setLineShape] = useState("default");
	const [breadcrumbs, setBreadcrumbs] = useState();
	const location = useLocation();

	useEffect(() => {
		if (shapeID !== openPanelID) {
			const openModal = !qs.parse(location.search, { ignoreQueryPrefix: true })
				.openModal;
			if (openModal) {
				setOpenPanelID(shapeID);
			}
		}
	}, [shapeID]);

	const { shapes, canvas, isLoading } = useTracker(() => {
		const handler = Meteor.subscribe("shapes", canvasID);
		const handler2 = Meteor.subscribe("canvases", canvasID);
		const handler3 = Meteor.subscribe("messagesByCanvasID", canvasID);
		//don't wait for this one, just have it ready for when they open a panel
		const handler4 = Meteor.subscribe("portals", Meteor.user()?.projectID);

		if (!handler.ready() || !handler2.ready() || !handler3.ready()) {
			return { isLoading: true };
		}

		return {
			canvas: Canvases.findOne({ _id: canvasID }),
			shapes: ShapesDB.find({ canvasID })
				.fetch()
				.map((shape) => ({
					...shape,
					id: shape._id,
					arrowHeadType: "arrowclosed",
				})),
			// messages: Messages.find({
			// 	canvasID,
			// }).fetch(),
		};
	});

	useEffect(async () => {
		if (canvas?._id) {
			setBreadcrumbs(
				await Meteor.callPromise(`canvases.findParentTree`, canvas._id)
			);
		}
	}, [canvas?._id]);

	useEffect(() => {
		const prevTitle = document.title;

		if (canvas?.name) {
			document.title = `${canvas?.name || "Canvas"} | TCS`;
		}
		return () => {
			document.title = prevTitle;
		};
	}, [canvas]);

	if (isLoading) {
		return (
			<div className="loader">
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<CanvasContext.Provider
			value={{
				openPanelID,
				setOpenPanelID,
				flowInstance,
				canvasID,
				canvas,
				paneClickStatus,
				setPaneClickStatus,
				shapeToMigrate,
				setShapeToMigrate,
			}}
		>
			<ReactFlowProvider>
				<Toolbar
					flowInstance={flowInstance}
					grid={grid}
					setGrid={setGrid}
					collab={collab}
					comments={comments}
					lineShape={lineShape}
					setLineShape={setLineShape}
				/>
				{breadcrumbs && (
					<GreyBox className="breadcrumbs-wrap">
						{breadcrumbs.map((group) => {
							return (
								<span key={group.id} className="breadcrumb">
									{group.name + "  >  "}
								</span>
							);
						})}
						<span className="breadcrumb">{canvas.name}</span>
					</GreyBox>
				)}
				<div id="canvas" ref={flowWrapper}>
					<ReactFlowChild
						elements={shapes}
						onLoad={(instance) => {
							setFlowInstance(instance);
						}}
						flowInstance={flowInstance}
						paneClickStatus={setPaneClickStatus}
						lineShapeMenu={lineShapeMenu}
						setLineShapeMenu={setLineShapeMenu}
						multiSelect={multiSelect}
						setMultiSelect={setMultiSelect}
						grid={grid}
						lineShape={lineShape}
						setLineShape={setLineShape}
					/>
				</div>
			</ReactFlowProvider>
			{lineShapeMenu && (
				<LineShapeMenu
					className={multiSelect && lineShapeMenu && "both-menus-open"}
					edgeToChange={lineShapeMenu}
				/>
			)}
			{multiSelect && <NodeFormatMenuMulti />}
		</CanvasContext.Provider>
	);
};

export default Canvas;
