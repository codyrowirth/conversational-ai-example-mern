import React, { Fragment, useContext } from "react";
import { CanvasContext } from "../Canvas";

import MessageShape from "../nodes/shapes/MessageShape";
import ThreadShape from "../nodes/shapes/ThreadShape";
import BrancherShape from "../nodes/shapes/BrancherShape";
import PortalShape from "../nodes/shapes/PortalShape";
import SendShape from "../nodes/shapes/SendShape";
import ReturnShape from "../nodes/shapes/ReturnShape";

import "./InsertShapePanel.scss";

const shapeButtons = {
	column1: {
		message: {
			type: "message",
			label: "Message",
			icon: <MessageShape />,
			class: "",
		},
		brancher: {
			type: "brancher",
			label: "Brancher",
			icon: <BrancherShape />,
			class: "",
		},
		portal: {
			type: "portal",
			label: "Portal",
			icon: <PortalShape />,
			class: "",
		},
		send: {
			type: "send",
			label: "Send",
			icon: <SendShape />,
			class: "",
		},
		return: {
			type: "return",
			label: "Return",
			icon: <ReturnShape />,
			class: "",
		},
	},
	column2: {
		thread: {
			type: "thread",
			label: "Thread",
			icon: <ThreadShape />,
			class: "",
		},
		start: {
			type: "start",
			label: "Start",
			icon: "",
			class: "no-icon",
		},
		end: {
			type: "end",
			label: "End",
			icon: "",
			class: "no-icon end",
		},
		note: {
			type: "note",
			label: "Note",
			icon: "/images/canvas-icons/shape-note.svg",
			class: "text-icon note",
		},
		text: {
			type: "text",
			label: "Text",
			icon: "/images/canvas-icons/shape-text.svg",
			class: "text-icon",
		},
	},
};

const InsertShapePanel = ({
	position,
	close,
	setOpenPanelID,
	flowInstance,
	setPaneClickStatus,
	shapes,
}) => {
	const { canvasID } = useContext(CanvasContext);
	const addShape = (shape) => {
		const shapePosition = flowInstance.project({
			x: position.x,
			y: position.y,
		});
		const emptyLabelObjects = ["text", "note"];
		Meteor.call(
			"shapes.insert",
			{
				data: { label: emptyLabelObjects.includes(shape) ? "" : "[[unnamed]]" },
				position: shapePosition,
				type: shape,
				canvasID,
			},
			(err, newID) => {
				if (err) {
					// eslint-disable-next-line
					console.error(err);
				} else {
					const interval = setTimeout(() => {
						// console.log(shapes, shapes[0].id, newID);
						// const shapeCreated = shapes.find((shape) => shape.id === newID);
						// console.log(shapeCreated);
						// if (shapeCreated) {
						// 	clearInterval(interval);
						// setOpenPanelID(newID);
						setPaneClickStatus(false);
						// }
					}, 250);
				}
			}
		);
		close();
	};
	return (
		<div
			className="insert-shape-panel"
			style={{ top: position.y, left: position.x }}
		>
			<p className="h6">Insert</p>
			<div className="shape-panel-buttons">
				<div className="shape-panel-buttons-inner">
					{Object.values(shapeButtons).map((shapeButtonGroup, index) => {
						return (
							<div
								key={index === 0 ? "col-1" : "col-2"}
								className="shape-panel-col"
							>
								{Object.values(shapeButtonGroup).map((shapeButton) => (
									<button
										key={shapeButton.type}
										className={`shape-button ${shapeButton.class}`}
										onClick={() => {
											addShape(shapeButton.type);
										}}
									>
										<div className="shape-button-inner">
											<div className="shape-button-icon">
												{shapeButton.class.includes("text-icon") ? (
													<img src={`${shapeButton.icon}`} />
												) : (
													shapeButton.icon
												)}
											</div>
											<div className="shape-button-text">
												<div className="shape-button-text-inner">
													{shapeButton.label}
												</div>
											</div>
										</div>
									</button>
								))}
							</div>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default InsertShapePanel;
