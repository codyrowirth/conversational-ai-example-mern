import React from "react";
import EntrySettingsInfoForm from "./EntrySettingsInfoForm";

const EntrySettingsInfo = (props) => {
	const { id, entry, groups } = props;

	return <EntrySettingsInfoForm {...props} entry={entry} groups={groups} />;
};

export default EntrySettingsInfo;
