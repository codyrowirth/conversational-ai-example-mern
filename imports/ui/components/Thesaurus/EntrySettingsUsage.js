import React from "react";
import { Link } from "react-router-dom";
import Attributes from "../../../api/collections/Attributes";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";

import GreyBox from "../../lib/components/GreyBox/GreyBox";

const EntrySettingsUsage = (props) => {
	const { id, options } = props;

	const listOfAttributeIDs = options.map((option) => option.attributeID);

	const { attributes, isLoading } = useTracker(() => {
		const handleAttributes = Meteor.subscribe("attributes", listOfAttributeIDs);
		if (!handleAttributes.ready()) {
			return { attributes: null, isLoading: true };
		}

		let attributes = Attributes.find()
			.fetch()
			.map((attribute) => ({ name: attribute.name, id: attribute._id }));

		return {
			isLoading: false,
			attributes,
		};
	});

	return (
		<section className="entry-usage">
			<GreyBox>
				{!attributes || attributes?.length === 0 ? (
					<h2> Not Currently Used in Any Attributes</h2>
				) : (
					<div>
						<h2>Currently Used In</h2>
						<div style={{ maxHeight: 300, overflowY: "auto" }}>
							<ul>
								{isLoading ? (
									<PropagateLoader color="#2f3d47" />
								) : (
									attributes.map((attribute) => {
										const matchingOption = options.find(
											(option) => option.attributeID === attribute.id
										);
										return (
											<li key={attribute.id}>
												<Link to={"/attributes/" + attribute.id}>
													{" "}
													{attribute.name + "(" + matchingOption?.name + ")"}
													{""}
												</Link>
											</li>
										);
									})
								)}
							</ul>
						</div>
					</div>
				)}
			</GreyBox>
		</section>
	);
};

export default EntrySettingsUsage;
