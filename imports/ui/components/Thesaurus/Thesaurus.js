import React from "react";
import Entries from "./Entries";

const Thesaurus = () => {
	return (
		<section className="thesaurus">
			<h1>Thesaurus</h1>
			<Entries />
		</section>
	);
};

export default Thesaurus;
