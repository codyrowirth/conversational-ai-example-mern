import React from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";

const UnlinkEntryModal = ({ show, hide }) => {
	// TO DO: Get entry name
	const entry = {
		name: "Entry Name Placeholder",
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Do you want to unlink this entry?</h3>
			<p>
				Please confirm that you’d like to unlink the{" "}
				<strong>{entry && entry?.name}</strong> entry.
			</p>
			<div className="button-wrap">
				<Button
					text="Unlink"
					onClick={() => {
						// Meteor.call("entries.remove", entry._id);
						alert("TO DO");
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default UnlinkEntryModal;
