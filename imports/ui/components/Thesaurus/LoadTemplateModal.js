import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import Button from "../../lib/components/Button/Button";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import Modal from "../../lib/components/Modal/Modal";

const LoadTemplateModal = ({ templates, setLoadedTemplate, show, hide }) => {
	const schema = yup.object().shape({
		template: yup.object().required("Please select a template."),
	});

	const {
		control,
		formState: { errors },
		handleSubmit,
		setValue,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		if (data.template.value !== null) {
			const templateID = data.template.value;
			setLoadedTemplate("");
			setLoadedTemplate(templateID);

			hide();
			setValue("template", null);
		}
	};

	const onCancel = () => {
		hide();
		setValue("template", null);
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Load Template</h3>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="template"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Template"
										options={templates.map((synonym) => ({
											value: synonym._id,
											label: synonym.name,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
			</form>
			<div className="button-wrap">
				<Button text="Load" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default LoadTemplateModal;
