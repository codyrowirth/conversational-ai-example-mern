import React, { useState } from "react";
import NewEntryForm from "./NewEntryForm";
import Button from "../../lib/components/Button/Button";
import NestedData from "../../lib/components/NestedData/NestedData";

function Entries() {
	const [newEntryFormExpanded, setNewEntryFormExpanded] = useState(false);

	return (
		<section>
			<div className="heading-with-button flex">
				<div>
					<h2>Entries</h2>
				</div>
				<div>
					<Button
						text={newEntryFormExpanded ? "Cancel" : "Add New Entry"}
						icon={newEntryFormExpanded ? "times" : "plus"}
						onClick={() => {
							setNewEntryFormExpanded(!newEntryFormExpanded);
						}}
					/>
				</div>
			</div>
			<NewEntryForm
				newEntryFormExpanded={newEntryFormExpanded}
				setNewEntryFormExpanded={setNewEntryFormExpanded}
			/>
			<NestedData type={"thesaurusEntry"} />
		</section>
	);
}

export default Entries;
