import React, { useEffect, useState } from "react";
import TextareaRHF from "../../lib/components/Form/TextareaRHF";
import { titleCase } from "title-case";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTracker } from "meteor/react-meteor-data";
import SynonymTemplates from "../../../api/collections/SynonymTemplates";
import LoadTemplateModal from "./LoadTemplateModal";
import SaveTemplateModal from "./SaveTemplateModal";
import DeleteTemplateModal from "./DeleteTemplateModal";
import Textarea from "../../lib/components/Form/Textarea";
import caseInsensitiveDedupe from "../../lib/utilities/caseInsensitiveDedupe";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import "../../lib/styles/contextmenu.scss";

const NewSynonymsForm = ({ synonymsValue, setSynonymsValue }) => {
	const [loadedTemplate, setLoadedTemplate] = useState("");
	const [loadTemplateModal, setLoadTemplateModal] = useState();
	const [saveTemplateModal, setSaveTemplateModal] = useState();
	const [deleteTemplateModal, setDeleteTemplateModal] = useState();

	const { templates, isTemplateLoading } = useTracker(() => {
		const handlerTemplates = Meteor.subscribe("synonymTemplates");
		if (!handlerTemplates.ready()) {
			return { templates: null, isTemplateLoading: true };
		}
		let templates = SynonymTemplates.find().fetch();
		return { templates, isTemplateLoading: false };
	});

	useEffect(() => {
		if (loadedTemplate !== "") {
			const matchingTemplate = templates.find(
				(template) => template._id === loadedTemplate
			);
			onModify(
				null,
				"duplicates",
				synonymsValue + "\n" + matchingTemplate.synonyms.join("\n")
			);
		}
	}, [loadedTemplate]);

	const onModify = (event, action, synonymsInput = synonymsValue) => {
		if (event) {
			event.preventDefault();
		}
		const synonyms = synonymsInput
			.replace(/\r\n/g, "\n")
			.split("\n")
			.filter((synonym) => synonym);

		if (synonyms.length) {
			let synonymsModified = [];

			if (action === "case") {
				synonymsModified = synonyms.map((synonym) => titleCase(synonym));
			} else if (action === "duplicates") {
				synonymsModified = caseInsensitiveDedupe(synonyms);
			} else if (action === "asc") {
				synonymsModified = synonyms.sort();
			} else if (action === "dsc") {
				synonymsModified = synonyms.reverse();
			}

			setSynonymsValue(synonymsModified.join("\r\n").trim());
		}
	};

	return (
		<>
			<p>List one synonym per line.</p>
			<div className="input-action-links-wrap">
				<div className="input-action-links-row top">
					<div className="input-action-links left">
						<span className="label">Format</span>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "case");
							}}
						>
							Title Case
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "duplicates");
							}}
						>
							Remove Duplicates
						</button>
					</div>
					<div className="input-action-links right">
						<span className="label">Sort</span>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "asc");
							}}
						>
							A-Z
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "dsc");
							}}
						>
							Z-A
						</button>
					</div>
				</div>
			</div>
			<Textarea
				name="synonyms"
				value={synonymsValue}
				onChange={(evt) => {
					setSynonymsValue(evt.target.value);
				}}
			/>
			<div className="input-action-links-wrap">
				<div className="input-action-links-row bottom">
					<div className="input-action-links left">
						<span className="label">Templates</span>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setLoadTemplateModal(templates);
							}}
						>
							<ContextMenuTrigger id="loadTemplate">Load</ContextMenuTrigger>
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setSaveTemplateModal(templates);
							}}
						>
							Save
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setDeleteTemplateModal(templates);
							}}
						>
							Delete
						</button>
					</div>
				</div>
			</div>
			<LoadTemplateModal
				templates={templates}
				setLoadedTemplate={setLoadedTemplate}
				show={loadTemplateModal}
				hide={() => {
					setLoadTemplateModal(null);
				}}
			/>
			<SaveTemplateModal
				templates={templates}
				loadedTemplate={loadedTemplate}
				setLoadedTemplate={setLoadedTemplate}
				synonymsValue={synonymsValue}
				show={saveTemplateModal}
				hide={() => {
					setSaveTemplateModal(null);
				}}
			/>
			<DeleteTemplateModal
				templates={templates}
				loadedTemplate={loadedTemplate}
				setLoadedTemplate={setLoadedTemplate}
				show={deleteTemplateModal}
				hide={() => {
					setDeleteTemplateModal(null);
				}}
			/>
			{templates ? (
				<ContextMenu id="loadTemplate">
					{templates.map((template) => {
						return (
							<MenuItem
								key={template._id}
								onClick={() => {
									setLoadedTemplate(template._id);
								}}
							>
								{template.name}
							</MenuItem>
						);
					})}
				</ContextMenu>
			) : null}
		</>
	);
};

export default NewSynonymsForm;
