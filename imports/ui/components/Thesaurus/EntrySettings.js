import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useTracker } from "meteor/react-meteor-data";
import EntrySettingsInfo from "./EntrySettingsInfo";
import EntrySettingsSynonyms from "./EntrySettingsSynonyms";
import DeleteEntryModal from "./DeleteEntryModal";
import EntrySettingsUsage from "./EntrySettingsUsage";
import EntrySettingsLinked from "./EntrySettingsLinked";

import "./EntrySettings.scss";
import ThesaurusEntries from "../../../api/collections/ThesaurusEntries";
import ThesaurusEntryGroups from "../../../api/collections/ThesaurusEntryGroups";
import { PropagateLoader } from "react-spinners";
import Options from "../../../api/collections/Options";

const EntrySettings = ({ modal, id, close }) => {
	if (!id) {
		id = useParams().id;
	}
	const [deleteEntryModal, setDeleteEntryModal] = useState();

	const { entryIsLoading, entry, groups, options } = useTracker(() => {
		const handler = Meteor.subscribe("thesaurusEntry", id);
		const handlerGroups = Meteor.subscribe(
			"thesaurusEntryGroups",
			Meteor.user()?.projectID
		);
		const handlerOptions = Meteor.subscribe("optionsByThesaurusEntry", id);

		if (!handler.ready() || !handlerGroups.ready() || !handlerOptions) {
			return { entryIsLoading: true, entry: null, groups: null };
		}

		let groups = ThesaurusEntryGroups.find()
			.fetch()
			.map((group) => ({ value: group._id, label: group.name }));
		groups.push({ value: "", label: "[[root]]" });

		let options = Options.find()
			.fetch()
			.map((option) => ({
				name: option.name,
				attributeID: option.attributeID,
				id: option._id,
			}));

		let entry = ThesaurusEntries.findOne({ _id: id });
		if (!entry?.parent) {
			entry.parent = "";
		}

		return {
			entryIsLoading: false,
			entry,
			groups,
			options,
		};
	});

	if (entryIsLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<>
			<section className="entry-settings">
				<h1>Thesaurus Entry Settings</h1>

				{!modal && <EntrySettingsInfo id={id} entry={entry} groups={groups} />}
				<EntrySettingsSynonyms
					id={id}
					entry={entry}
					groups={groups}
					close={close}
					modal={modal}
				/>
				{/* Add in the Linked Entries after they are hooked up. */}
				{/* <EntrySettingsLinked id={id} entry={entry} groups={groups} />  */}
				{!modal && <EntrySettingsUsage id={id} options={options} />}

				{!modal && (
					<section className="action-links single-link">
						<button
							className="action-link last"
							onClick={() => {
								setDeleteEntryModal(id);
							}}
						>
							Delete Entry
						</button>
					</section>
				)}
			</section>
			{!modal && (
				<DeleteEntryModal
					show={deleteEntryModal}
					hide={() => {
						setDeleteEntryModal(null);
					}}
				/>
			)}
		</>
	);
};

export default EntrySettings;
