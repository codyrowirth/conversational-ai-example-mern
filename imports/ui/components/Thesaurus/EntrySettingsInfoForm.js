import React, { useState } from "react";
import * as yup from "yup";
import { Controller, useForm } from "react-hook-form";
import { titleCase } from "title-case";
import { yupResolver } from "@hookform/resolvers/yup";
import InputRHF from "../../lib/components/Form/InputRHF";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import CheckboxRHF from "../../lib/components/Form/CheckboxRHF";
import Button from "../../lib/components/Button/Button";
import NewGroupModal from "../../lib/components/NestedData/NewGroupModal";
import {
	saveOnBlur,
	saveOnChange,
} from "../../lib/utilities/saveOnBlurRHFMeteor";
import TimeAgo from "react-timeago";
import LastSaved from "../../lib/components/LastSaved/LastSaved";

const EntrySettingsInfoForm = ({ entry, groups }) => {
	const [newGroupModal, setNewGroupModal] = useState();

	const schema = yup.object().shape({
		name: yup.string().required("Please enter an entry name."),
		parent: yup.object().required("Please select a group."),
	});

	const parentGroup = groups.find((group) => group.value === entry?.parent);

	const {
		register,
		formState: { errors },
		handleSubmit,
		setValue,
	} = useForm({
		defaultValues: {
			name: entry?.name || "",
			parent: parentGroup,
		},
		resolver: yupResolver(schema),
	});

	const onBlur = (evt) => {
		saveOnBlur("thesaurusEntries", entry._id, evt);
	};

	return (
		<section>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="name"
							label="Name"
							register={register}
							error={errors}
							onBlur={onBlur}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-half">
						<SelectRHF
							name={"parent"}
							label="Group"
							options={groups}
							ref={null}
							error={errors}
							value={groups.find((group) => group.value === entry?.parent)}
							onChange={(value, fieldData) => {
								saveOnChange("thesaurusEntries", entry._id, value, fieldData);
							}}
						/>
					</div>
					<div className="grid-col-half no-label">
						<div className="action-links-wrap">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setNewGroupModal(true);
								}}
							>
								Add New Group
							</button>
						</div>
					</div>
				</div>
				<p className="form-response global">
					<span>Last saved</span> <LastSaved timestamp={entry?.updatedAt} />
				</p>
			</form>
			<NewGroupModal
				show={newGroupModal}
				hide={() => {
					setNewGroupModal(null);
				}}
				type="entryGroup"
			/>
		</section>
	);
};

export default EntrySettingsInfoForm;
