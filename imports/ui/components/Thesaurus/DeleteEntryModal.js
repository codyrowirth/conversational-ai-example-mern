import React from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import { useTracker } from "meteor/react-meteor-data";
// import Entries from "../../../api/collections/Entries";
import { useHistory, useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";

const DeleteEntryModal = ({ show, hide }) => {
	const { id } = useParams();
	const history = useHistory();

	const loading = false;

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<Modal className="modal limit-modal end-buttons" show={show} hide={hide}>
			<h3>Do you want to delete this entry?</h3>
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{show && show?.name}</strong> thesaurus entry.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						history.push("/thesaurus");

						Meteor.call("thesaurusEntries.remove", id);

						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteEntryModal;
