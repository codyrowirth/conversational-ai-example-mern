import React from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";

const SimilarityWarningModal = ({ show, hide }) => {
	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			{/* TO DO: Use real data */}

			<h3>Did you mean “something else”?</h3>
			<p>
				There’s a thesaurus entry called “<strong>something else</strong>,”
				which is really close to “<strong>something</strong>.” Do you still want
				to create the “something” thesaurus entry?
			</p>
			<div className="button-wrap">
				<Button
					text="Yes"
					onClick={() => {
						// Meteor.call("options.remove", option._id);
						alert("TO DO");
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default SimilarityWarningModal;
