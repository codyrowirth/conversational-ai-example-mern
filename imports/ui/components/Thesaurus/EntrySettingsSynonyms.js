import React from "react";
import EntrySettingsSynonymsNew from "./EntrySettingsSynonymsNew";

const EntrySettingsSynonyms = (props) => {
	const { id, entry, groups, close, modal } = props;

	let synonymsString = entry.synonymList.join("\n");

	return (
		<section className="end-buttons">
			<h2>Synonyms</h2>
			<EntrySettingsSynonymsNew
				synonymsString={synonymsString}
				close={close}
				modal={modal}
				id={id}
			/>
		</section>
	);
};

export default EntrySettingsSynonyms;
