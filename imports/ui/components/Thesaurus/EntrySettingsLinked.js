import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import useWinWidth from "../../lib/utilities/useWinWidth";

import Button from "../../lib/components/Button/Button";
import UnlinkEntryModal from "./UnlinkEntryModal";
import AddEntryModal from "./AddEntryModal";
import { PropagateLoader } from "react-spinners";
import ThesaurusEntries from "../../../api/collections/ThesaurusEntries";
import { useTracker } from "meteor/react-meteor-data";

const EntrySettingsLinked = (props) => {
	const { entry } = props;
	let listOfLinkedEntries = entry.linkedEntryList;

	const { linkedEntries, isLoading } = useTracker(() => {
		const handleLinkedEntries = Meteor.subscribe(
			"thesaurusEntriesByIDs",
			listOfLinkedEntries
		);
		if (!handleLinkedEntries.ready()) {
			return { linkedEntries: null, isLoading: true };
		}

		let linkedEntries = ThesaurusEntries.find({ _id: { $nin: [entry._id] } })
			.fetch()
			.map((linkedEntry) => ({ name: linkedEntry.name, id: linkedEntry._id }));

		return {
			isLoading: false,
			linkedEntries,
		};
	});

	const [deleteEntryModal, setDeleteEntryModal] = useState();
	const [unlinkEntryModal, setUnlinkEntryModal] = useState();
	const [addEntryModal, setAddEntryModal] = useState();
	const [toggleOn, setToggleOn] = useState(false);
	const [rulesExpanded, setRulesExpanded] = useState(false);

	const history = useHistory();
	const screenWidth = useWinWidth();

	return (
		<section className="last">
			<h2>Linked Entries</h2>
			<form>
				<div className="entries-table">
					<table className="entries">
						{screenWidth >= 768 && (
							<colgroup>
								<col style={isLoading ? { width: "100%" } : { width: "80%" }} />
								<col style={isLoading ? { width: "0" } : { width: "20%" }} />
							</colgroup>
						)}
						<thead>
							<tr>
								<th scope="col">{isLoading ? "" : "Entries"}</th>
								<th scope="col">{isLoading ? "" : "Actions"}</th>
							</tr>
						</thead>
						<tbody>
							{isLoading ? (
								<tr className="loader">
									<td>
										<PropagateLoader color="#2f3d47" />
									</td>
								</tr>
							) : (
								linkedEntries.map((entry) => {
									return (
										<tr key={entry.id}>
											<td data-label="Entry">{entry.name}</td>
											<td data-label="Actions">
												<div className="action-links">
													<button
														className="action-link"
														onClick={(event) => {
															event.preventDefault();
															setUnlinkEntryModal(true);
														}}
													>
														Unlink
													</button>
												</div>
											</td>
										</tr>
									);
								})
							)}
						</tbody>
					</table>
				</div>
			</form>
			<div
				className="table-action-links-wrap"
				style={{ marginBottom: "var(--spacing-standard)" }}
			>
				<div className="table-action-links-row bottom">
					<div className="table-action-links left">
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setAddEntryModal(true);
							}}
						>
							Link Entry
						</button>
					</div>
				</div>
			</div>
			<div className="button-wrap">
				<Button
					text="Back"
					addClass="back"
					onClick={() => {
						history.push("/thesaurus");
					}}
				/>
			</div>
			<UnlinkEntryModal
				show={unlinkEntryModal}
				hide={() => {
					setUnlinkEntryModal(null);
				}}
			/>
			<AddEntryModal
				show={addEntryModal}
				hide={() => {
					setAddEntryModal(null);
				}}
			/>
		</section>
	);
};

export default EntrySettingsLinked;
