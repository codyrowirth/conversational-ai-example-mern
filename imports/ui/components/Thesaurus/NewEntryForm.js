import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { UnmountClosed as Collapse } from "react-collapse";
import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import GreyBox from "../../lib/components/GreyBox/GreyBox";
import NewGroupModal from "../../lib/components/NestedData/NewGroupModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";
import ThesaurusEntryGroups from "../../../api/collections/ThesaurusEntryGroups";
import ThesaurusEntries from "../../../api/collections/ThesaurusEntries";
import CheckboxRHF from "../../lib/components/Form/CheckboxRHF";
import NewSynonymsForm from "./NewSynonymsForm";
import caseInsensitiveDedupe from "../../lib/utilities/caseInsensitiveDedupe";
import SimilarityWarningModal from "./SimilarityWarningModal";

const NewEntryForm = ({ newEntryFormExpanded, setNewEntryFormExpanded }) => {
	return (
		<Collapse isOpened={newEntryFormExpanded}>
			<GreyBox className="end-buttons">
				<h3>Add New Entry</h3>
				<Form setNewEntryFormExpanded={setNewEntryFormExpanded} />
			</GreyBox>
		</Collapse>
	);
};

const Form = ({ setNewEntryFormExpanded }) => {
	const [newEntryFormSynonymsExpanded, setNewEntryFormSynonymsExpanded] =
		useState(false);

	// TO DO: Use real data
	// Change simWarningModal to true to activate the modal (new entry form must be expanded)
	const [simWarningModal, setSimWarningModal] = useState(false);

	const [newGroupModal, setNewGroupModal] = useState();
	const [synonymsValue, setSynonymsValue] = useState("");

	const { groups, isLoading } = useTracker(() => {
		const handlerGroups = Meteor.subscribe("thesaurusEntryGroups");
		if (!handlerGroups.ready()) {
			return { isLoading: true, groups: null };
		}

		let groups = ThesaurusEntryGroups.find({
			projectID: Meteor.user()?.projectID,
		})
			.fetch()
			.map((group) => ({ value: group._id, label: group.name }));
		groups.push({ value: "", label: "[[root]]" });
		return { groups, isLoading: false };
	});

	const schema = yup.object().shape({
		name: yup.string().required("Please enter an entry name."),
		group: yup.object().required("Please select a group."),
	});

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
		reset,
		setFocus,
		setError,
		setValue,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const resetForm = () => {
		reset();
		setValue("group", "");
		setSynonymsValue("");
	};

	const onSubmit = (data, closeForm = true) => {
		if (closeForm) {
			setNewEntryFormExpanded(false);
		}
		resetForm();

		const synonyms = caseInsensitiveDedupe(
			synonymsValue
				.replace(/\r\n/g, "\n")
				.split("\n")
				.filter((synonym) => synonym !== "")
		);

		let entryToAdd = {
			name: data.name,
			parent: data.group.value || null,
			session: data.session,
			updatedAt: new Date(),
			synonymList: synonyms,
			projectID: Meteor.user()?.projectID,
		};

		Meteor.call(
			`thesaurusEntries.duplicateNameCheck`,
			entryToAdd?.name,
			(err, foundObj) => {
				if (foundObj) {
					setError("name", {
						type: "string",
						message:
							"Entry with this name already exists. Please use a different name",
					});
				} else {
					Meteor.call(`thesaurusEntries.insert`, entryToAdd, (err, entryID) => {
						if (err) {
							alert(`Error creating entry`);
						}
					});

					if (closeForm) {
						setNewEntryFormExpanded(false);
					}
					resetForm();
				}
			}
		);
	};

	const onContinue = (data) => {
		onSubmit(data, false);
		setFocus("name");
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="name"
							label="Name"
							register={register}
							error={errors}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-half">
						{/* TO DO: Use real data */}
						<Controller
							name="group"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Group"
										options={groups.map((group) => ({
											value: group.value,
											label: group.label,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
						{/* <Controller
							name="group"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Group"
										options={groups}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/> */}
					</div>
					<div className="grid-col-half no-label">
						<div className="action-links-wrap">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setNewGroupModal(true);
								}}
							>
								Add New Group
							</button>
						</div>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<button
							className="collapse-trigger h4"
							onClick={(event) => {
								event.preventDefault();
								setNewEntryFormSynonymsExpanded(!newEntryFormSynonymsExpanded);
							}}
						>
							Synonyms
							<span className="arrow">
								<FontAwesomeIcon
									icon={
										newEntryFormSynonymsExpanded ? "chevron-up" : "chevron-down"
									}
								/>
							</span>
						</button>
						<Collapse isOpened={newEntryFormSynonymsExpanded}>
							<NewSynonymsForm
								synonymsValue={synonymsValue}
								setSynonymsValue={setSynonymsValue}
							/>
						</Collapse>
					</div>
				</div>
				<div className="button-wrap">
					<Button text="Save" onClick={handleSubmit(onSubmit)} />
					<Button
						text="Save and add another"
						onClick={handleSubmit(onContinue)}
					/>
				</div>
			</form>
			<SimilarityWarningModal
				show={simWarningModal}
				hide={() => {
					setSimWarningModal(null);
				}}
			/>

			{/* TO DO: Make this work once data is hooked up */}

			<NewGroupModal
				show={newGroupModal}
				hide={() => {
					setNewGroupModal(null);
				}}
				type={"thesaurusEntryGroup"}
			/>
		</>
	);
};

export default NewEntryForm;
