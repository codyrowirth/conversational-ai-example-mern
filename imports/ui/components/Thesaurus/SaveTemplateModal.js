import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import Modal from "../../lib/components/Modal/Modal";
import SynonymTemplates from "../../../api/collections/SynonymTemplates";

const SaveTemplateModal = ({
	templates,
	loadedTemplate,
	setLoadedTemplate,
	synonymsValue,
	show,
	hide,
}) => {
	const schema = yup.object().shape({
		templateName: yup.string().required("Please enter a template name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
		setValue,
		setError,
	} = useForm({
		resolver: yupResolver(schema),
	});

	setValue("templateName", "");

	const onSubmit = (data) => {
		// TO DO: Use real data

		const templateName = data.templateName;
		const alreadyExists = SynonymTemplates.findOne({ name: templateName });
		if (alreadyExists) {
			const matchingTemplate = templates.find(
				(template) => template.name === templateName
			);
			Meteor.call(`synonymsTemplates.remove`, matchingTemplate._id);
		}
		const synonyms = synonymsValue
			.replace(/\r\n/g, "\n")
			.split("\n")
			.filter((synonym) => synonym);

		Meteor.call(`synonymTemplates.insert`, {
			name: templateName,
			synonyms: synonyms,
		});

		hide();
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Save Template</h3>
			<form>
				<div className="grid">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="templateName"
							label="Template Name"
							register={register}
							error={errors}
						/>
					</div>
				</div>
			</form>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default SaveTemplateModal;
