import React, { useState, useEffect } from "react";
import NewSynonymsForm from "./NewSynonymsForm";
import Button from "../../lib/components/Button/Button";
import { useHistory, useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";
import caseInsensitiveDedupe from "../../lib/utilities/caseInsensitiveDedupe";

const SynonymSettingsSynonymsNew = (props) => {
	const [synonymsValue, setSynonymsValue] = useState(props.synonymsString);
	const { id, modal } = props;
	const isLoading = false;
	const history = useHistory();

	const save = () => {
		const synonyms = caseInsensitiveDedupe(
			synonymsValue
				.replace(/\r\n/g, "\n")
				.split("\n")
				.filter((synonym) => synonym !== "")
		);

		setSynonymsValue(synonyms.join("\r\n").trim());

		Meteor.call("thesaurusEntries.update", id, {
			synonymList: synonyms,
		});
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<>
			<NewSynonymsForm
				synonymsValue={synonymsValue}
				setSynonymsValue={setSynonymsValue}
			/>
			<div className="button-wrap">
				<Button text="Save Synonyms" onClick={save} />
				{props.close && (
					<Button
						text="Save & Close"
						onClick={() => {
							save();
							props.close();
						}}
					/>
				)}

				<Button
					text={props.close ? "Close" : "Back"}
					onClick={() => {
						if (props.close) {
							props.close();
						} else {
							history.goBack();
						}
					}}
				/>
			</div>
		</>
	);
};

export default SynonymSettingsSynonymsNew;
