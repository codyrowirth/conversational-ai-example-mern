import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Tutorials = () => {
	return (
		<section className="tutorials">
			<h1>Tutorials</h1>
			<ComingSoon />
		</section>
	);
};

export default Tutorials;
