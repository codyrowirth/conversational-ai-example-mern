import React from "react";
import UnmatchedTable from "./UnmatchedTable";

// TO DO: Use real data

const responses = [
	{
		_id: "responseOne",
		name: "Response one",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseTwo",
		name: "Response two",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseThree",
		name: "Response three",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseFour",
		name: "Response four",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "responseFive",
		name: "Response five",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
];

const Unmatched = () => {
	return (
		<section className="unmatched">
			<h1>Unmatched User Answers</h1>
			<UnmatchedTable type="full" responses={responses} />
		</section>
	);
};

export default Unmatched;
