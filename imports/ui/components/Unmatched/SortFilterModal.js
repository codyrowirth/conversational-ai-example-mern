import React from "react";
import { useForm, Controller } from "react-hook-form";

import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import SelectRHF from "../../lib/components/Form/SelectRHF";

const SortFilterModal = ({ subset = "", show, hide }) => {
	const {
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const onSubmit = () => {
		alert("TO DO");
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal
			className="modal limit-modal end-buttons conversations"
			show={!!show}
			hide={hide}
		>
			<h3>Sort &amp; Filter</h3>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="sort"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Sort"
										options={["Response", "Location"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="order"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Order"
										options={["Ascending", "Descending"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="message"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Message"
										options={["TO DO"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="button-wrap">
					<Button text="Apply" onClick={handleSubmit(onSubmit)} />
					<Button text="Cancel" onClick={onCancel} />
				</div>
			</form>
		</Modal>
	);
};

export default SortFilterModal;
