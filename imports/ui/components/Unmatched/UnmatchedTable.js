import React, { useState, useContext, useEffect } from "react";
import { Meteor } from "meteor/meteor";
// import { useTracker } from "meteor/react-meteor-data";
import { Link } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { PropagateLoader } from "react-spinners";
import Sessions from "../../../api/collections/Sessions";

// import ProjectsDB from "../../../api/collections/Projects";
import useWinWidth from "../../lib/utilities/useWinWidth";

// import AppContext from "../App/AppContext";

import IgnoreUnmatchedModal from "./IgnoreUnmatchedModal";
import SortFilterModal from "./SortFilterModal";
import CheckboxRHF from "../../lib/components/Form/CheckboxRHF";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import UnmatchedTableCheckbox from "./UnmatchedTableCheckbox";

import "../../lib/styles/tables.scss";
import "./UnmatchedTable.scss";
import SelectCreatableRHF from "../../lib/components/Form/SelectCreatableRHF";
import { useSnackbar } from "notistack";

const UnmatchedTable = ({ type, responses }) => {
	const [ignoreUnmatchedModal, setIgnoreUnmatchedModal] = useState();
	const [sortFilterModal, setSortFilterModal] = useState();
	const [checkAll, setCheckAll] = useState(false);
	const [unmatched, setUnmatched] = useState([]);
	const { enqueueSnackbar } = useSnackbar();

	const screenWidth = useWinWidth();

	const {
		register,
		control,
		formState: { errors },
	} = useForm();

	useEffect(() => {
		(async () => {
			const unmatchedDB = await Meteor.callPromise(`sessions.unmatched`);
			setUnmatched(unmatchedDB);
		})();
	}, []);

	if (!unmatched.length) {
		return <p>No unmatched user answers.</p>;
	}

	const isLoading = false;

	return (
		<section className="unmatched-table">
			<div className="table-action-links-wrap">
				<div className="table-action-links-row top">
					{/*<div className="table-action-links left">*/}
					{/*	<button*/}
					{/*		className="action-link"*/}
					{/*		onClick={(event) => {*/}
					{/*			event.preventDefault();*/}
					{/*			setIgnoreUnmatchedModal(true);*/}
					{/*		}}*/}
					{/*	>*/}
					{/*		Ignore*/}
					{/*	</button>*/}
					{/*</div>*/}
					{type !== "message" && (
						<div className="table-action-links right">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setSortFilterModal(true);
								}}
							>
								Sort &amp; Filter
							</button>
						</div>
					)}
				</div>
			</div>
			<table>
				{screenWidth >= 768 && (
					<colgroup>
						{/*<col style={isLoading ? { width: "100%" } : { width: "72px" }} />*/}
						<col style={isLoading ? { width: "0" } : { width: "50%" }} />
						<col style={isLoading ? { width: "0" } : { width: "50%" }} />
					</colgroup>
				)}
				<thead>
					<tr>
						{/*<th scope="col">*/}
						{/*	{isLoading ? (*/}
						{/*		""*/}
						{/*	) : (*/}
						{/*		<CheckboxRHF*/}
						{/*			name="checkAll"*/}
						{/*			register={register}*/}
						{/*			onChange={(event) => {*/}
						{/*				setCheckAll(event.target.checked);*/}
						{/*			}}*/}
						{/*			error={errors}*/}
						{/*		/>*/}
						{/*	)}*/}
						{/*</th>*/}
						<th scope="col">{isLoading ? "" : "Response"}</th>
						<th scope="col">{isLoading ? "" : "Thesaurus Entry"}</th>
					</tr>
				</thead>
				<tbody>
					{isLoading ? (
						<tr className="loader">
							<td>
								<PropagateLoader color="#2f3d47" />
							</td>
						</tr>
					) : (
						// TO DO: Use real data
						unmatched.map((unmatchedItem) => {
							return (
								<tr
									className={`${type} ${unmatchedItem.read}`}
									key={unmatchedItem._id}
								>
									{/*<td className="checkbox-wrap" data-label="Check">*/}
									{/*	<div className="unread-bar"></div>*/}
									{/*</td>*/}
									<td data-label="Response" className="response">
										<div className="text">
											{unmatchedItem.sessionMessage.content}
										</div>
										<div className="location">
											<span>
												<Link
													to={`/canvases/${unmatchedItem?.canvas._id}/${unmatchedItem?.shape?._id}`}
												>
													{unmatchedItem?.canvas?.name} &gt;{" "}
													{unmatchedItem?.shape?.data?.label}
												</Link>
											</span>
										</div>
									</td>
									<td data-label="Thesaurus Entry">
										<SelectCreatableRHF
											options={[
												{ value: "[ignore]", label: "[Ignore]" },
												...unmatchedItem.thesaurusEntries.map((option) => ({
													value: option._id,
													label: option.name,
												})),
											]}
											ref={null}
											error={errors}
											onChange={(val) => {
												if (!val) {
													//ignore
												} else {
													Meteor.call(`sessions.setMatched`, {
														sessionID: unmatchedItem.sessionID,
														messageID: unmatchedItem.sessionMessage._id,
													});

													if (val.value === "[ignore]") {
														enqueueSnackbar(
															"Successfully ignored this response.",
															{
																variant: "success",
																key: "ignored",
															}
														);
													} else {
														Meteor.call(
															`sessions.addUnmatchedToThesaurusEntry`,
															{
																thesaurusEntryID: val.value,
																synonym:
																	unmatchedItem.sessionMessage.content.trim(),
															}
														);
														enqueueSnackbar("Synonym added to thesaurus", {
															variant: "success",
															key: "synonymAdded",
														});
													}
												}
											}}
										/>
									</td>
								</tr>
							);
						})
					)}
				</tbody>
			</table>
			<IgnoreUnmatchedModal
				show={ignoreUnmatchedModal}
				hide={() => {
					setIgnoreUnmatchedModal(null);
				}}
			/>
			<SortFilterModal
				show={sortFilterModal}
				hide={() => {
					setSortFilterModal(null);
				}}
			/>
		</section>
	);
};

export default UnmatchedTable;
