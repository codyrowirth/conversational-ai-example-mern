import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Reports = () => {
	return (
		<section className="reports">
			<h1>Reports</h1>
			<ComingSoon />
		</section>
	);
};

export default Reports;
