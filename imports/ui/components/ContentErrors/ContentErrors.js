import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const ContentErrors = () => {
	return (
		<section className="content-errors">
			<h1>Content Errors</h1>
			<ComingSoon />
		</section>
	);
};

export default ContentErrors;
