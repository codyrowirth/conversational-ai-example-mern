import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const ArchitectureErrors = () => {
	return (
		<section className="architecture-errors">
			<h1>Architecture Errors</h1>
			<ComingSoon />
		</section>
	);
};

export default ArchitectureErrors;
