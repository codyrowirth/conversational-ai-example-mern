import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const PrivacyPolicy = () => {
	return (
		<section className="privacy-policy">
			<h1>Privacy Policy</h1>
			<ComingSoon />
		</section>
	);
};

export default PrivacyPolicy;
