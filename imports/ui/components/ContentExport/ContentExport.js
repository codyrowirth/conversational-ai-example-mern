import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";
import Canvases from "../../../api/collections/Canvases";
import Select from "react-select";
import Button from "../../lib/components/Button/Button";
import downloadjs from "downloadjs";
import { useSnackbar } from "notistack";

const ContentExport = () => {
	const [selectedCanvases, setSelectedCanvases] = useState([]);
	const [exporting, setExporting] = useState(false);
	const { enqueueSnackbar, closeSnackbar } = useSnackbar();

	const { canvases, loading } = useTracker(() => {
		const canvasesSub = Meteor.subscribe("canvases", Meteor.user()?.projectID);

		if (!canvasesSub.ready()) {
			return { loading: true };
		}

		return {
			loading: false,
			canvases: Canvases.find({ projectID: Meteor.user().projectID }).fetch(),
		};
	});

	if (loading) {
		return <h1>Loading...</h1>;
	}

	return (
		<div>
			<h1>Content Export</h1>
			<span
				onClick={() => {
					setSelectedCanvases(
						canvases.map((canvas) => ({
							label: canvas.name,
							value: canvas._id,
						}))
					);
				}}
			>
				Select all
			</span>
			<Select
				isMulti
				closeMenuOnSelect={false}
				options={canvases.map((canvas) => ({
					label: canvas.name,
					value: canvas._id,
				}))}
				value={selectedCanvases}
				onChange={(val) => {
					setSelectedCanvases(val);
				}}
			/>
			{selectedCanvases.length !== 0 && (
				<Button
					// disabled={exporting}
					text={exporting ? "Exporting..." : "Export Content"}
					onClick={async () => {
						setExporting(true);

						enqueueSnackbar(
							"Export started. Please wait, this may take a minute...",
							{
								variant: "info",
								key: "exportStarted",
								persist: true,
							}
						);

						const { randomID, fileName } = await Meteor.callPromise(
							"exportContent",
							selectedCanvases.map((selected) => selected.value)
						);

						const link = document.createElement("a");
						link.href = `https://contentexports.tuzagtcs.com/${randomID}.zip`;
						link.setAttribute("download", `${fileName}.zip`);
						document.body.appendChild(link);
						link.click();

						closeSnackbar("exportStarted");
						enqueueSnackbar("Export complete. Please check your downloads.", {
							variant: "success",
							key: "exportFinished",
						});

						setExporting(false);
					}}
				/>
			)}
		</div>
	);
};

export default ContentExport;
