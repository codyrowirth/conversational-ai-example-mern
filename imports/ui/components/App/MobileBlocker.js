import React from "react";
import "./MobileBlocker.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const MobileBlocker = () => {
	return (
		<main className="mobile-blocker">
			<section>
				<div className="mobile-blocker-inner">
					<div className="mobile-blocker-content">
						<div className="icon">
							<FontAwesomeIcon icon="expand-alt" />
						</div>
						<h1>We Need a Little More Space</h1>
						<p>
							Your screen size is a bit too small. Please expand the window or
							use a device with a larger screen.
						</p>
					</div>
				</div>
			</section>
		</main>
	);
};

export default MobileBlocker;
