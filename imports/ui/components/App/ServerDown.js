import React from "react";
import "./ServerDown.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ServerDown = () => {
	return (
		<main className="server-blocker">
			<section>
				<div className="server-blocker-inner">
					<div className="server-blocker-content">
						<div className="grid-row">
							<div className="grid-col-half">
								<div className="image-wrap">
									<div className="image">
										<img
											src={
												"https://media1.giphy.com/media/BuReg1EyvWaac/giphy.gif?cid=ecf05e47ilpcye8g5crjcixrzn4f92duq9qtqcrha96es2kh&rid=giphy.gif&ct=g"
											}
										/>
									</div>
								</div>
							</div>
							<div className="grid-col-half">
								<div className="icon">
									<FontAwesomeIcon icon="exclamation-triangle" />
								</div>
								<h1>We've lost connection to the TCS mothership!</h1>
								<p>
									TCS is experiencing some downtime. Our highly trained DevOps
									team is working on a fix — or a new TCS version might be
									coming your way soon.
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
	);
};

export default ServerDown;
