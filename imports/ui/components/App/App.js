import React, { useState, useEffect } from "react";
import {
	BrowserRouter as Router,
	Route,
	Switch,
	useLocation,
} from "react-router-dom";
import { createBrowserHistory } from "history";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import useMousetrap from "react-hook-mousetrap";
import useWinWidth from "../../lib/utilities/useWinWidth";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import "normalize.css";
import "focus-visible/dist/focus-visible.js";
import "../../lib/styles/layout.scss";
import "../../lib/styles/grid.scss";
import "../../lib/styles/tables.scss";
import "../../lib/components/Form/forms.scss";
import "../../lib/styles/collapse.scss";
import "../../lib/styles/fonts.scss";
import "../../lib/styles/contextmenu.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import "../../lib/styles/typography.scss";

import AppContext from "./AppContext";

import Sidebar from "../../lib/components/Sidebar/Sidebar";
import UtilityNav from "../../lib/components/UtilityNav/UtilityNav";
import Footer from "../../lib/components/Footer/Footer";
import Dashboard from "../Dashboard/Dashboard";
import CreditBar from "../Conversations/CreditBar";
import FeaturedThreads from "../Conversations/FeaturedThreads";
import BrowseThreads from "../Conversations/BrowseThreads";
import SavedThreads from "../Conversations/SavedThreads";
import ActiveThreads from "../Conversations/ActiveThreads";
import Thread from "../Conversations/Thread";
import Canvas from "../Canvas/Canvas";
import Canvases from "../Canvases/Canvases";
import Unmatched from "../Unmatched/Unmatched";
import Comments from "../Comments/Comments";
import CommentsArchive from "../Comments/CommentsArchive";
import CommentsPosted from "../Comments/CommentsPosted";
import Comment from "../Comments/Comment";
import AttributesManager from "../AttributesManager/AttributesManager";
import AttributeSettings from "../AttributesManager/AttributeSettings";
import Thesaurus from "../Thesaurus/Thesaurus";
import EntrySettings from "../Thesaurus/EntrySettings";
import Retrievers from "../Retrievers/Retrievers";
import ArchitectureErrors from "../ArchitectureErrors/ArchitectureErrors";
import ContentErrors from "../ContentErrors/ContentErrors";
import ContentLength from "../ContentLength/ContentLength";
import TestCases from "../TestCases/TestCases";
import Sandbox from "../Sandbox/Sandbox";
import Review from "../Review/Review";
import Publish from "../Publish/Publish";
import Reports from "../Reports/Reports";
import GlobalSettings from "../GlobalSettings/GlobalSettings";
import ProjectSettings from "../ProjectSettings/ProjectSettings";
import Search from "../Search/Search";
import Tutorials from "../Tutorials/Tutorials";
import Documentation from "../Documentation/Documentation";
import ReleaseNotes from "../ReleaseNotes/ReleaseNotes";
import Alerts from "../Alerts/Alerts";
import Tasks from "../Tasks/Tasks";
import Profile from "../Profile/Profile";
import Login from "../Login/Login";
import ForgotPassword from "../Login/ForgotPassword";
import SetPassword from "../Login/SetPassword";
import PrivacyPolicy from "../PrivacyPolicy/PrivacyPolicy";
import NotFound from "../NotFound/NotFound";

import QuickSwitcher from "./QuickSwitcher";
import MobileBlocker from "./MobileBlocker";
import ServerDown from "./ServerDown";
import { SnackbarProvider, useSnackbar } from "notistack";
import "./App.scss";
import successImgs from "../../lib/utilities/deployMessageImgs";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ContentExport from "../ContentExport/ContentExport";

library.add(far, fas);

const history = createBrowserHistory();

const theme = createTheme({
	palette: {
		primary: {
			main: "#2f3d47",
		},
		secondary: {
			main: "#16b1f0",
		},
	},
});

const VersionChecker = () => {
	const { enqueueSnackbar } = useSnackbar();

	useEffect(() => {
		Meteor.startup(() => {
			Reload._onMigrate(() => {
				const imageURL =
					successImgs[Math.floor(Math.random() * successImgs.length)];
				enqueueSnackbar(
					<span>
						<img src={imageURL} style={{ height: 25 }} /> New version of TCS
						available! Refresh to get it.
					</span>,
					{
						variant: "info",
						persist: true,
						key: "new-version",
					}
				);
				return [false];
			});
		});
	}, []);

	return <></>;
};

const App = () => {
	const [switcher, setSwitcher] = useState(false);
	const [copyBitID, setCopyBitID] = useState(window.localStorage.copyBitID);
	const [copyElementID, setCopyElementID] = useState(
		window.localStorage.copyElementID
	);

	const user = useTracker(() => {
		if (!Meteor.user()) {
			return;
		}
		const handler = Meteor.subscribe("Meteor.users.info");
		if (!handler.ready()) {
			return;
		}
		const user = Meteor.user();

		try {
			if (user) {
				window.Userback.name = `${user.profile.firstName || ""} ${
					user.profile.lastName || ""
				}`;
				window.Userback.email = user?.emails?.[0].address;
				window.Userback.setData({
					accountID: user.accountID,
					projectID: user.projectID,
				});
			}
		} catch (e) {
			// eslint-disable-next-line no-console
			console.error(e);
			// eslint-disable-next-line no-console
			console.error("Error setting user data into Userback");
		}
		return user;
	}, []);

	const screenWidth = useWinWidth();
	const serverStatus = Meteor.status();

	const serverDown =
		serverStatus?.status !== "connected" && serverStatus.retryCount > 2;
	if (serverDown) {
		// eslint-disable-next-line no-console
		console.log(serverStatus);
	}

	const onClickDismiss = (key) => () => {
		notistackRef.current.closeSnackbar(key);
	};
	const notistackRef = React.createRef();

	// Quick Switcher
	// Canvas
	useMousetrap("mod+shift+k", (event) => {
		setSwitcher("canvas");
	});

	// Message
	useMousetrap("mod+k", (event) => {
		setSwitcher("message");
	});

	return (
		<Router>
			<ThemeProvider theme={theme}>
				<SnackbarProvider
					maxSnack={3}
					hideIconVariant
					ref={notistackRef}
					preventDuplicate
					action={(key) => (
						<span onClick={onClickDismiss(key)}>
							<FontAwesomeIcon icon="times-circle" />
						</span>
					)}
				>
					{serverDown && <ServerDown />}
					<AppContext.Provider
						value={{
							user,
							copyBitID,
							setCopyBitID,
							copyElementID,
							setCopyElementID,
						}}
					>
						<VersionChecker />
						<div
							className="app"
							style={{ display: serverDown ? "none" : "inherit" }}
						>
							{screenWidth < 768 ? (
								<MobileBlocker />
							) : (
								<Switch>
									<Route
										exact
										path="/"
										render={() => (
											<PrivateRouteWithNav>
												<Dashboard />
											</PrivateRouteWithNav>
										)}
									/>
									<Route exact path={"/serverDown"}>
										<ServerDown />
									</Route>
									<Route
										exact
										path={"/conversations/featured"}
										render={() => (
											<PrivateRouteWithNav>
												<FeaturedThreads />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/conversations/browse"}
										render={() => (
											<PrivateRouteWithNav>
												<BrowseThreads />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/conversations/saved"}
										render={() => (
											<PrivateRouteWithNav>
												<SavedThreads />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/conversations/active"}
										render={() => (
											<PrivateRouteWithNav>
												<ActiveThreads />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/conversations/:threadID"}
										render={(props) => (
											<PrivateRouteWithNav>
												<Thread {...props} />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/canvases"}
										render={() => (
											<PrivateRouteWithNav>
												<Canvases />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/canvases/:canvasID/:shapeID"}
										render={(props) => (
											<PrivateRouteWithoutNav>
												<Canvas {...props} />
											</PrivateRouteWithoutNav>
										)}
									/>
									<Route
										exact
										path={"/canvases/:canvasID"}
										render={(props) => (
											<PrivateRouteWithoutNav>
												<Canvas {...props} />
											</PrivateRouteWithoutNav>
										)}
									/>
									<Route
										exact
										path={"/unmatched"}
										render={() => (
											<PrivateRouteWithNav>
												<Unmatched />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/comments"}
										render={() => (
											<PrivateRouteWithNav>
												<Comments />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/comments/archive"}
										render={() => (
											<PrivateRouteWithNav>
												<CommentsArchive />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/comments/posted"}
										render={() => (
											<PrivateRouteWithNav>
												<CommentsPosted />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path={"/comments/:commentID"}
										render={() => (
											<PrivateRouteWithNav>
												<Comment />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/attributes-manager"
										render={() => (
											<PrivateRouteWithNav>
												<AttributesManager />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/attributes/:id"
										render={() => (
											<PrivateRouteWithNav>
												<AttributeSettings />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/thesaurus"
										render={() => (
											<PrivateRouteWithNav>
												<Thesaurus />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/thesaurusEntries/:id"
										render={() => (
											<PrivateRouteWithNav>
												<EntrySettings />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/retrievers"
										render={() => (
											<PrivateRouteWithNav>
												<Retrievers />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/architecture-errors"
										render={() => (
											<PrivateRouteWithNav>
												<ArchitectureErrors />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/content-errors"
										render={() => (
											<PrivateRouteWithNav>
												<ContentErrors />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/export-content"
										render={() => (
											<PrivateRouteWithNav>
												<ContentExport />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/content-length"
										render={() => (
											<PrivateRouteWithNav>
												<ContentLength />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/test-cases"
										render={() => (
											<PrivateRouteWithNav>
												<TestCases />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/sandbox"
										render={() => (
											<PrivateRouteWithNav>
												<Sandbox />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/review"
										render={() => (
											<PrivateRouteWithNav>
												<Review />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/publish"
										render={() => (
											<PrivateRouteWithNav>
												<Publish />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/reports"
										render={() => (
											<PrivateRouteWithNav>
												<Reports />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/project-settings"
										render={() => (
											<PrivateRouteWithNav>
												<ProjectSettings />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/global-settings"
										render={() => (
											<PrivateRouteWithNav>
												<GlobalSettings />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/search"
										render={() => (
											<PrivateRouteWithNav>
												<Search />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/tutorials"
										render={() => (
											<PrivateRouteWithNav>
												<Tutorials />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/documentation"
										render={() => (
											<PrivateRouteWithNav>
												<Documentation />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/release-notes"
										render={() => (
											<PrivateRouteWithNav>
												<ReleaseNotes />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/alerts"
										render={() => (
											<PrivateRouteWithNav>
												<Alerts />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/tasks"
										render={() => (
											<PrivateRouteWithNav>
												<Tasks />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/profile"
										render={() => (
											<PrivateRouteWithNav>
												<Profile />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										exact
										path="/login"
										render={() => (
											<PublicRoute>
												<Login />
											</PublicRoute>
										)}
									/>
									<Route
										exact
										path={"/logout"}
										render={() => {
											Meteor.logout(() => {
												window.location = "/login";
											});

											return <p>Please wait, logging out...</p>;
										}}
									/>
									<Route
										exact
										path="/forgot-password"
										render={() => (
											<PublicRoute>
												<ForgotPassword />
											</PublicRoute>
										)}
									/>
									<Route
										exact
										path="/set-password"
										render={() => (
											<PublicRoute>
												<SetPassword />
											</PublicRoute>
										)}
									/>
									<Route
										exact
										path="/privacy-policy"
										render={() => (
											<PrivateRouteWithNav>
												<PrivacyPolicy />
											</PrivateRouteWithNav>
										)}
									/>
									<Route
										render={() => (
											<PrivateRouteWithNav>
												<NotFound />
											</PrivateRouteWithNav>
										)}
									/>
								</Switch>
							)}
							{switcher !== false && (
								<QuickSwitcher
									show={switcher}
									hide={() => {
										setSwitcher(false);
									}}
								/>
							)}
						</div>
					</AppContext.Provider>
				</SnackbarProvider>
			</ThemeProvider>
		</Router>
	);
};

const PublicRoute = ({ children }) => {
	if (Meteor.userId()) {
		window.location = "/";
		return <p>Please wait... redirecting</p>;
	} else {
		return children;
	}
};

const PrivateRouteWithoutNav = ({ children }) => {
	if (!Meteor.userId()) {
		window.location = "/login";
		return <p>Please wait... redirecting</p>;
	} else {
		return (
			<div className={"layout flex no-nav"}>
				<div className="layout-col-1"></div>
				<div className="layout-col-2 flex">
					<main>{children}</main>
				</div>
			</div>
		);
	}
};

const PrivateRouteWithNav = ({ children }) => {
	if (!Meteor.userId()) {
		window.location = "/login";
		return <p>Please wait... redirecting</p>;
	} else {
		const screenWidth = useWinWidth();
		const location = useLocation();

		return (
			<div className={"layout flex"}>
				<div className="layout-col-1">{<Sidebar />}</div>
				<div className="layout-col-2 flex">
					{screenWidth >= 1200 && <UtilityNav />}
					{location.pathname.includes("/conversations/") && <CreditBar />}
					<main>{children}</main>
					<Footer />
				</div>
			</div>
		);
	}
};

const AppWithRouter = () => (
	<Router>
		<App />
	</Router>
);

export default AppWithRouter;
