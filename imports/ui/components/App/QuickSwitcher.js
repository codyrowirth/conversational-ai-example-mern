import React, { useEffect, useState } from "react";

import Modal from "../../lib/components/Modal/Modal";
import Button from "../../lib/components/Button/Button";
import SelectAsync from "../../lib/components/Form/SelectAsync";
import debounce from "lodash.debounce";
import { useHistory } from "react-router-dom";

const loadOptionsDebounced = (show, query, callback) => {
	Meteor.call(
		show === "message" ? "shapes.messageSearch" : "canvases.quickSearch",
		query,
		(err, data) => {
			callback(
				data.map((match) => ({
					value: match._id,
					label: show === "message" ? match.data.label : match.name,
					canvasID: match?.canvasID,
				}))
			);
		}
	);
};

const loadOptions = debounce(loadOptionsDebounced, 500);

const QuickSwitcher = ({ show, hide }) => {
	const history = useHistory();
	const [selected, setSelected] = useState();
	let type = "";

	if (show === "message") {
		type = "Message";
	} else if (show === "canvas") {
		type = "Canvas";
	}

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Jump To A {type}</h3>
			<p>Select a {type.toLowerCase()} to go directly to it.</p>
			<form className="qs-form">
				<div className="grid-row">
					<div className="grid-col-full">
						<SelectAsync
							name={"name"}
							autoFocus={true}
							value={selected}
							onChange={(val) => {
								if (show === "canvas") {
									history.push(`/canvases/${val.value}`);
									hide();
								} else {
									history.push(`/canvases/${val.canvasID}/${val.value}`);
									hide();
								}
							}}
							loadOptions={(query, callback) => {
								loadOptions(show, query, callback);
							}}
						/>
					</div>
				</div>
			</form>
			<div className="button-wrap">
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default QuickSwitcher;
