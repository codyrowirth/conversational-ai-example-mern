import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Tasks = () => {
	return (
		<section className="tasks">
			<h1>Tasks</h1>
			<ComingSoon />
		</section>
	);
};

export default Tasks;
