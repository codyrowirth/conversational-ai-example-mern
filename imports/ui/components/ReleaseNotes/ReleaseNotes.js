import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const ReleaseNotes = () => {
	return (
		<section className="release-notes">
			<h1>Release Notes</h1>
			<ComingSoon />
		</section>
	);
};

export default ReleaseNotes;
