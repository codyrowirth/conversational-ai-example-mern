import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Publish = () => {
	return (
		<section className="publish">
			<h1>Publish</h1>
			<ComingSoon />
		</section>
	);
};

export default Publish;
