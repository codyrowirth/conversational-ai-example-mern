import React, { useState, useEffect, useContext, createContext } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Collapse } from "react-collapse";
import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import GreyBox from "../../lib/components/GreyBox/GreyBox";
import NewGroupModal from "../../lib/components/NestedData/NewGroupModal";
import { useTracker } from "meteor/react-meteor-data";
import CanvasGroups from "../../../api/collections/CanvasGroups";
import { PropagateLoader } from "react-spinners";
import Canvases from "../../../api/collections/Canvases";

const NewCanvasContext = createContext({});

const NewCanvasForm = ({
	newCanvasFormExpanded,
	setNewCanvasFormExpanded,
	panel = false,
}) => {
	const [loadedTemplate, setLoadedTemplate] = useState("");
	const [optionsValue, setOptionsValue] = useState("");

	return (
		<NewCanvasContext.Provider
			value={{
				loadedTemplate,
				setLoadedTemplate,
				optionsValue,
				setOptionsValue,
			}}
		>
			<Collapse isOpened={newCanvasFormExpanded}>
				<GreyBox className="end-buttons">
					<h3>Add New Canvas</h3>
					<Form
						newCanvasContext={NewCanvasContext}
						setNewCanvasFormExpanded={setNewCanvasFormExpanded}
						panel={panel}
					/>
				</GreyBox>
			</Collapse>
		</NewCanvasContext.Provider>
	);
};

const Form = ({ newCanvasContext, setNewCanvasFormExpanded, panel }) => {
	const [newCanvasFormOptionsExpanded, setNewCanvasFormOptionsExpanded] =
		useState(false);

	const [newGroupModal, setNewGroupModal] = useState();

	const { groups, isLoading } = useTracker(() => {
		const handler = Meteor.subscribe("canvasGroups");
		const handlerCanvases = Meteor.subscribe("canvasesByParent");
		if (!handler.ready() || !handlerCanvases.ready()) {
			return { isLoading: true, groups: null };
		}
		let groups = CanvasGroups.find()
			.fetch()
			.map((group) => ({ value: group._id, label: group.name }));
		groups.push({ value: null, label: "[[root]]" });
		return {
			groups,
			isLoading: false,
		};
	});

	const schema = yup.object().shape({
		name: yup.string().required("Please enter a canvas name."),
		group: yup.object().required("Please select a group."),
	});

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
		reset,
		setFocus,
		setValue,
		setError,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const resetForm = () => {
		reset();
		setValue("group", "");
	};

	const onSubmit = (data, shouldCollapseForm = true) => {
		const alreadyExists = Canvases.findOne({ name: data.name });
		if (alreadyExists) {
			setError(
				"name",
				{
					type: "string",
					message:
						"A canvas with this name already exists. Please rename and try again.",
				},
				{ shouldFocus: true }
			);
			throw new Error("name exists");
		}
		Meteor.call("canvases.insert", {
			name: data.name,
			parent: data.group.value,
		});

		if (shouldCollapseForm) {
			setNewCanvasFormExpanded(false);
		}
		resetForm();
	};

	const onContinue = (data) => {
		onSubmit(data, false);
		setFocus("name");
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<>
			<form>
				<div className="grid-row">
					<div className="grid-col-half">
						<InputRHF
							type="text"
							name="name"
							label="Name"
							register={register}
							error={errors}
						/>
					</div>
					<div className="grid-col-half">
						<Controller
							name="group"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Group"
										options={groups}
										panel={panel}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<div className="action-links">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setNewGroupModal(true);
								}}
							>
								Add New Group
							</button>
						</div>
					</div>
				</div>
				<div className="button-wrap">
					<Button text="Save" onClick={handleSubmit(onSubmit)} />
					<Button
						text="Save and add another"
						onClick={handleSubmit(onContinue)}
					/>
				</div>
			</form>
			<NewGroupModal
				type={"canvasGroup"}
				show={newGroupModal}
				hide={() => {
					setNewGroupModal(null);
				}}
			/>
		</>
	);
};

export { NewCanvasContext };
export default NewCanvasForm;
