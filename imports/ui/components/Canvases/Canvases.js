import React, { useState } from "react";
import NewCanvasForm from "./NewCanvasForm";
import Button from "../../lib/components/Button/Button";
import NestedData from "../../lib/components/NestedData/NestedData";

function Canvases() {
	const [newCanvasFormExpanded, setNewCanvasFormExpanded] = useState(false);

	return (
		<section>
			<div className="heading-with-button flex">
				<div>
					<h2>Canvases</h2>
				</div>
				<div>
					<Button
						text={newCanvasFormExpanded ? "Cancel" : "Add New Canvas"}
						icon={newCanvasFormExpanded ? "times" : "plus"}
						onClick={() => {
							setNewCanvasFormExpanded(!newCanvasFormExpanded);
						}}
					/>
				</div>
			</div>
			<NewCanvasForm
				newCanvasFormExpanded={newCanvasFormExpanded}
				setNewCanvasFormExpanded={setNewCanvasFormExpanded}
			/>
			<NestedData type="canvas" />
		</section>
	);
}

export default Canvases;
