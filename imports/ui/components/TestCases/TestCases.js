import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const TestCases = () => {
	return (
		<section className="test-cases">
			<h1>Test Cases</h1>
			<ComingSoon />
		</section>
	);
};

export default TestCases;
