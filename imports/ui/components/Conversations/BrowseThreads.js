import React, { useState } from "react";
import threadsDB from "./threadsDB";
import SearchBar from "../../lib/components/SearchBar/SearchBar";
import SortFilterModal from "./SortFilterModal";
import ThreadListing from "./ThreadListing";

const BrowseThreads = () => {
	const [searchBarText, setSearchBarText] = useState("");
	const [sortFilterModal, setSortFilterModal] = useState();

	return (
		<section className="conversations">
			<h1>Browse Conversation Threads</h1>
			<SearchBar
				placeholder="Search threads"
				searchBarText={searchBarText}
				setSearchBarText={setSearchBarText}
			/>
			<div className="input-action-links-wrap">
				<div className="input-action-links-row top">
					<div className="input-action-links right">
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setSortFilterModal(true);
							}}
						>
							Sort &amp; Filter
						</button>
					</div>
				</div>
			</div>
			<ThreadListing type="full" threads={threadsDB} />
			<SortFilterModal
				show={sortFilterModal}
				hide={() => {
					setSortFilterModal(null);
				}}
			/>
		</section>
	);
};

export default BrowseThreads;
