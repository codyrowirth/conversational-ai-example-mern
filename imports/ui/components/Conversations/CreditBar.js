import React from "react";
import CreditIcon from "./CreditIcon";
import "./CreditBar.scss";

// TO DO: Hook up to DB
const creditGroups = {
	credits: {
		id: "credits",
		type: "Credits",
		amount: 1000,
	},
	active: {
		id: "active",
		type: "Active",
		amount: 400,
	},
	available: {
		id: "available",
		type: "Available",
		amount: 600,
	},
};

const CreditBar = () => {
	return (
		<div className="credit-bar">
			<div className="credit-bar-inner">
				{Object.values(creditGroups).map((group) => {
					return (
						<div key={group.type} className="credit-group">
							<div className="credit-type">{group.type}</div>
							<CreditIcon dark={true} />
							<div className="credit-amount">{group.amount}</div>
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default CreditBar;
