import React, { useState, Fragment } from "react";
import { Link } from "react-router-dom";
import parse from "html-react-parser";

import threadsDB from "./threadsDB";

import Modal from "../../lib/components/Modal/Modal";
import GreyBox from "../../lib/components/GreyBox/GreyBox";
import Button from "../../lib/components/Button/Button";
import CreditIcon from "./CreditIcon";
import ThreadListing from "./ThreadListing";
import ContentPreviewModal from "./ContentPreviewModal";

import "./Thread.scss";

const Thread = (props) => {
	const [contentPreviewModal, setContentPreviewModal] = useState();

	const threadID = props.match.params.threadID;
	const thread = Object.values(threadsDB).find(
		(thread) => thread.id === threadID
	);

	return (
		<section className="conversations">
			<h1>{thread.name}</h1>
			{(thread.categories || thread.isBundle === true) && (
				<div className="tax-boxes">
					<div className="tax-boxes-inner">
						{thread.categories.map((category) => {
							return (
								<div key={category} className="tax-box">
									{category}
								</div>
							);
						})}
						{thread.isBundle === true && (
							<div key="bundle" className="tax-box">
								Bundle
							</div>
						)}
					</div>
				</div>
			)}
			<GreyBox>
				<h2>Highlights</h2>
				{thread.highlights && (
					<ul>
						{thread.highlights.map((highlight) => (
							<li key={highlight}>{highlight}</li>
						))}
					</ul>
				)}
				<div className="preview">
					<div className="action-links">
						<div className="label">Preview</div>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setContentPreviewModal(thread);
							}}
						>
							Content
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
							}}
						>
							Canvas
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
							}}
						>
							Interactive
						</button>
					</div>
				</div>
				<div className="button-wrap with-credit-cost">
					<div className="button-wrap-inner">
						<Button
							text={
								thread.saved === true
									? "Saved"
									: thread.active === true
									? "Active"
									: "Save"
							}
							disabled={
								thread.saved === true || thread.active === true ? true : false
							}
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
							}}
						/>
						<div className="credit-cost">
							<div className="credit-cost-inner">
								<CreditIcon />
								{thread.cost}
							</div>
						</div>
					</div>
				</div>
				{thread.inBundle !== "" && (
					<div className="bundle-alert">
						<p>
							<strong>Bundle Available!</strong>
						</p>
						<p>
							This thread is also available as part of the{" "}
							<Link to={`/conversations/${thread.inBundle}`}>
								Bundle Name Lorem Ipsum Dolor Sit Amet
							</Link>{" "}
							bundle.
						</p>
					</div>
				)}
			</GreyBox>
			{thread.longDescription && (
				<section>
					<h2>Details</h2>
					{parse(thread.longDescription)}
				</section>
			)}
			<section className="specifications">
				<h2>Specifications</h2>
				<div className="message-count">
					<p>
						<span>Messages:</span> {thread.specifications.messageCount}
					</p>
				</div>
				{thread.specifications.dependencies.length > 0 && (
					<div className="dependencies">
						<p>
							<span>Dependencies:</span>{" "}
							{thread.specifications.dependencies.map((dependency, index) => {
								const dependencyObj = Object.values(threadsDB).find(
									(thread) => thread.id === dependency
								);

								return (
									<Fragment key={dependency}>
										<Link to={`/conversations/${dependency}`}>
											{dependencyObj.name}
										</Link>
										{index + 1 < thread.specifications.dependencies.length &&
											", "}
									</Fragment>
								);
							})}
						</p>
					</div>
				)}
			</section>
			<section>
				<h2>Related</h2>
				<ThreadListing type="related" threads={threadsDB} />
			</section>
			<ContentPreviewModal
				show={contentPreviewModal}
				hide={() => {
					setContentPreviewModal(null);
				}}
			/>
		</section>
	);
};

export default Thread;
