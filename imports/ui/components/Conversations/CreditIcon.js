import React from "react";
import "./CreditIcon.scss";

const CreditIcon = ({ dark }) => {
	return (
		<div
			className={`credit-icon ${dark === true && "dark"}`}
			aria-label="Credit icon"
		></div>
	);
};

export default CreditIcon;
