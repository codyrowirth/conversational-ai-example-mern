import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Button from "../../lib/components/Button/Button";
import CreditIcon from "./CreditIcon";
import QuickViewModal from "./QuickViewModal";
import "./conversations.scss";
import "./ThreadListing.scss";

const ThreadListingItem = ({ type, dark, thread }) => {
	const [quickViewModal, setQuickViewModal] = useState();

	const history = useHistory();

	return (
		<div className="thread-listing-item">
			<h4>
				<button
					onClick={() => {
						history.push(`/conversations/${thread.id}`);
					}}
				>
					{thread.name}
				</button>
			</h4>
			{(thread?.categories || thread?.isBundle === true) && (
				<div className="tax-boxes">
					<div className="tax-boxes-inner">
						{thread?.categories.map((category) => {
							return (
								<div key={category} className="tax-box">
									{category}
								</div>
							);
						})}
						{thread.isBundle === true && (
							<div key="bundle" className="tax-box">
								Bundle
							</div>
						)}
					</div>
				</div>
			)}
			{thread?.shortDescription && (
				<p className="short-description">{thread.shortDescription}</p>
			)}
			<button
				className="action-link"
				onClick={(event) => {
					event.preventDefault();
					setQuickViewModal(thread);
				}}
			>
				Quick View
			</button>
			<div className="button-wrap with-credit-cost">
				<div className="button-wrap-inner">
					<Button
						text={
							thread.saved === true
								? "Saved"
								: thread.active === true
								? "Active"
								: "Save"
						}
						dark={type === "featured" ? true : false}
						disabled={
							thread.saved === true || thread.active === true ? true : false
						}
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					/>
					<div className="credit-cost">
						<div className="credit-cost-inner">
							<CreditIcon dark={dark} />
							{thread.cost}
						</div>
					</div>
				</div>
			</div>
			<QuickViewModal
				show={quickViewModal}
				hide={() => {
					setQuickViewModal(null);
				}}
			/>
		</div>
	);
};

export default ThreadListingItem;
