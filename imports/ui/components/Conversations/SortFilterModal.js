import React from "react";
import { useForm, Controller } from "react-hook-form";

import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import CheckboxRHF from "../../lib/components/Form/CheckboxRHF";

const SortFilterModal = ({ subset = "", show, hide }) => {
	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const onSubmit = () => {
		alert("TO DO");
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal
			className="modal limit-modal end-buttons conversations"
			show={!!show}
			hide={hide}
		>
			<h3>Sort &amp; Filter</h3>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="sort"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Sort"
										options={[
											"Name",
											"Release Date",
											"Popularity",
											"Credits",
										].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="order"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Order"
										options={["Ascending", "Descending"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<Controller
							name="category"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Category"
										options={["TO DO"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-third">
						<CheckboxRHF
							name={"bundle"}
							label="Bundle"
							register={register}
							error={errors}
						/>
					</div>
					{subset !== "saved" && subset !== "active" && (
						<div className="grid-col-third">
							<CheckboxRHF
								name={"saved"}
								label="Saved"
								register={register}
								error={errors}
							/>
						</div>
					)}
					{subset !== "saved" && subset !== "active" && (
						<div className="grid-col-third">
							<CheckboxRHF
								name={"active"}
								label="Active"
								register={register}
								error={errors}
							/>
						</div>
					)}
				</div>
				<div className="button-wrap">
					<Button text="Apply" onClick={handleSubmit(onSubmit)} />
					<Button text="Cancel" onClick={onCancel} />
				</div>
			</form>
		</Modal>
	);
};

export default SortFilterModal;
