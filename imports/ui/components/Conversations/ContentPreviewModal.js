import React from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import "./QuickViewModal.scss";
// import "./conversations.scss";

const ContentPreviewModal = ({ show, hide }) => {
	return (
		<Modal
			className="modal end-buttons conversations"
			show={!!show}
			hide={hide}
		>
			<h3>Content Preview</h3>
			<p>To do</p>
			<div className="button-wrap">
				<Button
					text="Close"
					onClick={(event) => {
						event.preventDefault();
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default ContentPreviewModal;
