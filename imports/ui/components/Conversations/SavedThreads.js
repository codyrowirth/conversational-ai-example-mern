import React, { useState } from "react";

import threadsDB from "./threadsDB";

import CreditIcon from "./CreditIcon";
import SearchBar from "../../lib/components/SearchBar/SearchBar";
import SortFilterModal from "./SortFilterModal";
import ThreadListing from "./ThreadListing";

const SavedThreads = () => {
	const [searchBarText, setSearchBarText] = useState("");
	const [sortFilterModal, setSortFilterModal] = useState();

	return (
		<section className="conversations">
			<h1>Saved Conversation Threads</h1>
			<SearchBar
				placeholder="Search saved"
				searchBarText={searchBarText}
				setSearchBarText={setSearchBarText}
			/>
			<div className="input-action-links-wrap">
				<div className="input-action-links-row top">
					<div className="input-action-links left">
						<div className="page-credit-total">
							<div className="page-credit-total-inner">
								{/* TO DO: Get saved thread credits total */}
								<span>Total</span> <CreditIcon /> 100 (to do)
							</div>
						</div>
					</div>
					<div className="input-action-links right">
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setSortFilterModal(true);
							}}
						>
							Sort &amp; Filter
						</button>
					</div>
				</div>
			</div>
			<ThreadListing type="saved" threads={threadsDB} />
			<SortFilterModal
				subset="saved"
				show={sortFilterModal}
				hide={() => {
					setSortFilterModal(null);
				}}
			/>
		</section>
	);
};

export default SavedThreads;
