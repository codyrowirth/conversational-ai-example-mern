import React from "react";
import Pagination from "../../lib/components/Pagination/Pagination";
import ThreadListingItem from "./ThreadListingItem";
import "./ThreadListing.scss";

const ThreadListing = ({ type, dark = false, threads }) => {
	let filteredThreads = [];

	if (type === "featured") {
		filteredThreads = Object.values(threads)
			.filter((thread) => thread.featured === true)
			.slice(0, 3);
	} else if (type === "popular") {
		filteredThreads = Object.values(threads)
			.filter((thread) => thread.featured === false)
			.sort((a, b) => {
				return b.activeCount - a.activeCount;
			})
			.slice(0, 3);
	} else if (type === "new") {
		filteredThreads = Object.values(threads)
			.filter((thread) => thread.featured === false)
			.sort((a, b) => {
				return b.date - a.date;
			})
			.slice(0, 3);
	} else if (type === "related") {
		// TO DO: Related threads logic
		// This is just the "new" thread listing logic copied from above
		filteredThreads = Object.values(threads)
			.filter((thread) => thread.featured === false)
			.sort((a, b) => {
				return b.activeCount - a.activeCount;
			})
			.slice(0, 3);
	} else if (type === "saved") {
		filteredThreads = Object.values(threads)
			.filter((thread) => thread.saved === true)
			.sort((a, b) => {
				const nameA = a.name.toLowerCase();
				const nameB = b.name.toLowerCase();

				if (nameA < nameB) {
					return -1;
				}

				if (nameA > nameB) {
					return 1;
				}

				return 0;
			})
			.slice(0, 9);
	} else if (type === "active") {
		filteredThreads = Object.values(threads)
			.filter((thread) => thread.active === true)
			.sort((a, b) => {
				const nameA = a.name.toLowerCase();
				const nameB = b.name.toLowerCase();

				if (nameA < nameB) {
					return -1;
				}

				if (nameA > nameB) {
					return 1;
				}

				return 0;
			})
			.slice(0, 9);
	} else {
		filteredThreads = Object.values(threads)
			.sort((a, b) => {
				const nameA = a.name.toLowerCase();
				const nameB = b.name.toLowerCase();

				if (nameA < nameB) {
					return -1;
				}

				if (nameA > nameB) {
					return 1;
				}

				return 0;
			})
			.slice(0, 9);
	}

	return (
		<div className={`thread-listing ${type}`}>
			<div className="thread-listing-inner">
				{filteredThreads.map((thread) => {
					return (
						<ThreadListingItem
							key={thread.id}
							type={type}
							dark={dark}
							thread={thread}
						/>
					);
				})}
			</div>
			{type !== "featured" &&
				type !== "popular" &&
				type !== "new" &&
				type !== "related" && (
					// filteredThreads.length > 9 &&
					<Pagination />
				)}
		</div>
	);
};

export default ThreadListing;
