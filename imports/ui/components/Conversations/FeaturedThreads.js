import React from "react";
import threadsDB from "./threadsDB";
import ThreadListing from "./ThreadListing";

const FeaturedThreads = () => {
	return (
		<section className="conversations">
			<h1>Featured Conversation Threads</h1>
			<section>
				<ThreadListing type="featured" dark={true} threads={threadsDB} />
			</section>
			<section>
				<h2>Popular</h2>
				<ThreadListing type="popular" threads={threadsDB} />
			</section>
			<section>
				<h2>New</h2>
				<ThreadListing type="new" threads={threadsDB} />
			</section>
		</section>
	);
};

export default FeaturedThreads;
