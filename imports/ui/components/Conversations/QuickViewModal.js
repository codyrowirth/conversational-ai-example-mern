import React from "react";
import { Link, useHistory } from "react-router-dom";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import CreditIcon from "./CreditIcon";
import "./QuickViewModal.scss";
import "./conversations.scss";

const QuickView = ({ show, hide }) => {
	const history = useHistory();

	return (
		<Modal
			className="modal end-buttons conversations"
			show={!!show}
			hide={hide}
		>
			<h3>{show?.name && show.name}</h3>
			{(show?.categories || show?.isBundle === true) && (
				<div className="tax-boxes">
					<div className="tax-boxes-inner">
						{show?.categories.map((category) => {
							return (
								<div key={category} className="tax-box">
									{category}
								</div>
							);
						})}
						{show?.isBundle === true && (
							<div key="bundle" className="tax-box">
								Bundle
							</div>
						)}
					</div>
				</div>
			)}
			{show?.highlights && (
				<ul>
					{show.highlights.map((highlight) => (
						<li key={highlight}>{highlight}</li>
					))}
				</ul>
			)}
			<div className="button-wrap with-credit-cost">
				<div className="button-wrap-inner">
					<Button
						text={
							show?.saved === true
								? "Saved"
								: show?.active === true
								? "Active"
								: "Save"
						}
						disabled={
							show?.saved === true || show?.active === true ? true : false
						}
						onClick={(event) => {
							event.preventDefault();
							alert("TO DO");
						}}
					/>
					<div className="credit-cost">
						<div className="credit-cost-inner">
							<CreditIcon />
							{show?.cost}
						</div>
					</div>
				</div>
			</div>

			{show?.inBundle !== "" && (
				<div className="bundle-alert">
					<p>
						<strong>Bundle Available!</strong>
					</p>
					<p>
						This thread is also available as part of the{" "}
						<Link to={`/conversations/${show?.inBundle}`}>
							Bundle Name Lorem Ipsum Dolor Sit Amet
						</Link>{" "}
						bundle.
					</p>
				</div>
			)}

			<div className="button-wrap">
				<Button
					text="More Info"
					onClick={(event) => {
						event.preventDefault();
						history.push(`/conversations/${show?.id}`);
					}}
				/>
				<Button
					text="Close"
					onClick={(event) => {
						event.preventDefault();
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default QuickView;
