import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Retrievers = () => {
	return (
		<section className="retrievers">
			<h1>Retrievers</h1>
			<ComingSoon />
		</section>
	);
};

export default Retrievers;
