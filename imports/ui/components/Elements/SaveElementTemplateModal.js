import React, { useState } from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import ElementTemplates from "../../../api/collections/ElementTemplates";
import SelectCreatableRHF from "../../lib/components/Form/SelectCreatableRHF";
import { useTracker } from "meteor/react-meteor-data";

const SaveElementTemplateModal = ({ show, hide, elements }) => {
	const [templateToUpdate, setTemplateToUpdate] = useState();

	const { loading, templates } = useTracker(() => {
		const sub = Meteor.subscribe("elementTemplates", Meteor.user().projectID);
		if (!sub.ready()) {
			return { loading: true, templates: [] };
		}

		return {
			templates: ElementTemplates.find({
				projectID: Meteor.user().projectID,
			}).fetch(),
			loading: false,
		};
	});

	const onSubmit = () => {
		if (templateToUpdate.__isNew__) {
			Meteor.call(`elementTemplates.insert`, {
				name: templateToUpdate.value,
				elements: elements.map((element) => ({
					name: element.name,
					content: element.content,
				})),
			});
		} else {
			Meteor.call(`elementTemplates.update`, templateToUpdate.value, {
				elements: elements.map((element) => ({
					name: element.name,
					content: element.content,
				})),
			});
		}
		hide();
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Save Template</h3>
			<form>
				<div className="grid">
					<div className="grid-col-full">
						<SelectCreatableRHF
							type="text"
							name="templateName"
							label="Template Name"
							options={templates.map((template) => ({
								value: template._id,
								label: template.name,
							}))}
							value={{
								label: templateToUpdate?.label || "",
								value: templateToUpdate?._id || "",
							}}
							onChange={(val) => {
								setTemplateToUpdate(val);
							}}
						/>
					</div>
				</div>
			</form>
			<div className="button-wrap">
				<Button text="Save" onClick={onSubmit} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default SaveElementTemplateModal;
