import React from "react";
import SlateComp from "./Slate";
import { Meteor } from "meteor/meteor";

const ElementContent = ({ element, elements, message }) => {
	return (
		<div id="element-content">
			<SlateComp
				type="element"
				value={
					element?.content || [{ type: "paragraph", children: [{ text: "" }] }]
				}
				onBlur={(value) => {
					Meteor.call("elements.update", element._id, { content: value });
				}}
				element={element}
				elements={elements}
				message={message}
				messageID={element?.messageID}
			/>
		</div>
	);
};

export default ElementContent;
