import React, {
	useEffect,
	useCallback,
	useMemo,
	useRef,
	useState,
	useContext,
} from "react";
import {
	Slate,
	Editable,
	withReact,
	useSlate,
	ReactEditor,
	useSelected,
} from "slate-react";
import { withHistory } from "slate-history";
import { createEditor, Editor, Transforms, Node, Range } from "slate";
import { Button, Icon, Toolbar, Menu } from "./SlateLib";
import isHotkey from "is-hotkey";
import { css } from "@emotion/css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import MenuItem from "@material-ui/core/MenuItem";
import MUIMenu from "@material-ui/core/Menu";
import DialogContent from "@material-ui/core/DialogContent";
import Dialog from "@material-ui/core/Dialog";
// import MessageBrowser from "./MessageBrowser";
import {
	ElementContext,
	pageStore,
} from "../Canvas/modals/inner-modals/ElementModalForm";
import { plainTextSerialize } from "../../lib/utilities/plainTextSerialize";
import isUrl from "is-url";
import "./Slate.scss";
import ElementContent from "./ElementContent";
import { useTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import Attributes from "../../../api/collections/Attributes";
import RCMenu, { MenuItem as RCMenuItem } from "rc-menu";
import "rc-menu/assets/index.css";
import { usePopper } from "react-popper";
import Modal from "../../lib/components/Modal/Modal";
import NewAttributeForm from "../AttributesManager/NewAttributeForm";
import NestedData from "../../lib/components/NestedData/NestedData";
import FileUpload from "../../lib/components/Form/FileUpload";
import TCSButton from "../../lib/components/Button/Button";

import Toggle from "../../lib/components/Form/Toggle";
import Select from "react-select";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import { PropagateLoader } from "react-spinners";
import Messages from "../../../api/collections/Messages";
import Shapes from "../../../api/collections/Shapes";
import Elements from "../../../api/collections/Elements";
import AppContext from "../App/AppContext";

const HOTKEYS = {
	"mod+b": "bold",
	"mod+i": "italic",
	"mod+u": "underline",
	"mod+s": "save",
};

const LIST_TYPES = ["numbered-list", "bulleted-list"];

let _bitCreating = false;

const withMentions = (editor) => {
	const { isInline, isVoid } = editor;

	editor.isInline = (element) => {
		return element.passthrough ||
			element.passthrough ||
			element.lme ||
			(element.elementID && element.messageID) ||
			element.bitID ||
			element.imageElementID
			? true
			: isInline(element);
	};

	editor.isVoid = (element) => {
		return element.passthrough ||
			element.passthrough ||
			element.lme ||
			(element.elementID && element.messageID) ||
			element.bitID ||
			element.imageElementID
			? true
			: isVoid(element);
	};

	return editor;
};

const withLinks = (editor) => {
	const { insertData, insertText, isInline } = editor;

	editor.isInline = (element) => {
		return element.type === "link" ? true : isInline(element);
	};

	editor.insertText = (text) => {
		if (text && isUrl(text)) {
			wrapLink(editor, text);
		} else {
			insertText(text);
		}
	};

	editor.insertData = (data) => {
		const text = data.getData("text/plain");

		if (text && isUrl(text)) {
			wrapLink(editor, text);
		} else {
			insertData(data);
		}
	};

	return editor;
};

const SlateComp = (props) => {
	const { copyBitID } = useContext(AppContext);
	const [value, setValue] = useState(props.value);
	const renderElement = useCallback(
		(props2) => (
			<Element
				{...props2}
				pageStore={props.pageStore}
				save={props.save}
				elementOrBitCaseID={
					props.type === "element" ? props.element._id : props.bitCase.id
				}
				type={props.type}
			/>
		),
		[
			props.pageStore,
			props.save,
			props.bitCase,
			props?.element?._id,
			props.type,
		]
	);
	const renderLeaf = useCallback((props) => <Leaf {...props} />, []);
	const editor = useMemo(
		() => withLinks(withHistory(withMentions(withReact(createEditor())))),
		[]
	);

	const [length, setLength] = useState(null);

	useEffect(() => {
		if (props.element) {
			if (props.element.minLength !== props.element.maxLength) {
				setLength(`${props.element.minLength}-${props.element.maxLength}`);
			} else if (
				props.element.minLength === 0 &&
				props.element.maxLength === 0
			) {
				//ignore
			} else {
				setLength(props.element.minLength);
			}
		} else if (props.bitCase) {
			(async () => {
				const plainTextValue = plainTextSerialize(value);
				let min = plainTextValue.length;
				let max = plainTextValue.length;
				let containsBits = false;
				const bits = JSON.stringify(value).matchAll(/"bitID":"(.+?)"/g);
				// for (const bit of bits) {
				// containsBits = true;
				// const bitID = bit[1];
				// const bitsDB = await client
				// 	.service("bit")
				// 	.find({ query: { id: parseInt(bitID) } });
				// for (const bitDB of bitsDB) {
				// 	min += bitDB.minLength;
				// 	max += bitDB.maxLength;
				// }
				// }
				if (containsBits) {
					if (min !== max) {
						setLength(`${min}-${max}`);
					} else if (min === 0 && max === 0) {
						//ignore
					} else {
						setLength(min);
					}
				} else if (plainTextValue.length) {
					setLength(plainTextValue.length);
				}
			})();
		}
		// eslint-disable-next-line
	}, []);

	const attributes = useTracker(() => {
		if (props.message) {
			const attributesSubscription = Meteor.subscribe(
				"attributes",
				props.message?.attributes || []
			);

			if (!attributesSubscription.ready()) {
				return [];
			}

			return Attributes.find({
				_id: { $in: props.message?.attributes || [] },
			}).fetch();
		}
		return [];
	});

	return (
		<div style={{ margin: 20 }}>
			<Slate
				editor={editor}
				value={parseSlateValue(value)}
				onChange={(value) => {
					setValue(parseSlateValue(value));
				}}
				data-gramm="false"
			>
				<Toolbar>
					<MarkButton format="bold" icon="format_bold" />
					<MarkButton format="italic" icon="format_italic" />
					<MarkButton format="underline" icon="format_underlined" />
					<BlockButton format="numbered-list" icon="format_list_numbered" />
					<BlockButton format="bulleted-list" icon="format_list_bulleted" />
					<LinkButton />
					<LMEButton elements={props.elements} element={props.element} />
					{/*<ExternalLMEButton elements={props.elements} />*/}
					{attributes && attributes.length > 0 ? (
						<PassthroughButton attributes={attributes} />
					) : null}
					{copyBitID && props.element._id && props.type === "element" ? (
						<PasteButton
							copyBitID={window.localStorage.copyBitID}
							elementID={props.element._id}
							copyBitText={window.localStorage.copyBitText}
						/>
					) : null}
					{/*{!props.pageStore.copyBitID &&*/}
					{/*window.localStorage.copyBitID &&*/}
					{/*props.element._id ? (*/}
					{/*	<PasteButton*/}
					{/*		copyBitID={window.localStorage.copyBitID}*/}
					{/*		elementID={props.element._id}*/}
					{/*		copyBitText={window.localStorage.copyBitText}*/}
					{/*		bitCaseID={props.bitCase?.id}*/}
					{/*	/>*/}
					{/*) : null}*/}
					{/*<ImageElementButton elements={pageStore.imageElements} />*/}
					{props.toolbar ? (
						<div style={{ float: "right" }}>{props.toolbar}</div>
					) : null}
					{length ? (
						<p style={{ color: "gray", textAlign: "right" }}>{length}</p>
					) : null}
				</Toolbar>
				<HoveringToolbar
					elementOrBitCaseID={
						props.type === "element" ? props?.element?._id : props?.bitCase._id
					}
					type={props.type}
					bitCase={props.bitCase}
					element={props.element}
					messageID={props?.element?.messageID}
				/>
				<Editable
					renderElement={renderElement}
					renderLeaf={renderLeaf}
					placeholder="Enter your brilliant content here..."
					spellCheck
					autoFocus={props.type === "element"}
					style={{ lineHeight: 1.5 }}
					onBlur={() => {
						props.onBlur(value);
					}}
					onKeyDown={(event) => {
						const { selection } = editor;
						if (selection) {
							for (const hotkey in HOTKEYS) {
								if (isHotkey(hotkey, event)) {
									event.preventDefault();
									if (hotkey === "mod+s") {
									} else {
										const mark = HOTKEYS[hotkey];
										toggleMark(editor, mark);
									}
								}
							}
						}
					}}
				/>
			</Slate>
		</div>
	);
};

/**
 * @func parseSlateValue - Recursively strip smart quotes & smart apostrophes
 * from Slate content
 * @param list
 */
const parseSlateValue = (list) => list.map(fixSlateNode);

const fixSlateNode = (obj) => {
	const fixedObj = fixText(obj);
	const { children } = fixedObj;
	// NOTE: We have to return a new object instead of mutating the existing
	// property because Slate uses frozen Objects instead of mutable objects
	return !children
		? fixedObj
		: {
				...fixedObj,
				children: children.map((child) => fixSlateNode(child)),
		  };
};

const fixText = (obj) =>
	!obj.text
		? obj
		: {
				...obj,
				text: replaceSmartApostrophes(replaceSmartQuotes(obj.text)),
		  };

const replaceSmartApostrophes = (str) =>
	str.replaceAll(`‘`, `'`).replaceAll(`’`, `'`);
const replaceSmartQuotes = (str) =>
	str.replaceAll(`“`, `"`).replaceAll(`”`, `"`);

const HoveringToolbar = ({
	elementOrBitCaseID,
	type,
	bitCase,
	element,
	messageID,
}) => {
	const ref = useRef();
	const editor = useSlate();

	useEffect(() => {
		const el = ref.current;
		const { selection } = editor;

		if (!el) {
			return;
		}

		let selectedNode;

		try {
			if (selection) {
				selectedNode = Node.get(editor, selection.focus.path);
			}
		} catch (e) {
			return;
		}

		if (
			!selection ||
			(selectedNode && selectedNode.bit) ||
			!ReactEditor.isFocused(editor) ||
			Range.isCollapsed(selection) ||
			Editor.string(editor, selection) === ""
		) {
			el.removeAttribute("style");
			return;
		}

		const domSelection = window.getSelection();
		const domRange = domSelection.getRangeAt(0);
		const rect = domRange.getBoundingClientRect();
		let modal = el.closest(".modal");

		if (!modal) {
			const paperEl = document.querySelector(
				"body > div.MuiDialog-root > div.MuiDialog-container.MuiDialog-scrollPaper > div"
			);

			el.style.top = `${rect.top + paperEl.scrollTop - el.offsetHeight}px`;
			el.style.left = `${
				rect.left + window.pageXOffset - el.offsetWidth / 2 + rect.width / 2
			}px`;
		} else {
			el.style.top = `${
				rect.top -
				(window.innerHeight - modal.offsetHeight) / 2 -
				el.offsetHeight +
				modal.scrollTop
			}px`;

			el.style.left = `${
				rect.left -
				(window.innerWidth - modal.offsetWidth) / 2 -
				el.offsetWidth / 2 +
				window.pageXOffset +
				rect.width / 2
			}px`;
		}
		el.style.opacity = 1;
	});

	return (
		<Menu
			ref={ref}
			className={css`
				padding: 8px 7px 6px;
				position: absolute;
				z-index: 1;
				top: -10000px;
				left: -10000px;
				margin-top: -6px;
				opacity: 0;
				background-color: #222;
				border-radius: 4px;
				transition: opacity 0.75s;
			`}
		>
			<FormatButton format="bold" icon="format_bold" />
			<FormatButton format="italic" icon="format_italic" />
			<FormatButton format="underline" icon="format_underlined" />
			{type === "element" && (
				<BitButton
					elementOrBitCaseID={elementOrBitCaseID}
					type={type}
					bitCase={bitCase}
				/>
			)}
			{/*<NewLMEButton messageID={messageID} />*/}
		</Menu>
	);
};

const BitButton = ({ format, elementOrBitCaseID, type, save, bitCase }) => {
	const editor = useSlate();
	return (
		<Button
			reversed
			active={isFormatActive(editor, format)}
			onClick={async (event) => {
				event.preventDefault();
				if (!_bitCreating) {
					_bitCreating = true;
					await addBit(editor, elementOrBitCaseID, type, save, bitCase);
					_bitCreating = false;
				}
			}}
		>
			<img
				src="/images/editor-icons/bit.svg"
				style={{ maxHeight: 20 }}
				alt="icon"
			/>
		</Button>
	);
};

const NewLMEButton = ({ messageID, loadElements, save }) => {
	const editor = useSlate();

	return (
		<Button
			onClick={async (event) => {
				event.preventDefault();
				const name = window.getSelection().toString() || "LME";

				try {
					let element;
					const elements = await client
						.service("element")
						.find({ query: { messageDBID: messageID, name } });

					if (elements.length) {
						element = elements[0];
					} else {
						element = await client.service("element").create({
							messageDBID: messageID,
							name,
						});
						await loadElements();
					}

					Transforms.insertNodes(editor, {
						label: name,
						lme: element.id,
						children: [{ text: "" }],
					});

					setTimeout(() => {
						save(false, element.id);
					}, 250);
				} catch (e) {
					// eslint-disable-next-line no-console
					console.error(e);
					alert("Error creating new LME");
				}
			}}
		>
			<img
				src="/images/editor-icons/newlme.svg"
				style={{ maxHeight: 20 }}
				alt="icon"
			/>
		</Button>
	);
};

const addBit = async (editor, elementOrBitCaseID, type, save, bitCase) => {
	Meteor.call(
		`bits.insert`,
		{ elementID: elementOrBitCaseID },
		(err, bitID) => {
			if (err) {
				alert(`Error creating bit, please contact support`);
			} else {
				Transforms.insertNodes(editor, {
					label: window.getSelection().toString() || "bit",
					bitID: bitID,
					children: [{ text: "" }],
				});

				//openbit
			}
		}
	);
};

const FormatButton = ({ format, icon, img, elementID, type }) => {
	const editor = useSlate();
	return (
		<Button
			reversed
			active={isFormatActive(editor, format)}
			onMouseDown={(event) => {
				event.preventDefault();
				toggleMark(editor, format, elementID, type);
			}}
		>
			{img ? (
				<img src={img} style={{ maxHeight: 20 }} alt="icon" />
			) : (
				<Icon>{icon}</Icon>
			)}
		</Button>
	);
};

const isFormatActive = (editor, format) => {
	try {
		const [match] = Editor.nodes(editor, {
			match: (n) => n[format] === true,
			mode: "all",
		});
		return !!match;
	} catch (e) {
		return false;
	}
};

const toggleBlock = (editor, format) => {
	const isActive = isBlockActive(editor, format);
	const isList = LIST_TYPES.includes(format);

	Transforms.unwrapNodes(editor, {
		match: (n) => LIST_TYPES.includes(n.type),
		split: true,
	});

	let type = format;
	if (isActive) {
		type = "paragraph";
	} else if (isList) {
		type = "list-item";
	}
	Transforms.setNodes(editor, {
		type,
	});

	if (!isActive && isList) {
		const block = { type: format, children: [] };
		Transforms.wrapNodes(editor, block);
	}
};

const toggleMark = async (editor, format, elementID) => {
	const isActive = isMarkActive(editor, format);

	if (isActive) {
		Editor.removeMark(editor, format);
	} else {
		Editor.addMark(editor, format, true);
	}
};

const isBlockActive = (editor, format) => {
	try {
		const [match] = Editor.nodes(editor, {
			match: (n) => n.type === format,
		});

		return !!match;
	} catch (e) {
		return false;
	}
};

const isMarkActive = (editor, format) => {
	try {
		const marks = Editor.marks(editor);
		return marks ? marks[format] === true : false;
	} catch (e) {
		return false;
	}
};

const Element = ({
	attributes,
	children,
	element,
	pageStore,
	save,
	elementOrBitCaseID,
	type,
}) => {
	const { setOpenBitID } = useContext(ElementContext);

	const selected = useSelected();
	if (element.bitID) {
		return (
			<span
				className="bit"
				{...attributes}
				onClick={(evt) => {
					evt.preventDefault();
					if (evt.detail === 1) {
						//don't fire second time if double click
						setOpenBitID(element.bitID);
					}
				}}
				contentEditable={false}
				style={{
					boxShadow: selected ? "0 0 0 2px var(--blue)" : "none",
					userSelect: "none",
					color: "var(--blue)",
				}}
			>
				{element.label}
				{children}
			</span>
		);
	}

	if (element.imageElementID) {
		return (
			<span
				className="bit"
				{...attributes}
				onClick={(evt) => {
					evt.preventDefault();
					if (evt.detail === 1) {
						//don't fire second time if double click
						// bitClicked(element, pageStore, save, elementOrBitCaseID, type);
					}
				}}
				contentEditable={false}
				style={{
					boxShadow: selected ? "0 0 0 2px var(--blue)" : "none",
					userSelect: "none",
					color: "var(--blue)",
				}}
			>
				{element.label}
				{children}
			</span>
		);
	}

	if (element.passthrough) {
		return (
			<span
				className="passthrough"
				{...attributes}
				contentEditable={false}
				style={{
					boxShadow: selected ? "0 0 0 2px var(--yellow)" : "none",
					userSelect: "none",
					color: "var(--yellow)",
				}}
			>
				{element.label}
				{children}
			</span>
		);
	}

	if (element.lme || (element.elementID && element.messageID)) {
		return (
			<span
				className="lme"
				{...attributes}
				onClick={(evt) => {
					// lmeClicked(evt, leaf, pageStore, save);
				}}
				contentEditable={false}
				style={{
					boxShadow: selected ? "0 0 0 2px var(--green)" : "none",
					userSelect: "none",
					color: "var(--green)",
				}}
			>
				{element.label}
				{children}
			</span>
		);
	}

	switch (element.type) {
		case "bulleted-list":
			return <ul {...attributes}>{children}</ul>;
		case "list-item":
			return <li {...attributes}>{children}</li>;
		case "numbered-list":
			return <ol {...attributes}>{children}</ol>;
		case "link":
			return (
				<a
					{...attributes}
					href={element.url}
					style={{ color: "blue", textDecoration: "underline" }}
				>
					{children}
				</a>
			);
		default:
			return <p {...attributes}>{children}</p>;
	}
};

const Leaf = ({ attributes, children, leaf, pageStore, save }) => {
	if (leaf.bold) {
		children = <span style={{ fontWeight: 700 }}>{children}</span>;
	}

	if (leaf.italic) {
		children = <span style={{ fontStyle: "italic" }}>{children}</span>;
	}

	if (leaf.underline) {
		children = <span style={{ textDecoration: "underline" }}>{children}</span>;
	}

	return <span {...attributes}>{children}</span>;
};

const BlockButton = ({ format, icon }) => {
	const editor = useSlate();
	return (
		<Button
			active={isBlockActive(editor, format)}
			onMouseDown={(event) => {
				event.preventDefault();
				toggleBlock(editor, format);
			}}
		>
			<Icon>{icon}</Icon>
		</Button>
	);
};

const MarkButton = ({ format, icon }) => {
	const editor = useSlate();
	return (
		<Button
			active={isMarkActive(editor, format)}
			onMouseDown={(event) => {
				event.preventDefault();
				toggleMark(editor, format);
			}}
		>
			<Icon>{icon}</Icon>
		</Button>
	);
};

const LMEButton = ({ elements, element }) => {
	const editor = useSlate();
	const [lmeModalOpen, setLMEModalOpen] = useState(false);
	const [currentSelection, setCurrentSelection] = useState();
	return (
		<>
			<Button
				active={isMarkActive(editor, "lme")}
				onMouseDown={(event) => {
					setCurrentSelection(editor.selection);
					event.preventDefault();
					setLMEModalOpen(true);
				}}
			>
				<FontAwesomeIcon icon="project-diagram" />
			</Button>
			{lmeModalOpen && (
				<LMEModal
					show={lmeModalOpen}
					hide={() => {
						setLMEModalOpen(false);
					}}
					elements={elements}
					element={element}
					editor={editor}
					currentSelection={currentSelection}
				/>
			)}
		</>
	);
};

const LMEModal = ({
	editor,
	elements,
	element,
	show,
	hide,
	currentSelection,
}) => {
	const [externalLME, setExternalLME] = useState(false);
	const [selectedElement, setSelectedElement] = useState();
	const [selectedMessage, setSelectedMessage] = useState();
	const [xlmeElements, setXLMEElements] = useState([]);

	const { shapes, isLoading } = useTracker(() => {
		if (!externalLME) {
			return { isLoading: false, messages: [], shapes: [] };
		}
		const sub = Meteor.subscribe(
			"projectMessageShapes",
			Meteor.user().projectID
		);

		if (!sub.ready()) {
			return { isLoading: true, elementsDB: [] };
		}

		return {
			isLoading: false,
			shapes: Shapes.find(
				{ type: "message" },
				{ fields: { _id: 1, "data.label": 1 } }
			).fetch(),
		};
	});

	useEffect(() => {
		if (externalLME && selectedMessage?.value) {
			setSelectedElement(null);
			Meteor.call(
				"elements.getByShapeID",
				selectedMessage.value,
				(err, data) => {
					setXLMEElements(data);
				}
			);
		} else {
			setXLMEElements([]);
		}
	}, [externalLME, selectedMessage]);

	const insertLME = () => {
		Transforms.select(editor, currentSelection);
		setTimeout(() => {
			if (externalLME) {
				Transforms.insertNodes(
					editor,
					{
						label: `[${selectedMessage.label}]:[${selectedElement.label}]`,
						elementID: selectedElement.value,
						messageID: selectedMessage.value,
						children: [{ text: "" }],
					},
					{ at: currentSelection.anchor }
				);
			} else {
				Transforms.insertNodes(
					editor,
					{
						label: selectedElement.label,
						lme: selectedElement.value,
						children: [{ text: "" }],
					},
					{ at: currentSelection.anchor }
				);
			}

			hide();
			setSelectedElement(null);
			setSelectedMessage(null);
			setExternalLME(false);

			setTimeout(() => {
				ReactEditor.focus(editor);
			}, 50);
		}, 1);
	};

	return (
		<Modal className="modal end-buttons" show={!!show} hide={hide}>
			<h3>Insert LME</h3>
			<div>
				<Toggle
					name="lmeType"
					labelLeft="From This Message"
					labelRight="From Another Message"
					checked={externalLME}
					setChecked={setExternalLME}
				/>
			</div>
			<div className="row">
				{externalLME ? (
					<div className={"col-xs-6"}>
						{isLoading ? (
							<div style={{ textAlign: "center" }}>
								<PropagateLoader color="#2f3d47" />
							</div>
						) : (
							<SelectRHF
								name={"message"}
								label={"Message"}
								value={selectedMessage}
								onChange={setSelectedMessage}
								options={shapes.map((shape) => ({
									value: shape._id,
									label: shape.data?.label,
								}))}
							/>
						)}
					</div>
				) : null}
				<div className={`col-xs-${externalLME ? 6 : 12}`}>
					<SelectRHF
						name={"elements"}
						label={"Elements"}
						value={selectedElement}
						onChange={setSelectedElement}
						options={(externalLME ? xlmeElements : elements)
							.filter((loopElement) => {
								return loopElement._id !== element._id;
							})
							.map((element) => ({
								value: element._id,
								label: element.name,
							}))}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<TCSButton
					text="Insert"
					onClick={insertLME}
					disabled={!selectedElement}
				/>
				<TCSButton text="Close" onClick={hide} />
			</div>
		</Modal>
	);
};

const PassthroughButton = ({ attributes }) => {
	const editor = useSlate();
	const [lmeButton, setLMEButton] = useState();
	const [referenceElement, setReferenceElement] = useState(null);
	const [popperElement, setPopperElement] = useState(null);
	const popper = usePopper(referenceElement, popperElement, {});

	return attributes && attributes.length ? (
		<>
			<Button
				active={isMarkActive(editor, "passthrough")}
				onMouseDown={(event) => {
					setLMEButton(event.target);
				}}
				ref={setReferenceElement}
			>
				<FontAwesomeIcon icon="handshake" />
			</Button>
			{lmeButton && (
				<div
					ref={setPopperElement}
					style={{ ...popper.styles.popper, zIndex: 2, background: "white" }}
					{...popper.attributes.popper}
				>
					<RCMenu>
						{attributes.map((element) => {
							return (
								<RCMenuItem
									key={element._id}
									onClick={() => {
										Transforms.insertNodes(editor, {
											label: element.name,
											passthrough: element._id,
											children: [{ text: "" }],
										});
										Transforms.move(editor);
										setLMEButton(null);

										setTimeout(() => {
											ReactEditor.focus(editor);
										}, 50);
									}}
								>
									{element.name}
								</RCMenuItem>
							);
						})}
					</RCMenu>
				</div>
			)}
		</>
	) : null;
};

const PasteButton = ({ copyBitID, elementID, copyBitText, bitCaseID }) => {
	const editor = useSlate();

	return (
		<Button
			active={false}
			onMouseDown={async (event) => {
				if (copyBitID && elementID) {
					const newBitID = await Meteor.callPromise(`bits.paste`, {
						elementID,
						copyBitID,
					});

					Transforms.insertNodes(editor, {
						label: copyBitText,
						bitID: newBitID,
						children: [{ text: "" }],
					});

					setTimeout(() => {
						ReactEditor.focus(editor);
					}, 50);
				} else {
					alert("Error pasting bit. Please contact support");
				}
			}}
		>
			<FontAwesomeIcon icon="clipboard" />
		</Button>
	);
};

const ImageElementButton = ({ elements }) => {
	const editor = useSlate();
	const [imageElementButton, setImageElementButton] = useState();

	return elements && elements.length ? (
		<>
			<Button
				active={isMarkActive(editor, "lme")}
				onMouseDown={(event) => {
					event.preventDefault();
					setImageElementButton(event.target);
				}}
			>
				<FontAwesomeIcon icon={faImage} />
			</Button>
			<MUIMenu
				key={2}
				anchorEl={imageElementButton}
				keepMounted
				open={!!imageElementButton}
				onClose={() => {
					setImageElementButton(null);
				}}
			>
				{elements.map((element) => {
					return (
						<MenuItem
							key={element.id}
							onClick={() => {
								Transforms.insertNodes(editor, {
									label: `IMG: [${element.name}]`,
									imageElementID: element.id,
									children: [{ text: "" }],
								});
								setImageElementButton(null);

								setTimeout(() => {
									ReactEditor.focus(editor);
								}, 50);
							}}
						>
							{element.name}
						</MenuItem>
					);
				})}
			</MUIMenu>
		</>
	) : null;
};

const isLinkActive = (editor) => {
	const [link] = Editor.nodes(editor, { match: (n) => n.type === "link" });
	return !!link;
};

const insertLink = (editor, url) => {
	if (editor.selection) {
		wrapLink(editor, url);
	}
};

const wrapLink = (editor, url) => {
	if (isLinkActive(editor)) {
		unwrapLink(editor);
	}

	const { selection } = editor;
	const isCollapsed = selection && Range.isCollapsed(selection);
	const link = {
		type: "link",
		url,
		children: isCollapsed ? [{ text: url }] : [],
	};

	if (isCollapsed) {
		Transforms.insertNodes(editor, link);
	} else {
		Transforms.wrapNodes(editor, link, { split: true });
		Transforms.collapse(editor, { edge: "end" });
	}
};

const unwrapLink = (editor) => {
	Transforms.unwrapNodes(editor, { match: (n) => n.type === "link" });
};

const LinkButton = () => {
	const editor = useSlate();

	return (
		<Button
			active={isLinkActive(editor)}
			onMouseDown={(event) => {
				event.preventDefault();
				const url = window.prompt("Enter the URL of the link:");
				if (!url) return;
				insertLink(editor, url);
			}}
		>
			<Icon>link</Icon>
		</Button>
	);
};

export default SlateComp;
