import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import Button from "../../lib/components/Button/Button";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import Modal from "../../lib/components/Modal/Modal";
import { useTracker } from "meteor/react-meteor-data";
import ElementTemplates from "../../../api/collections/ElementTemplates";
import { PropagateLoader } from "react-spinners";

const DeleteElementTemplateModal = ({
	loadedTemplate,
	setLoadedTemplate,
	show,
	hide,
}) => {
	const schema = yup.object().shape({
		template: yup.object().required("Please select a template."),
	});

	const { loading, templates } = useTracker(() => {
		const sub = Meteor.subscribe("elementTemplates", Meteor.user().projectID);
		if (!sub.ready()) {
			return { loading: true, templates: [] };
		}

		return {
			templates: ElementTemplates.find({
				projectID: Meteor.user().projectID,
			}).fetch(),
			loading: false,
		};
	});

	const {
		control,
		formState: { errors },
		handleSubmit,
		setValue,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		const templateID = data.template.value;
		Meteor.call(`elementTemplates.remove`, templateID);

		if (templateID === loadedTemplate) {
			setLoadedTemplate("");
		}

		hide();
		setValue("template", {});
	};

	const onCancel = () => {
		hide();
		setValue("template", {});
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Delete Template</h3>
			{loading ? (
				<PropagateLoader color="#2f3d47" />
			) : (
				<form>
					<div className="grid-row">
						<div className="grid-col-full">
							<Controller
								name="template"
								control={control}
								render={({ field }) => {
									return (
										<SelectRHF
											name={field.name}
											label="Template"
											options={templates.map((option) => ({
												value: option._id,
												label: option.name,
											}))}
											{...field}
											ref={null}
											error={errors}
										/>
									);
								}}
							/>
						</div>
					</div>
				</form>
			)}
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={handleSubmit(onSubmit)}
					disabled={loading}
				/>
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default DeleteElementTemplateModal;
