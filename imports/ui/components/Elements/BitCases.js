import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import BitCasesDB from "../../../api/collections/BitCases";
import { PropagateLoader } from "react-spinners";
import VisibilitySensor from "react-visibility-sensor";
import {
	BitCaseRuleContainer,
	BrancherRuleContainer,
} from "../Canvas/modals/inner-modals/BrancherRulesModal";
import SlateComp from "./Slate";
import "./BitCases.scss";
import ToggleControlRHF from "../../lib/components/Form/ToggleControlRHF";
import { useSnackbar } from "notistack";
import Grid from "@mui/material/Grid";

const BitCases = ({ openBitID, message, elements, element }) => {
	const { bitCases, loading } = useTracker(() => {
		const handler = Meteor.subscribe("bitCases", openBitID);

		if (!handler.ready()) {
			return { loading: true, bitCases: [] };
		}

		const bitCases = BitCasesDB.find({ bitID: openBitID }).fetch();

		if (bitCases.length === 0) {
			Meteor.call(`bitCases.insert`, {
				bitID: openBitID,
				chainedOperator: "and",
			});
			return { loading: true };
		}

		return {
			loading: false,
			bitCases,
		};
	});

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<div id="bitCases">
			{bitCases.map((bitCase, index) => (
				<BitCase
					// attributes={this.props.attributes}
					bitCase={bitCase}
					// bitID={this.props.bitID}
					index={index}
					// attributesData={this.state.attributesData}
					key={bitCase._id}
					message={message}
					elements={elements}
					element={element}
					// setActiveElement={this.props.setActiveElement}
					// history={this.props.history}
					// commentBitCaseID={this.props.commentBitCaseID}
					// save={this.props.save}
					// selectedElement={this.props.selectedElement}
					// messageDBID={this.props.messageDBID}
					// loadElements={this.props.loadElements}
				/>
			))}
		</div>
	);
};

const BitCase = ({ element, elements, bitCase, index, message }) => {
	const [isVisible, setIsVisible] = useState(false);
	const { enqueueSnackbar } = useSnackbar();

	let content = bitCase?.content;
	if (typeof content === "string") {
		content = JSON.parse(bitCase.content);
	}

	const newBitCase = () => {
		Meteor.call(`bitCases.insert`, {
			bitID: bitCase.bitID,
			chainedOperator: "and",
		});
	};

	const duplicateBitCase = async () => {
		await Meteor.callPromise(`bitCases.duplicate`, {
			bitCaseID: bitCase._id,
			bitID: bitCase.bitID,
		});
		enqueueSnackbar(
			"Bit case duplicated and added to the bottom of the cases.",
			{
				variant: "success",
				key: "bitCaseDuplicated",
			}
		);
	};

	const deleteBitCase = () => {
		if (window.confirm(`You sure you want to delete the bit case?`)) {
			Meteor.call(`bitCases.remove`, bitCase._id);
		}
	};

	return (
		<div className={"bitCase"}>
			<VisibilitySensor
				partialVisibility={true}
				onChange={(visible) => {
					if (!isVisible) {
						setIsVisible(visible);
					}
				}}
			>
				<>
					<Grid container className={"bit-case-header"}>
						<Grid item xs={3}>
							<p>Bit Case #{index + 1}</p>
						</Grid>
						<Grid item xs={9} className={"action-links"}>
							<button className="action-link" onClick={newBitCase}>
								New
							</button>
							<button className="action-link" onClick={duplicateBitCase}>
								Duplicate
							</button>
							<button className="action-link" onClick={deleteBitCase}>
								Delete
							</button>
						</Grid>
					</Grid>
					<SlateComp
						type="bitCase"
						value={content || [{ type: "paragraph", children: [{ text: "" }] }]}
						onBlur={(value) => {
							Meteor.call("bitCases.update", bitCase._id, { content: value });
						}}
						bitCase={bitCase}
						message={message}
						element={element}
						elements={elements}
					/>
					<div className="bit-case-content">
						<ToggleControlRHF
							element={element}
							elements={elements}
							name={"ifElse"}
							labelLeft="If"
							labelRight={"Else"}
							error={{}}
							checked={bitCase?.else || false}
							onChange={() => {
								Meteor.call("bitCases.setElse", bitCase._id, {
									else: !bitCase?.else,
								});
							}}
							className={"toggle"}
						/>
						{isVisible && !bitCase?.else && (
							<BitCaseRuleContainer bitCase={bitCase} message={message} />
						)}
					</div>
				</>
			</VisibilitySensor>
		</div>
	);
};

export default BitCases;
