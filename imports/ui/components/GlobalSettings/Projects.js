import React, { useState, useContext, createContext } from "react";
import NewProjectForm from "./NewProjectForm";
import ProjectsTable from "./ProjectsTable";
import Button from "../../lib/components/Button/Button";

import AppContext from "../App/AppContext";

const ProjectSettingsContext = createContext({});

const Projects = () => {
	const [newProjectFormExpanded, setNewProjectFormExpanded] = useState(false);

	const { user } = useContext(AppContext);

	return (
		<ProjectSettingsContext.Provider
			value={{ newProjectFormExpanded, setNewProjectFormExpanded }}
		>
			<section>
				<div className="heading-with-button flex">
					<div>
						<h2>Projects</h2>
					</div>
					{user?.accountID && (
						<div>
							<Button
								text={newProjectFormExpanded ? "Cancel" : "Add New Project"}
								icon={newProjectFormExpanded ? "times" : "plus"}
								onClick={() => {
									setNewProjectFormExpanded(!newProjectFormExpanded);
								}}
							/>
						</div>
					)}
				</div>
				<NewProjectForm
					newProjectFormExpanded={newProjectFormExpanded}
					setNewProjectFormExpanded={setNewProjectFormExpanded}
				/>
				<ProjectsTable />
			</section>
		</ProjectSettingsContext.Provider>
	);
};

export { ProjectSettingsContext };
export default Projects;
