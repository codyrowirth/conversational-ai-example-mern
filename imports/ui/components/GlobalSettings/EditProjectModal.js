import React from "react";
import { Meteor } from "meteor/meteor";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import InputRHF from "../../lib/components/Form/InputRHF";

const EditProjectModal = ({ project, hide }) => {
	return (
		<Modal
			key={project && project._id}
			className="modal limit-modal end-buttons"
			show={!!project}
			hide={hide}
		>
			<h3>Edit Project</h3>
			<Form hide={hide} project={project} />
		</Modal>
	);
};

const Form = ({ hide, project }) => {
	const schema = yup.object().shape({
		projectName: yup.string().required("Please enter an account name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		Meteor.call("projects.update", project._id, { name: data.projectName });
		hide();
	};

	const onCancel = () => {
		hide();
	};

	return (
		<form>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="projectName"
						label="Project Name"
						defaultValue={project && project?.name}
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={handleSubmit(onCancel)} />
			</div>
		</form>
	);
};

export default EditProjectModal;
