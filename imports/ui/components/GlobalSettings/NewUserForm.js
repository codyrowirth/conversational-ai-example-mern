import React, { useContext } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Collapse } from "react-collapse";

import { UserSettingsContext } from "./Users";

import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import GreyBox from "../../lib/components/GreyBox/GreyBox";

const NewUserForm = () => {
	const { newUserFormExpanded, setNewUserFormExpanded } =
		useContext(UserSettingsContext);

	return (
		<Collapse isOpened={newUserFormExpanded}>
			<GreyBox className="end-buttons">
				<h3>Add New User</h3>
				<Form setNewUserFormExpanded={setNewUserFormExpanded} />
			</GreyBox>
		</Collapse>
	);
};

const Form = ({ setNewUserFormExpanded }) => {
	const schema = yup.object().shape({
		firstName: yup.string().required("Please enter a first name."),
		lastName: yup.string().required("Please enter a last name."),
		email: yup
			.string()
			.email("Please enter a valid email address.")
			.required("Please enter an email address."),
		userType: yup.object().required("Please select a user type."),
		defaultRole: yup.object().required("Please select a default role."),
	});

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
		reset,
		setFocus,
		setValue,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const resetForm = () => {
		reset();
		setValue("userType", "");
		setValue("defaultRole", "");
	};

	const onSubmit = (data) => {
		Meteor.call("user.create", {
			email: data.email,
			profile: { firstName: data.firstName, lastName: data.lastName },
			userType: data.userType.value,
			defaultRole: data.defaultRole.value,
		});
		setNewUserFormExpanded(false);
		resetForm();
	};

	const onContinue = (data) => {
		Meteor.call("user.create", {
			email: data.email,
			profile: { firstName: data.firstName, lastName: data.lastName },
			userType: data.userType.value,
			defaultRole: data.defaultRole.value,
		});
		resetForm();
		setFocus("firstName");
	};

	return (
		<form>
			<div className="grid-row">
				<div className="grid-col-half">
					<InputRHF
						type="text"
						name="firstName"
						label="First Name"
						register={register}
						error={errors}
					/>
				</div>
				<div className="grid-col-half">
					<InputRHF
						type="text"
						name="lastName"
						label="Last Name"
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="email"
						label="Email Address"
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="grid-row">
				<div className="grid-col-half">
					<Controller
						name="userType"
						control={control}
						render={({ field }) => {
							return (
								<SelectRHF
									name={field.name}
									label="User Type"
									options={["Standard", "Administrator", "Overlord"].map(
										(option) => ({
											value: option.toLowerCase(),
											label: option,
										})
									)}
									{...field}
									ref={null}
									error={errors}
								/>
							);
						}}
					/>
				</div>
				<div className="grid-col-half">
					<Controller
						name="defaultRole"
						control={control}
						render={({ field }) => (
							<SelectRHF
								name={field.name}
								label="Default Role"
								options={[
									"Reviewer",
									"Writer",
									"Architect",
									"Developer",
									"Account Manager",
								].map((option) => ({
									value: option.toLowerCase(),
									label: option,
								}))}
								{...field}
								ref={null}
								error={errors}
							/>
						)}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button
					text="Save and add another"
					onClick={handleSubmit(onContinue)}
				/>
			</div>
		</form>
	);
};

export default NewUserForm;
