import React, { useContext } from "react";
import AppContext from "../App/AppContext";
import Accounts from "./Accounts";
import Projects from "./Projects";
import Users from "./Users";

const GlobalSettings = () => {
	const { user } = useContext(AppContext);

	return (
		<section className="global-settings">
			<h1>Global Settings</h1>
			{user?.userType === "overlord" && <Accounts />}
			<Projects />
			<Users />
		</section>
	);
};

export default GlobalSettings;
