import React from "react";
import { Meteor } from "meteor/meteor";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import InputRHF from "../../lib/components/Form/InputRHF";

const EditAccountModal = ({ account, hide }) => {
	return (
		<Modal
			key={account && account._id}
			className="modal limit-modal end-buttons"
			show={!!account}
			hide={hide}
		>
			<h3>Edit Account</h3>
			<Form hide={hide} account={account} />
		</Modal>
	);
};

const Form = ({ hide, account }) => {
	const schema = yup.object().shape({
		accountName: yup.string().required("Please enter an account name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		Meteor.call("accounts.update", account._id, { name: data.accountName });
		hide();
	};

	const onCancel = () => {
		hide();
	};

	return (
		<form>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="accountName"
						label="Account Name"
						defaultValue={account && account?.name}
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={handleSubmit(onCancel)} />
			</div>
		</form>
	);
};

export default EditAccountModal;
