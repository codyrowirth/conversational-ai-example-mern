import React, { useContext } from "react";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";
import { titleCase } from "title-case";
import useWinWidth from "../../lib/utilities/useWinWidth";
import AppContext from "../App/AppContext";

import "../../lib/styles/tables.scss";
import "./UsersTable.scss";

const UsersTable = () => {
	const screenWidth = useWinWidth();

	const { user } = useContext(AppContext);
	const { users, isLoading } = useTracker(() => {
		if (!Meteor.user()) {
			return { users: [] };
		}
		const handler = Meteor.subscribe(
			"Meteor.users.team",
			Meteor.user()?.accountID
		);
		if (!handler.ready()) {
			return { users: [], isLoading: true };
		}

		if (user?.userType === "overlord") {
			const users = Meteor.users.find().fetch();
			return { users };
		} else {
			const users = Meteor.users
				.find({ userType: { $ne: "overlord" } })
				.fetch();
			return { users };
		}
	});

	return (
		<>
			<table className="users">
				{screenWidth >= 768 && (
					<colgroup>
						<col style={isLoading ? { width: "100%" } : { width: "35%" }} />
						<col style={isLoading ? { width: "0" } : { width: "20%" }} />
						<col style={isLoading ? { width: "0" } : { width: "20%" }} />
						<col style={isLoading ? { width: "0" } : { width: "25%" }} />
					</colgroup>
				)}
				<thead>
					<tr>
						<th scope="col">{isLoading ? "" : "User"}</th>
						<th scope="col">{isLoading ? "" : "User Type"}</th>
						<th scope="col">{isLoading ? "" : "Default Role"}</th>
						<th scope="col">{isLoading ? "" : "Actions"}</th>
					</tr>
				</thead>
				<tbody>
					{isLoading ? (
						<tr className="loader">
							<td>
								<PropagateLoader color="#2f3d47" />
							</td>
						</tr>
					) : (
						users.map((user, index) => {
							const email = user?.emails?.[0]?.address;
							return (
								<tr key={user._id}>
									<td data-label="User">
										{user?.profile?.firstName} {user?.profile?.lastName}
										{email ? (
											<a
												className="email-address"
												href={`mailto:${email}`}
												target="_blank"
											>
												{email}
											</a>
										) : null}
									</td>
									<td data-label="User Type">{titleCase(user.userType)}</td>
									<td data-label="Default Role">
										{titleCase(user.defaultRole)}
									</td>
									<td data-label="Actions">
										<div className="action-links">
											<button
												className="action-link"
												onClick={() => {
													alert("TODO");
												}}
											>
												Edit
											</button>
											<button
												className="action-link"
												onClick={() => {
													alert("TODO");
												}}
											>
												Delete
											</button>
										</div>
									</td>
								</tr>
							);
						})
					)}
					{!isLoading && users.length === 0 ? (
						<tr>
							<td>No users exist yet. Create one using the button above.</td>
						</tr>
					) : null}
				</tbody>
			</table>
			{user.userType === "overlord" ? (
				<p className="users-table-note">
					Overlord accounts are not viewable by other user types.
				</p>
			) : null}
		</>
	);
};

export default UsersTable;
