import React, { useState, createContext } from "react";
import NewAccountForm from "./NewAccountForm";
import AccountsTable from "./AccountsTable";
import Button from "../../lib/components/Button/Button";

const AccountSettingsContext = createContext({});

const Accounts = () => {
	const [newAccountFormExpanded, setNewAccountFormExpanded] = useState(false);

	return (
		<AccountSettingsContext.Provider
			value={{ newAccountFormExpanded, setNewAccountFormExpanded }}
		>
			<section>
				<div className="heading-with-button flex">
					<div>
						<h2>Accounts</h2>
					</div>
					<div>
						<Button
							text={newAccountFormExpanded ? "Cancel" : "Add New Account"}
							icon={newAccountFormExpanded ? "times" : "plus"}
							onClick={() => {
								setNewAccountFormExpanded(!newAccountFormExpanded);
							}}
						/>
					</div>
				</div>
				<NewAccountForm
					newAccountFormExpanded={newAccountFormExpanded}
					setNewAccountFormExpanded={setNewAccountFormExpanded}
				/>
				<AccountsTable />
			</section>
		</AccountSettingsContext.Provider>
	);
};

export { AccountSettingsContext };
export default Accounts;
