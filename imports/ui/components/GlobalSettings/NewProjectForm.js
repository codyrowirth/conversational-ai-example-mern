import React, { useContext } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Collapse } from "react-collapse";

import { ProjectSettingsContext } from "./Projects";

import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import GreyBox from "../../lib/components/GreyBox/GreyBox";

const NewProjectForm = () => {
	const { newProjectFormExpanded, setNewProjectFormExpanded } = useContext(
		ProjectSettingsContext
	);

	return (
		<Collapse isOpened={newProjectFormExpanded}>
			<GreyBox className="end-buttons">
				<h3>Add New Project</h3>
				<Form setNewProjectFormExpanded={setNewProjectFormExpanded} />
			</GreyBox>
		</Collapse>
	);
};

const Form = ({ setNewProjectFormExpanded }) => {
	const schema = yup.object().shape({
		projectName: yup.string().required("Please enter a project name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
		reset,
		setFocus,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		Meteor.call("projects.insert", data.projectName);
		setNewProjectFormExpanded(false);
		reset();
	};

	const onContinue = (data) => {
		Meteor.call("projects.insert", data.projectName);
		reset();
		setFocus("projectName");
	};

	return (
		<form>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="projectName"
						label="Project Name"
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button
					text="Save and add another"
					onClick={handleSubmit(onContinue)}
				/>
			</div>
		</form>
	);
};

export default NewProjectForm;
