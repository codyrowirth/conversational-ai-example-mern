import React, { useState, useContext } from "react";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import { useForm } from "react-hook-form";
import { PropagateLoader } from "react-spinners";

import ProjectsDB from "../../../api/collections/Projects";
import useWinWidth from "../../lib/utilities/useWinWidth";

import AppContext from "../App/AppContext";

import EditProjectModal from "./EditProjectModal";
import DeleteProjectModal from "./DeleteProjectModal";
import InputRHF from "../../lib/components/Form/InputRHF";
import Radio from "../../lib/components/Form/Radio";

import "../../lib/styles/tables.scss";

const ProjectsTable = () => {
	const [editProjectModal, setEditProjectModal] = useState();
	const [deleteProjectModal, setDeleteProjectModal] = useState();

	const { user } = useContext(AppContext);

	const screenWidth = useWinWidth();

	const {
		register,
		formState: { errors },
	} = useForm();

	const { projects, isLoading } = useTracker(() => {
		const noDataAvailable = { tasks: [] };
		if (!Meteor.user()) {
			return { projects: [] };
		}
		const handler = Meteor.subscribe("projects", user?.accountID);
		if (!handler.ready()) {
			return { ...noDataAvailable, isLoading: true };
		}

		const projects = ProjectsDB.find().fetch();
		return { projects };
	});

	return (
		<>
			<table className="projects">
				{screenWidth >= 768 && (
					<colgroup>
						<col style={isLoading ? { width: "100%" } : { width: "44%" }} />
						<col style={isLoading ? { width: "0" } : { width: "16%" }} />
						<col style={isLoading ? { width: "0" } : { width: "15%" }} />
						<col style={isLoading ? { width: "0" } : { width: "25%" }} />
					</colgroup>
				)}
				<thead>
					<tr>
						<th scope="col">{isLoading ? "" : "Project"}</th>
						<th scope="col">{isLoading ? "" : "Credits"}</th>
						<th scope="col" style={{ textAlign: "center" }}>
							{isLoading ? "" : "Active"}
						</th>
						<th scope="col">{isLoading ? "" : "Actions"}</th>
					</tr>
				</thead>
				<tbody>
					{isLoading ? (
						<tr className="loader">
							<td>
								<PropagateLoader color="#2f3d47" />
							</td>
						</tr>
					) : (
						projects.map((project, index) => {
							return (
								<tr key={project._id}>
									<td data-label="Project">{project.name}</td>
									<td data-label="Credits">
										{/* TO DO: Input functionality */}
										<InputRHF
											type="text"
											name="credits"
											register={register}
											error={errors}
										/>
									</td>
									<td
										data-label="Active"
										style={
											screenWidth >= 768
												? { textAlign: "center" }
												: { textAlign: "right" }
										}
									>
										<Radio
											name="active"
											checked={project._id === user.projectID}
											onClick={() => {
												Meteor.call("user.setActiveProject", project._id);
											}}
										/>
									</td>
									<td data-label="Actions">
										<div className="action-links">
											<button
												className="action-link"
												onClick={() => {
													setEditProjectModal(project);
												}}
											>
												Edit
											</button>
											<button
												className="action-link"
												onClick={() => {
													setDeleteProjectModal(project);
												}}
											>
												Delete
											</button>
										</div>
									</td>
								</tr>
							);
						})
					)}
					{!isLoading && projects.length === 0 ? (
						<tr>
							{!user?.accountID ? (
								<td>
									Please activate an account with overlord permissions to work
									with projects.
								</td>
							) : (
								<td>
									No projects exist yet. Create one using the button above.
								</td>
							)}
						</tr>
					) : null}
				</tbody>
			</table>
			<EditProjectModal
				project={editProjectModal}
				hide={() => {
					setEditProjectModal(null);
				}}
			/>
			<DeleteProjectModal
				project={deleteProjectModal}
				hide={() => {
					setDeleteProjectModal(null);
				}}
			/>
		</>
	);
};

export default ProjectsTable;
