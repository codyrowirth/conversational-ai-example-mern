import React from "react";
import { Meteor } from "meteor/meteor";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";

const DeleteAccountModal = ({ account, hide }) => {
	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!account}
			hide={hide}
		>
			<h3>Do you want to delete this account?</h3>
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{account && account?.name}</strong> account. Access for all
				users in this account will be revoked.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						Meteor.call("accounts.remove", account._id);
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteAccountModal;
