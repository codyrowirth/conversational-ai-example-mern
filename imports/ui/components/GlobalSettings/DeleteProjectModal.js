import React from "react";
import { Meteor } from "meteor/meteor";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";

const DeleteProjectModal = ({ project, hide }) => {
	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!project}
			hide={hide}
		>
			<h3>Do you want to delete this project?</h3>
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{project && project?.name}</strong> project.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						Meteor.call("projects.remove", project._id);
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteProjectModal;
