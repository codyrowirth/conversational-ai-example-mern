import React, { createContext, useContext, useState } from "react";
import NewUserForm from "./NewUserForm";
import UsersTable from "./UsersTable";
import Button from "../../lib/components/Button/Button";
import AppContext from "../App/AppContext";

const UserSettingsContext = createContext({});

const Users = () => {
	const [newUserFormExpanded, setNewUserFormExpanded] = useState(false);
	const { user } = useContext(AppContext);

	return (
		<UserSettingsContext.Provider
			value={{ newUserFormExpanded, setNewUserFormExpanded }}
		>
			<section>
				<div className="heading-with-button flex">
					<div>
						<h2>Users</h2>
					</div>
					{user?.accountID && (
						<div>
							<Button
								text={newUserFormExpanded ? "Cancel" : "Add New User"}
								icon={newUserFormExpanded ? "times" : "plus"}
								onClick={() => {
									setNewUserFormExpanded(!newUserFormExpanded);
								}}
							/>
						</div>
					)}
				</div>
				{!user?.accountID ? (
					<p>Select an active account above to view users.</p>
				) : (
					<>
						<NewUserForm
							newUserFormExpanded={newUserFormExpanded}
							setNewUserFormExpanded={setNewUserFormExpanded}
						/>
						<UsersTable />
					</>
				)}
			</section>
		</UserSettingsContext.Provider>
	);
};

export { UserSettingsContext };
export default Users;
