import React, { useContext } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Collapse } from "react-collapse";

import { AccountSettingsContext } from "./Accounts";

import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import GreyBox from "../../lib/components/GreyBox/GreyBox";

const NewAccountForm = () => {
	const { newAccountFormExpanded, setNewAccountFormExpanded } = useContext(
		AccountSettingsContext
	);

	return (
		<Collapse isOpened={newAccountFormExpanded}>
			<GreyBox className="end-buttons">
				<h3>Add New Account</h3>
				<Form setNewAccountFormExpanded={setNewAccountFormExpanded} />
			</GreyBox>
		</Collapse>
	);
};

const Form = ({ setNewAccountFormExpanded }) => {
	const schema = yup.object().shape({
		accountName: yup.string().required("Please enter an account name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
		reset,
		setFocus,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		Meteor.call("accounts.insert", data.accountName);
		setNewAccountFormExpanded(false);
		reset();
	};

	const onContinue = (data) => {
		Meteor.call("accounts.insert", data.accountName);
		reset();
		setFocus("accountName");
	};

	return (
		<form>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="accountName"
						label="Account Name"
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button
					text="Save and add another"
					onClick={handleSubmit(onContinue)}
				/>
			</div>
		</form>
	);
};

export default NewAccountForm;
