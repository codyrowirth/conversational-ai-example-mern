import React, { useState, useEffect, useContext } from "react";
import { Meteor } from "meteor/meteor";
import { useTracker } from "meteor/react-meteor-data";
import { useHistory } from "react-router-dom";
import { PropagateLoader } from "react-spinners";
import AccountsDB from "../../../api/collections/Accounts";
import AppContext from "../App/AppContext";

import useWinWidth from "../../lib/utilities/useWinWidth";

import Radio from "../../lib/components/Form/Radio";
import EditAccountModal from "./EditAccountModal";
import DeleteAccountModal from "./DeleteAccountModal";

import "../../lib/styles/tables.scss";

const AccountsTable = () => {
	const [deleteAccountModal, setDeleteAccountModal] = useState();
	const [editAccountModal, setEditAccountModal] = useState();
	const { user } = useContext(AppContext);

	const history = useHistory();

	const screenWidth = useWinWidth();

	const { accounts, isLoading } = useTracker(() => {
		const noDataAvailable = { tasks: [] };
		if (!Meteor.user()) {
			return { accounts: [] };
		}
		const handler = Meteor.subscribe("accounts");
		if (!handler.ready()) {
			return { ...noDataAvailable, isLoading: true };
		}

		const accounts = AccountsDB.find().fetch();
		return { accounts };
	});

	return (
		<>
			<table className="accounts">
				{screenWidth >= 768 && (
					<colgroup>
						<col style={isLoading ? { width: "100%" } : { width: "60%" }} />
						<col style={isLoading ? { width: "0" } : { width: "15%" }} />
						<col style={isLoading ? { width: "0" } : { width: "25%" }} />
					</colgroup>
				)}
				<thead>
					<tr>
						<th scope="col">{isLoading ? "" : "Account"}</th>
						<th scope="col" style={{ textAlign: "center" }}>
							{isLoading ? "" : "Active"}
						</th>
						<th scope="col">{isLoading ? "" : "Actions"}</th>
					</tr>
				</thead>
				<tbody>
					{isLoading ? (
						<tr className="loader">
							<td>
								<PropagateLoader color="#2f3d47" />
							</td>
						</tr>
					) : (
						accounts.map((account, index) => {
							return (
								<tr key={account._id}>
									<td data-label="Account">{account.name}</td>
									<td
										data-label="Active"
										style={
											screenWidth >= 768
												? { textAlign: "center" }
												: { textAlign: "right" }
										}
									>
										<Radio
											name="active"
											checked={user.accountID === account._id}
											onClick={() => {
												Meteor.call("user.setActiveAccount", account._id);
											}}
										/>
									</td>
									<td data-label="Actions">
										<div className="action-links">
											<button
												className="action-link"
												onClick={() => {
													setEditAccountModal(account);
												}}
											>
												Edit
											</button>
											<button
												className="action-link"
												onClick={() => {
													setDeleteAccountModal(account);
												}}
											>
												Delete
											</button>
										</div>
									</td>
								</tr>
							);
						})
					)}
				</tbody>
			</table>
			<EditAccountModal
				account={editAccountModal}
				hide={() => {
					setEditAccountModal(null);
				}}
			/>
			<DeleteAccountModal
				account={deleteAccountModal}
				hide={() => {
					setDeleteAccountModal(null);
				}}
			/>
		</>
	);
};

export default AccountsTable;
