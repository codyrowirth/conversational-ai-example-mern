import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const StatusGroup = ({ statusGroup, tasksDB }) => {
	const [groupExpanded, setGroupExpanded] = useState(true);

	return (
		<div
			className={`status-group ${
				groupExpanded === true ? "expanded" : "collapsed"
			}`}
		>
			<div className="group-inner">
				<button
					className="group-title"
					onClick={() => {
						setGroupExpanded(!groupExpanded);
					}}
				>
					<div className="group-title-inner">
						<span className="text">{statusGroup} </span>
						<span className="icon">
							<FontAwesomeIcon
								icon={groupExpanded === true ? "chevron-left" : "chevron-right"}
							/>
						</span>
					</div>
				</button>
				<div className="group-tasks">
					{tasksDB
						.filter((task) => task.group === statusGroup.toLowerCase())
						.map((task, index) => {
							return (
								<button
									key={index}
									className="task"
									onClick={() => {
										alert("TO DO");
									}}
								>
									{task.name}
								</button>
							);
						})}
				</div>
			</div>
		</div>
	);
};

export default StatusGroup;
