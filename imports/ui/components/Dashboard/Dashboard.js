import React from "react";

import GreyBox from "../../lib/components/GreyBox/GreyBox";
import NotifBadge from "../../lib/components/NotifBadge/NotifBadge";
import StatusGroup from "./StatusGroup";

import "./Dashboard.scss";

// TO DO: Use real data

const comments = 3;
const unmatched = 5;

const statusGroups = [
	"Backlog",
	"To Do",
	"Doing",
	"Done",
	"Blocked",
	"Testing",
	"Approved",
];

// TO DO: Use real data

const tasksDB = [
	{
		name: "Message Name Lorem Ipsum",
		group: "backlog",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "backlog",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "backlog",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "to do",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "doing",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "doing",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "done",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "done",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "blocked",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "testing",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "testing",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "approved",
	},
	{
		name: "Message Name Lorem Ipsum",
		group: "approved",
	},
];

const Dashboard = () => {
	return (
		<section className="dashboard">
			{/* TO DO: Use real data */}
			<h1>Welcome Back, Lorem!</h1>
			<GreyBox className="notif-box">
				{((comments && comments > 0) || (unmatched && unmatched > 0)) && (
					<div className="notifs">
						{comments && comments > 0 && (
							<div className="notif">
								<NotifBadge count={comments} small={true} />
								<p>
									<strong>New </strong>Comments (
									<button
										className="action-link"
										onClick={(event) => {
											event.preventDefault();
											alert("TO DO");
											// setCommentsModal(true);
										}}
									>
										view
									</button>
									)
								</p>
							</div>
						)}
						{unmatched && unmatched > 0 && (
							<div className="notif">
								<NotifBadge count={unmatched} small={true} alt={true} />
								<p>
									<strong>New </strong>Unmatched Responses (
									<button
										className="action-link"
										onClick={(event) => {
											event.preventDefault();
											alert("TO DO");
											// setUnmatchedModal(true);
										}}
									>
										view
									</button>
									)
								</p>
							</div>
						)}
					</div>
				)}
			</GreyBox>
			<div className="kanban">
				<div className="kanban-inner">
					{statusGroups.map((statusGroup) => {
						return (
							<StatusGroup
								key={statusGroup}
								statusGroup={statusGroup}
								tasksDB={tasksDB}
							/>
						);
					})}
				</div>
			</div>
		</section>
	);
};

export default Dashboard;
