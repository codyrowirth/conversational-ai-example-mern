import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
	return (
		<section className="not-found">
			<h1>Error 404</h1>
			<p>It looks like you've reached a URL that doesn't exist.</p>
			<ul>
				<li><Link to="/">Home</Link></li>
				<li><Link to="/tutorials">Tutorials</Link></li>
				<li><Link to="/documentation">Documentation</Link></li>
			</ul>
		</section>
	);
};

export default NotFound;
