import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const ContentLength = () => {
	return (
		<section className="content-length">
			<h1>Content Length</h1>
			<ComingSoon />
		</section>
	);
};

export default ContentLength;
