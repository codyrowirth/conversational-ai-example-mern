import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Documentation = () => {
	return (
		<section className="documentation">
			<h1>Documentation</h1>
			<ComingSoon />
		</section>
	);
};

export default Documentation;
