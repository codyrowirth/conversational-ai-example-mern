import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Review = () => {
	return (
		<section className="review">
			<h1>Review</h1>
			<ComingSoon />
		</section>
	);
};

export default Review;
