import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";

import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import Datepicker from "../../lib/components/Form/Datepicker";

const SortFilterModal = ({ subset = "", show, hide }) => {
	const [startDate, setStartDate] = useState(new Date());
	const [endDate, setEndDate] = useState(new Date());

	const {
		control,
		formState: { errors },
		handleSubmit,
	} = useForm();

	const onSubmit = (data) => {
		console.log(data);
		alert("TO DO");
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal
			className="modal end-buttons conversations"
			show={!!show}
			hide={hide}
		>
			<h3>Sort &amp; Filter</h3>
			<form>
				<div className="grid-row">
					<div className="grid-col-half">
						<Controller
							name="sort"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Sort"
										options={["Author", "Date", "Content Type", "Location"].map(
											(option) => ({
												value: option.toLowerCase(),
												label: option,
											})
										)}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
					<div className="grid-col-half">
						<Controller
							name="order"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Order"
										options={["Ascending", "Descending"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-half">
						<Controller
							name="author"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Author"
										options={["TO DO"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
					<div className="grid-col-quarter">
						<Controller
							control={control}
							name="startDate"
							render={({ field }) => {
								return (
									<Datepicker
										selectedDate={startDate}
										setSelectedDate={setStartDate}
										name={field.name}
										label="Start Date"
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
					<div className="grid-col-quarter">
						<Controller
							control={control}
							name="endDate"
							render={({ field }) => {
								return (
									<Datepicker
										selectedDate={endDate}
										setSelectedDate={setEndDate}
										name={field.name}
										label="End Date"
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-quarter">
						<Controller
							name="contentType"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Content Type"
										options={["TO DO"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
					<div className="grid-col-three-quarters">
						<Controller
							name="location"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Location"
										options={["TO DO"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="button-wrap">
					<Button text="Apply" onClick={handleSubmit(onSubmit)} />
					<Button text="Cancel" onClick={onCancel} />
				</div>
			</form>
		</Modal>
	);
};

export default SortFilterModal;
