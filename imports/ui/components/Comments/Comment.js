import React from "react";
import { Link, useParams } from "react-router-dom";

import commentsDB from "./commentsDB";

import GreyBox from "../../lib/components/GreyBox/GreyBox";
import CommentThread from "./CommentThread";
import Button from "../../lib/components/Button/Button";

import "./Comment.scss";

const Comment = () => {
	const urlInfo = useParams();
	const commentID = urlInfo.commentID;
	const comment = commentsDB.find((comment) => comment._id === commentID);
	const parentCount = comment.location.length;

	return (
		<section className="comment-thread">
			<GreyBox className="breadcrumbs">
				<div className="breadcrumbs-inner">
					{comment.location.map((parent, index) => {
						return (
							<span key={index}>
								<Link
									to={`${parent.path}`}
									onClick={(event) => {
										event.preventDefault();
										alert("TO DO");
									}}
								>
									{parent.name}
								</Link>
								{index < parentCount - 1 ? " > " : ""}
							</span>
						);
					})}
				</div>
			</GreyBox>
			<h1>Comments</h1>
			<div className="content-type">
				<div className="content-type-inner">{comment.contentType}</div>
			</div>
			<h2>{comment.location[parentCount - 1].name}</h2>
			<div className="action-links">
				<div className="label">Back</div>
				<Link to="/comments" className="action-link">
					Inbox
				</Link>
				<Link to="/comments/archive" className="action-link">
					Archive
				</Link>
				<Link to="/comments/posted" className="action-link">
					Posted
				</Link>
			</div>
			<CommentThread thread={comment.thread} />
			<div className="button-wrap">
				<Button
					text="Post"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				/>
				<Button
					text="Cancel"
					onClick={(event) => {
						event.preventDefault();
						alert("TO DO");
					}}
				/>
			</div>
		</section>
	);
};

export default Comment;
