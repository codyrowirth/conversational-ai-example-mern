import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";

import Modal from "../../lib/components/Modal/Modal";
import Button from "../../lib/components/Button/Button";

import "./TranscriptModal.scss";

const TranscriptModal = ({ show, hide }) => {
	const { id } = useParams();
	const history = useHistory();

	// TO DO: Use real concierge name from project settings (not developed yet)
	const conciergeName = "Marti";
	let messageFrom = conciergeName;

	// TO DO: Use real data

	const loading = false;

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<Modal
			className="modal end-buttons transcript-modal"
			show={show}
			hide={hide}
		>
			<h3>Transcript</h3>
			{/* TO DO: Use actual data */}
			{Object.entries(show).map((transcript) => {
				if (transcript[1].from === "concierge") {
					messageFrom = conciergeName;
				} else if (transcript[1].from === "user") {
					messageFrom = "User";
				}

				return (
					<div key={transcript[0]}>
						<p className="from">{messageFrom}:</p>
						<p className="message">{transcript[1].message}</p>
					</div>
				);
			})}
			<div className="button-wrap">
				<Button
					text="Close"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default TranscriptModal;
