import React, { useState } from "react";

import Button from "../../lib/components/Button/Button";
import DeleteCommentModal from "./DeleteCommentModal";
import TranscriptModal from "./TranscriptModal";

const CommentThreadItem = ({ thread, item, index }) => {
	const [replyToComment, setReplyToComment] = useState(false);
	const [editComment, setEditComment] = useState(false);
	const [deleteCommentModal, setDeleteCommentModal] = useState(null);
	const [transcriptModal, setTranscriptModal] = useState(false);

	const itemMarkup = (item, reply = false) => (
		<div key={item._id} className={`comment-thread-item ${reply && "reply"}`}>
			<div className="meta">
				<div className="meta-inner">
					<div
						className="profile-photo"
						style={{ backgroundImage: `url(${item.photo})` }}
					></div>
					<div className="info">
						<p className="comment-author">{item.author}</p>
						<p className="comment-date-time">
							{item.date} at {item.time}
						</p>
					</div>
					<div className="action-links">
						{!item.replyTo && (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setReplyToComment(item);
								}}
							>
								Reply
							</button>
						)}
						{item.transcript?.length > 0 && (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setTranscriptModal(item.transcript);
								}}
							>
								View Transcript
							</button>
						)}
						{item.owner === true && (
							<>
								<button
									className="action-link"
									onClick={(event) => {
										event.preventDefault();
										setEditComment(item);
									}}
								>
									Edit
								</button>
								<button
									className="action-link"
									onClick={(event) => {
										event.preventDefault();
										setDeleteCommentModal(item);
									}}
								>
									Delete
								</button>
							</>
						)}
						{index === 0 && !item.replyTo && (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									alert("TO DO");
								}}
							>
								Archive
							</button>
						)}
					</div>
				</div>
			</div>
			<div className="content">
				{item.quote && (
					<blockquote>
						<span className="quote-heading">Message:</span>
						{item.quote}
					</blockquote>
				)}
				{editComment?._id === item._id ? (
					<div className="edit">
						<p>Rich text editor goes here</p>
						<div className="button-wrap">
							<Button
								text="Update"
								onClick={(event) => {
									event.preventDefault();
									alert("TO DO");
								}}
							/>
							<Button
								text="Cancel"
								onClick={(event) => {
									event.preventDefault();
									setEditComment(false);
								}}
							/>
						</div>
					</div>
				) : (
					item.content
				)}
			</div>
		</div>
	);

	return (
		<>
			{itemMarkup(item)}
			{!item.replyTo ? (
				<div className="replies">
					{replyToComment !== false ? (
						<div className="compose-reply">
							<p>Rich text editor goes here</p>
							<div className="button-wrap">
								<Button
									text="Reply"
									onClick={(event) => {
										event.preventDefault();
										alert("TO DO");
									}}
								/>
								<Button
									text="Cancel"
									onClick={(event) => {
										event.preventDefault();
										setReplyToComment(false);
									}}
								/>
							</div>
						</div>
					) : null}
					{item.replies.length
						? thread
								.filter((reply) => reply.replyTo === item._id)
								.map((reply) => itemMarkup(reply, true))
						: null}
				</div>
			) : null}

			<DeleteCommentModal
				show={deleteCommentModal}
				hide={() => {
					setDeleteCommentModal(null);
				}}
			/>
			<TranscriptModal
				show={transcriptModal}
				hide={() => {
					setTranscriptModal(false);
				}}
			/>
		</>
	);
};

export default CommentThreadItem;
