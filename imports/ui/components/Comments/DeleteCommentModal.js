import React from "react";
// import { useTracker } from "meteor/react-meteor-data";
import { useHistory, useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";
// import Attributes from "../../../api/collections/Attributes";
import Modal from "../../lib/components/Modal/Modal";
import Button from "../../lib/components/Button/Button";

const DeleteCommentModal = ({ show, hide }) => {
	// TO DO: Use real data

	const loading = false;

	// const { id } = useParams();
	// const history = useHistory();

	// const { attribute, loading } = useTracker(() => {
	// 	const handler = Meteor.subscribe("attribute", id);

	// 	if (!handler.ready()) {
	// 		return { loading: true };
	// 	}

	// 	return {
	// 		attribute: Attributes.findOne({ _id: id }),
	// 		loading: false,
	// 	};
	// });

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<Modal className="modal limit-modal end-buttons" show={show} hide={hide}>
			<h3>Do you want to delete this comment?</h3>
			<p>
				Please confirm that you’d like to delete this comment. You won’t be able
				to undo this action.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={(event) => {
						// history.push("/attributes-manager");
						// Meteor.call("attributes.remove", attribute._id);
						// hide();
						event.preventDefault();
						alert("TO DO");
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteCommentModal;
