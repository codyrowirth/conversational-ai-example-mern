import React from "react";
import CommentThreadItem from "./CommentThreadItem";

const CommentThread = ({ thread }) => {
	return (
		<>
			<div className="comment-thread-list">
				{thread
					.filter((item) => !item.replyTo)
					.map((item, index) => {
						return (
							<CommentThreadItem
								key={item._id}
								thread={thread}
								item={item}
								index={index}
							/>
						);
					})}
			</div>
			{/* TO DO: Rich text editor */}
			<p>Rich text editor goes here</p>
		</>
	);
};

export default CommentThread;
