import React, { useState } from "react";
import { Link } from "react-router-dom";
import CommentsSearch from "./CommentsSearch";
import CommentsTable from "./CommentsTable";

// TO DO: Use real data

const comments = [
	{
		_id: "commentOne",
		author: "Author One",
		excerpt:
			"Comment one lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "2:22 PM",
		contentType: "canvas",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
		],
		read: false,
	},
	{
		_id: "commentTwo",
		author: "Author Two",
		excerpt:
			"Comment two lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "10:07 AM",
		contentType: "brancher",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Brancher Name Lorem Ipsum",
				path: "#",
			},
		],
		read: false,
	},
	{
		_id: "commentThree",
		author: "Author Three",
		excerpt:
			"Comment three lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "Aug 23",
		contentType: "bit case",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Element Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Bit Case #1",
				path: "#",
			},
		],
		read: false,
	},
	{
		_id: "commentFour",
		author: "Author Four",
		excerpt:
			"Comment four lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "Jan 07",
		contentType: "element",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Element Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
	{
		_id: "commentFive",
		author: "Author Five",
		excerpt:
			"Comment five lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "12/14/2020",
		contentType: "message",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
	},
];

const CommentsArchive = () => {
	return (
		<section className="comments">
			<h1>Comments</h1>
			{/* TO DO: Make search work */}
			<CommentsSearch />
			<h2 className="heading-with-action-links">
				Archive
				<div className="action-links">
					<div className="label">View</div>
					<Link to="/comments" className="action-link">
						Inbox
					</Link>
					<Link to="/comments/posted" className="action-link">
						Posted
					</Link>
				</div>
			</h2>
			<CommentsTable view="archive" type="full" comments={comments} />
		</section>
	);
};

export default CommentsArchive;
