import React, { useState } from "react";
import SearchBar from "../../lib/components/SearchBar/SearchBar";

const CommentsSearch = () => {
  const [searchBarText, setSearchBarText] = useState();
  
	return (
		<SearchBar
			placeholder="Search comments (TO DO)"
			setSearchBarText={setSearchBarText}
		/>
	);
};

export default CommentsSearch;
