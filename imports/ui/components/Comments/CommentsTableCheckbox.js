import React, { useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import CheckboxControlRHF from "../../lib/components/Form/CheckboxControlRHF";

const CommentsTableCheckbox = ({ comment, checkAll }) => {
	const [checked, setChecked] = useState(false);

	const {
		control,
		formState: { errors },
	} = useForm();

	useEffect(() => {
		setChecked(checkAll);
	}, [checkAll]);

	return (
		<Controller
			name={comment.name}
			control={control}
			render={({ field }) => {
				return (
					<CheckboxControlRHF
						name={field.name}
						control={control}
						onChange={() => {
							setChecked(!checked);
						}}
						checked={checked}
						error={errors}
					/>
				);
			}}
		/>
	);
};

export default CommentsTableCheckbox;
