import React, { Fragment, useState, useContext } from "react";
// import { Meteor } from "meteor/meteor";
// import { useTracker } from "meteor/react-meteor-data";
import { Link, useHistory } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { PropagateLoader } from "react-spinners";

// import ProjectsDB from "../../../api/collections/Projects";
import useWinWidth from "../../lib/utilities/useWinWidth";

// import AppContext from "../App/AppContext";

import DeleteCommentsModal from "./DeleteCommentsModal";
import SortFilterModal from "./SortFilterModal";
import CommentModal from "../Canvas/modals/inner-modals/CommentModal";
import CheckboxRHF from "../../lib/components/Form/CheckboxRHF";
import CommentsTableCheckbox from "./CommentsTableCheckbox";
import Pagination from "../../lib/components/Pagination/Pagination";

import "../../lib/styles/tables.scss";
import "./CommentsTable.scss";
import { ContextMenuTrigger } from "react-contextmenu";

const CommentsTable = ({ type, view, comments }) => {
	const [deleteCommentsModal, setDeleteCommentsModal] = useState();
	const [sortFilterModal, setSortFilterModal] = useState();
	const [checkAll, setCheckAll] = useState(false);
	const [commentModal, setCommentModal] = useState(null);

	// const { user } = useContext(AppContext);

	const history = useHistory();

	const screenWidth = useWinWidth();

	const {
		register,
		control,
		formState: { errors },
	} = useForm();

	// TO DO: Use real data

	const isLoading = false;

	// const { projects, isLoading } = useTracker(() => {
	// 	const noDataAvailable = { tasks: [] };
	// 	if (!Meteor.user()) {
	// 		return { projects: [] };
	// 	}
	// 	const handler = Meteor.subscribe("projects", user?.accountID);
	// 	if (!handler.ready()) {
	// 		return { ...noDataAvailable, isLoading: true };
	// 	}

	// 	const projects = ProjectsDB.find().fetch();
	// 	return { projects };
	// });

	return (
		<section className="comments-table">
			<div className="table-action-links-wrap">
				<div className="table-action-links-row top">
					<div className="table-action-links left">
						{/* TO DO: Make this work; also, "Mark Unread" should change to "Mark Read" text and functionality when appropriate */}

						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
							}}
						>
							Mark Unread
						</button>
						{type !== "message" && (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									alert("TO DO");
								}}
							>
								{view === "archive" ? "Unarchive" : "Archive"}
							</button>
						)}
						{view === "posted" && (
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setDeleteCommentsModal(true);
								}}
							>
								Delete
							</button>
						)}
					</div>
					{type !== "message" && (
						<div className="table-action-links right">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setSortFilterModal(true);
								}}
							>
								Sort &amp; Filter
							</button>
						</div>
					)}
				</div>
			</div>
			<table className="table-toggle">
				{screenWidth >= 768 && (
					<colgroup>
						<col style={isLoading ? { width: "100%" } : { width: "72px" }} />
						{view !== "posted" && (
							<col style={isLoading ? { width: "0" } : { width: "30%" }} />
						)}

						<col
							style={
								isLoading
									? { width: "0" }
									: { width: view === "posted" ? "75%" : "45%" }
							}
						/>
						<col style={isLoading ? { width: "0" } : { width: "25%" }} />
					</colgroup>
				)}
				<thead>
					<tr>
						<th scope="col">
							{isLoading ? (
								""
							) : (
								<CheckboxRHF
									name="checkAll"
									register={register}
									onChange={(event) => {
										setCheckAll(event.target.checked);
									}}
									error={errors}
								/>
							)}
						</th>
						{view !== "posted" && (
							<th scope="col">{isLoading ? "" : "Author"}</th>
						)}
						<th scope="col">{isLoading ? "" : "Comment"}</th>
						<th scope="col">{isLoading ? "" : "Date"}</th>
					</tr>
				</thead>
				<tbody>
					{isLoading ? (
						<tr className="loader">
							<td>
								<PropagateLoader color="#2f3d47" />
							</td>
						</tr>
					) : (
						// TO DO: Use real data
						comments.map((comment, index) => {
							const rowClass = index % 2 === 0 ? "even" : "odd";

							return (
								<Fragment key={comment._id}>
									<tr className={`${type} ${rowClass} ${comment.read} info`}>
										<td className="checkbox-wrap" data-label="Check">
											<div className="unread-bar"></div>
											{/* TO DO: Include checkbox data in overall form data (not working currently) */}
											<CommentsTableCheckbox
												comment={comment}
												checkAll={checkAll}
												setCheckAll={setCheckAll}
											/>
										</td>
										{view !== "posted" && (
											<td
												data-label="Author"
												className="author"
												onClick={() => {
													if (type === "message") {
														setCommentModal(comment);
													} else {
														history.push(`/comments/${comment._id}`);
													}
												}}
											>
												{comment.author}
											</td>
										)}

										<td
											data-label="Comment"
											className="excerpt"
											onClick={() => {
												if (type === "message") {
													setCommentModal(comment);
												} else {
													history.push(`/comments/${comment._id}`);
												}
											}}
										>
											<div className="text">{comment.excerpt}</div>
										</td>
										<td
											data-label="Date"
											className="date"
											onClick={() => {
												if (type === "message") {
													setCommentModal(comment);
												} else {
													history.push(`/comments/${comment._id}`);
												}
											}}
										>
											{comment.date}
										</td>
									</tr>
									<tr
										className={`${type} ${rowClass} ${comment.read} table-toggle-content location`}
									>
										<td colSpan="4">
											<div className="unread-bar"></div>
											<div className="location-inner">
												<div className="content-type">
													<div className="content-type-inner">
														{comment.contentType}
													</div>
												</div>
												<div className="location-links">
													{comment.location.map((parent, index) => {
														const parentCount = comment.location.length;

														return (
															<span key={index}>
																<Link
																	to={`${parent.path}`}
																	onClick={(event) => {
																		event.preventDefault();
																		alert("TO DO");
																	}}
																>
																	{parent.name}
																</Link>
																{index < parentCount - 1 ? " > " : ""}
															</span>
														);
													})}
												</div>
											</div>
										</td>
									</tr>
								</Fragment>
							);
						})
					)}
				</tbody>
			</table>
			{/* TO DO: Pagination */}
			{/* filteredThreads.length > 9 && */}
			{type !== "message" && <Pagination />}
			<DeleteCommentsModal
				show={deleteCommentsModal}
				hide={() => {
					setDeleteCommentsModal(null);
				}}
			/>
			<SortFilterModal
				show={sortFilterModal}
				hide={() => {
					setSortFilterModal(null);
				}}
			/>
			<CommentModal
				show={commentModal}
				hide={() => {
					setCommentModal(null);
				}}
			/>
		</section>
	);
};

export default CommentsTable;
