// TO DO: Use real data

const commentsDB = [
	{
		_id: "commentOne",
		author: "Author One",
		excerpt:
			"Comment one lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "2:22 PM",
		contentType: "sandbox",
		location: [
			{
				name: "Project Name Lorem Ipsum",
				path: "#",
			},
		],
		read: false,
		thread: [
			{
				_id: "100",
				author: "Author One",
				photo: "/images/placeholder-profile-photo.png",
				quote: "",
				content:
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
				transcript: [
					{
						from: "concierge",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "user",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "concierge",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "user",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "concierge",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
				],
				date: "August 21, 2021",
				time: "2:22 PM",
				owner: true,
				replyTo: "",
				replies: ["101", "102"],
			},
			{
				_id: "101",
				author: "Author Two",
				photo: "/images/placeholder-profile-photo.png",
				quote: "",
				content:
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
				transcript: [],
				date: "August 21, 2021",
				time: "4:25 PM",
				owner: false,
				replyTo: "100",
				replies: [],
			},
			{
				_id: "102",
				author: "Author One",
				photo: "/images/placeholder-profile-photo.png",
				quote: "",
				content:
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
				transcript: [],
				date: "August 22, 2021",
				time: "10:20 AM",
				owner: true,
				replyTo: "100",
				replies: [],
			},
			{
				_id: "103",
				author: "Author Two",
				photo: "/images/placeholder-profile-photo.png",
				quote: "Message text lorem ipsum dolor sit amet?",
				content:
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
				transcript: [
					{
						from: "concierge",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "user",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "concierge",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "user",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
					{
						from: "concierge",
						message:
							"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis cursus. In id tellus nec arcu vehicula iaculis sed nec est. Pellentesque rhoncus vel elit nec maximus.",
					},
				],
				date: "August 22, 2021",
				time: "11:15 AM",
				owner: false,
				replyTo: "",
				replies: [],
			},
			{
				_id: "104",
				author: "Author Three",
				photo: "/images/placeholder-profile-photo.png",
				quote: "",
				content:
					"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
				transcript: [],
				date: "August 22, 2021",
				time: "3:41 PM",
				owner: false,
				replyTo: "",
				replies: [],
			},
		],
	},
	{
		_id: "commentTwo",
		author: "Author Two",
		excerpt:
			"Comment two lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "10:07 AM",
		contentType: "brancher",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Brancher Name Lorem Ipsum",
				path: "#",
			},
		],
		read: false,
		thread: [],
	},
	{
		_id: "commentThree",
		author: "Author Three",
		excerpt:
			"Comment three lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "Aug 23",
		contentType: "bit case",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Element Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Bit Case #1",
				path: "#",
			},
		],
		read: false,
		thread: [],
	},
	{
		_id: "commentFour",
		author: "Author Four",
		excerpt:
			"Comment four lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "Jan 07",
		contentType: "element",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Element Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
		thread: [],
	},
	{
		_id: "commentFive",
		author: "Author Five",
		excerpt:
			"Comment five lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales, mauris vitae maximus tempus, urna tellus sodales tortor, sed facilisis mauris leo eu velit.",
		date: "12/14/2020",
		contentType: "message",
		location: [
			{
				name: "Canvas Name Lorem Ipsum",
				path: "#",
			},
			{
				name: "Message Name Lorem Ipsum",
				path: "#",
			},
		],
		read: true,
		thread: [],
	},
];

export default commentsDB;
