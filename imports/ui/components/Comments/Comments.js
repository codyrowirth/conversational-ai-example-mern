import React, { useState } from "react";
import { Link } from "react-router-dom";
import commentsDB from "./commentsDB";
import CommentsSearch from "./CommentsSearch";
import CommentsTable from "./CommentsTable";

const Comments = () => {
	const [searchBarText, setSearchBarText] = useState();

	return (
		<section className="comments">
			<h1>Comments</h1>
			{/* TO DO: Make search work */}
			<CommentsSearch />
			<h2 className="heading-with-action-links">
				Inbox
				<div className="action-links">
					<div className="label">View</div>
					<Link to="/comments/archive" className="action-link">
						Archive
					</Link>
					<Link to="/comments/posted" className="action-link">
						Posted
					</Link>
				</div>
			</h2>
			<CommentsTable view="inbox" type="full" comments={commentsDB} />
		</section>
	);
};

export default Comments;
