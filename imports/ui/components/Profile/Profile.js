import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Profile = () => {
	return (
		<section className="profile">
			<h1>Profile</h1>
			<ComingSoon />
		</section>
	);
};

export default Profile;
