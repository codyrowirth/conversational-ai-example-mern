import React from "react";
import "./Loading.scss";

const Loading = () => (
	<div className="loading">
		<div className="three-balls">
			<div className="ball ball1" />
			<div className="ball ball2" />
			<div className="ball ball3" />
		</div>
	</div>
);

export default Loading;
