import React, { useState } from "react";
import { UnmountClosed as Collapse } from "react-collapse";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Button from "../../lib/components/Button/Button";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import GreyBox from "../../lib/components/GreyBox/GreyBox";
import CommentsModal from "./CommentsModal";
import NewProfileModal from "./NewProfileModal";
import EditProfileModal from "./EditProfileModal";
import ResetProfileModal from "./ResetProfileModal";
import DeleteProfileModal from "./DeleteProfileModal";
import SandboxModal from "./SandboxModal";

import "./Sandbox.scss";
import { useTracker } from "meteor/react-meteor-data";
import OptionsTemplates from "../../../api/collections/OptionsTemplates";
import { PropagateLoader } from "react-spinners";
import Canvases from "../../../api/collections/Canvases";
import Shapes from "../../../api/collections/Shapes";
import Sessions from "../../../api/collections/Sessions";

const Sandbox = () => {
	// const [userProfile, setUserProfile] = useState(null);
	const [userProfileDataExpanded, setUserProfileDataExpanded] = useState(false);

	const [commentsModal, setCommentsModal] = useState(null);
	const [newProfileModal, setNewProfileModal] = useState(null);
	const [editProfileModal, setEditProfileModal] = useState(null);
	const [resetProfileModal, setResetProfileModal] = useState(null);
	const [deleteProfileModal, setDeleteProfileModal] = useState(null);
	const [sandboxModal, setSandboxModal] = useState(null);
	const [startingCanvas, setStartingCanvas] = useState();
	const [startingShape, setStartingShape] = useState();

	const { canvases, messages, session, loading } = useTracker(() => {
		const canvasSub = Meteor.subscribe("canvases", Meteor.user()?.projectID);
		const sessionSub = Meteor.subscribe("sessions");

		if (!canvasSub.ready() || !sessionSub.ready()) {
			return { loading: true };
		}

		let messages;
		if (startingCanvas) {
			const messageSub = Meteor.subscribe("shapes", startingCanvas.value);

			if (!messageSub.ready()) {
				return { loading: true };
			}

			const shapes = Shapes.find({
				canvasID: startingCanvas.value,
				type: { $in: ["brancher", "message", "start"] },
			}).fetch();
			messages = shapes.map((shape) => ({
				value: shape._id,
				label: `${shape?.data?.label} (${shape.type})`,
			}));
		}

		const sessions = Sessions.find({
			userID: Meteor.user()._id,
			sessionCompleted: false,
		}).fetch();

		return {
			canvases: Canvases.find({
				projectID: Meteor.user().projectID,
			}).fetch(),
			messages,
			session: sessions[0],
		};
	});

	// TO DO: Get actual comment count
	const comments = 0;

	const onSubmit = (data) => {
		setSandboxModal(data);
	};

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#fff" />
			</div>
		);
	}

	const startSession = (
		<div>
			<h2>Configuration</h2>

			<div className="grid-row">
				<div className="grid-col-half">
					<SelectRHF
						name={"startingCanvas"}
						label="Starting Canvas"
						options={canvases.map((canvas) => ({
							value: canvas._id,
							label: canvas.name,
						}))}
						value={startingCanvas}
						onChange={(value) => {
							setStartingCanvas(value);
							setStartingShape(null);
						}}
					/>
				</div>
				{startingCanvas && (
					<div className="grid-col-half">
						<SelectRHF
							name={"startingShape"}
							label="Starting Message"
							options={messages}
							value={startingShape}
							onChange={(value) => {
								setStartingShape(value);
							}}
						/>
					</div>
				)}
			</div>
			<div className="button-wrap">
				{startingCanvas && startingShape && (
					<Button
						text="Enter Sandbox"
						onClick={(evt) => {
							evt.preventDefault();
							setSandboxModal(true);
						}}
					/>
				)}
			</div>
		</div>
	);

	const resumeSession = (
		<div>
			<Button
				text="Resume Session"
				onClick={(evt) => {
					evt.preventDefault();
					setSandboxModal(true);
				}}
			/>
			<Button
				text="Clear Session"
				onClick={(evt) => {
					evt.preventDefault();
					Meteor.call(`sessions.complete`);
				}}
			/>
		</div>
	);

	return (
		<section className="sandbox">
			<h1>Sandbox</h1>
			{/*<section className="interaction">*/}
			{/*	<div className="action-links">*/}
			{/*		<button*/}
			{/*			className="action-link notif-link"*/}
			{/*			onClick={(event) => {*/}
			{/*				event.preventDefault();*/}
			{/*				setCommentsModal(true);*/}
			{/*			}}*/}
			{/*		>*/}
			{/*			<span className="notif-type">Comments</span>*/}
			{/*			{comments > 0 ? ` (${comments})` : ""}*/}
			{/*		</button>*/}
			{/*	</div>*/}
			{/*</section>*/}
			<section>
				<form>
					{session ? resumeSession : startSession}
					{/*<div className="grid-row">*/}
					{/*	<div className="grid-col-half">*/}
					{/*		<Controller*/}
					{/*			name="profile"*/}
					{/*			control={control}*/}
					{/*			render={({ field }) => {*/}
					{/*				return (*/}
					{/*					<SelectRHF*/}
					{/*						name={field.name}*/}
					{/*						label="Test User Profile"*/}
					{/*						options={["TO DO"].map((option) => ({*/}
					{/*							value: option.toLowerCase(),*/}
					{/*							label: option,*/}
					{/*						}))}*/}
					{/*						{...field}*/}
					{/*						ref={null}*/}
					{/*						error={errors}*/}
					{/*						onChange={(event) => {*/}
					{/*							field.onChange(event);*/}
					{/*							setUserProfile(event);*/}
					{/*						}}*/}
					{/*					/>*/}
					{/*				);*/}
					{/*			}}*/}
					{/*		/>*/}
					{/*	</div>*/}
					{/*	<div className="grid-col-half no-label">*/}
					{/*		<div className="action-links-wrap">*/}
					{/*			<button*/}
					{/*				className="action-link"*/}
					{/*				onClick={(event) => {*/}
					{/*					event.preventDefault();*/}
					{/*					setNewProfileModal(true);*/}
					{/*				}}*/}
					{/*			>*/}
					{/*				New*/}
					{/*			</button>*/}
					{/*			{userProfile !== null && (*/}
					{/*				<>*/}
					{/*					<button*/}
					{/*						className="action-link"*/}
					{/*						onClick={(event) => {*/}
					{/*							event.preventDefault();*/}
					{/*							setEditProfileModal(userProfile);*/}
					{/*						}}*/}
					{/*					>*/}
					{/*						Edit*/}
					{/*					</button>*/}
					{/*					<button*/}
					{/*						className="action-link"*/}
					{/*						onClick={(event) => {*/}
					{/*							event.preventDefault();*/}
					{/*							setResetProfileModal(userProfile);*/}
					{/*						}}*/}
					{/*					>*/}
					{/*						Reset*/}
					{/*					</button>*/}
					{/*					<button*/}
					{/*						className="action-link"*/}
					{/*						onClick={(event) => {*/}
					{/*							event.preventDefault();*/}
					{/*							setDeleteProfileModal(userProfile);*/}
					{/*						}}*/}
					{/*					>*/}
					{/*						Delete*/}
					{/*					</button>*/}
					{/*				</>*/}
					{/*			)}*/}
					{/*		</div>*/}
					{/*	</div>*/}
					{/*</div>*/}
					{/*<GreyBox>*/}
					{/*	<button*/}
					{/*		className="collapse-trigger h3"*/}
					{/*		onClick={(event) => {*/}
					{/*			event.preventDefault();*/}
					{/*			setUserProfileDataExpanded(!userProfileDataExpanded);*/}
					{/*		}}*/}
					{/*	>*/}
					{/*		User Profile Data*/}
					{/*		<span className="arrow">*/}
					{/*			<FontAwesomeIcon*/}
					{/*				icon={userProfileDataExpanded ? "chevron-up" : "chevron-down"}*/}
					{/*			/>*/}
					{/*		</span>*/}
					{/*	</button>*/}
					{/*	<Collapse isOpened={userProfileDataExpanded}>*/}
					{/*		<ul className="user-data-list">*/}
					{/*			/!* TO DO: Use real data *!/*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*			<li>*/}
					{/*				<span className="key-name">Lorem Ipsum:</span> Dolor Sit Ame*/}
					{/*			</li>*/}
					{/*		</ul>*/}
					{/*	</Collapse>*/}
					{/*</GreyBox>*/}
				</form>
			</section>
			<CommentsModal
				show={commentsModal}
				hide={() => {
					setCommentsModal(null);
				}}
			/>
			<NewProfileModal
				show={newProfileModal}
				hide={() => {
					setNewProfileModal(null);
				}}
			/>
			<EditProfileModal
				show={editProfileModal}
				hide={() => {
					setEditProfileModal(null);
				}}
			/>
			<ResetProfileModal
				show={resetProfileModal}
				hide={() => {
					setResetProfileModal(null);
				}}
			/>
			<DeleteProfileModal
				show={deleteProfileModal}
				hide={() => {
					setDeleteProfileModal(null);
				}}
			/>
			<SandboxModal
				show={sandboxModal}
				hide={() => {
					setSandboxModal(null);
				}}
				comments={comments}
				startingCanvas={startingCanvas}
				startingShape={startingShape}
			/>
		</section>
	);
};

export default Sandbox;
