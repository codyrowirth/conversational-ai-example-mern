import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { PropagateLoader } from "react-spinners";

import Modal from "../../lib/components/Modal/Modal";
import InputRHF from "../../lib/components/Form/InputRHF";
import Button from "../../lib/components/Button/Button";

const NewProfileModal = ({ show, hide }) => {
	const schema = yup.object().shape({
		userProfileName: yup.string().required("Please enter a user profile name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm({
		resolver: yupResolver(schema),
	});

	// TO DO: Use real data

	const loading = false;

	const onSubmit = (data) => {
		alert("TO DO");
		hide();
	};

	const onCancel = () => {
		hide();
	};

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>New Test User Profile</h3>
			<form>
				<div className="grid">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="userProfileName"
							label="Name"
							register={register}
							error={errors}
							autoFocus={true}
						/>
					</div>
				</div>
			</form>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default NewProfileModal;
