import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import {
	Widget,
	toggleWidget,
	addResponseMessage,
	renderCustomComponent,
	isWidgetOpened,
	deleteMessages,
	addUserMessage,
	setQuickButtons,
	toggleInputDisabled,
} from "react-chat-widget";

import CommentsModal from "./CommentsModal";
import Modal from "../../lib/components/Modal/Modal";
import InputRHF from "../../lib/components/Form/InputRHF";
import Button from "../../lib/components/Button/Button";

import "react-chat-widget/lib/styles.css";
import "./SandboxModal.scss";
import InputControlRHF from "../../lib/components/Form/InputControlRHF";
import ContentAPI from "../../lib/utilities/ContentAPI";
import { PropagateLoader } from "react-spinners";
import { useTracker } from "meteor/react-meteor-data";
import Sessions from "../../../api/collections/Sessions";
import CheckboxControlRHF from "../../lib/components/Form/CheckboxControlRHF";
import { Meteor } from "meteor/meteor";
import Loading from "./Loading";

let session;
let userData = {};
let timerSeconds = 5 * 60;

const SandboxModal = ({
	show,
	hide,
	comments,
	startingCanvas,
	startingShape,
}) => {
	const [loading, setLoading] = useState(true);
	const [commentsModal, setCommentsModal] = useState(false);
	const [currentMessage, setCurrentMessage] = useState();
	const [statementMode, setStatementMode] = useState(false);
	const [currentMessageID, setCurrentMessageID] = useState();
	const [statement, setStatement] = useState(false);
	const [showTimer, setShowTimer] = useState(false);
	const [shouldEndSession, setShouldEndSession] = useState(false);

	const { sessionDB, user } = useTracker(() => {
		const handler = Meteor.subscribe("sessions");
		const handlerUser = Meteor.subscribe("Meteor.users.info");

		if (!handler.ready() || !handlerUser.ready()) {
			return { sessionDB: null };
		}

		const user = Meteor.users.findOne(
			{ _id: Meteor.user()._id },
			{ fields: { typingDelay: 1 } }
		);

		const sessionDB = Sessions.findOne({
			userID: Meteor.user()._id,
			sessionCompleted: false,
		});

		return {
			sessionDB,
			user,
		};
	});

	useEffect(() => {
		if (sessionDB?.currentShapeID) {
			(async () => {
				const currentShape = await Meteor.callPromise(
					`shapes.find`,
					sessionDB.currentShapeID
				);
				setCurrentMessage(currentShape);
			})();
		}
	}, [sessionDB?.currentShapeID]);

	useEffect(() => {
		if (show) {
			(async () => {
				let sessionStatus = await ContentAPI(
					`/conversation/session/status/${Meteor.user()._id}`,
					"GET"
				);

				if (!sessionStatus.sessionInProgress) {
					sessionStatus = await ContentAPI(
						`/conversation/session/start`,
						"POST",
						{
							userID: Meteor.user()._id,
							startingShapeID: startingShape.value,
						}
					);

					if (sessionStatus.error) {
						hide();
					}
				}
				session = sessionStatus.sessionDetails;
				deleteMessages();
				for (const [index, message] of session.messages.entries()) {
					if (message.sentByUser) {
						addUserMessage(message.content, message._id);
						renderCustomComponent(meta, {
							date: new Date(message.timestamp),
							type: "user",
						});
					} else {
						renderCustomComponent(conciergeMessage, {
							message: message.content,
							date: new Date(message.timestamp),
						});

						if (
							index + 1 === session.messages.length ||
							session.messages[index + 1]?.sentByUser
						) {
							renderCustomComponent(meta, {
								date: new Date(message.timestamp),
								type: "concierge",
							});
						}
					}
				}

				setLoading(false);
			})();
		} else {
			deleteMessages();
		}
	}, [show]);

	const meta = ({ date, type }) => {
		const timestamp = new Intl.DateTimeFormat("en-US", {
			hour: "numeric",
			minute: "2-digit",
		}).format(date);

		return (
			<div className={`rcw-meta ${type === "user" && "rcw-client"}`}>
				<div className="rcw-timestamp">{timestamp}</div>
				{type === "concierge" && false && (
					<div className="rcw-links action-links">
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
								// TO DO: Route to message
							}}
						>
							Open
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								alert("TO DO");
								// TO DO: Route to adding a comment with quoted message text
							}}
						>
							Comment
						</button>
					</div>
				)}
			</div>
		);
	};

	const conciergeMessage = ({ message }) => {
		const createMarkup = () => ({
			__html: message,
		});

		return (
			<div className={`rcw-response`}>
				<div className={`rcw-message-text`}>
					<p>
						<span dangerouslySetInnerHTML={createMarkup()} />
					</p>
				</div>
			</div>
		);
	};

	const typingMessage = () => (
		<div className={`rcw-response`}>
			<div className={`rcw-message-text loading-message`}>
				<Loading />
			</div>
		</div>
	);

	const randInt = (min, max) => {
		return Math.floor(Math.random() * (max - min + 1) + min);
	};

	const handleNewUserMessage = async (message) => {
		const typingDelay = !!user?.typingDelay;
		const sendTime = new Date();
		if (typingDelay) {
			toggleInputDisabled();
		}

		if (!statementMode) {
			renderCustomComponent(meta, {
				date: Date.now(),
				type: "user",
			});
		}

		const data = await ContentAPI(`/conversation/message`, "POST", {
			sessionID: session._id,
			message,
		});

		userData = { ...userData, ...data.newUserData };

		let firstMessageWaitTime, secondMessageWaitTime;

		if (!typingDelay) {
			firstMessageWaitTime = 1;
			secondMessageWaitTime = 250;
		} else {
			firstMessageWaitTime = Math.max(
				1500,
				Math.max(
					data.newMessages[0].length * randInt(10, 14) -
						Math.abs(new Date() - sendTime),
					0
				),
				0
			);

			secondMessageWaitTime =
				data.newMessages.length === 2
					? data.newMessages[1].length * randInt(10, 14) + firstMessageWaitTime
					: firstMessageWaitTime;
		}

		renderCustomComponent(
			typingMessage,
			{
				date: Date.now(),
				type: "concierge",
			},
			false,
			"loading"
		);

		setTimeout(() => {
			deleteMessages(1, "loading");
			renderCustomComponent(conciergeMessage, {
				message: data.newMessages[0].content,
			});

			if (data.newMessages.length === 2) {
				renderCustomComponent(
					typingMessage,
					{
						date: Date.now(),
						type: "concierge",
					},
					false,
					"loading"
				);
				setTimeout(() => {
					deleteMessages(1, "loading");
					renderCustomComponent(conciergeMessage, {
						message: data.newMessages[1].content,
					});
					renderCustomComponent(meta, {
						date: Date.now(),
						type: "concierge",
					});
					handleUIChangesAfterMessageShown();
				}, secondMessageWaitTime);
			} else {
				renderCustomComponent(meta, {
					date: Date.now(),
					type: "concierge",
				});
				handleUIChangesAfterMessageShown();
			}
		}, firstMessageWaitTime);

		const handleUIChangesAfterMessageShown = () => {
			if (data.nextMessageID === 12691) {
				if (userData["BP 5 Minute Check Start"]) setShowTimer(true);
			}
			setCurrentMessageID(data.nextMessageID);
			setStatement(
				data.messageType === "statement" ? data.statementButton : null
			);
			setStatementMode(data.messageType === "statement");
			setShouldEndSession(data.shouldEndSession);
			if (typingDelay) {
				toggleInputDisabled();
			}
			document.getElementsByClassName("rcw-input")[0].focus();
			if (data.shouldEndSession) {
				setQuickButtons([
					{
						label: "Start Over",
						value: "Start Over",
					},
				]);
			}
		};
	};

	useEffect(() => {
		if (!isWidgetOpened()) {
			toggleWidget();
		}
		renderCustomComponent(
			typingMessage,
			{
				date: Date.now(),
				type: "concierge",
			},
			false,
			"loading"
		);
	}, []);

	useEffect(() => {
		if (statementMode) {
			setQuickButtons([
				{
					label: statement
						? statement.replace("<p>", "").replace("</p>", "")
						: "Continue",
					value: "continue",
				},
			]);
		} else {
			setQuickButtons([]);
		}
	}, [statementMode]);

	const handleQuickButtonClicked = async () => {
		if (shouldEndSession) {
			hide();
		} else {
			await handleNewUserMessage("");
			setStatementMode(false);
		}
	};

	if (loading) {
		return (
			<Modal className="modal sandbox-modal" show={!!show} hide={hide}>
				<div style={{ textAlign: "center", paddingBottom: 20 }}>
					<PropagateLoader color="#2f3d47" />
				</div>
			</Modal>
		);
	}

	return (
		<>
			<Modal className="modal sandbox-modal" show={!!show} hide={hide}>
				<div className="sandbox-modal-inner">
					<div className="info">
						<div className="info-inner">
							<div className="info-top">
								<h3>Sandbox</h3>
								<section className="links">
									<div className="action-links">
										{/*<button*/}
										{/*	className="action-link notif-link"*/}
										{/*	onClick={(event) => {*/}
										{/*		event.preventDefault();*/}
										{/*		setCommentsModal(true);*/}
										{/*	}}*/}
										{/*>*/}
										{/*	<span className="notif-type">Comments</span>*/}
										{/*	{comments > 0 ? ` (${comments})` : ""}*/}
										{/*</button>*/}
									</div>
									<div className="action-links">
										{/*<button*/}
										{/*	className="action-link"*/}
										{/*	onClick={(event) => {*/}
										{/*		event.preventDefault();*/}
										{/*		alert("TO DO");*/}
										{/*	}}*/}
										{/*>*/}
										{/*	Download Transcript*/}
										{/*</button>*/}
									</div>
								</section>
								<div className="data-section">
									<p className="data-heading">Starting Shape:</p>
									<p>
										{startingCanvas?.label} &gt; {startingShape?.label}
									</p>
								</div>
								{currentMessage?.data?.label && (
									<div className="data-section">
										<p className="data-heading">Current Message:</p>
										<p>
											<a
												href={`/canvases/${currentMessage.canvasID}/${currentMessage._id}`}
												target={"_blank"}
												rel="noreferrer"
											>
												{currentMessage?.data?.label}
											</a>
										</p>
									</div>
								)}
								<div className={"data-section"}>
									<CheckboxControlRHF
										checked={user?.typingDelay || false}
										label={"Typing delay"}
										onChange={(evt) => {
											Meteor.call("user.setTypingDelay", !user?.typingDelay);
										}}
									/>
								</div>
								{sessionDB?.messages && (
									<div className="data-section">
										<div className="data-heading">Message Path:</div>
										<MessagePath sessionDB={sessionDB} />
									</div>
								)}
								<div className="data-section">
									<p className="data-heading">User Data:</p>
									<ul>
										{!sessionDB || sessionDB.sessionData.length === 0 ? (
											<li key="no-data">[[no user data set yet]]</li>
										) : (
											sessionDB.sessionData.map(
												({ key, value, sessionAttribute }) => {
													return (
														<li key={key}>
															<span style={{ fontWeight: 700 }}>{key}:</span>{" "}
															{value}
														</li>
													);
												}
											)
										)}
									</ul>
								</div>
								{/*<h4>User Profile</h4>*/}
								{/*<form className="profile">*/}
								{/*	<div className="grid">*/}
								{/*		<div className="grid-col-full">*/}
								{/*			<InputControlRHF type="text" name="userProfileName" />*/}
								{/*			<div className="action-links">*/}
								{/*				<button*/}
								{/*					className="action-link"*/}
								{/*					onClick={(event) => {*/}
								{/*						event.preventDefault();*/}
								{/*						alert("TO DO");*/}
								{/*					}}*/}
								{/*				>*/}
								{/*					Reset*/}
								{/*				</button>*/}
								{/*				<button*/}
								{/*					className="action-link"*/}
								{/*					onClick={(event) => {*/}
								{/*						event.preventDefault();*/}
								{/*						alert("TO DO");*/}
								{/*					}}*/}
								{/*				>*/}
								{/*					Delete*/}
								{/*				</button>*/}
								{/*			</div>*/}
								{/*		</div>*/}
								{/*	</div>*/}
								{/*</form>*/}
							</div>
							<div className="info-bottom">
								<div className="button-wrap">
									<Button
										text="Close"
										onClick={(event) => {
											event.preventDefault();
											hide();
										}}
									/>
								</div>
							</div>
						</div>
					</div>
					<div
						className={`interaction ${
							statementMode === true ? "statement" : ""
						}`}
					>
						<Widget
							handleNewUserMessage={handleNewUserMessage}
							handleQuickButtonClicked={handleQuickButtonClicked}
							fullScreenMode={true}
							title=""
							subtitle=""
							showTimeStamp={false}
						/>
					</div>
				</div>
			</Modal>
			<CommentsModal
				show={commentsModal}
				hide={() => {
					setCommentsModal(false);
				}}
			/>
		</>
	);
};

const MessagePath = ({ sessionDB }) => {
	const [messageMap, setMessageMap] = useState({});

	useEffect(() => {
		let messageIDs = [];
		for (const message of sessionDB.messages) {
			if (message.messageID) {
				messageIDs.push(message.messageID);
			}
		}
		(async () => {
			setMessageMap(
				await Meteor.callPromise("messages.namesFromIDs", messageIDs)
			);
		})();
	}, [sessionDB]);

	let messagesDeduped = [];
	for (const [index, message] of sessionDB.messages.entries()) {
		if (!message.messageID) {
			//ignore
		} else if (index === 0) {
			messagesDeduped.push(message);
		} else if (
			message?.messageID !==
			messagesDeduped[messagesDeduped.length - 1]?.messageID
		) {
			messagesDeduped.push(message);
		}
	}

	return (
		<ul>
			{messagesDeduped.map((message) => {
				if (messageMap[message.messageID]) {
					return <li key={message._id}>{messageMap[message.messageID]}</li>;
				}
			})}
		</ul>
	);
};

export default SandboxModal;
