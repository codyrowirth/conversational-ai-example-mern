import React from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import { useTracker } from "meteor/react-meteor-data";
import Attributes from "../../../api/collections/Attributes";
import { useHistory, useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";

const ResetProfileModal = ({ show, hide }) => {
	const { id } = useParams();
	const history = useHistory();

	// TO DO: Use real data

	const loading = false;

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<Modal className="modal limit-modal end-buttons" show={show} hide={hide}>
			<h3>Do you want to reset this user profile?</h3>
			{/* TO DO: Use actual profile name below */}
			<p>
				Please confirm that you’d like to reset the{" "}
				<strong>Profile Name Lorem Ipsum</strong> user profile. You won’t be
				able to undo this action.
			</p>
			<div className="button-wrap">
				<Button
					text="Reset"
					onClick={() => {
						alert("TO DO");
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default ResetProfileModal;
