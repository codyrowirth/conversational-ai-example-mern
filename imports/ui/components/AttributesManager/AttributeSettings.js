import React, { useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useTracker } from "meteor/react-meteor-data";
import Button from "../../lib/components/Button/Button";
import AttributeSettingsInfo from "./AttributeSettingsInfo";
import AttributeSettingsOptions from "./AttributeSettingsOptions";
import DeleteAttributeModal from "./DeleteAttributeModal";
import AttributeSettingsCode from "./AttributeSettingsCode";
import AttributeSettingsUsage from "./AttributeSettingsUsage";

import "./AttributeSettings.scss";
import Attributes from "../../../api/collections/Attributes";
import { PropagateLoader } from "react-spinners";
import AttributeGroups from "../../../api/collections/AttributeGroups";

const AttributeSettings = () => {
	const { id } = useParams();

	const [deleteAttributeModal, setDeleteAttributeModal] = useState();

	const { attribute } = useTracker(() => {
		const handler = Meteor.subscribe("attribute", id);

		if (!handler.ready()) {
			return { attributeIsLoading: true, attribute: null };
		}

		let attribute = Attributes.findOne({ _id: id });
		if (!attribute.parent) {
			attribute.parent = "";
		}

		return {
			attribute,
			attributeIsLoading: false,
		};
	});

	const history = useHistory();

	return (
		<>
			<section className="attribute-settings">
				<h1>Attribute Settings</h1>

				<AttributeSettingsInfo id={id} />
				{/* <AttributeSettingsUsage /> */}
				{attribute?.type !== "retrieved" && <AttributeSettingsOptions />}
				{(attribute?.type === "calculated" ||
					attribute?.type === "retrieved") && (
					<AttributeSettingsCode attribute={attribute} />
				)}

				<div className="button-wrap">
					<Button
						text="Back"
						onClick={() => {
							history.push("/attributes-manager");
						}}
					/>
				</div>
				<section className="action-links single-link">
					<button
						className="action-link last"
						onClick={() => {
							setDeleteAttributeModal(id);
						}}
					>
						Delete Attribute
					</button>
				</section>
			</section>
			<DeleteAttributeModal
				show={deleteAttributeModal}
				hide={() => {
					setDeleteAttributeModal(null);
				}}
			/>
		</>
	);
};

export default AttributeSettings;
