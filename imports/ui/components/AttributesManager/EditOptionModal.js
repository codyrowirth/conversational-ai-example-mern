import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import InputRHF from "../../lib/components/Form/InputRHF";

const EditOptionModal = ({ option, options, hide }) => {
	return (
		<Modal
			key={option && option._id}
			className="modal limit-modal end-buttons"
			show={!!option}
			hide={hide}
		>
			<h3>Edit Option</h3>
			<Form hide={hide} option={option} options={options} />
		</Modal>
	);
};

const Form = ({ option, options, hide }) => {
	const schema = yup.object().shape({
		optionName: yup.string().required("Please enter an option name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
		setError,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		const nameExists = options.find(
			(option) => option.name.toLowerCase() === data.optionName.toLowerCase()
		);
		if (nameExists) {
			setError("optionName", {
				type: "string",
				message:
					"Option with this name already exists. Please use a different name",
			});
		} else {
			Meteor.call("options.update", option._id, { name: data.optionName });
			hide();
		}
	};

	const onCancel = () => {
		hide();
	};

	return (
		<form>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="optionName"
						label="Option Name"
						defaultValue={option && option?.name}
						register={register}
						error={errors}
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={handleSubmit(onCancel)} />
			</div>
		</form>
	);
};

export default EditOptionModal;
