import React, { useState, useEffect } from "react";
import NewOptionsForm from "./NewOptionsForm";
import Button from "../../lib/components/Button/Button";
import { useParams } from "react-router-dom";
import { useTracker } from "meteor/react-meteor-data";
import Options from "../../../api/collections/Options";
import { PropagateLoader } from "react-spinners";
import caseInsensitiveDedupe from "../../lib/utilities/caseInsensitiveDedupe";

const AttributeSettingsOptionsNew = () => {
	const [optionsValue, setOptionsValue] = useState("");
	const { id } = useParams();

	const { existingOptions, isLoading } = useTracker(() => {
		const handler = Meteor.subscribe("optionsByAttribute", id);

		if (!handler.ready()) {
			return { isLoading: true, options: null };
		}

		return {
			isLoading: false,
			existingOptions: Options.find({ attributeID: id }).fetch(),
		};
	});

	const save = () => {
		const existingOptionNames = existingOptions.map((opt) =>
			opt.name.toLowerCase()
		);
		const options = caseInsensitiveDedupe(
			optionsValue
				.split("\n")
				.filter((option) => option !== "")
				.filter((option) => !existingOptionNames.includes(option.toLowerCase()))
		);

		for (const option of options) {
			Meteor.call(
				"thesaurusEntries.insert",
				{
					name: option.replace(/\*/g, ""),
					parent: null,
					projectID: Meteor.user().projectID,
					synonymList: [],
					linkedEntryLIst: [],
					updatedAt: new Date(),
				},
				(err, entryID) => {
					Meteor.call(
						`options.insert`,
						{
							name: option.replace(/\*/g, ""),
							attributeID: id,
							thesaurusEntryID: entryID,
						},
						(err, data) => {
							if (option.charAt(option.length - 1) === "*") {
								Meteor.call("options.setDefault", data, id);
							}
						}
					);
				}
			);
		}
		setOptionsValue("");
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<>
			<h3>Add New Options</h3>
			<NewOptionsForm
				optionsValue={optionsValue}
				setOptionsValue={setOptionsValue}
			/>
			<div className="button-wrap">
				<Button text="Save Options" onClick={save} />
			</div>
		</>
	);
};

export default AttributeSettingsOptionsNew;
