import React, { useState } from "react";

import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import { saveOnBlur } from "../../lib/utilities/saveOnBlurRHFMeteor";

const AttributeSettingsCode = ({ attribute }) => {
	const [code, setCode] = useState(attribute?.code || "");

	return (
		<section className="code-settings end-buttons">
			<h2>Code</h2>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<div className="pretend-code-editor flex">
							{/*<div className="code-editor-line-numbers">*/}
							{/*	<span>1</span>*/}
							{/*	<span>2</span>*/}
							{/*	<span>3</span>*/}
							{/*</div>*/}
							<textarea
								className="code-editor"
								name={"code"}
								value={code}
								onChange={(evt) => {
									setCode(evt.target.value);
								}}
								onBlur={(evt) => saveOnBlur("attributes", attribute._id, evt)}
							/>
						</div>
					</div>
				</div>
				{/*<div className="grid-row">*/}
				{/*	<div className="grid-col-full">*/}
				{/*		<InputRHF*/}
				{/*			type="text"*/}
				{/*			name="codeInput"*/}
				{/*			label="Input"*/}
				{/*			className="code-input"*/}
				{/*			register={register}*/}
				{/*			error={errors}*/}
				{/*		/>*/}
				{/*	</div>*/}
				{/*</div>*/}
				{/*<div className="grid-row">*/}
				{/*	<div className="grid-col-full">*/}
				{/*		<div className="code-output">*/}
				{/*			<p>Output</p>*/}
				{/*			<div className="output">Output</div>*/}
				{/*		</div>*/}
				{/*	</div>*/}
				{/*</div>*/}
				{/*<div className="button-wrap">*/}
				{/*	<Button text="Save" onClick={handleSubmit(onSubmit)} />*/}
				{/*	<Button text="Run" onClick={handleSubmit(onRun)} />*/}
				{/*</div>*/}
			</form>
		</section>
	);
};

export default AttributeSettingsCode;
