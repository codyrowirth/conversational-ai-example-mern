import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { UnmountClosed as Collapse } from "react-collapse";
import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import GreyBox from "../../lib/components/GreyBox/GreyBox";
import NewGroupModal from "../../lib/components/NestedData/NewGroupModal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";
import AttributeGroups from "../../../api/collections/AttributeGroups";
import CheckboxRHF from "../../lib/components/Form/CheckboxRHF";
import NewOptionsForm from "./NewOptionsForm";
import caseInsensitiveDedupe from "../../lib/utilities/caseInsensitiveDedupe";

const NewAttributeForm = ({
	newAttributeFormExpanded,
	setNewAttributeFormExpanded,
}) => {
	return (
		<Collapse isOpened={newAttributeFormExpanded}>
			<GreyBox className="end-buttons">
				<h3>Add New Attribute</h3>
				<Form setNewAttributeFormExpanded={setNewAttributeFormExpanded} />
			</GreyBox>
		</Collapse>
	);
};

const Form = ({ setNewAttributeFormExpanded }) => {
	const [newAttributeFormOptionsExpanded, setNewAttributeFormOptionsExpanded] =
		useState(false);

	const [newGroupModal, setNewGroupModal] = useState();
	const [optionsValue, setOptionsValue] = useState("");

	const { groups, isLoading } = useTracker(() => {
		const handlerGroups = Meteor.subscribe(
			"attributeGroups",
			Meteor.user()?.projectID
		);
		if (!handlerGroups.ready()) {
			return { isLoading: true, groups: null };
		}

		let groups = AttributeGroups.find({ projectID: Meteor.user()?.projectID })
			.fetch()
			.map((group) => ({ value: group._id, label: group.name }));
		groups.push({ value: "", label: "[[root]]" });
		return { groups, isLoading: false };
	});

	const schema = yup.object().shape({
		name: yup.string().required("Please enter an attribute name."),
		type: yup.object().required("Please select an attribute type."),
		group: yup.object().required("Please select a group."),
	});

	const {
		register,
		control,
		formState: { errors },
		handleSubmit,
		reset,
		setFocus,
		setError,
		setValue,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const resetForm = () => {
		reset();
		setValue("type", "");
		setValue("group", "");
		setOptionsValue("");
	};

	const onSubmit = (data, closeForm = true) => {
		const attributeToAdd = {
			name: data.name,
			parent: data.group.value || null,
			type: data.type.value,
			session: data.session,
			updatedAt: new Date(),
		};
		Meteor.call(
			`attributes.duplicateNameCheck`,
			{ name: data.name },
			(err, foundObj) => {
				if (foundObj) {
					setError("name", {
						type: "string",
						message:
							"Attribute with this name already exists. Please use a different name",
					});
				} else {
					Meteor.call(
						`attributes.insert`,
						attributeToAdd,
						(err, attributeID) => {
							if (err) {
								alert(`Error creating attribute`);
							} else {
								const options = caseInsensitiveDedupe(
									optionsValue.split("\n").filter((option) => option !== "")
								);
								for (const option of options) {
									if (option.charAt(option.length - 1) === "*") {
										Meteor.call(
											"thesaurusEntries.insert",
											{
												name: option.substring(0, option.length - 1),
												parent: null,
												projectID: Meteor.user().projectID,
												synonymList: [],
												linkedEntryList: [],
												updatedAt: new Date(),
											},
											(err, id) => {
												Meteor.call(`options.insert`, {
													name: option.substring(0, option.length - 1),
													attributeID,
													default: true,
													thesaurusEntryID: id,
													updatedAt: new Date(),
												});
											}
										);
									} else {
										Meteor.call(
											"thesaurusEntries.insert",
											{
												name: option,
												parent: null,
												projectID: Meteor.user().projectID,
												synonymList: [],
												linkedEntryList: [],
												updatedAt: new Date(),
											},
											(err, id) => {
												Meteor.call(`options.insert`, {
													name: option,
													attributeID,
													thesaurusEntryID: id,
													updatedAt: new Date(),
												});
											}
										);
									}
								}
							}
						}
					);

					if (closeForm) {
						setNewAttributeFormExpanded(false);
					}
					resetForm();
				}
			}
		);
	};

	const onContinue = (data) => {
		onSubmit(data, false);
		setFocus("name");
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<>
			<form>
				<div className="grid-row">
					<div className="grid-col-half">
						<InputRHF
							type="text"
							name="name"
							label="Name"
							register={register}
							error={errors}
						/>
					</div>
					<div className="grid-col-half">
						<Controller
							name="type"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Type"
										options={["Direct", "Calculated", "Retrieved"].map(
											(option) => ({
												value: option.toLowerCase(),
												label: option,
											})
										)}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-half">
						<Controller
							name="group"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Group"
										options={groups}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div>
					<div className="grid-col-half no-label">
						<div className="action-links-wrap">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setNewGroupModal(true);
								}}
							>
								Add New Group
							</button>
						</div>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-half">
						<CheckboxRHF
							name={"session"}
							label="Session attribute?"
							register={register}
							error={errors}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-full">
						<button
							className="collapse-trigger h4"
							onClick={(event) => {
								event.preventDefault();
								setNewAttributeFormOptionsExpanded(
									!newAttributeFormOptionsExpanded
								);
							}}
						>
							Options
							<span className="arrow">
								<FontAwesomeIcon
									icon={
										newAttributeFormOptionsExpanded
											? "chevron-up"
											: "chevron-down"
									}
								/>
							</span>
						</button>
						<Collapse isOpened={newAttributeFormOptionsExpanded}>
							<NewOptionsForm
								optionsValue={optionsValue}
								setOptionsValue={setOptionsValue}
							/>
						</Collapse>
					</div>
				</div>
				<div className="button-wrap">
					<Button text="Save" onClick={handleSubmit(onSubmit)} />
					<Button
						text="Save and add another"
						onClick={handleSubmit(onContinue)}
					/>
				</div>
			</form>
			<NewGroupModal
				show={newGroupModal}
				hide={() => {
					setNewGroupModal(null);
				}}
				type={"attributeGroup"}
			/>
		</>
	);
};

export default NewAttributeForm;
