import React from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";

const DeleteOptionModal = ({ option, hide }) => {
	return (
		<Modal
			className="modal limit-modal end-buttons"
			show={!!option}
			hide={hide}
		>
			<h3>Do you want to delete this option?</h3>
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{option && option?.name}</strong> option.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						Meteor.call("options.remove", option._id);
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteOptionModal;
