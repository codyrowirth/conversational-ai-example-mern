import React from "react";
import { Link } from "react-router-dom";

import GreyBox from "../../lib/components/GreyBox/GreyBox";

const AttributeSettingsUsage = () => {
	return (
		<section className="attribute-usage">
			<GreyBox>
				<h2>Currently Used In</h2>
				<div>
					<ul>
						<li>
							<Link to="/">Message Name Lorem Ipsum</Link>, Message (bit rule)
						</li>
						<li>
							<Link to="/">Brancher Name Lorem Ipsum</Link>, Brancher (brancher
							rule)
						</li>
						<li>
							<Link to="/">Message Name Lorem Ipsum</Link>, Message (bit rule)
						</li>
					</ul>
				</div>
			</GreyBox>
		</section>
	);
};

export default AttributeSettingsUsage;
