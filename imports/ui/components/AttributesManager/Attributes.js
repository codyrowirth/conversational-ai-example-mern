import React, { useState, createContext } from "react";
import NewAttributeForm from "./NewAttributeForm";
import Button from "../../lib/components/Button/Button";
import NestedData from "../../lib/components/NestedData/NestedData";
import Papa from "papaparse";
import downloadjs from "downloadjs";
import { useSnackbar } from "notistack";

function Attributes() {
	const { enqueueSnackbar, closeSnackbar } = useSnackbar();
	const [newAttributeFormExpanded, setNewAttributeFormExpanded] =
		useState(false);

	const [downloading, setDownloading] = useState(false);

	return (
		<section>
			<div className="heading-with-button flex">
				<div>
					<h2>Attributes</h2>
				</div>

				<div>
					<Button
						text={newAttributeFormExpanded ? "Cancel" : "Add New Attribute"}
						icon={newAttributeFormExpanded ? "times" : "plus"}
						onClick={() => {
							setNewAttributeFormExpanded(!newAttributeFormExpanded);
						}}
					/>
				</div>
			</div>
			<NewAttributeForm
				newAttributeFormExpanded={newAttributeFormExpanded}
				setNewAttributeFormExpanded={setNewAttributeFormExpanded}
			/>
			<NestedData
				type={"attribute"}
				downloading={downloading}
				setDownloading={setDownloading}
				enqueueSnackbar={enqueueSnackbar}
				closeSnackbar={closeSnackbar}
			/>
		</section>
	);
}

export default Attributes;
