import React from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import { useTracker } from "meteor/react-meteor-data";
import Attributes from "../../../api/collections/Attributes";
import { useHistory, useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";

const DeleteAttributeModal = ({ show, hide }) => {
	const { id } = useParams();
	const history = useHistory();

	const { attribute, loading } = useTracker(() => {
		const handler = Meteor.subscribe("attribute", id);

		if (!handler.ready()) {
			return { loading: true };
		}

		return {
			attribute: Attributes.findOne({ _id: id }),
			loading: false,
		};
	});

	if (loading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<Modal className="modal limit-modal end-buttons" show={show} hide={hide}>
			<h3>Do you want to delete this attribute?</h3>
			<p>
				Please confirm that you’d like to delete the{" "}
				<strong>{attribute && attribute?.name}</strong> attribute.
			</p>
			<div className="button-wrap">
				<Button
					text="Delete"
					onClick={() => {
						history.push("/attributes-manager");
						Meteor.call("attributes.remove", attribute._id);
						hide();
					}}
				/>
				<Button
					text="Cancel"
					onClick={() => {
						hide();
					}}
				/>
			</div>
		</Modal>
	);
};

export default DeleteAttributeModal;
