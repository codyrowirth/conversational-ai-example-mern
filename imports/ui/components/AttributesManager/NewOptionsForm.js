import React, { useEffect, useState } from "react";
import TextareaRHF from "../../lib/components/Form/TextareaRHF";
import { titleCase } from "title-case";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useTracker } from "meteor/react-meteor-data";
import OptionsTemplates from "../../../api/collections/OptionsTemplates";
import LoadTemplateModal from "./LoadTemplateModal";
import SaveTemplateModal from "./SaveTemplateModal";
import DeleteTemplateModal from "./DeleteTemplateModal";
import Textarea from "../../lib/components/Form/Textarea";
import caseInsensitiveDedupe from "../../lib/utilities/caseInsensitiveDedupe";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import "../../lib/styles/contextmenu.scss";

const NewOptionsForm = ({ optionsValue, setOptionsValue }) => {
	const [loadedTemplate, setLoadedTemplate] = useState("");
	const [loadTemplateModal, setLoadTemplateModal] = useState();
	const [saveTemplateModal, setSaveTemplateModal] = useState();
	const [deleteTemplateModal, setDeleteTemplateModal] = useState();

	const { templates, isTemplateLoading } = useTracker(() => {
		const handlerTemplates = Meteor.subscribe("optionsTemplates");
		if (!handlerTemplates.ready()) {
			return { templates: null, isTemplateLoading: true };
		}
		let templates = OptionsTemplates.find().fetch();
		return { templates, isTemplateLoading: false };
	});

	useEffect(() => {
		if (loadedTemplate !== "") {
			const matchingTemplate = templates.find(
				(template) => template._id === loadedTemplate
			);
			onModify(
				null,
				"duplicates",
				optionsValue + "\n" + matchingTemplate.options.join("\n")
			);
		}
	}, [loadedTemplate]);

	const onModify = (event, action, optionsInput = optionsValue) => {
		if (event) {
			event.preventDefault();
		}
		const options = optionsInput
			.replace(/\r\n/g, "\n")
			.split("\n")
			.filter((option) => option);

		if (options.length) {
			let optionsModified = [];

			if (action === "case") {
				optionsModified = options.map((option) => titleCase(option));
			} else if (action === "duplicates") {
				optionsModified = caseInsensitiveDedupe(options);
			} else if (action === "asc") {
				optionsModified = options.sort();
			} else if (action === "dsc") {
				optionsModified = options.reverse();
			}

			setOptionsValue(optionsModified.join("\r\n").trim());
		}
	};

	return (
		<>
			<p>
				List one option name per line. Add an asterisk (*) to the end of an
				option name to set it as default.
			</p>
			<div className="input-action-links-wrap">
				<div className="input-action-links-row top">
					<div className="input-action-links left">
						<span className="label">Format</span>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "case");
							}}
						>
							Title Case
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "duplicates");
							}}
						>
							Remove Duplicates
						</button>
					</div>
					<div className="input-action-links right">
						<span className="label">Sort</span>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "asc");
							}}
						>
							A-Z
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								onModify(event, "dsc");
							}}
						>
							Z-A
						</button>
					</div>
				</div>
			</div>
			<Textarea
				name="options"
				value={optionsValue}
				onChange={(evt) => {
					setOptionsValue(evt.target.value);
				}}
			/>
			<div className="input-action-links-wrap">
				<div className="input-action-links-row bottom">
					<div className="input-action-links left">
						<span className="label">Templates</span>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setLoadTemplateModal(templates);
							}}
						>
							<ContextMenuTrigger id="loadTemplate">Load</ContextMenuTrigger>
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setSaveTemplateModal(templates);
							}}
						>
							Save
						</button>
						<button
							className="action-link"
							onClick={(event) => {
								event.preventDefault();
								setDeleteTemplateModal(templates);
							}}
						>
							Delete
						</button>
					</div>
				</div>
			</div>
			<LoadTemplateModal
				templates={templates}
				setLoadedTemplate={setLoadedTemplate}
				show={loadTemplateModal}
				hide={() => {
					setLoadTemplateModal(null);
				}}
			/>
			<SaveTemplateModal
				templates={templates}
				loadedTemplate={loadedTemplate}
				setLoadedTemplate={setLoadedTemplate}
				optionsValue={optionsValue}
				show={saveTemplateModal}
				hide={() => {
					setSaveTemplateModal(null);
				}}
			/>
			<DeleteTemplateModal
				templates={templates}
				loadedTemplate={loadedTemplate}
				setLoadedTemplate={setLoadedTemplate}
				show={deleteTemplateModal}
				hide={() => {
					setDeleteTemplateModal(null);
				}}
			/>
			{templates ? (
				<ContextMenu id="loadTemplate">
					{templates.map((template) => {
						return (
							<MenuItem
								key={template._id}
								onClick={() => {
									setLoadedTemplate(template._id);
								}}
							>
								{template.name}
							</MenuItem>
						);
					})}
				</ContextMenu>
			) : null}
		</>
	);
};

export default NewOptionsForm;
