import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";
import useWinWidth from "../../lib/utilities/useWinWidth";
import Radio from "../../lib/components/Form/Radio";
import ManageOptionThesaurusModal, {
	EditSynonymsInAttributeModal,
} from "./ManageOptionThesaurusModal";
import EditOptionModal from "./EditOptionModal";
import DeleteOptionModal from "./DeleteOptionModal";
import { useParams } from "react-router-dom";
import { PropagateLoader } from "react-spinners";
import Options from "../../../api/collections/Options";
import ThesaurusEntries from "../../../api/collections/ThesaurusEntries";

const AttributeSettingsOptionsTable = () => {
	const { id } = useParams();

	const [manageOptionThesaurusModal, setManageOptionThesaurusModal] =
		useState();
	const [editOptionModal, setEditOptionModal] = useState();
	const [deleteOptionModal, setDeleteOptionModal] = useState();
	const [editThesaurusID, setEditThesaurusID] = useState();

	const { options, isLoading, thesaurusEntryList } = useTracker(() => {
		const handler = Meteor.subscribe("optionsByAttribute", id);

		if (!handler.ready()) {
			return { isLoading: true, options: null, thesaurusEntryList: null };
		}

		const listOfEntryIDs = Options.find({ attributeID: id })
			.fetch()
			.map((option) => option.thesaurusEntryID);

		const handler2 = Meteor.subscribe(
			"thesaurusEntriesByIDs",
			listOfEntryIDs || []
		);

		if (!handler2.ready()) {
			return {
				isLoading: true,
				options: Options.find({ attributeID: id }).fetch(),
				thesaurusEntryList: null,
			};
		}

		return {
			isLoading: false,
			options: Options.find({ attributeID: id }).fetch(),
			thesaurusEntryList: ThesaurusEntries.find().fetch(),
		};
	});

	const screenWidth = useWinWidth();

	const setDefaultOption = (option) => {
		Meteor.call("options.setDefault", option._id, option.attributeID);
	};

	if (isLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<>
			<div className="options-table">
				<table className="options">
					{screenWidth >= 768 && (
						<colgroup>
							<col style={isLoading ? { width: "100%" } : { width: "40%" }} />
							<col style={isLoading ? { width: "0" } : { width: "15%" }} />
							<col style={isLoading ? { width: "0" } : { width: "20%" }} />
							<col style={isLoading ? { width: "0" } : { width: "25%" }} />
						</colgroup>
					)}
					<thead>
						<tr>
							<th scope="col">{isLoading ? "" : "Option"}</th>
							<th scope="col">{isLoading ? "" : "Default"}</th>
							<th scope="col">{isLoading ? "" : "Thesaurus"}</th>
							<th scope="col">{isLoading ? "" : "Actions"}</th>
						</tr>
					</thead>
					<tbody>
						{options.map((option) => {
							const matchingThesaurusEntry = thesaurusEntryList.find(
								(entry) => entry._id === option.thesaurusEntryID
							);
							return (
								<tr key={option._id}>
									<td data-label="Option">{option.name}</td>
									<td
										data-label="Default"
										style={
											screenWidth >= 768
												? { textAlign: "center" }
												: { textAlign: "right" }
										}
									>
										<Radio
											name="default"
											aria-label={`Set ${option.name} as default`}
											value={option.name}
											checked={option.default}
											onClick={() => {
												setDefaultOption(option);
											}}
										/>
									</td>
									<td data-label="Thesaurus">
										<div className="action-links">
											<button
												className="action-link"
												onClick={() => {
													setManageOptionThesaurusModal(option?._id);
												}}
											>
												{matchingThesaurusEntry
													? matchingThesaurusEntry?.name
													: "Manage"}
											</button>
										</div>
									</td>
									<td data-label="Actions">
										<div className="action-links">
											<button
												className="action-link"
												onClick={() => {
													setEditOptionModal(option);
												}}
											>
												Edit
											</button>
											<button
												className="action-link"
												onClick={() => {
													setDeleteOptionModal(option);
												}}
											>
												Delete
											</button>
										</div>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
			<ManageOptionThesaurusModal
				optionID={manageOptionThesaurusModal}
				hide={() => {
					setManageOptionThesaurusModal(null);
				}}
				setEditThesaurusID={setEditThesaurusID}
			/>
			<EditSynonymsInAttributeModal
				show={editThesaurusID}
				hide={() => {
					setEditThesaurusID(null);
				}}
				thesaurusID={editThesaurusID}
			/>
			<EditOptionModal
				option={editOptionModal}
				hide={() => {
					setEditOptionModal(null);
				}}
				options={options}
			/>
			<DeleteOptionModal
				option={deleteOptionModal}
				hide={() => {
					setDeleteOptionModal(null);
				}}
			/>
		</>
	);
};

export default AttributeSettingsOptionsTable;
