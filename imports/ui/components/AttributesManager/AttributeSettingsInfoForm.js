import React, { useState } from "react";
import * as yup from "yup";
import { Controller, useForm } from "react-hook-form";
import { titleCase } from "title-case";
import { yupResolver } from "@hookform/resolvers/yup";
import InputRHF from "../../lib/components/Form/InputRHF";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import CheckboxRHF from "../../lib/components/Form/CheckboxRHF";
import Button from "../../lib/components/Button/Button";
import NewGroupModal from "../../lib/components/NestedData/NewGroupModal";
import {
	saveOnBlur,
	saveOnChange,
} from "../../lib/utilities/saveOnBlurRHFMeteor";
import TimeAgo from "react-timeago";
import LastSaved from "../../lib/components/LastSaved/LastSaved";

const AttributeSettingsInfoForm = ({ attribute, groups }) => {
	const [newGroupModal, setNewGroupModal] = useState();

	const schema = yup.object().shape({
		name: yup.string().required("Please enter an attribute name."),
		type: yup.object().required("Please select an attribute type."),
		parent: yup.object().required("Please select a group."),
		retriever: yup.object().required("Please select a retriever."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
		setValue,
	} = useForm({
		defaultValues: {
			name: attribute?.name || "",
			// retriever: { value: "to do", label: "To Do" },
			session: attribute.session || false,
			parent: groups.find(
				(groupFromList) => groupFromList.value === attribute.parent
			),
			type: {
				value: attribute?.type || "",
				label: titleCase(attribute?.type || ""),
			},
			defaultValue: attribute?.defaultValue || "",
		},
		resolver: yupResolver(schema),
	});

	const onBlur = (evt) => {
		saveOnBlur("attributes", attribute._id, evt);
	};

	return (
		<section>
			<form>
				<div className="grid-row">
					<div className="grid-col-half">
						<InputRHF
							type="text"
							name="name"
							label="Name"
							register={register}
							error={errors}
							onBlur={onBlur}
						/>
					</div>
					<div className="grid-col-half">
						<SelectRHF
							name={"type"}
							label="Type"
							options={["Direct", "Calculated", "Retrieved"].map((option) => ({
								value: option.toLowerCase(),
								label: option,
							}))}
							ref={null}
							error={errors}
							value={{
								value: attribute?.type || "",
								label: titleCase(attribute?.type || ""),
							}}
							onChange={(value, fieldData) => {
								saveOnChange("attributes", attribute._id, value, fieldData);
							}}
						/>
					</div>
				</div>
				<div className="grid-row">
					<div className="grid-col-half">
						<SelectRHF
							name={"parent"}
							label="Group"
							options={groups}
							ref={null}
							error={errors}
							value={groups.find(
								(groupFromList) => groupFromList.value === attribute.parent
							)}
							onChange={(value, fieldData) => {
								saveOnChange("attributes", attribute._id, value, fieldData);
							}}
						/>
					</div>
					<div className="grid-col-half no-label">
						<div className="action-links-wrap">
							<button
								className="action-link"
								onClick={(event) => {
									event.preventDefault();
									setNewGroupModal(true);
								}}
							>
								Add New Group
							</button>
						</div>
					</div>
				</div>
				{attribute?.type === "retrieved" && (
					<div className="grid-row">
						<div className="grid-col-half">
							<InputRHF
								type="text"
								name="defaultValue"
								label="Default Value"
								register={register}
								error={errors}
								onBlur={onBlur}
							/>
						</div>
					</div>
				)}
				<div className="grid-row">
					{/* <div className="grid-col-half">
						<Controller
							name="retriever"
							control={control}
							render={({ field }) => {
								return (
									<SelectRHF
										name={field.name}
										label="Retriever"
										options={["To Do"].map((option) => ({
											value: option.toLowerCase(),
											label: option,
										}))}
										{...field}
										ref={null}
										error={errors}
									/>
								);
							}}
						/>
					</div> */}
					{/* <div className="grid-col-half no-label">  */}
					<div className="grid-col-half">
						<CheckboxRHF
							name="session"
							label="Session attribute?"
							register={register}
							error={errors}
							checked={attribute?.session || false}
							onChange={(evt) => {
								saveOnChange(
									"attributes",
									attribute._id,
									evt.target.checked,
									evt.target
								);
							}}
						/>
					</div>
				</div>
				<p className="form-response global">
					<span>Last saved</span> <LastSaved timestamp={attribute.updatedAt} />
				</p>
			</form>
			<NewGroupModal
				show={newGroupModal}
				hide={() => {
					setNewGroupModal(null);
				}}
				type="attributeGroup"
			/>
		</section>
	);
};

export default AttributeSettingsInfoForm;
