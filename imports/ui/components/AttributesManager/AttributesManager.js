import React from "react";
import Attributes from "./Attributes";

const AttributesManager = () => {
	return (
		<section className="attributes-manager">
			<h1>Attributes Manager</h1>
			<Attributes />
		</section>
	);
};

export default AttributesManager;
