import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import Button from "../../lib/components/Button/Button";
import InputRHF from "../../lib/components/Form/InputRHF";
import Modal from "../../lib/components/Modal/Modal";
import OptionsTemplates from "../../../api/collections/OptionsTemplates";

const SaveTemplateModal = ({
	templates,
	loadedTemplate,
	setLoadedTemplate,
	optionsValue,
	show,
	hide,
}) => {
	const schema = yup.object().shape({
		templateName: yup.string().required("Please enter a template name."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
		setValue,
		setError,
	} = useForm({
		resolver: yupResolver(schema),
	});

	setValue("templateName", "");
	//The line above was added and following lines commented out on October 19th, 2021 by Nathan James.
	//These changes make it so that when saving the template the field defaults to blank instead of the last template loaded.
	//The goal is to make someone retype the name of the template they are trying to save.
	//Simply remove the above line and uncomment the UseEffect function so that the save template will default to the loaded template.
	// useEffect(() => {
	// 	if (loadedTemplate) {
	// 		const matchingTemplate = templates.find(template => template._id === loadedTemplate);
	// 		if (matchingTemplate){
	// 			const templateName = matchingTemplate.name;
	// 			setValue("templateName", templateName);
	// 		}
	// 	}
	// }, [loadedTemplate]);

	const onSubmit = (data) => {
		const templateName = data.templateName;
		const alreadyExists = OptionsTemplates.findOne({ name: templateName });
		if (alreadyExists) {
			const matchingTemplate = templates.find(
				(template) => template.name === templateName
			);
			Meteor.call(`optionsTemplates.remove`, matchingTemplate._id);
		}
		const options = optionsValue
			.replace(/\r\n/g, "\n")
			.split("\n")
			.filter((option) => option);

		Meteor.call(`optionsTemplates.insert`, {
			name: templateName,
			options: options,
		});

		hide();
	};

	const onCancel = () => {
		hide();
	};

	return (
		<Modal className="modal limit-modal end-buttons" show={!!show} hide={hide}>
			<h3>Save Template</h3>
			<form>
				<div className="grid">
					<div className="grid-col-full">
						<InputRHF
							type="text"
							name="templateName"
							label="Template Name"
							register={register}
							error={errors}
						/>
					</div>
				</div>
			</form>
			<div className="button-wrap">
				<Button text="Save" onClick={handleSubmit(onSubmit)} />
				<Button text="Cancel" onClick={onCancel} />
			</div>
		</Modal>
	);
};

export default SaveTemplateModal;
