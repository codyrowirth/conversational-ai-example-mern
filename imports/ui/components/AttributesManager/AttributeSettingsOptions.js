import React from "react";
import AttributeSettingsOptionsTable from "./AttributeSettingsOptionsTable";
import AttributeSettingsOptionsNew from "./AttributeSettingsOptionsNew";

const AttributeSettingsOptions = () => {
	return (
		<section className="end-buttons">
			<h2>Options</h2>
			<AttributeSettingsOptionsTable />
			<AttributeSettingsOptionsNew />
		</section>
	);
};

export default AttributeSettingsOptions;
