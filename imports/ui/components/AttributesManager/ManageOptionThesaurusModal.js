import React, { useState } from "react";
import Button from "../../lib/components/Button/Button";
import Modal from "../../lib/components/Modal/Modal";
import SelectRHF from "../../lib/components/Form/SelectRHF";
import ThesaurusEntries from "../../../api/collections/ThesaurusEntries";
import { useTracker } from "meteor/react-meteor-data";
import { PropagateLoader } from "react-spinners";
import { useHistory } from "react-router-dom";
import Options from "../../../api/collections/Options";
import EntrySettingsSynonyms from "../Thesaurus/EntrySettingsSynonyms";
import ThesaurusEntryGroups from "../../../api/collections/ThesaurusEntryGroups";
import EntrySettings from "../Thesaurus/EntrySettings";

const ManageOptionThesaurusModal = ({ optionID, hide, setEditThesaurusID }) => {
	const { isLoading, thesaurusEntries, option } = useTracker(() => {
		if (!optionID) {
			return { isLoading: true };
		}
		const handler = Meteor.subscribe(
			"thesaurusEntries",
			Meteor.user()?.projectID
		);

		const option = Options.findOne({ _id: optionID });

		if (!handler.ready()) {
			return { isLoading: true, thesaurusEntries: null, option: null };
		}
		let thesaurusEntries = ThesaurusEntries.find({
			projectID: Meteor.user()?.projectID,
		})
			.fetch()
			.map((entry) => ({ value: entry._id, label: entry.name }));
		return { isLoading: false, thesaurusEntries, option };
	});

	const onSubmit = (data) => {
		hide();
	};

	const onCancel = () => {
		hide();
	};

	if (isLoading) {
		return (
			<Modal
				key={optionID}
				className="modal limit-modal end-buttons"
				show={!!optionID}
				hide={hide}
			>
				<PropagateLoader color="#2f3d47" />
			</Modal>
		);
	}

	const matchingThesaurusEntry = thesaurusEntries.find(
		(entry) => entry.value === option.thesaurusEntryID
	);

	return (
		<Modal
			key={optionID}
			className="modal limit-modal end-buttons"
			show={!!optionID}
			hide={hide}
		>
			<h3>Manage Thesaurus</h3>
			<form>
				<div className="grid-row">
					<div className="grid-col-full">
						<SelectRHF
							name={"entry"}
							label="Entry"
							value={matchingThesaurusEntry}
							options={thesaurusEntries.map((entry) => ({
								value: entry.value,
								label: entry.label,
							}))}
							onChange={(value) => {
								Meteor.call("options.update", option._id, {
									thesaurusEntryID: value.value,
								});
							}}
						/>
					</div>
				</div>
				<div className="button-wrap">
					<Button text="Save" onClick={onSubmit} />
					<Button text="Cancel" onClick={onCancel} />
					<Button
						text="Edit"
						onClick={(evt) => {
							evt.preventDefault();
							setEditThesaurusID(matchingThesaurusEntry?.value);
							hide();
						}}
					/>
				</div>
			</form>
		</Modal>
	);
};

export const EditSynonymsInAttributeModal = ({ show, hide, thesaurusID }) => {
	return (
		<Modal className="modal end-buttons" show={show} hide={hide}>
			<EntrySettings modal={true} id={thesaurusID} close={hide} />
		</Modal>
	);
};

export default ManageOptionThesaurusModal;
