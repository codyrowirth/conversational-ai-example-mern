import React from "react";
import { useTracker } from "meteor/react-meteor-data";
import Attributes from "../../../api/collections/Attributes";
import { PropagateLoader } from "react-spinners";
import AttributeSettingsInfoForm from "./AttributeSettingsInfoForm";
import AttributeGroups from "../../../api/collections/AttributeGroups";

const AttributeSettingsInfo = (props) => {
	const { id } = props;

	const { attribute, attributeIsLoading, groups } = useTracker(() => {
		const handler = Meteor.subscribe("attribute", id);
		const handlerGroups = Meteor.subscribe("attributeGroups");

		if (!handler.ready() || !handlerGroups.ready()) {
			return { attributeIsLoading: true, attribute: null, groups: null };
		}

		let groups = AttributeGroups.find()
			.fetch()
			.map((group) => ({ value: group._id, label: group.name }));
		groups.push({ value: "", label: "[[root]]" });

		let attribute = Attributes.findOne({ _id: id });
		if (!attribute.parent) {
			attribute.parent = "";
		}

		return {
			attribute,
			groups,
			attributeIsLoading: false,
		};
	});

	if (attributeIsLoading) {
		return (
			<div style={{ textAlign: "center", paddingBottom: 20 }}>
				<PropagateLoader color="#2f3d47" />
			</div>
		);
	}

	return (
		<AttributeSettingsInfoForm
			{...props}
			attribute={attribute}
			groups={groups}
		/>
	);
};

export default AttributeSettingsInfo;
