import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Alerts = () => {
	return (
		<section className="alerts">
			<h1>Alerts</h1>
			<ComingSoon />
		</section>
	);
};

export default Alerts;
