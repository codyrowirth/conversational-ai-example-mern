import React from "react";
import ComingSoon from "../../lib/components/ComingSoon/ComingSoon";

const Search = () => {
	return (
		<section className="search">
			<h1>Search</h1>
			<ComingSoon />
		</section>
	);
};

export default Search;
