import React, { useState } from "react";
import queryString from "query-string";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import InputRHF from "../../lib/components/Form/InputRHF";
import Button from "../../lib/components/Button/Button";
import "./Login.scss";

const parsed = queryString.parse(location.search);
const isEnroll = parsed.enroll === "true";
const token = parsed.token;

const SetPassword = () => {
	return (
		<section className="login flex">
			<div className="login-inner flex">
				<img className="logo" src="/images/tcs-logo-dark.svg" alt="Logo" />
				<h1 className="h3">{isEnroll ? "Set" : "Reset"} your password</h1>
				<p className="above-list">
					Please create a new password that contains at least:
				</p>
				<ul>
					<li>12 characters</li>
					<li>an uppercase letter</li>
					<li>a lowercase letter</li>
					<li>a number</li>
					<li>a special character</li>
				</ul>
				<Form />
			</div>
		</section>
	);
};

const Form = () => {
	const [error, setError] = useState();

	const schema = yup.object().shape({
		password: yup
			.string()
			.matches(
				"(?=.*[A-Z])",
				"Your password must contain at least one uppercase letter."
			)
			.matches(
				"(?=.*[a-z])",
				"Your password must contain at least one lowercase letter."
			)
			.matches("(?=.*[0-9])", "Your password must contain at least one number.")
			.matches(
				"(?=.*[^A-Za-z0-9])",
				"Your password must contain at least one special character."
			)
			.matches(
				"(?=.{12,})",
				"Your password must contain at least 12 characters."
			)
			.required("Please enter a password."),
		confirmPassword: yup
			.string()
			.oneOf([yup.ref("password"), null], "Passwords must match.")
			.required("Please re-enter the password."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		const password = data.password;

		Meteor.call("users.findByToken", token, (err, user) => {
			if (err) {
				console.error(err);
				setError("Something went wrong in setting your password.");
			} else {
				Accounts.resetPassword(token, password, (error) => {
					if (error) {
						console.error(error);
						setError("Something went wrong in setting your password.");
					} else {
						Meteor.loginWithPassword(user._id, password, (err) => {
							if (err) {
								console.error(err);
								setError("Error signing you in for the first time.");
							} else {
								useHistory().push("/dashboard");
							}
						});
					}
				});
			}
		});
	};

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="password"
						name="password"
						label="New Password"
						register={register}
						error={errors}
						autoComplete="new-password"
					/>
				</div>
			</div>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="password"
						name="confirmPassword"
						label="Confirm New Password"
						register={register}
						error={errors}
						autoComplete="new-password"
					/>
				</div>
			</div>
			{error && <p className="form-response error global">{error}</p>}
			<div className="button-wrap">
				<Button
					addClass="no-margin-right no-margin-bottom"
					type="submit"
					text={isEnroll ? "Set" : "Reset"}
				/>
			</div>
		</form>
	);
};

export default SetPassword;
