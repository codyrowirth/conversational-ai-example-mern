import React from "react";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import InputRHF from "../../lib/components/Form/InputRHF";
import Button from "../../lib/components/Button/Button";
import "./Login.scss";

const ForgotPassword = () => {
	return (
		<section className="login flex">
			<div className="login-inner flex">
				<img className="logo" src="/images/tcs-logo-dark.svg" alt="Logo" />
				<h1 className="h3">Forgot your password?</h1>
				<p>
					Submit your email address below and we’ll send you a link to reset
					your password.
				</p>
				<Form />
				<p className="forgot">
					Remember your password? <Link to="/login">Sign in</Link>.
				</p>
			</div>
		</section>
	);
};

const Form = () => {
	const schema = yup.object().shape({
		email: yup
			.string()
			.email("Please enter a valid email address.")
			.required("Please enter an email address."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
		setError,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		Accounts.forgotPassword({ email: data.email }, (err) => {
			if (err) {
				setError("email", { type: "string", message: err.reason });
			} else {
				setError("email", {
					type: "string",
					message:
						"Password reset email has been sent. Please check your inbox.",
				});
			}
		});
	};

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="email"
						label="Email address"
						register={register}
						error={errors}
						autoComplete="username"
					/>
				</div>
			</div>
			<div className="button-wrap">
				<Button
					addClass="no-margin-right no-margin-bottom"
					type="submit"
					text="Submit"
				/>
			</div>
		</form>
	);
};

export default ForgotPassword;
