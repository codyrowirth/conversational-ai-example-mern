import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Meteor } from "meteor/meteor";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import InputRHF from "../../lib/components/Form/InputRHF";
import Button from "../../lib/components/Button/Button";
import "./Login.scss";

const Login = () => {
	const [versionNumber, setVersionNumber] = useState();

	useEffect(() => {
		Meteor.call("getVersion", (error, data) => {
			setVersionNumber(data);
		});
	}, []);

	return (
		<section className="login flex">
			<div className="login-inner flex">
				<img className="logo" src="/images/tcs-logo-dark.svg" alt="Logo" />
				<h1 className="h3">Sign in to Your Account</h1>
				<Form />
				<p className="forgot">
					Forgot your password? <Link to="/forgot-password">Reset it</Link>.
				</p>
				{versionNumber && <p className="version">v{versionNumber}</p>}
			</div>
		</section>
	);
};

const Form = () => {
	const [error, setError] = useState("");

	const schema = yup.object().shape({
		email: yup.string().required("Please enter an email address."),
		password: yup.string().required("Please enter a password."),
	});

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm({
		resolver: yupResolver(schema),
	});

	const onSubmit = (data) => {
		Meteor.loginWithPassword(data.email, data.password, (result) => {
			if (result && result.isClientSafe && result.reason) {
				setError(result.reason);
			} else if (result) {
				setError("Something went wrong. Please contact support.");
				console.error(result);
			} else {
				window.location = "/";
			}
		});
	};

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="text"
						name="email"
						label="Email Address"
						register={register}
						error={errors}
						autoComplete="username"
					/>
				</div>
			</div>
			<div className="grid-row">
				<div className="grid-col-full">
					<InputRHF
						type="password"
						name="password"
						label="Password"
						register={register}
						error={errors}
						autoComplete="current-password"
					/>
				</div>
			</div>
			{error && <p className="form-response error global">{error}</p>}
			<div className="button-wrap">
				<Button
					addClass="no-margin-right no-margin-bottom"
					type="submit"
					text="Submit"
				/>
			</div>
		</form>
	);
};

export default Login;
